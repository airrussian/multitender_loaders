<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of scraperwiki
 *  
 * @author AIR
 */
class scraperwiki_parser extends base_parser {
    
    public function parse($content) {
        $return = json_decode($content, true);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                echo 'OK';
                break;
            case JSON_ERROR_DEPTH:
                echo 'Stack is full';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo 'no correct mode';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo 'no correct control char';
                break;
            case JSON_ERROR_SYNTAX:
                echo 'Sintax JSON is bad';
                break;
            case JSON_ERROR_UTF8:
                echo 'no correct charset utf8';
                break;
            default:
                echo 'other error';
                break;
        }
        return $return;
    }
    
}
