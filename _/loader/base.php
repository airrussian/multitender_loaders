<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loader
 *
 * @author AIR
 */
class base_loader {
    
    public $name;
    public $param;
    public $url;
    public $parser_name;
    
    public function __construct() {        
        $this->name = get_class($this);        
        $loader = new loader_model();
        $data = $loader->load($this->name);
        
        $this->url = $data['url'];
        $this->parser_name = $data['parser_name'] . "_parser";
        $this->param = unserialize($data['param']);
    }
    
    public function getContent($replace = false) {        
        $result = false;
        
        $this->param['req']['cmd'] = str_replace("#ID#", 1, $this->param['req']['cmd']);
        $this->param['req']['cmd'] = str_replace("#LIMIT#", 100, $this->param['req']['cmd']);
        
        var_dump($this->param['req']);
        
        $this->debug("LOADER PAGE = " . $this->url . ' PARAM=' . serialize($this->param['req']));        
        
        $browser = new emul_br();    
        $browser->URI     = $this->url;
        $browser->referer = $this->url;
        
        $browser->curl   = false;
        if(function_exists('curl_init')) {
            $browser->curl   = true;
        }
        
        $browser->SetPost($this->param['req']);
        $browser->exec();
        $content = $browser->GetBody();        
        
        return $content;
    } 
    
    public function debug($message, $title = '') {
        echo date("Y-m-d H:i:s") . "[{$this->name}]: " . $message . "\n";
    }
    
    
}
