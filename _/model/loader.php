<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loader
 *
 * @author AIR
 */
class loader_model extends base_model {
    
    public $tableName = 'loaders';
    
    public function load($name) {
        $sql = "SELECT * FROM {$this->tableName} WHERE `name` LIKE ?";
        $param = $this->DB->getRow($sql, array($name));
        return $param;
    }
    
    public function loadParam($name) {
        $sql = "SELECT param FROM {$this->tableName} WHERE `name` LIKE ?";
        $param = $this->DB->getOne($sql, array($name));   
        return $param;
    }
    
}
