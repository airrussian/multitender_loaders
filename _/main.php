<?php

require_once dirname(__FILE__) . '/config.php';
require_once $GLOBALS['conf']['dir']['libs'] . '/adodb5/adodb.inc.php';
require_once $GLOBALS['conf']['dir']['libs'] . '/class.emul_br_4a.php';

$config = &$GLOBALS['conf'];
$db = ADONewConnection($config['db']['tenders']['dns']);
$db->debug = $config['db']['tenders']['debug'];
$db->Execute("SET NAMES UTF8");
$config['db']['tenders']['connect'] = $db;

function __autoload($class) {
    $a = explode("_", $class);
    $dir = end($a); unset($a[count($a)-1]);
    $class = implode(".", $a) . ".php";
    
    require_once dirname(__FILE__) . '/' . $dir . '/' . $class;
    
}