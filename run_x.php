<?php
// задержка до 90 сек для балансировки
$time = mt_rand(1,900)*(100*1000);
echo "Sleep " . ($time/1000000) . " s\n";
// usleep($time);

require_once dirname(__FILE__) . "/main.php";

function run_x($class_id, $func) {
    $dir = dir( dirname(__FILE__) . "/pages/" );
    while (false !== ($entry = $dir->read())) {
        if ( preg_match("/$class_id.php$/i", $entry) || preg_match("/^$class_id/i", $entry) ) {
            $class_name = 'loader_' . preg_get("/(.*).php/", $entry);
            break;
        }
    }

    // for comptitable
    $dir = dir( dirname(__FILE__) . "/site/" );
    while (false !== ($entry = $dir->read())) {
        if ( preg_match("/$class_id.php$/i", $entry) || preg_match("/^$class_id/i", $entry) ) {
            $class_name = 'loader_' . preg_get("/(.*).php/", $entry);
            break;
        }
    }

    if (!$class_name) {
        exit("NO some files\n");
    }

    $page = new $class_name;

    if (!empty($page)) {
        //if (defined('REAL_RUN')) {
        $page->test_run  = false;
        $page->test_nodb = false;
        //}
        $page->$func();
        return true;
    }
    return false;
}

$page = trim($_SERVER['argv'][1]);
$what = trim($_SERVER['argv'][2]);

if(!empty($page) && !empty($what)) {
    run_x($page, $what);
} else {
    exit("ERROR\n");
}
