<?php


class site_load {
    public $proxy      = '';
    public $proxy_user = '';
    public $proxy_pass = '';
    public $curl         = false;
    public $curl_timeout = 60;


    public $no_curl = false;

    /**
     * Set some parametors to emul_br
     * @param string $url
     * @return emul_br
     */
    function emul_br_init($url = null) {
        $emul_br = new emul_br();

        if($url) {
            $emul_br->URI     = $url;
            $emul_br->referer = $url;
        }

        $emul_br->proxy           = $this->proxy;
        $emul_br->proxy_user      = $this->proxy_user;
        $emul_br->proxy_pass      = $this->proxy_pass;
        $emul_br->curlopt_timeout = $this->curl_timeout;

        $emul_br->curl   = false;
        if(function_exists('curl_init')) {
            $emul_br->curl   = true;
        }

        if ($this->no_curl) {
            $emul_br->curl = false;
        }


        return $emul_br;
    }

    function emul_br_get($url) {
        // TODO class url
        if(strlen($url) < 10) {
            return 0;
        }

        $emul_br = $this->emul_br_init($url);
        return $emul_br->exec();
    }

    function emul_br_get_body($url) {
        $emul_br = $this->emul_br_init($url);
        $emul_br->exec();
        return $emul_br->GetBody();
    }

}

/**
 * Parser
 */
class parser_base extends site_load {
    function test_list_parse() {
    }

    function list_parse () {
    }

    public $fileds_list = array(
            'internal_id',
            'name',
            'date_publication' => 'maybe',
    );

    protected $tidy_config = array( 'output-xhtml' => true,
            'show-body-only' => true,
            'char-encoding' => 'utf8',
            'input-encoding' => 'utf8',
            'output-encoding' => 'utf8',
            'preserve-entities' => true, // иначе nbsp => UTF
            'wrap' => 10000
    );

    /**
     * устаревшая
     * @param string $date
     * @return int
     */
    final function date_convert($date) {
        return $this->text_date_convert($date);
    }

    final function currency_to_rub($sum, $type="RUB") {
        $type = strtolower($type);
        switch ($type) {
            case 'rub':
                return $sum * 1;
            case 'usd':
                return $sum * 30;
            case 'eur':
                return $sum * 40;
            default:
                exit("unknown type currency = $type");
        }
    }


    // регулярные выражения - нужно находить закрывающийся тэг
    final function parse_table($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table ', 0);
        if(empty($table)) {
            return null;
        }

        //проверка на вложенные теги tbody и thead
        $check=array();
        $i=0;
        foreach ($table->children() as $ch) {
            if ((strcasecmp($ch->tag,"tbody")==0) || (strcasecmp($ch->tag,"thead")==0)) {
                $check[$i]=$ch;
                $i++;
            }
        }
        $check[$i]=$table;

        //$trs = $content_dom->find('tr');

        $return = array();


        $i = 0;
        foreach ($check as $table) {
            foreach ($table->children() as $tr ) {
                if( strcasecmp($tr->tag,"tr")==0 ) {
                    $j = 0;
                    foreach ($tr->children() as $td ) {
                        if ( (strcasecmp($td->tag,"td")==0) || (strcasecmp($td->tag,"th")==0) ) {
                            $return[$i][$j] = trim($td->innertext);
                            $j++;
                        }
                    }
                    $i++;
                }
            }
        }

        $content_dom->__destruct();

        return $return;
    }

    /**
     * Работает лишь тогда, когда кол-во колонок в заголовке и теле совпадает
     * FIXME работает, если есть 0 и 1. А не первая и вторая
     * @param <type> $t1
     * @return array
     */
    final function createstruct($t1) {
        for($i=0;$i<count($t1[1]);$i++) {
            $t1[0][$i] = $this->text_clear_all(@$t1[0][$i]);
            if ( empty($t1[0][$i]) ) {
                $t1[0][$i] = $i;
            }
        }

        $res=array();
        for($i=1;$i<count($t1);$i++) {
            $res[$i]=array_combine($t1[0],$t1[$i]);

        }

        return $res;
    }

    /**
     * Для варианта, когда заголовок длинее содержимого
     * @param <type> $t1
     * @return array
     */
    final function createstruct_v2($t1) {
        $size = max(count($t1[0]), count($t1[1]));
        for($i=0; $i< $size; $i++) {
            $t1[0][$i] = $this->text_clear_all(@$t1[0][$i]);
            if ( empty($t1[0][$i]) ) {
                $t1[0][$i] = $i;
            }
            if ( empty($t1[1][$i]) ) {
                unset($t1[0][$i]);
            }
        }
        $res=array();
        for($i=1;$i<count($t1);$i++) {
            $res[$i]=array_combine($t1[0],$t1[$i]);
        }
        return $res;
    }

    /**
     * Для варианта, когда заголовок длинее содержимого, более прав. версия
     * @param <type> $t1
     * @return array
     */
    final function createstruct_v3($t1) {
        $size = min(count($t1[0]), count($t1[1]));

        if (count($t1[0]) > $size) {
            $t1[0] = array_slice($t1[0],0,$size);
        }

        for($i=0; $i< $size; $i++) {
            $t1[0][$i] = $this->text_clear_all(@$t1[0][$i]);
            if ( empty($t1[0][$i]) ) {
                $t1[0][$i] = $i;
            }
        }

        $return=array();
        for($i=1;$i<count($t1);$i++) {
            $return[$i]=array_combine($t1[0],$t1[$i]);
        }
        return $return;
    }

    /**
     * $item = array('Столбец такой-то' => 'значение',);
     * Базовая очистка, без удаления тэгов уже включена (text_clear_enteries).
     * @param array $item    одномерный массив
     * @param array $colomn  одномерный массив
     * @return array
     */
    final function list_set_colomn( $item, $colomn=null ) {
        if( is_null($colomn) ) {
            $colomn = $this->list_colomn;
        }

        foreach ( $colomn as $c_key=>$c_val ) {
            $c_funct = explode('|', $c_val);
            $new_key = array_shift($c_funct);

            if( ! isset($return[$new_key]) ) {
                $return[$new_key] = null;
                $return[$new_key.'_src'] = null;
            }

            array_unshift($c_funct, 'text_clear_enteries');

            if( is_string($c_key) ) {
                $c_key = $this->text_clear_hard($c_key);
            }
            foreach($item as $key=>$val) {
                if( is_string($key) ) {
                    $key = $this->text_clear_hard($key);
                }

                if( preg_match("/$c_key/u", $key) ) {
                    $return[$new_key.'_src'] = $val;
                    $return[$new_key] = $this->text_run_rules($val, $c_funct);
                }
            }
        }
        return $return;
    }

    /*
     * Функция сортирует входящий массив по задонному заданию в favorite
     *    'Ответственное лицо/Электронный адрес' => 'customer_email:plaintext',
     *    'Заказчики' => 'customer',
    */
    final function detail_sort($detail, $delete=true, $favorite = null) {
        $return = array();

        if(!$favorite) {
            $favorite = $this->detail_sort;
        }

        foreach($detail as $key=>$val) {
            if(isset($favorite[$key])) {
                if($favorite[$key] === 'delete') {
                    if($delete) {
                        continue;
                    }
                }
                elseif(preg_match('/(.*?):(.*)/', $favorite[$key], $m)) {
                    $return['db'][$m[1]] = call_user_func(array($this, $m[2]), $val);
                    if($delete) {
                        continue;
                    }
                }
                else {
                    $return['db'][$favorite[$key]] = $val;
                    if($delete) {
                        continue;
                    }
                }
            }
            // no continue
            $return['other'][$key] = $val;
        }

        return $return;
    }

    /**
     * Функция сортирует входящий массив по задонному заданию в favorite
     *    'Ответственное лицо/Электронный адрес' => 'customer_email|plaintext',
     *    'Заказчики' => 'customer',
     */
    final function detail_sort_2($detail, $delete=false, $clear=true, $favorite = null) {
        $return = array();

        if ($clear) {
            $clear = 'clear_all|';
        }

        if(!$favorite) {
            $favorite = $this->detail_sort;
        }

        foreach($detail as $key=>$val) {
            if(isset($favorite[$key])) {
                if($favorite[$key] === 'delete') {
                    if($delete) {
                        continue;
                    }
                }
                elseif(preg_match('/^([^|]+)\|(.+)/', $favorite[$key], $m)) {
                    $return['db'][$m[1]] = $this->text_run_rules($val, $clear . $m[2]);
                    if($delete) {
                        continue;
                    }
                }
                else {
                    $return['db'][$favorite[$key]] = $this->text_run_rules($val, $clear);
                    if($delete) {
                        continue;
                    }
                }
            }
            // no continue
            $return['other'][$key] = $this->text_run_rules($val, $clear);
        }

        return $return;
    }

    /**
     * Функция сортирует входящий массив по задонному заданию в favorite,
     * достаточно частичного совпадения, вомзожно использовать RegExp.
     *    'Ответственное лицо/Электронный адрес' => 'customer_email|plaintext',
     *    'Заказчики' => 'customer',
     * @param array  $detail    входной массив для сортировки
     * @param boolen $delete    удалять ли совпавшие из other? По умолчанию НЕТ
     * @param boolen $clear     делать ли очистку всех? По умолчанию ДА
     * @param array  $favorite  задание, по умолчанию из $this->detail_sort
     * @return array
     */
    protected final function detail_sort_3(array $detail, $delete=false, $clear=true, array $favorite = null) {
        $return = array();

        if ($clear) {
            $clear = 'clear_all|';
        }

        if ( !$favorite ) {
            $favorite = $this->detail_sort;
        }

        foreach ( $detail as $key=>$val ) {
            $delete_this = false;
            foreach ( $favorite as $f_key=>$f_val ) {
                if ( preg_match("~$f_key~ui", $key ) ) {
                    if( $f_val === 'delete' ) {
                        $delete_this = true;
                    } else {
                        preg_match('/^([^|]+)(\|(.+))?/', $f_val, $m);
                        $return['db'][$m[1]] = $this->text_run_rules($val, $clear . $m[3]);
                    }
                    if ( $delete ) {
                        $delete_this = true;
                    }
                    break;
                }
            }
            if ( ! $delete_this ) {
                $return['other'][$key] = $this->text_run_rules($val, $clear);
            }
        }

        return $return;
    }

    final function text_run_rules($str, $rules) {
        //echo "<$str>, <$rules>\n";

        if(is_string($rules)) {
            $c_funct = explode('|', $rules);
        }
        else {
            $c_funct = $rules;
        }
//      print_r($rules); exit;
        foreach($c_funct as $f) {
            if (empty($f)) {
                continue;
            }
            if( ! is_callable( array($this, $f) ) ) {
                $f = "text_$f";
            }

            $str = call_user_func(array($this, $f), $str);
        }
        return $str;
    }

    /**
     * Очишает строку от различных HTML символов.
     * Требует постоянной доработки по ходу дела.
     * Предполагатся UTF-8.
     * $convert = array('&some;'=>' ',);
     * @param string $str
     * @param array $convert_add дополнительные для конвертации
     * @return string
     */
    final function text_clear_enteries( $str, $convert_add=null ) {
        $convert = array(
                '&nbsp;'  => ' ',
                ' '       => ' ',
                '&shy;'   => '',
                '&#8470;' => '№',
                '&ndash;' => '-',
                '–'       => '-',
                '&mdash;' => '-',
                '—'       => '-',
                '--'      => '-',
                '−'       => '-', // minus
                '&lt;'    => '«',
                '&gt;'    => '»',
        );

        if( is_array( $convert_add ) ) {
            $convert += $convert_add;
        }

        $str = str_replace( array_keys($convert), array_values($convert), $str );
        $str = html_entity_decode( $str, ENT_QUOTES, 'UTF-8' );        
        return $this->text_strip_space( $str );
    }

    /**
     * Очень жестко очишает строку - использовать ТОЛЬКО для сравнения;
     * удаляет все тэги, &.., пробельные символы, переводит в нижний регистр.
     * Предполагатся UTF-8.
     * @param string $str
     * @param string $charset default UTF-8
     * @return string
     */
    final function text_clear_hard( $str, $charset = 'UTF-8' ) {
        $str = $this->text_clear_all_dots( $str );
        $str = mb_strtolower( $str, $charset );
        return $str;
    }

    /**
     * Очишает строку от повторяющихся пробельных символов.
     * @param string $str
     * @return string
     */
    final function text_strip_space( $str ) {
        $str = preg_replace('/\s+/u', ' ', $str);
        return trim($str);
    }

    /**
     * Удаляет тэги, символы.
     * @param string $str
     * @return string
     */
    final function text_clear_all($str) {
        if( is_null($str) ) {
            return null;
        }

        // порядок очень важен (ОГУК Концертно-выставочный центр &lt;Губернский>)
        $str = $this->text_clear_enteries($str);
        $str = $this->text_strip_tags($str);
        return $str;
    }

    function text_clear_all_dots($text) {
        $text = $this->text_clear_all($text);
        $text = preg_replace(array('/[.,]+$/','/^[.,]+/'), '', $text);
        $text = $this->text_clear_all($text);
        return $text;
    }

    function text_from_win($text) {
        return mb_convert_encoding($text, 'UTF-8', 'CP1251');
    }

    /**
     * Удаляет все тэги, весьма топорно.
     * @param string $str
     * @return string
     */
    final function text_strip_tags( $str ) {
        $str = preg_replace(array('#<script[^>/]*>.*</script>#siU',
                '#<style[^>/]*>.*</style>#siU',
                '#<!--.*-->#siU'), ' ', $str);
        // тэги-разделители
        $str = preg_replace('/<(br|p|tr|td)/i', ' $0', $str);
        $str = strip_tags($str);
        return $this->text_strip_space($str);
    }


    /**
     * 26.08.2009 -> 20090826;
     * 12.10.2009 10:30 -> 20091012;
     * 16/02/2010 -> 20100216
     * @param string $date
     * @return int
     */
    final function text_date_convert($date) {
        $date = trim($date);
        if(preg_match("/^(\d{1,2})\D(\d{2})\D(\d{4})/", $date, $match)) {
            $date = (int) ( $match[3] . $match[2] . sprintf('%02d', $match[1]) );

            $getdate = getdate();
            $year    = (int)$getdate['year'];
            $year_minus = (int)(($year - 2) . '0131');
            $year_plus  = (int)(($year + 2) . '0131');

            if( ($date < $year_minus) || ($date > $year_plus) ) {

                //FIXME каким было образом возвращать ошибку, что дата не верная
                // ошибка в базе, обычно 19700101
                if( $date <= 19860101 ) {
                    return (int) date('Ymd');
                }
                trigger_error("UNKNOWN DATE??, date = $date");
            }
            return $date;
        }
        return null;
    }

    /**
     * bla bla 26.08.2009 bla bla -> 20090826;
     * bla bla 26/08/2009 bla bla -> 20090826;
     * @param string $date_string
     * @return int
     */
    final function text_date_convert_search($date_string) {
        if (preg_match('/\d{2}\D\d{2}\D\d{4}/', $date_string, $m)) {
            return $this->text_date_convert($m[0]);
        } else {
            return null;
        }
    }
    
    /**
     * 26.08.09 -> 20090826;
     * 12.10.09 10:30 -> 20091012;
     * @param string $date
     * @return int
     */
    final function text_date_convert_short($date) {
        $p = explode('.', $date);
        $date = "{$p[0]}.{$p[1]}.20{$p[2]}";
        return $this->text_date_convert($date);
    }

    /**
     * 5 марта => дата
     * @param <type> $str
     * @return <type>
     */
    function text_date_convert_text($str) {
        $month = array(
                'январ'     => '01',
                'феврал'    => '02',
                'март'      => '03',
                'апрел'     => '04',
                'ма(?:й|я)' => '05',
                'июн'       => '06',
                'июл'       => '07',
                'авгус'     => '08',
                'сентяб'    => '09',
                'октяб'     => '10',
                'ноябр'     => '11',
                'декаб'     => '12',
        );

        $date_part = preg_get('#(\d+) (\D+)#i', $str);
        if (!$date_part) {
            return null;
        }
        foreach ($month as $key=>$val) {
            $date_part[1] = preg_replace("#$key\S*#iu", $val, $date_part[1]);
        }
        $m = (int) $date_part[1];
        $d = (int) $date_part[0];
        if (!$m || !$d) {
            return null;
        }
        // -> 26.08.2009
        return $this->text_date_convert(sprintf('%02d.%02d', $d, $m).'.'.date('Y'));
    }

    /**
     * 18 Февраля 2010 г. => дата
     * @param <type> $str
     * @return <type>
     */
    function text_date_convert_text_full($str) {
        $month = array(
                'январ'     => '01',
                'феврал'    => '02',
                'март'      => '03',
                'апрел'     => '04',
                'ма(?:й|я)' => '05',
                'июн'       => '06',
                'июл'       => '07',
                'авгус'     => '08',
                'сентяб'    => '09',
                'октяб'     => '10',
                'ноябр'     => '11',
                'декаб'     => '12',
        );

        $date_part = preg_get('#(\d+)(\D+)(\d+)#i', $str);
        if (!$date_part) {
            return null;
        }
        foreach ($month as $key=>$val) {
            $date_part[1] = preg_replace("#$key\S*#iu", $val, $date_part[1]);
        }

        $d = (int) $date_part[0];
        $m = (int) $date_part[1];
        $y = (int) $date_part[2];

        if (!$m || !$d || !$y) {
            return null;
        }
        // -> 26.08.2009
        return $this->text_date_convert(sprintf('%02d.%02d', $d, $m).'.'.$y);
    }

    /**
     * 2010-08-09 -> 20090826;
     * @param string $date
     * @return int
     */
    final function text_date_convert_sql($date) {
        $date = preg_get("#(\d{4}-\d{2}-\d{2})#sui", $date);
        $date = strtotime($date);
        return $this->text_date_convert(date('d.m.Y', $date));
    }
    
    function text_date_convert_my($date) {
        return implode("", preg_get("#(\d{4}).(\d{2}).(\d{2})#si", $date));       
    }    

    final function text_to_int($str, $delemiter=false, $charfloar=false) {
        if ($delemiter == ",") {
            $str = preg_replace("#\,\s*#si", "", $str);
        }
        if ($delemiter == ".") {
            $str = preg_replace("#\.\s*#si", "", $str);
        }
        if ($delemiter == " ") {
            $str = preg_replace("#\s*#si", "", $str);
        }
        
        return (int) $str;
    }

    /**
     * to price
     * @param string $str
     * @return int
     */
    final function text_to_price($str) {
        $price = (int) preg_replace("/[^\d.,]/", "", $str);
        return $price ? $price : null;
    }

    /**
     * to price only first digit
     * @param string $str
     * @return int
     */
    final function text_to_price_first($str) {
        $str = preg_get('/\d[\d,.\s]+/ui', $str);
        return $this->text_to_price($str);
    }

    /**
     * Нормилизовать типы согласно БД
     * @param array $item 1D
     * @return boolen
     */
    final function fields_normalize( $item ) {
        $return = array();
        foreach($item as $key=>$val) {
            if ( is_string($val) ) {
                $val = $this->text_clear_enteries($val);
            }

            $return[$key] = $val;

            switch ($key) {
                case 'name':
                    if( empty($val) ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
                case 'price':
                case 'internal_id':
                    $return[$key] = (int) $val;
                case 'internal_id':
                    if( $return[$key] < 1 ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
                case 'date_end':
                case 'date_conf':
                case 'date_publication':
                    $return[$key] = (int) $val;
                    if ( ($val > 20200101) || ($val < 19900101 ) ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
            }
        }
        return $item;
    }

    /**
     * Проверить наличие правилам
     * @return boolen
     */
    final function fields_check( $item, $rules ) {
        $return = $this->fields_normalize($item);

        foreach($rules as $f=>$r) {
            if(is_int($f)) {
                $f = $r;
                $r = 'required';
            }

            switch ($r) {
                case 'required':
                    if( empty( $item ) ) {
                        return false;
                    }
                case 'maybe':
                    break;

                default:
                    return false;
            }
        }
        return true;
    }

    /**
     * Проверка списка при написании парсера
     * @param <type> $items
     */
    final function dev_list_check( $content ) {
        $list = $this->list_parse( $content );
        foreach( $list['items'] as &$item ) {
            $item = $this->fields_normalize( $item );
            if( ! $this->fields_check($item, $fileds_list)) {
                trigger_error("не хватает некоторых полей");
            }
        }
    }

    protected $asp_params_post    = array();
    protected $asp_params_cookies = array();
    function asp_set_params(emul_br &$emul_br, $decode_url=false ) {
        //<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value=""
        ini_set('pcre.backtrack_limit', 1000000);
        $inputs = preg_get_all(
                "/<input[^>]*?name=\"(__[A-Z]*)\"[^>]*?value=\"(.*?)\"/i",
                $emul_br->GetBody());
        if ($inputs) {
            if ($decode_url) {
                $inputs[1] = array_map('urldecode', $inputs[1]);
            }
            $this->asp_params_post  = array_merge($this->asp_params_post, array_combine($inputs[0], $inputs[1]));
        }
        $this->asp_params_cookies   = array_merge($this->asp_params_cookies, $emul_br->GetResponseCookieArray());
    }

    function asp_clear_params() {
        $this->asp_params_post = array();
        $this->asp_params_cookies = array();
    }

    public $list_get_link;
    public $list_get_link_base;

    /**
     * Установить переменные текущей стр
     * @param <type> $link
     */
    function list_get_set_links($link) {
        $this->list_get_link      = $link;
        $this->list_get_link_base = dirname($link);
    }

    /**
     * Определение типа по тексту
     * Первая версия, сделанная для Ижевска
     * @param string $str строка с типом
     * @return string приведенный тип
     */
    function text_type_1($str) {
        $search = array(
                'запрос.*котир'    => 'Запрос котировок',
                'ЗК\s'             => 'Запрос котировок',
                'котиров.*?заяв'   => 'Запрос котировок',
                'конкурс'          => 'Открытый конкурс',
                'эл.*аукцион'      => 'Открытый аукцион в электронной форме',
                'аукцион.*электр'  => 'Открытый аукцион в электронной форме',
                'аукцион'          => 'Открытый аукцион',
        );
        return $this->text_type_1_common($str, $search);
    }

    function text_type_1_common($str, $search) {
        foreach ($search as $preg => $type) {
            if (preg_match("/$preg/ui", $str)) {
                return $type;
            }
        }
        return null;
    }

    // Чистит и ремонтирует HTML фрагмент
    function text_clear_html_gently($string) {
        $string = preg_replace(array('#(id|class|style)=".*?"#ui','#</?(font|span).*?>#ui','/<!--.*?-->/sui'), '', $string);
        $string = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $string);
        $string = tidy_repair_string($string, $this->tidy_config);
        return $string;
    }

    function detail_content_cache($content) {

        //$content = tidy_repair_string($content, array( 'output-xhtml' => true, 'char-encoding' => 'utf8', 'input-encoding' => 'utf8', 'output-encoding' => 'utf8', 'preserve-entities' => true, 'wrap' => 10000));
        
        $domen = parse_url($this->loader->detail_link, PHP_URL_HOST);

        $panel = "
            <style>
                #panel_cache_mt { border-bottom: 2px solid red; padding: 5px; background: rgb(242, 242, 242) none repeat scroll 0% 0%; display: block; position: fixed; visibility: visible; font-size: 20px; text-align: left; width: 100%; }
                #panel_cache_mt .logo { background:transparent url(http://multitender.ru/templates/mt/img/l.png) no-repeat scroll 0 0; float:left; height:50px; text-align:center; width:235px; }
                #panel_cache_mt .logo a { color:#FFFFFF; display:block; font-size:24px; font-weight:bold; line-height:36px; text-decoration:none; font-family:Arial,Helvetica,sans-serif; }
                #panel_cache_mt .text { float: left; font-size:15px; line-height:40px; padding-left:20px; }
            </style>
            <div id='panel_cache_mt'>
                <div class='logo'><a style='' href='http://multitender.ru/'>Мультитендер.ру</a></div>
                <div class='text'>Сохраненная копия страницы <a href='{$this->detail_link}'>{$this->detail_link}</a>. Дата создания копии: ".date("d.m.Y")."</div>
                <br clear='all' />
            </div>
            <div style='height:50px;'></div>";
        $content = preg_replace("#<body(.*?)>#", "<body\\1>".$panel, $content);
        $content = preg_replace("#<head(.*?)>#si", "<head\\1><base href='http://".$domen."/' />", $content);
        $content = preg_replace("#charset=.*?\"#si", "charset=utf-8\"", $content);
        return $content;
    }


    /**
     * перевод текста. Основано на translate.google.com
     * @param string $text текст для перевода
     * @param string $fromlang перевод с языка
     * @param string $tolang перевод с языка
     */
    function text_translate($text, $fromlang, $tolang='ru') {
        $emul_br = new emul_br();
        $emul_br->URI = "http://translate.google.com/translate_a/t?client=t&text=".urlencode($text)."&hl=ru&sl=$fromlang&tl=$tolang&multires=1&ssel=0&tsel=0&sc=1";
        $emul_br->exec();
        $content = $emul_br->GetBody();
        return preg_get("#\[\[\[\"(.*?)\",#siu", $content);
    }


    /**
     * Антикапча:
     * $filename - полный путь к файлу
     * $apikey   - ключ для работы
     * $rtimeout - задержка между опросами статуса капчи
     * $mtimeout - время ожидания ввода капчи
     *
     * включить/выключить verbose mode (комментирование происходящего):
     * $is_verbose - false(выключить),  true(включить)
     *
     * дополнительно (дефолтные параметры править не нужно без необходимости):
     * $is_phrase - 0 либо 1 - флаг "в капче 2 и более слов"
     * $is_regsense - 0 либо 1 - флаг "регистр букв в капче имеет значение"
     * $is_numeric -  0 либо 1 - флаг "капча состоит только из цифр"
     * $min_len    -  0 (без ограничений), любая другая цифра указывает минимальную длину текста капчи
     * $max_len    -  0 (без ограничений), любая другая цифра указывает максимальную длину текста капчи
     * $is_russian -  0 либо 1 - флаг "капча должна демонстрироваться только русскоязычным работникам"
     *
     * пример:
     *  $text=recognize("/path/to/file/captcha.jpg","ваш_ключ_из_админки",true,"antigate.com");
     *  $text=recognize("/path/to/file/captcha.jpg","ваш_ключ_из_админки",false,"antigate.com");  //отключено комментирование
     *  $text=recognize("/path/to/file/captcha.jpg","ваш_ключ_из_админки",false,"antigate.com",1,0,0,5);  //отключено комментирование, капча состоит из двух слов, общая минимальная длина равна 5 символам
     */
    function recognize($filename, $apikey, $is_verbose = true, $domain="antigate.com", $rtimeout = 5, $mtimeout = 120, $is_phrase = 0, $is_regsense = 0, $is_numeric = 0, $min_len = 0, $max_len = 0, $is_russian = 0) {
	if (!file_exists($filename)) {
            if ($is_verbose) echo "file $filename not found\n";
            return false;
	}
        $postdata = array(
            'method'    => 'post',
            'key'       => $apikey,
            'file'      => '@'.$filename, //полный путь к файлу
            'phrase'	=> $is_phrase,
            'regsense'	=> $is_regsense,
            'numeric'	=> $is_numeric,
            'min_len'	=> $min_len,
            'max_len'	=> $max_len,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,             "http://$domain/in.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,     1);
        curl_setopt($ch, CURLOPT_TIMEOUT,             60);
        curl_setopt($ch, CURLOPT_POST,                 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,         $postdata);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            if ($is_verbose) echo "CURL returned error: ".curl_error($ch)."\n";
            return false;
        }
        curl_close($ch);

        if (strpos($result, "ERROR")!==false) {
            if ($is_verbose) echo "server returned error: $result\n";
            return false;
        } else {
            $ex = explode("|", $result);
            $captcha_id = $ex[1];
            if ($is_verbose) echo "captcha sent, got captcha ID $captcha_id\n";
            $waittime = 0;
            if ($is_verbose) echo "waiting for $rtimeout seconds\n";
            sleep($rtimeout);
            while(true) {
                $result = file_get_contents("http://$domain/res.php?key=".$apikey.'&action=get&id='.$captcha_id);
                if (strpos($result, 'ERROR')!==false) {
                    if ($is_verbose) echo "server returned error: $result\n";
                    return false;
                }
                if ($result=="CAPCHA_NOT_READY") {
                    if ($is_verbose) echo "captcha is not ready yet\n";
                    $waittime += $rtimeout;
                    if ($waittime>$mtimeout) {
            		if ($is_verbose) echo "timelimit ($mtimeout) hit\n";
            		break;
                    }
                    if ($is_verbose) echo "waiting for $rtimeout seconds\n";
                    sleep($rtimeout);
                } else {
                    $ex = explode('|', $result);
                    if (trim($ex[0])=='OK') return trim($ex[1]);
                }
            }
            return false;
        }
    }

}
