<?php

/*
 * ОАО "Башкирэнерго"
*/

class loader_1_0068_01_0_00_ritek extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.ritek.ru/';
    public $list_link           = 'http://www.ritek.ru/tender/public_tenders/';
    public $parser_name         = 'parser_1_0068_01_0_00_ritek';
    public $parser_name_detail  = 'parser_1_0068_01_0_00_ritek_detail';

    public $fields_list = array(           
            'name',
            'internal_id',
            'date_publication',           
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0068_01_0_00_ritek extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'P_internal_id'     =>  'internal_id',
        'P_name'            =>  'name|clear_all',
        'P_date_publication'=>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link$page \n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_publication'] < date("Ymd", time()-31*24*60*60)) {
                continue;
            }
           
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $arr = preg_get_all("#<a href=\"/tender/public_tenders/\d+.*?<div class=\"news-line\">.*?</div>#si", $content);

        foreach ($arr as $row) {
            $items[] = array(
                'P_internal_id'         =>  preg_get("#<a href=\"/tender/public_tenders/(\d+)/\">Размещено .*?</a>#sui", $row),
                'P_date_publication'    =>  preg_get("#<a href=\"/tender/public_tenders/\d+/\">Размещено (\d{2}.\d{2}.\d{4})</a>#sui", $row),
                'P_name'                =>  preg_get("#<br>(.*)<br>#si", $row),
            );
        }
        
        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;
        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0068_01_0_00_ritek_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    public $detail_sort = array(
        'Место проведения'                  =>  'address|clear_all',
        'Срок предоставления'               =>  'date_end|clear_all',
    );

    function detail_get($id) {

        $this->detail_link = "http://www.ritek.ru/tender/public_tenders/$id/";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $return = $this->detail_sort_3($parse);
      

        //$return['content'] = $this->detail_content_cache($this->text_from_win($content));
        $return['db']['date_end']  = $this->text_date_convert_text_full(str_replace(array("«", "»"), "", preg_get("#«\d{2}».+?\d{4}#si", $return['db']['date_end'])));
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['address']);

        var_dump($return); exit;

        return $return;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $detail = preg_get("#<td><p><b>Размещено.*?</td>#siu", $content);

        $arr = preg_get_all("#<p>(.*?)</p>#si", $detail);
        foreach ($arr as $row) {
            $data = preg_get("#(.*?):(.*)#si", $row);
            if (preg_match("#Срок предоставления#siu", $row)) {
                $data = preg_get("#(.*?)\sдо\s(.*)#si", $row);
            }
            $return[$this->text_clear_all($data[0])] = $this->text_clear_all($data[1]);
        }

        return $return;
    }


}

