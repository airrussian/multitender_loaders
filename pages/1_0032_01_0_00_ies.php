<?php
/**
 * http://etp.zakazrf.ru/Reductions.aspx?stage=1
 * c 2.09.2010 - новая площадка
 * @author airrussian@mail.ru
 */

class loader_1_0032_01_0_00_ies extends loader_1_0000_02_0_00_temp {
    public $base_url           = 'http://ieservice.ru/';
    public $list_link          = 'http://ieservice.ru/Reductions.aspx?stage=1';
    public $parser_name        = 'parser_1_0032_01_0_00_ies';
    public $parser_name_detail = 'parser_1_0032_01_0_00_ies_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'price',
            'date_conf',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; 
    public $item_rewrite  = true;

    protected $run_detail_order = 'date_publication ASC';

    public $page_total_rewrite = false;

    public $page_last = 200;

    public $sleep_list = 7;

    function test_detail($id=3128) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);
        var_dump($arr);
    }

}

class parser_1_0032_01_0_00_ies extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Номер'             => 'num|clear_all',
            'Время начала'      => 'date_conf|date_convert_short',
            'Название'          => 'name|clear_all',
            'Общая цена'        => 'price|to_price',
            '7'                 => 'internal_id',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");
        return $this->emul_br_get_body( $link );       
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#<a.*\?id=(\d+)#i", $item['internal_id']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => ceil($parse['item_total']/sizeof($items)),
                'items_total' => $parse['item_total'],
                'items'       => $items,
        );

        return $return;
    }


    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $data = $content_dom->find("div#ctl00_Content_UpdatePanel", 0);

        $pager = $data->find("table", 0);
        $table = $data->find("table#ctl00_Content_ReductionsGrid", 0);

        $arr   = $this->parse_table($table->outertext);

        $return['items'] = $this->createstruct($arr);
        $return['item_total'] = preg_get('/id="ctl00_Content_RecordsCountLabel">(\d+)</i', $content);

        $content_dom->clear();

        return $return;

    }

}

class parser_1_0032_01_0_00_ies_detail extends parser_1_0032_01_0_00_ies {

    protected $detail_link = 'http://ieservice.ru/ViewReduction.aspx?id=';

     public $detail_sort = array(
        'Почтовый адрес заказчика' => 'customer_address|clear_all',       
        'Адрес электронной почты заказчика' => 'customer_email|clear_all',
        'Номер контактного телефона заказчика' => 'customer_phone|clear_all',
    );

    function arr_double2one(array $arr) {
        $return = array(); $i=0;
        foreach ($arr as $row) {

            if (count($row)==2) {
                $key = $this->text_clear_all($row[0]);
                $val = $row[1];
                
            } else {
                $val = $row[0];
                $key = $i++;
            }
            $return[$key] = $val;
        }
        return $return;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);        
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $docs = $parse['документы'];
        $date_publication = $this->text_date_convert_short(preg_get("#размещен.*?(\d{2}.\d{2}.\d{2})#sui", $parse[0]));       
        $return = $this->detail_sort_3($parse);
        $return['db']['date_publication'] = $date_publication;
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        foreach ($docs as $doc) {
            $detail_link = preg_get("#ViewForms/File.ashx\?id=.*?\"#si", $doc);
            $return['docs'][] = array(
                'detail_link' => $detail_link,
                'internal_id' => abs(crc_p($detail_link)),
                'name' => preg_get("#<a.*?>(.*?)</a>#si", $doc),
            );
        }
        unset($return['other'][0]);
        foreach ($return['other'] as $key => $row) {
            if ($row=="") {
                unset($return['other'][$key]);
            }
        }
        return $return;
    }

    function detail_parse_pre($content) {
        $html = str_get_html($content);
        $detail = $html->find("table#ctl00_Content_content_table", 0);
        $data = $detail->find("td", 0);

        $return = array();
        // Документы
        if ($data->find("table#ctl00_Content_ReductionViewForm_FilesViewForm_FilesGrid", 0)) {
            $docs = $data->find("table#ctl00_Content_ReductionViewForm_FilesViewForm_FilesGrid", 0)->innertext;
            $return['документы'] = preg_get_all("#<a.*?a>#si", $docs);
        }
        // Лоты
        //if ($data->find("table#ctl00_Content_ReductionViewForm_ViewLotsListControl_LotsList", 0)) {
        //    $return['лоты'] = $data->find("table#ctl00_Content_ReductionViewForm_ViewLotsListControl_LotsList", 0)->outertext;
        //}

        $data = preg_get_all("#<table width=\"100%\" border=\"0\">.*?</table>#si", $data->innertext);

        foreach ($data as $tbl) {
            $rows = $this->arr_double2one($this->parse_table($tbl));
            $return = array_merge($return, $rows);
        }

        return $return;
    }

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

}
