<?php

/*
 * Закупки ОАО Газпром Комплектация
*/

class loader_1_0051_03_0_00_gazpromcr extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.komplekt.gazprom.ru/';
    public $list_link           = 'http://www.gcr.gazprom.ru/customsdeclared.aspx';
    public $parser_name         = 'parser_1_0051_03_0_00_gazpromcr';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0051_03_0_00_gazpromcr extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Способ закупок прием заявок до'        =>  'date_end|clear_all',
        'Номер, наименование и предмет закупки' =>  'name|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_end']       = $this->text_date_convert_search(preg_get("#<span id=\"ctl01_mainContent_ctl00_cCustomListControl_lstCustomList_.*?_lblStartDate\".*?>(.*?)</span>#si", $item['date_end_src']));
            $item['name']           = preg_get("#<span id=\"ctl01_mainContent_ctl00_cCustomListControl_lstCustomList_.*?_lblDescription\">(.*?)</span>#si", $item['name_src']);
            $item['internal_id']    = preg_get("#href=\"CustomDetails.aspx\?id=(\d+)\"#si", $item['name_src']);
            $item['num']            = preg_get("#<a.*?>(.*?)</a>#si", $item['name_src']);

            $item['date_publication'] = preg_get("#\d{2}\.\d{2}\.\d{2}#si", $item['num']);            
            $item['date_publication'] = $this->text_date_convert_short($item['date_publication']);

            $items[$k] = $item;
        }      

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);

        $content = tidy_repair_string($content, $this->tidy_config);

        $table = $html->find("table#ctl01_mainContent_ctl00_cCustomListControl_lstCustomList_itemPlaceholderContainer", 0)->outertext;
        $html->clear();

        $arr = $this->parse_table($table);

        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

