<?php
/**
 * Красноярский край, переработка
 * FIXME: тестирование
 * @author Paul-labor@yandex.ru
 */

/**
 * krasgz
 * @author Paul-labor@yandex.ru
 */
class loader_1_0007_01_0_00_krasgz extends loader_1_0000_04_0_00_one_doc {
    public $site_id = 2;

    public $test_run  = true;
    public $test_nodb = true;

    //var $site = 'krasgz';
    //var $site_id = 1;

    public $base_url    = 'http://www.krasgz.ru/';
    public $list_link   = 'http://www.krasgz.ru/torgi/?action=filter-short&page=';


    public $many_sleep = 9;
    public $many_count = 3;
    public $item_sleep = 9;

    public $fields_list = array(
            'internal_id',
            'name',
            'date_end',
            'date_conf',
            'type',
            'price' => 'maybenull',
    );

    // var $params_cookies = array();
    // var $item_link         = 'http://www.krasgz.ru/torgi/?action=filter-short';

    //var $item_sleep = 3;
    var $item_pages = 900; //количество "Объявленных" меньше сего
    var $item_stop = 55; // остановить на этой старнице

    /*
     * Обнаружен косяк - иногда, по совершенно не объяснимой причине,
     * поиск сбрасываеться. Тогда делаться ещё одна (и единственная)
     * попытка. i_run - количество попыток
    */
    var $item_trouble = 2;

    var $detail_link = 'http://krasgz.ru/torgi/?action=view&id=';

    public $parser_name          = 'parser_1_0007_01_0_00_krasgz';
    public $parser_name_detail   = 'parser_1_0007_01_0_00_krasgz_detail';
    public $parser_name_doc      = 'parser_1_0007_01_0_00_krasgz_detail';
    public $parser_name_protocol = 'parser_1_0007_01_0_00_krasgz_detail';
    
    public $item_rewrite  = false; // попытка исправить регион, или обходить все??

    public $break_by_pass = false; // не ясная сортировка
    function list_stop(array $list) {
        return $this->list_stop_date_conf($list);
    }

    public $debug = true;

    function test_detail_all($id = 51292) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;
        $r = $this->parser->detail_all($id);
        var_dump($r);
    }

    function test_doc_get($id = 79926) {
        $this->parser = new $this->parser_name_doc;
        $this->parser->loader = & $this;
        $r = $this->parser->doc_get(array('internal_id'=>$id));
        unset ($r['data']);
        var_dump($r);
    }

    function test_run_doc($id = 79926) {
        $this->run_doc($id);
    }

    function test_detail_get($id = 46667) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_detail_get($id);
    }

    function test_get_detail_param_sort($id = 46667) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_get_detail_param_sort($id);
    }


    function test_detail_get_stuff($id = 46667) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        print_r($this->parser->detail_get_stuff($id));
    }

    function run_detail_one_int($id) {
        $item = new item();
        if($item->Load("site_id = $this->site_id AND internal_id = $id")) {
            $this->run_detail_one($item->id);
        }
    }
}

/**
 * LIST
 */
class parser_1_0007_01_0_00_krasgz extends parser_1_0000_04_0_00_one_doc {
    public $region_param;

    public $curl_timeout = 60;
    /* protected $list_search_params_link  = 'http://zakupki.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link         = 'http://zakupki.gov.ru/Tender/Purchase.aspx';
    protected $detail_link              = 'http://zakupki.gov.ru/Tender/ViewPurchase.aspx?PurchaseId='; */

    protected $error_msg = 'При обработке запроса возникли технические неполадки';

    protected $colomn = array(
            'P_date'    => 'date_end|date_convert',
            'P_name'    => 'name',
            'P_price'   => 'price|to_price',
    );
    
    function list_get_page( $link, $page = 1 ) {
        $page = (int) $page;
        $this->loader->debug("NEXT PAGE = $page");
        $link = $link.$page;
        $this->loader->debug("PAGE = $link");
        $content = $this->emul_br_get($link);
        return $content;
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            
            $item['internal_id'] = (int) preg_get("#id=(\d*)#", $item['name_src']);
            $item['type']        = trim(preg_get("#<div style=['\"]color..666['\"]>(.*?)\(Объявленные\)</div>#sui", $item['name_src']));
            $item['name']        = $this->text_clear_all(preg_replace("#<div style=['\"]color..666['\"]>(.*?)\(Объявленные\)</div>#sui", "", $item['name_src']));

            // завершенные
            if (empty($item['type'])) {
                continue;
            }

            $item['date_conf'] = $item['date_end'];

            $items[] = $item;
        }

        $return = array (
                'page_total'  => 100, //$parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],            
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        //<div class="commandbar"><a name="result"></a>xxxxxx: <b>28289</b></div>
        $total = (int) $content_dom->find("div.commandbar b", 0)->innertext;

        $content_dom->clear();
        
        if(!$total) {
            trigger_error("ERROR");
            return null;
        }
        $return['items_total'] = $total;

        $pages = preg_get_all("/action=filter-short&amp;page=(\d+)/", $content);
        $return['page_total'] = max($pages);

        $page = preg_get("|<b style=\"background:#dddddd;color:#000000;padding:0 5px 0 5px;\">(\d+)</b>|i", $content);
        $return['page_now'] = $page;

        $table = preg_get("#<div class=\"commandbar\">.*?(<table width=\"100%\">.*?</table>)#si", $content);
        
        preg_match_all("#<tr>(.*?)</tr>#si", $table, $match);
        $trs = $match[1];

        $arr[0] = array('P_date', 'P_name', 'P_price'); $i=1;
        foreach($trs as $tr) {
            if( ! preg_match_all("#<td.*?>(.*?)</td>#si", $tr, $match)) {
                continue;
            }
            $tds = $match[1];

            $arr[$i][] = $tds[0];
            $arr[$i][] = $tds[1];
            $arr[$i][] = $tds[2];
            $i++;
        }

        $content_dom->clear();

        $return['items'] = $this->createstruct($arr);

        return $return;
    }
}

/*
 * DETAIL
*/
class parser_1_0007_01_0_00_krasgz_detail extends parser_1_0000_04_0_00_one_doc_detail {

    protected $detail_link = 'http://www.krasgz.ru/torgi/?action=view&id=';

    var $detail_sort = array(
            'Ответственное лицо/Телефон'            => 'customer_phone',
            'Организатор/Электронный адрес'         => 'customer_email',
            'Ответственное лицо/Электронный'        => 'customer_email',
            'Заказчики'                             => 'customer',
            'Заказчики/Адрес'                       => 'customer_address',
            'Заседание комиссии'                    => 'date_conf|date_convert',
            'Прием заявок'                          => 'date_end|get_date_end',
            'Закупка добавлена'                     => 'date_publication|date_convert',
            //[Предмет закупок] => Выполнение инженерно-геодезических работ
            //[Начальная (максимальная) цена контракта(ов) (цена лота(ов))] => 29 975,22&nbsp;р
            //[Источник финансирования] => Внебюджетные средства
            //[Способ размещения] => Открытый конкурс
            //[Статус закупки] => Объявленные
            //[Организатор] => Комитет по управлению муниципальным имуществом администрации Канского района
            //[Организатор/Адрес] =>  663600, г.Канск, ул.Ленина 4\1
            //[Организатор/Телефон] =>  8(39161)3-31-67
            //[Ответственное лицо] => Кислянская Светлана Александровна
            //[Ответственное лицо/Должность] =>  Ведущий специалист
            //[Ответственное лицо/Телефон] =>  8 (39161) 3-46-58

            //[Ответственное лицо/Электронный адрес] =>  <a href="mailto:byiskeh@mail.ru">byiskeh@mail.ru</a>
            //[Заказчики] => Комитет по управлению муниципальным имуществом администрации Канского района
            //[Заказчики/Адрес] =>  663600, г.Канск, ул.Ленина 4\1
            //[Заказчики/Телефон] =>  8(39161)3-31-67
            //'Заказчики/Телефон' => 'customer_phone',
            //[Заседание комиссии] => 20.10.2009г.&nbsp; 10:00
            //[Прием заявок] => 07.09.2009г.&nbsp; 08:00 &mdash; 20.10.2009г.&nbsp; 10:00
            //'Прием заявок' => 'date_end:date_convert',
            //[Способ получения документации] => Получение документации бесплатное
            //[Порядок предоставления заявок] => В конверте, запечатанные
            //[Преференции для инвалидов] => нет
            //[Закупка добавлена] => 04.09.2009г.
    );

    function text_get_date_end($str) {
        $dates = preg_get_all("/\d{2}.\d{2}.\d{4}/", $str);

        if(!$dates) {
            exit("error, str = $str");
        }

        $date = array_pop($dates);

        return $this->date_convert($date);
    }

    function detail_all($id) {
        $text = $this->detail_get($id);
        return $this->detail_parse($text);
    }

    function detail_get($id) {
        $this->loader->debug("krasgz, detail id = $id");

        $link = $this->detail_link . $id;

        $this->loader->debug( $link );

        $emul_br = $this->emul_br_init( $link );
        $emul_br->exec();

        $retun = $emul_br->GetBody();

        $this->loader->debug($emul_br->GetAllHeaders());

        return $retun;
    }

    function detail_parse($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);
        $table = $content_dom->find('table[class=none]', 0);

        $trs = $table->find('tr');

        $detail = array();

        foreach($trs as $tr) {
            $tds = $tr->find('td');

            if(empty($tds)) {
                continue;
            }

            $detail[trim($tds[0]->innertext)] = trim($tds[1]->innertext);
        }

        $content_dom->__destruct();

        foreach($detail as $k=>$v) {
            $k = preg_replace("/:$/i", "", $k);

            if( ($sub = preg_get("#<a.*?>(.*?)</a>#i", $v)) ) {
                $return['detail'][$k] = $sub;
                if( ($subs = preg_get_all_order("#<span.*?>(.*?):</span>(.*?)<br />#i", $v)) ) {
                    foreach($subs as $s) {
                        $return['detail'][$k . '/' . $s[1]] = $s[2];
                    }
                }
            }
            else {
                $return['detail'][$k] = $v;
            }
        }

        $return = $this->detail_sort_2($return['detail']);

        // Левые даты http://krasgz.ru/torgi/?action=view&id=32538
        if( ( $return['db']['date_publication'] > 20010131 ) ) {
            unset($return['db']['date_publication']);
        }

        //<h3>Документы:</h3>
        //<ul>
        //<li style="padding-bottom:0.4em"><a href="?action=doc&doc=85887">Извещение о проведении открытого конкурса №19</a><br />(04.09.2009г.)</li>
        //<li style="padding-bottom:0.4em"><a href="?action=doc&doc=85888">Конкурсная документация №19</a><br />(04.09.2009г.)</li>
        //</ul>
        $docs_str   = preg_get("#<h3>Документы:</h3>\s*(<ul>.*?</ul>)#si", $content);
        $parts = preg_get_all_order(
                "#&doc=(\d*)\">(.*?)</a>.*?(\d{2}\.\d{2}\.\d{4})#i",
                $docs_str);

        if($parts) {
            foreach($parts as $k=>$p) {
                $return['docs'][$k] = array(
                        'internal_id' => $p[1],
                        'name'        => $p[2],
                        'date'        => $this->date_convert($p[3]),
                );
            }
        }
        else {
            $doc_tmp[$k] = array();

        }
        print_r($return);
        return $return;
    }

    function test_detail_parse() {
        $content = file_get_contents( dirname(__FILE__) . '/detail_parse.htm');
        $list = $this->detail_parse($content);
        print_r($list);
        return $list;
    }

    /**
     * Получить документ
     * @param array $doc
     * @param array $item
     * @return array
     */
    function doc_get($doc, $item = null) {
        $this->loader->debug("krasgz, docs_load");

        $link = 'http://krasgz.ru/torgi/?action=doc&doc=' . $doc['internal_id'];
        
        $this->loader->debug($link);

        $emul_br = $this->emul_br_init( $link );
        $emul_br->exec();

        $return['data'] = $emul_br->GetBody();

        //Content-Disposition: attachment; filename="Èçâåùåíèå î ïðîâåäåíèè îòêðûòîãî êîíêóðñà ¹19.doc"

        $return['md5']  = md5($return['data']);
        $return['size'] = strlen($return['data']);

        $f = $emul_br->GetResponseHeader('Content-Disposition');
        $f = preg_get("/filename=(.*)/i", $f);
        $f = preg_replace(array("/^['\"]+/i", "/['\"]+$/i"), "", $f);
        $f = preg_replace("/['\"]/i", "_", $f);

        $return['filename'] = $this->text_from_win($f);

        $this->loader->debug($emul_br->GetAllHeaders());

        return $return;
    }

}