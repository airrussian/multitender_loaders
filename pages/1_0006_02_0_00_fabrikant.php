<?php

/**
 * Межотраслевая Торговая Система "Фабрикант"
 */
class loader_1_0006_02_0_00_fabrikant extends loader_1_0000_01_0_00_one {

    public $base_url = 'http://www.fabrikant.ru/market/?action=';
    public $fields_list = array(
        'internal_id',
        'name',
        'detail_link',
        'type',
    );
    public $parser_name = 'parser_1_0006_02_0_00_fabrikant_scraperwiki';
    public $parser_name_detail = 'parser_1_0006_02_0_00_fabrikant_detail';
    public $break_by_pass = true;

    /*    public $fields_rewrite = array(
      'type'         => 'Коммерческий',
      'type_dict_id' => 1000,
      'type_id'      => 100,
      'sector_id'    => 2,
      ); */
}

class parser_1_0006_02_0_00_fabrikant_scraperwiki extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'internal_id' => 'internal_id',
        'name' => 'name',
        'type' => 'type',
        'url' => 'detail_link'
    );

    function get($id, $limit) {
        $emul_br = $this->emul_br_init('http://free-ec2.scraperwiki.com/clif4ci/exec');

        $post = array(
            'apikey' => '3e76d0ed-52ee-4224-9426-c57ae0c18ff6',
            'cmd' => "/usr/bin/php /home/projects/fabrikant/select.php id=$id limit=$limit"
        );

        $emul_br->SetPost($post);
        $emul_br->exec();
        $content = $emul_br->GetBody();

        return $content;
    }

}
/*
class parser_1_0006_02_0_00_fabrikant extends parser_1_0000_02_0_00_temp {

    function list_get_page($link, $page = 1) {
        $page = ($page - 1) * 20;
        $link = $link . $page;
        $this->loader->debug("$link");
        return $this->emul_br_get($link);
    }

    protected $colomn = array(
        '№' => 'internal_id',
        'Организатор' => 'customer|clear_all',
        'Предмет' => 'name|clear_all',
        'Цена' => 'price|to_price',
        'Дата' => 'date_publication',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach ($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_publication'] = $this->text_date_convert(preg_get("#(\d{2}.\d{2}.\d{4}).*?<br>#sui", $item['date_publication_src']));
            $item['date_conf'] = $this->text_date_convert(preg_get("#<br>(\d{2}.\d{2}.\d{4}).*?<br>#sui", $item['date_publication_src']));
            $item['date_end'] = $this->text_date_convert(preg_get("#<br>(\d{2}.\d{2}.\d{4})#sui", $item['date_publication_src']));

            $item['customer'] = $this->text_clear_all(preg_get('#>(.*?)</a>#', $item['customer_src']));
            $item['name'] = $this->text_clear_all(preg_replace('#<br>#', '. ', $item['name_src']));
            $item['detail_link_prefix'] = "http://www.fabrikant.ru/" . $this->text_clear_all(preg_get("#href=['\"](.+?)['\"]#si", $item['name_src']));

            $items[$k] = $item;
        }

        $return = array(
            'page_total' => $parse['page_total'],
            'page_now' => $parse['page_now'],
            'items_total' => $parse['items_total'],
            'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $list = $content_dom->find("table.list", 0);
        if (!$list) {
            return false;
        }

        $arr = $this->parse_table($list->outertext);

        if (count($arr[1]) == 2) {
            for ($i = 1; $i < count($arr) - 1; $i++) {
                $arr[$i] = $arr[$i + 1];
            }
            unset($arr[count($arr) - 1]);
            for ($i = count($arr[0]); $i > 4; $i--) {
                $arr[0][$i] = $arr[0][$i - 1];
            }
            $arr[0][4] = 'Цена';
            $arr[0][5] = 'Понижение';
        }

        $lots = preg_get_all("#(\d+)\sлот#sui", $list->prev_sibling()->innertext);
        $sum = 0;
        foreach ($lots as $l) {
            $sum = $sum + $l;
        }
        $ret['items_total'] = $sum;

        if (($pgs = $content_dom->find("div.navbar", 0))) {
            $ret['page_now'] = preg_get("#<b>(\d+)</b>#sui", $pgs->innertext);
            $ret['page_total'] = max(preg_get_all("#>&nbsp;(\d+)&nbsp;<#sui", $pgs->innertext));
        } else {
            $ret['page_now'] = 1;
            $ret['page_total'] = 1;
        }

        $content_dom->__destruct();

        $ret['items'] = $this->createstruct($arr);

        return $ret;
    }

}*/

class parser_1_0006_02_0_00_fabrikant_detail extends loader_1_0000_01_0_00_one {

    protected $detail_base = 'http://www.fabrikant.ru/';
    protected $detail_link = 'http://www.fabrikant.ru/market/view.html?id=';
    public $no_curl = true;
    public $detail_sort = array(
        'Предмет закупки:'              => 'name|clear_all',
        'Номер извещения'               =>  'num|clear_all',
        'Дата публикации:'              => 'date_publication|date_convert',
        'Дата завершения процедуры:'    => 'date_end|date_convert_search',
        'Заказчик:'                     => 'customer|clear_all',
        'Организатор процедуры:'        => 'region_id|region_convert',
    );

    function detail_get($id) {
        $this->detail_link = $this->item_now['detail_link'];
        $this->loader->debug("detail id = $id");
        $this->loader->debug("link = " . $this->detail_link);

        $emul_br = $this->emul_br_init($this->detail_link);
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);

        if (!$content) {
            return false;
        }

        $parse_detail = $this->list_parse_pre($content);

        $return = $this->detail_sort_2($parse_detail);

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $return = array();
        $return['Предмет закупки:'] = $content_dom->find("h1", 0)->innertext;

        $tbl = $content_dom->find("table.blank", 0);

        $tbl = $this->parse_table($tbl->outertext);

        foreach ($tbl as $row) {
            if (count($row) == 1) {
                $return['Тип'] = $this->text_clear_all($row[0]);
                $return['Номер извещения'] = preg_get("#№ (\d+)#si", $return['Тип']);
            } else {
                $return[$row[0]] = $row[1];
            }
        }

        $content_dom->__destruct();

        return $return;
    }

    function region_convert($text) {
        return $this->loader->geocoder_auto($text);
    }

}
