<?php

/*
 * Закупки ОАО Газпром Комплектация
*/

class loader_1_0051_02_0_00_gazpromkom extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.komplekt.gazprom.ru/';
    public $list_link           = 'http://www.komplekt.gazprom.ru/tenders/tenders-notification/';
    public $parser_name         = 'parser_1_0051_02_0_00_gazpromkom';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end'          => 'maybenull',
            'date_conf'         => 'maybenull',
            'doc'               => 'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0051_02_0_00_gazpromkom extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'num'           =>  'num|clear_all',
        'name'          =>  'name|clear_all',
        'pubDate'       =>  'date_publication|clear_all|date_convert_text_full',
        'endDate'       =>  'date_end|clear_all|date_convert_text_full',
        'confDate'      =>  'date_conf|clear_all|date_convert_text_full',
        'detail_link'   =>  'detail_link',
        'doc'           =>  'doc',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            print_r($item);

            $item['internal_id'] = abs(crc_p($item['num'] . $item['name']));

            $docs = preg_get_all("#<a.*?a>#si", $item['doc']);
            
            $item['doc'] = array(); 
            foreach ($docs as $doc) {

                $detail_link = preg_get("#href=['\"](.*?)['\"]#si", $doc);
                $name_doc    = preg_get("#<a.*?>(.*?)</a>#si", $doc);
                $internal_id = abs(crc_p($detail_link));

                $item['doc'][] = array(
                    'internal_id'   =>  $internal_id,
                    'name'          =>  $name_doc,
                    'detail_link'   =>  $detail_link,
                );
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $items = preg_get_all('~<a href="#s\d+">№ .*?</a>~si', $content);

        foreach ($items as &$item) {
            $detail_link = preg_get('~href="#(\S+?)"~si', $item);
            $detail = preg_get('~<a name="'.$detail_link.'".*?<a href="#s01"~si', $content);

            $name = preg_get("~text-decoration: underline.*?Предмет запроса предложений.*?<p>(.*?)</p>~sui", $detail);
            $pubDate = preg_get("#Дата и место начала приёма заявок &ndash; (.*?) г\.#sui", $detail);
            $endDate = preg_get("#Дата, время и место окончания приема заявок &ndash; (.*?) г.#sui", $detail);
            $confDate = preg_get("#Дата, время и место вскрытия конвертов &ndash; (.*?) г.#sui", $detail);

            $doc = preg_get("#<ol>(.*?)</ol>#si", $detail);

            $item = array(
                'num'           =>  $item,
                'name'          =>  $name,
                'pubDate'       =>  $pubDate,
                'endDate'       =>  $endDate,
                'confDate'      =>  $confDate,
                'detail_link'   =>  $detail_link,
                'doc'           =>  $doc
            );
        }

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

