<?php

/**
 * @author airrussian@mail.ru
 */
class loader_1_1006_01_0_00_ots extends loader_1_0000_01_0_00_one {

    public $base_url = 'http://www.otc.ru/';
    public $list_link = 'http://www.otc.ru/tender/i-frame/FindTenders.aspx?jqGridID=ctl00_BXContent_otctenderintegration1_otctenderintegration1_otctenderintegration1_jqgTrade&auto_Number=&auto_Name=&auto_UseTradeName=true&auto_UseLotName=false&auto_TradeLotState=-1&auto_PurchaseMethod=-1&auto_CollectivePurchasing=false&auto_AuctionVariant=-1&auto_StartPriceMin=&auto_StartPriceMax=&auto_PublicationDate.From=&auto_PublicationDate.To=&auto_PositionNumber=&auto_Orgnizer=&auto_UseOrganizerName=true&auto_UseOrganizerInn=false&auto_Customer=&auto_UseCustomerName=true&auto_UseCustomerInn=false&auto_Region=&auto_ApplicationEndDate.From=&auto_ApplicationEndDate.To=&auto_BusinessOperatorId=-1&nd=1377771344732&rows=100&sidx=&sord=asc&page=';
    public $parser_name = 'parser_1_1006_01_0_00_ots';
    public $parser_name_detail = 'parser_1_1006_01_0_00_ots_detail';
    public $fields_list = array(
        'internal_id',
        'name',
        'num',
        'date_end',
        'date_publication',
        'type',
        'region',
        'customer'
    );
    
    public $break_by_pass = true;
    public $item_rewrite = false;
}

class parser_1_1006_01_0_00_ots extends parser_1000_one {

    protected $colomn = array(
        'Процедура'         =>  'type|clear_all',
        'Наименование'      =>  'name|clear_all',
        'Номер лота'        =>  'num',
        '№'                =>  'internal_id',
        'Дата публикации'   =>  'date_publication|date_convert',
        'Дата окончания'    =>  'date_end|date_convert',
        'Заказчик'          =>  'customer|clear_all',
        'Регион'            =>  'region|clear_all',
    );

    function list_get_page($link , $page = 1) {
        $this->loader->debug($link . $page);
        $emul_br = $this->emul_br_init($link . $page);
        $emul_br->exec();        
        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
                
        foreach ($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);                                   
            $items[] = $item;
        }

        $return = array(
            'page_total' => $parse['page_total'],
            'items_total' => $parse['items_total'],
            'page_now'      =>  $parse['page_now'],
            'items' => $items,
        );
        
        return $return;
    }

    function list_parse_pre($content) {

        $result = json_decode($content);

        $page = $result->page;
        $total = $result->total;
        $records = $result->records;

        $items = array();

        foreach ($result->rows as $row) {
            $cell = $row->cell;

            $items[] = array(
                '№' => $cell[2],
                'Наименование' => $cell[3],
                'Номер лота' => $cell[1],
                'Название лота' => $cell[4],
                'Процедура' => $cell[5],
                'Статус' => $cell[6],
                'Организатор' => $cell[7],
                'Заказчик' => $cell[8],
                'Регион' => $cell[10],
                'Дата публикации' => $cell[11],
                'Дата окончания' => $cell[12],
                'Бизнес оператор' => $cell[13],
            );
        }
        
        $return = array (
                'page_total'  => $total,
                'page_now'    => $page,
                'items_total' => $records,
                'items'       => $items,
        );        

        return $return;
    }

}

class parser_1_1006_01_0_00_ots_detail extends parser_1_1006_01_0_00_ots {
    
    public $detail_sort = array(
        "Основные сведения/Номер закупки"           =>  "num",
        "Основные сведения/Наименование закупки"    =>  "name|clear_all",
        "Основные сведения/Название процедуры в соответствии с положением о закупках"   =>  "type|clear_all",
        
        "Основные сведения/Заказчик"                =>  "customer|clear_all",
        "Основные сведения/Место нахождения"        =>  "customer_address|clear_all",
        "Основные сведения/Адрес электронной почты" =>  "customer_email|clear_all",
        
        "Основные сведения/Телефон"                 =>  "customer_telephone|clear_all",
        
        "Сроки и пояснения/Дата начала подачи заявок"               =>  'date_publication|date_convert',
        "Сроки и пояснения/Дата окончания подачи заявок"            =>  "date_end|date_convert",
        "Сроки и пояснения/Дата рассмотрения первых частей заявок"  =>  "date_conf|date_convert",
        
    );
    

    function detail_all($id) {
        
        $link = "http://www.otc.ru/tender/i-frame/TradeLotDetail.aspx?Id=";
        $this->loader->debug($link . $id);
        $content      = $this->emul_br_get_body($link . $id);        
        $return = $this->detail_parse_pre($content);                               
        return $return;        
    }
    
    function detail_parse_pre($content) {
        
        $dom = str_get_dom($content);
        
        $data = $dom->find("div.labelminwidth", 0);
        
        $return = array();
                
        foreach ($data->find("div.form_auction") as $form_auction) {            
            if (!$block_title = $form_auction->find("div.block_title", 0)) {                continue; }
            $h2 = $this->text_clear_all($block_title->find("h2", 0)->innertext);
            
            foreach ($form_auction->find("fieldset.openPart") as $openPart) {
                $title = $this->text_clear_all($openPart->find("label", 0)->innertext);
                $v = $openPart->find("span", 0)->innertext;                
                $return[$h2 . '/' . $title] = $v;
            }                        
        }
        
        $return = $this->detail_sort_3($return);
        /*
        $json = $data->find("input#ctl00_BXContent_otctenderintegration1_otctenderintegration1_otctenderintegration1_ufFiles_hfDocuments", 0)->getAttribute("value");        
        $json = str_replace(array("&quot;"), array('"'), $json);        
        $docs = json_decode($json);
        
        foreach ($docs as $doc) {
            $return['docs'][] = array(
                'name'          =>  $doc->FileName,
                'detail_link'   =>  "http://sites.otc-tender.ru/FileServiceSite/FileDownloadHandler.ashx?FileGuid=" . $doc->FileGuid,
                'internal_id'   =>  crc_p($doc->FileGuid),
            );
        }*/
        
        $return['html'] = $content;        

        return $return;
    }

}
