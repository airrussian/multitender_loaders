<?php
/**
 * Годовые планы государственных закупок - Портал государственных закупок
 * @author airrussian@mail.ru
 */

class loader_1_3001_01_0_00_goszakup extends loader_1_0000_01_0_00_one {

    public $base_url    = 'http://goszakup.gov.kz/';
    public $list_link   = 'http://goszakup.gov.kz/app/index.php/ru/publictrade/openbuys/';
    public $parser_name = 'parser_1_3001_01_0_00_goszakup';
    public $fields_list = array(
            'internal_id',
            'name',
            'price'      => 'maybenull',
            'customer'   => 'maybenull',
            'date_publication',
            'date_end',
            'region_id',
            'type',
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = false;

    function run_list($region_id = null) {
        // DEBUG
        // $region_id = 1;
        $this->debug('START');

        $region = new region_dict();

        if($region_id) {
            $region_id = (int) $region_id;
            if (!$region->Load("site_id = $this->site_id AND region_id = $region_id")) {
                exit("Unknown region");
            }
            $regionArray = array($region);
        }
        else {
            $regionArray = $region->Find('site_id = ' . $this->site_id);
        }

        $this->debug('START regions, count = ' . sizeof($regionArray));

        $i = 0;
        foreach($regionArray as $region) {
            $this->parser->region_id = $region->region_id;
            ;
            $this->parser->region_internal_id = $region->internal_id;

            $this->debug('START region = ' . $region->region_id );

            parent::run_list();

            sleep($this->item_sleep);

            $i ++;
        }
    }

}

/**
 * LIST
 */
class parser_1_3001_01_0_00_goszakup extends parser_1_0000_01_0_00_one {

    private $cookie;

    protected $colomn = array(
            '№'                         => 'internal_id|clear_all',
            'Заказчик'                  => 'customer',
            'Наименование закупки'      => 'name|clear_all',
            'Сумма'                     => 'price',
            'Способ'                    => 'type',
            'Начало'                    => 'date_publication|date_convert',
            'Конец'                     => 'date_end|date_convert',
    );

    function list_get_page( $link, $page = 1 ) {
        $page = (int) $page;
        $this->loader->debug("LINK = " . $link.$page);
        $emul_br = $this->emul_br_init($link.$page);
        if ($page==1) {
            $post = array(
                    "filter[zakazchik]" => "",
                    "filter[buy_name]"  => "",
                    "filter[buystatus]" => 2,
                    "filter[method]"    => "NOTSELECTED",
                    "filter[summ_from]" => "",
                    "filter[summ_to]"   => "",
                    "filter[zak_region]"=> $this->region_internal_id,
                    "filter[start_date]"=> "",
                    "filter[end_date]"  => "",
                    "filter[submit]"    => "Применить"
            );
            $emul_br->set_post($post);
        }
        if ($page > 1) {
            $emul_br->SetCookie($this->cookie);
        }
        $content = $emul_br->exec();

        if ($page==1) {
            $this->cookie = $emul_br->GetResponseCookieArray();
        }

        echo "Регион = " . preg_get("#<select.*?zak_region.*?>.*?<option.*?selected=\"selected\">(.*?)</option>.*?</select>#si", $content) . "\n";

        return $content;
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end']<date("Ymd")) { continue; }

            $item['price'] = $this->text_to_price($item['price_src'] * 0.2);
            $item['customer'] = $this->text_clear_all(preg_get("#title=\"(.*?)\">#si", $item['customer']));
            $item['region_id'] = $this->region_id;

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = tidy_repair_string($content, $this->tidy_config);

        $content_dom = str_get_html($content);

        $table = $content_dom->find("div#trade", 0)->find("table.zebra", 0)->outertext;

        $arr = $this->parse_table($table);
        $arr[0][7] = $arr[1][0];
        $arr[0][8] = $arr[1][1];
        $arr[1] = $arr[51];
        unset($arr[51]);

        $paginator = $content_dom->find("div.paginator", 0)->innertext;
        $return['items_total'] = count($arr);
        $return['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paginator));
        $return['page_now'] = (int) preg_get("#<strong>(\d+)</strong>#si", $paginator);

        $content_dom->__destruct();

        $return['items'] = $this->createstruct($arr);

        return $return;
    }
}

