<?php
/**
 * Норильский Никель
 * 21.07.2010 - Написание
 * 09.08.2010 - запуск
 */
class loader_1_0011_01_0_00_NNzf extends loader_1_0000_02_0_00_temp_many {
    public $base_url    = 'http://www.zf.norilsknickel.ru/';
    public $list_link   = 'http://www.zf.norilsknickel.ru/';
    public $parser_name = 'parser_1_0011_01_0_00_NNzf';
    public $parser_name_detail = 'parser_1_0011_01_0_00_NNzf_detail';

    protected $pages_list = array(
            array(
            // Список объявленных запросов (активные)
                            'link' => 'guest_listsets.aspx?subtype=2&category=-1&page=',
                            'type' => 'запрос котировок',
            ),
            array(
            // Открытые конкурсы
                            'link' => 'guest_listsets.aspx?category=-1&page=',
                            'type' => 'Открытый конкурс',
            ),
    );


    public $fields_list = array(
            'name',
            'date_publication',
            'type',
            'internal_id',
            'doc',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    function run_test_detail($id=1902) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;
        $arr = $this->parser->detail_all($id);
        var_dump($arr);
        exit;
    }


}

class parser_1_0011_01_0_00_NNzf extends parser_1_0000_02_0_00_temp_many {

    protected $colomn = array(
            'p_internal_id' => 'internal_id',
            'p_name'        => 'name|clear_all',
            'p_date'        => 'date_publication|date_convert',
            'p_docs'        => 'doc',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.($page-1) );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#guest_set_header.aspx\?tender=(\d+)#si", $item['internal_id_src']);

            $item['doc'] = array();
            $item['doc'][0]['internal_id'] = preg_get("#set_getfile.aspx?set=(\d+)#si", $item['doc_src']);
            $item['doc'][0]['name'] = 'документация';

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $table = $content_dom->find('table.GuestListSet', 0);

        if (empty($table)) { return false; }

        $i=0;
        foreach ($table->find("table") as $row) {

            if (preg_match("#<td id=\"l\d+#si", $row->innertext)) {
                
                $arr[$i]['p_date'] = $row->find("td.Date", 0)->innertext;
                $arr[$i]['p_name'] = $row->find("td.Name", 0)->innertext;
                $links = preg_get_all("#<a.*?a>#si", $row->outertext);

                $arr[$i]['p_internal_id'] = $links[0];
                $arr[$i]['p_docs'] = $links[1];

                $i++;
            }
        }

        $ret['items'] = $arr;
        
        if ($page = $content_dom->find('table.Page', 0)) {
            $ret['page_total']  = max(preg_get_all("#<a.*?<b>(\d+)</b>#si", $page->innertext));
            $ret['page_now']    = (int) preg_get("#\[(\d+)\]#", $page->find("label.NN", 0)->innertext);
        } else {
            $ret['page_total']  = 1;
            $ret['page_now']    = 1;            
        }
        $ret['items_total'] = (int) preg_get("#Всего: (\d+)#sui", $content);
        
        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0011_01_0_00_NNzf_detail extends parser_1_0000_02_0_00_temp_many {

    protected $detail_link = 'http://www.zf.norilsknickel.ru/guest_set_header.aspx?tender=';

     public $detail_sort = array(
         'Период приёма документов' => 'date_publication',        
    );

    function detail_all($id) {

        $this->detail_link         =  'http://www.zf.norilsknickel.ru/guest_set_header.aspx?tender=' . $id;        
        $this->loader->debug("LINK = " . $this->detail_link);
        $content      = $this->emul_br_get_body($this->detail_link);
        $parse_detail = $this->list_parse_pre($content);


        $html = $parse_detail['html'];
        unset($parse_detail['html']);
        // sort function
        $return = $this->detail_sort_3($parse_detail);

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        $return['html'] = $this->text_clear_html_gently($html);
        $return['html'] = preg_replace(array('#<a.*?>#i','#</a>#i'), '', $return['html']);

        $period = preg_get("#(.*?) - (.*)#si", $return['db']['date_publication']);        

        $return['db']['date_publication'] = $this->text_date_convert($period[0]);
        $return['db']['date_end'] = $this->text_date_convert($period[1]);       

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['other'][3]);

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tbl = $content_dom->find("table.GuestListSet", 0);

        $arr['html'] = $tbl->outertext;

        $name = $this->text_clear_all($tbl->find("td.Date", 0)->find("td", 0)->innertext);
        $date = $tbl->find("td.Date", 0)->find("td", 1)->innertext;
        
        $arr[$name] = $date;

        foreach ($tbl->find("td.Name") as $name) {
            $arr[] = $name->innertext;
        }

        $lots = $tbl->find("td.lotlist", 0)->find("table", 0);

        $lots = $this->parse_table($lots->outertext);

        foreach ($lots as $lot) {
            $arr[] = $this->text_clear_all($lot[0]) . " " . $this->text_clear_all($lot[1]);
        }

        $content_dom->__destruct();

        return $arr;
    }

    function text_clear_html_gently($string) {
        $string = preg_replace(array('#(id|class|style)=".*?"#ui','#</?(font|span).*?>#ui','/<!--.*?-->/sui'), '', $string);
        $string = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $string);
        $string = tidy_repair_string($string, $this->tidy_config);
        return $string;
    }

}

