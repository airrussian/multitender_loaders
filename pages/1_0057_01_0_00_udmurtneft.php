<?php

/*
 * Тендеры на поставку услуг ОАО «Удмуртнефть»
*/

class loader_1_0057_01_0_00_udmurtneft extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.udmurtneft.ru/';
    public $list_link           = 'http://www.udmurtneft.ru/tender/tendersall';
    public $parser_name         = 'parser_1_0057_01_0_00_udmurtneft';
    public $parser_name_detail  = 'parser_1_0057_01_0_00_udmurtneft_detail';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'customer',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0057_01_0_00_udmurtneft extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        'Название'              =>  'name|clear_all',
        'Заказчик'              =>  'customer|clear_all',
        '2'                     =>  'doc',
        'Начало'                =>  'date_publication|clear_all|date_convert',
        'Окончание'             =>  'date_end|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['num'] = preg_get("#^(.*?\d\D?)\s#si", $item['name']);
            if ($item['num']) {
                $item['name'] = trim(str_replace($item['num'], "", $item['name']));
                $item['num'] = trim(str_replace("Лот № ", "", $item['num']));
            }
            $item['internal_id']    =   preg_get("#node/(\d+)#si", $item['name_src']);

            $doc_detail_link = preg_get("#tenders_doc/(.*?)['\"]#si", $item['doc']);
            $item['doc'] = array(
                0   =>  array(
                    'name'          =>  'Пакет документов',
                    'detail_link'   =>  $doc_detail_link,
                    'internal_id'   =>  abs(crc_p($doc_detail_link))
                )
            );

            $items[$k] = $item;
        }
       
        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);
        $data = $html->find("div.view-content-tendersall", 0)->find("table", 0)->outertext;
        $html->clear();

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0057_01_0_00_udmurtneft_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    public $detail_sort = array(
        'номер'                     =>  'num|clear_all',
        'Дата и время окончания'    =>  'date_end|clear_all|date_convert',
    );

    function detail_get($id) {

        $this->detail_link = "http://www.udmurtneft.ru/node/$id";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {       
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $return = $this->detail_sort_3($parse);

        $return['content'] = $this->detail_content_cache($content);
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['other']['Контактная информация/адрес подачи документов']);
        foreach ($return['other'] as $k => $v) {
            if (is_null($v)) { unset($return['other'][$k]); }
        }

        return $return;
    }

    function detail_parse_pre($content) {

        $dom = str_get_html($content);

        $return = array();

        $html = $dom->find("div.view-content-tender", 0);

        $return['номер'] = $html->find("div.tender-date", 0)->find("strong",0)->innertext;
        $return['Важная информация'] = $html->find("div.important_info", 0)->innertext;
        $return['Контактная информация/телефоны для справок'] = $html->find("div.contact-tel", 0)->find("span", 0)->innertext;
        $return['Контактная информация/адрес электронной почты'] = $html->find("div.contact-email", 0)->find("span", 0)->innertext;
        $return['Контактная информация/адрес подачи документов'] = $html->find("div.contact-adress", 0)->find("span", 0)->innertext;
        
        $dom->clear();

        return $return;
    }

}