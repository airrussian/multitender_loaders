<?php
/**
 * SetOnline. Система электронных торгов
 * Написание: 28.07.2010
 * 2010.08.10 - запуск
 */
class loader_1_0019_01_0_00_setonline extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.setonline.ru/';
    public $list_link           = 'http://www.setonline.ru/set/announced.php';
    public $parser_name         = 'parser_1_0019_01_0_00_setonline';
    public $parser_name_detail  = 'parser_1_0019_01_0_00_setonline_detail';

    public $fields_list = array(
            'name',
            'date_end',
            'internal_id',
            'customer',
    );
    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function test_detail($id=9169) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);

        var_dump($arr);
        exit;
    }

}

class parser_1_0019_01_0_00_setonline extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Заказчик'          => 'customer|clear_all',
            'Предмет'           => 'name|clear_all',
            'Дата проведения'   => 'date_end|date_convert',
            'Начальная цена'    => 'price|to_price',
            'internal_id'       => 'internal_id',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $ten = $content_dom->find("table", 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $internal_ids = preg_get_all("#openAuction2\('(\d+)'\)#si", $ten->outertext);

        $i=0;
        foreach ($ret['items'] as &$items) {
            $items['internal_id'] = $internal_ids[$i];
            $i++;
        }

        $ret['page_now'] = 1;
        $ret['page_total'] = 1;
        $ret['items_total'] = count($ret['items']);

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0019_01_0_00_setonline_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
            'Почтовый адрес Заказчика'      => 'customer_address|clear_all',
    );

    function detail_all($id) {
        $link         = "http://www.setonline.ru/set/announce.php?id=".$id;
        $content      = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);

        $docs = $parse_detail['Конкурсная документация'];
        unset($parse_detail['Конкурсная документация']);

        $return = $this->detail_sort_2($parse_detail);
        
        $return['docs'] = $docs;

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);

        var_dump($return);

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $det = $detail_dom->find("div#text", 0);

        foreach ($det->find("p") as $p) {
            if ($name = $p->find("strong", 0)) {
                $value = preg_replace("#<strong>.*?</strong>#si", "", $p->innertext);
                $name = preg_replace(array("#\d+\.\s#si", "#:#si"), "", $name->innertext);
                $arr[$this->text_clear_all($name)] = $this->text_clear_all($value);
            } else {
                $arr[] = $this->text_clear_all($p->innertext);
            }
        }
        
        if ($doc = $det->find("form#dok", 0)) {
            $arr['Конкурсная документация'] = array();
            $arr['Конкурсная документация'][0]['name'] = 'Документация';
            $arr['Конкурсная документация'][0]['detail_link'] = preg_get("#action=['\"](.*?)['\"]#si", $doc->outertext)."&doc_dem_type=documentation";             
            $arr['Конкурсная документация'][0]['internal_id'] = abs(crc_p($arr['Конкурсная документация'][0]['name'].$arr['Конкурсная документация'][0]['detail_link']));
        }

        $detail_dom->__destruct();

        return $arr;
    }

}


