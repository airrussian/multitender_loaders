<?php

/**
 * @version > Закупки ГОВ.РУ
 * @var     > http://zakupnew.gov.ru/
 * @param   > run_list, run_detail
 * @author  > airrussian@mail.ru
 */
class loader_1_1000_44_0_00_zakupki extends loader_1_0000_01_0_00_one {

    public $base_url = 'http://zakupki.gov.ru';
    public $parser_name = 'parser_1_1000_44_0_00_zakupki_ftp_scraperwiki';
    
    public $many_count = 15;
    public $many_sleep = 4;
    public $fields_list = array(
        'num',
        'name',
        'date_publication',
        'date_end' => 'maybenull',
        'customer' => 'maybenull',
        'customer_telephone' => 'maybenull',
        'customer_email' => 'maybenull',
        'customer_address' => 'maybenull',
        'type',
        'internal_id',
        'region'    => 'maybenull',
        'price' => 'maybenull',
        'detail_link',
        'doc' => 'maybenull'
    );
    
    /*public $fields_rewrite = array(
        'region_id' => 100,
    );*/
    
    public $break_by_pass_count = 10;
    public $sleep_list = 15;
    public $page_total_rewrite = true;
    public $break_by_pass = false;
    public $item_rewrite = true;
}

class parser_1_1000_44_0_00_zakupki_ftp_scraperwiki extends parser_1_0000_01_0_00_one {
    
    protected $colomn = array(
        'num'           => 'num',
        'internal_id'   => 'internal_id',
        'name'          => 'name',
        'date_pub'      => 'date_publication|date_convert_my',
        'date_end'      => 'date_end|date_convert_my',
        'type'          => 'type',
        'region'        => 'region',
        'url'           => 'detail_link',
        'price'         => 'price',
        'region'        => 'region',
        'type'          => 'type',
        'organizator'   => 'customer',
        'doc'           => 'doc'
    );
    
    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        
        $items = array();
        foreach ($parse as $key => $item) {
            if ($item['date_end'] == '' ) {
                $item['date_end'] = date('Y-m-d', strtotime($item['date_pub']) + 31 * 24 * 60 * 60);                
            }            
            
            $item['url'] = 'http://zakupki.gov.ru/epz/order/notice/zk44/view/common-info.html?regNumber=' . $item['num'];
            $item['internal_id'] = abs(crc_p($item['num']));
                                
            if (!empty($item['doc'])) {
                $docs = $item['doc'];
                foreach ($docs as &$doc) {
                    $doc['detail_link'] = $doc['url'];
                    $doc['internal_id'] = abs(crc_p($doc['url']));
                }            
                unset($item['doc']);
            }
            
            $item = $this->list_set_colomn($item, $this->colomn);
            
            $item['customer'] = $item['customer_src']['name'];
            $item['customer_telephone'] = $item['customer_src']['phone'];
            $item['customer_address']  = $item['customer_src']['postaddress'];
            $item['customer_email'] = $item['customer_src']['email'];
            if (isset($docs)) { 
                $item['doc'] = $docs;
            }
            $items[$key] = $item; 
            
        }                   
                
        $return = array(
            'id_max' => max(array_keys($parse)),
            'id_min' => min(array_keys($parse)),
            'items' => $items
        );                
        
        return $return;
    }    
    
    function get($id, $limit) {
        $emul_br = $this->emul_br_init('http://free-ec2.scraperwiki.com/clif4ci/exec');

        $post = array(
            'apikey' => '3e76d0ed-52ee-4224-9426-c57ae0c18ff6',
            'cmd' => "/usr/bin/php /home/projects/zakupki/select.php id=$id limit=$limit"
        );

        $emul_br->SetPost($post);
        $emul_br->exec();
        $content = $emul_br->GetBody();
        return $content;
    }

}

