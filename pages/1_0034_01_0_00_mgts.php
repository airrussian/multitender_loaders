<?php
/**
 * MGTS
 * 09.12.2010 - Написание
 */
class loader_1_0034_01_0_00_MGTS extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://mgts.ru/';
    public $list_link           = 'http://mgts.ru/company/competitions/purchase/open_requests/';
    public $parser_name         = 'parser_1_0034_01_0_00_MGTS';   

    public $fields_list = array(
            'name',
            'date_end',
            'type',
            'internal_id',
            'doc' => 'maybenull',
    );
    
    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );


    public $break_by_pass = true;
    public $item_rewrite  = false;
}

class parser_1_0034_01_0_00_MGTS extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'p_name'        => 'name|clear_all',
            'p_doc'         => 'doc',
            'p_other'       => 'other',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("LINK = $link \n");
        return $this->emul_br_get_body( $link );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = abs(crc_p($item['name']));

            $item['date_end'] = $this->text_date_convert_text_full(preg_get("#Дата окончания.*?(\d+\s\S+\s\d+).*?</p>#si", $item['other']));
            $item['date_publication'] = $this->text_date_convert_text_full(preg_get("#Дата подачи.*?(\d+\s\S+\s\d+)#si", $item['other']));

            if (empty($item['date_end'])) {
                $item['date_end'] = $this->text_date_convert(preg_get("#<p>(\d+\.\d+\.\d+)</p>#si", $item['other']));
            }

            if (!empty($item['date_publication'])) {
                $dp = $item['date_publication'];
                $dp = substr($dp,6,2).".".substr($dp,4,2).".".substr($dp,0,4);
                $dp = strtotime($dp)+14*24*60*60;
                $item['date_end'] = date("Ymd", $dp);
            }

            $doc = preg_get_all("#(<a.*?a>)#si", $item['doc_src']);
            $i=0;
            $item['doc'] = array();
            foreach ($doc as $d) {
                $item['doc'][$i]['name'] = $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#si", $d));
                $item['doc'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"].*?>#si", $d);
                $item['doc'][$i]['internal_id'] = abs(crc_p($item['doc'][$i]['name'].$item['doc'][$i]['detail_link'].$item['internal_id']));
                $i++;
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $items = $content_dom->find('div#content', 0);

        $i = 0;
        foreach ($items->find("h3") as $h3) {
            $arr[$i]['p_name'] = $h3->innertext;
            $detail = $h3->next_sibling();
            $arr[$i]['p_doc'] = "";
            foreach ($detail->find("p.file") as $doc) { $arr[$i]['p_doc'] .= $doc->innertext; }
            $arr[$i]['p_other'] = $detail->outertext;
            $i++;
        }

        $ret['items'] = $arr;
        $ret['items_total'] = $i;
        $ret['page_total'] = 1;
        $ret['page_now'] = 1;

        $content_dom->__destruct();

        return $ret;
    }
}