<?php
/**
 * Аукционный Конкурсный Дом http://www.a-k-d.ru/
 * 2010.08.05 - написание
 *
 * Особое внимание detail есть отличающейся структуры от всех остальных, хотя возможно парсер вообще стоит
 * убрать detailов, т.к. регион из них определить нельзя
 */
class loader_1_0025_01_0_00_AKD extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.a-k-d.ru/';
    public $list_link           = 'http://www.a-k-d.ru/page/torg_list_buy?page=';
    public $parser_name         = 'parser_1_0025_01_0_00_AKD';


    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'customer',
            'price',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    function list_stop(array $list) {

        $row = reset($list['items']);
        if ($row['date_end'] < date("Ymd")) {
            return true;
        } else {
            return false;
        }
    }

}

class parser_1_0025_01_0_00_AKD extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Номер'                     =>  'num|clear_all',
            'Организатор'               =>  'customer|clear_all',
            'Предмет'                   =>  'name|clear_all',
            'Начальная'                 =>  'price|clear_all|to_price',
            'Сведения'                   =>  'date_end|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $item) {                       
            $item = $this->list_set_colomn($item, $this->colomn);
            
            
            $item['num']    =   $this->text_clear_all(preg_get("#<a.+?>(.+?)</a>#si", $item['num_src']));
                                   
            $item['internal_id'] = preg_get("#tender/(\d+)#si", $item['num_src']);
            $item['date_end'] = (int) str_replace("-", "", preg_get("#(\d{4}-\d{2}-\d{2})#si", $item['date_end']));
            
            $d = preg_get_all("#(\d{2}\s\S+\s\d{4}) г.#sui", $item['date_end_src']);                       
            $item['date_publication'] = $this->text_date_convert_text_full($d[0]);
            $item['date_end'] = $this->text_date_convert_text_full($d[1]);
            
            $items[] = $item;
        }

        $return = array (
                'page_total'  => @ $parse['page_total'] ? $parse['page_total'] : 1,
                'page_now'    => @ $parse['page_now'] ? $parse['page_now'] : 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $tbl = $content_dom->find("table.dataGridTable", 0);

        $tmparr = $this->parse_table($tbl->outertext);

        $arr = array();
        foreach ($tmparr as $key => $row) {
            if (($key % 2==1) || ($key==0)) {
                $arr[] = $row;
            }
        }

        $ret['items'] = $this->createstruct($arr);

        $page = $content_dom->find("div.pages", 0);

        $ret['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#si", $page->innertext));
        if ($ret['page_total']>600) {
            $ret['page_total']=600;
        }
        $ret['page_now']   = (int) $page->find("li.active", 0)->innertext;

        $content_dom->__destruct();

        return $ret;
    }
}
