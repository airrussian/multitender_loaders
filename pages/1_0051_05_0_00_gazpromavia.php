<?php

/*
 * Закупки ОАО Газпром Авиа
*/

class loader_1_0051_05_0_00_gazpromavia extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.gazpromavia.ru/';
    public $list_link           = 'http://www.gazpromavia.ru/index.php?id=';
    public $parser_name         = 'parser_1_0051_05_0_00_gazpromavia';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0051_05_0_00_gazpromavia extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        '№'             =>  'num|clear_all',
        'Наименование'  =>  'name|clear_all',
        'начала|публикации'        =>  'date_publication|clear_all|date_convert',
        'окончания'     =>  'date_end|clear_all|date_convert'
    );

    function list_get_page( $link, $page=1 ) {
        if ($page==1) { $id = 287; }
        if ($page==2) { $id = 64; }
        if ($page==3) { $id = 65; }
        $this->loader->debug("\n\n LINK = $link$id \n\n");
        return $this->emul_br_get_body( $link.$id );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            if (is_null($item['name'])) { continue; }
            if (is_null($item['date_end'])) { continue; }
            if ($item['date_end']<date("Ymd")) { continue; }

            $item['num'] = preg_get("#№(.*)#si", $item['num']);
            $item['internal_id'] = preg_get("#/index.php\?id=(\d+)#si", $item['name_src']);

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => 3,
                'page_now'    => NULL,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_html($content);
        $table = $html->find("td#content", 0)->find("table", 0)->outertext;
        $html->clear();

        $arr = $this->parse_table($table);

        $items = $this->createstruct($arr);
       
        $ret['items_total'] = count($items);

        $ret['items'] = $items;

        return $ret;
    }
}

