<?php

/*
 * Тендеры Weatherford
*/

class loader_1_0059_01_0_00_weatherford extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.weatherford.ru/';
    public $list_link           = 'http://www.weatherford.ru/ru/purchases-and-selling/tenders';
    public $parser_name         = 'parser_1_0059_01_0_00_weatherford';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'customer',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0059_01_0_00_weatherford extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        'Тема'                          =>  'name|clear_all',
        'Дата публикации'               =>  'date_publication|clear_all|date_convert',
        'Предприятие'                   =>  'customer|clear_all',
        'Окончание сбора оферт'         =>  'date_end|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end']<date("Ymd")) { continue; }

            $detail_link = preg_get("#href=['\"](.*?)['\"]#si", $item['name_src']);

            $item['internal_id'] = abs(crc_p($detail_link));

            $item['doc'][] = array(
                'name'          =>  'Документ',
                'internal_id'   =>  $item['internal_id'],
                'detail_link'   =>  $detail_link,
            );
        
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);
        $data = $html->find("table#tenders", 0)->outertext;
        $html->clear();

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

