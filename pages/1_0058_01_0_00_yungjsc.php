<?php

/*
 * Тендеры ООО "РН-Юганскнефтегаз". Не производственне услуги
*/

class loader_1_0058_01_0_00_yungjsc extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://tender.yungjsc.com/';
    public $list_link           = 'http://tender.yungjsc.com/uslugi_tek.html';
    public $parser_name         = 'parser_1_0058_01_0_00_yungjsc';

    public $fields_list = array(
            'num'           =>  'maybenull',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'detail_link_prefix',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0058_01_0_00_yungjsc extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_name'              =>  'name|clear_all',
            'P_date'              =>  'date|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
           
            $item['date_publication'] = $this->text_date_convert(preg_get("#с\s(\d{2}.\d{2}.\d{4})#sui", $item['date']));
            $item['date_end'] = $this->text_date_convert(preg_get("#по\s(\d{2}.\d{2}.\d{4})#sui", $item['date']));

            $item['detail_link_prefix'] = preg_get("#href=\"(.*?)\"#si", $item['name_src']);
            $item['internal_id']    = abs(crc_p($item['detail_link_prefix']));

            $item['num'] = $this->text_clear_all(preg_get("#лот № (.*)#siu", $item['name_src']));

            if ($item['date_end'] < date("Ymd")) {
                continue;
            }

            $items[$k] = $item;
        }
        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $tbl = preg_get("#<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\" width=\"100%\">.*?</table>#si", $content);

        $arr  = $this->parse_table($tbl);

        foreach ($arr as $row) {
            if (isset($row[1])) {
                $items[] = array(
                        'P_name'    =>  preg_get("#<a.*?>.*?</a>#si", $row[1]),
                        'P_date'    =>  preg_get("#<span class=\"footer\">(.*?)</span>#si", $row[1]),
                );
            }
        }

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

