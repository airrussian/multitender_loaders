<?php
/**
 * Олимп строй
 * 2010.08.02 - написание
 * 2010.08.10 - пока лишь return.html
 * 2010.08.26 - доработка
 * 2010.09.06 - навигация по страницам
 */
class loader_1_0024_01_0_00_olimstroy extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.sc-os.ru/';
    public $list_link           = 'http://www.sc-os.ru/ru/contest/index.php?&from2_20=';
    public $parser_name         = 'parser_1_0024_01_0_00_olimstroy';
    public $parser_name_detail  = 'parser_1_0024_01_0_00_olimstroy_detail';


    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function test_detail($id=334) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->detail_all($id);
    }


}

class parser_1_0024_01_0_00_olimstroy extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'P_name'        => 'name|clear_all',
            'P_date_pub'    => 'date_publication|date_convert',
            'P_date_end'    => 'date_end|date_convert',           
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $this->loader->debug("\n\nLINK = $link$page \n\n");
        return $this->emul_br_get_body( $link.$page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_end'] = $this->text_date_convert(preg_get("#(\d{2}\.\d{2}\.\d{4})#sui", $item['date_end_src']));
            $item['name'] = trim(preg_replace("#\d+.#", "", $item['name']));

            // Если дата окончания пуста, то это соответствует, что дата приема заявок окочена, поэтому тендер старый и его пропускаем
            if (empty($item['date_end'])) {
                continue;
            }

            $item['internal_id'] = preg_get("#id_20=(\d+)#si", $item['name_src']);

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div.item");

        $arr = array(); $i=0;
        foreach ($tenders as $tender) {
            $ten = $tender->parent()->next_sibling();
            if ($ten->find("div.nnn", 0)) {
                $arr[$i]['P_date_pub']  = $tender->find("div.tender_current", 0)->innertext;
                $arr[$i]['P_name']      = $tender->find("a", 0)->outertext;
                $arr[$i]['P_desc']      = $tender->find("div", 1)->innertext;    
                $arr[$i]['P_date_end']  = $ten->find("div.nnn", 0)->find("div", 0)->outertext;
                $i++;
            }
        }

        $links = $content_dom->find("div.sub-links", 0);
        if (!preg_match("#print#si", $links->innertext)) {
            $ret['page_now'] = preg_get("#\d+#si", preg_replace("#<.*?>.*?</.*?>#si", "", $links->innertext));
            $ret['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#si", $links->innertext));
        } else {
            $ret['page_now'] = 1;
            $ret['page_total'] = 1;
        }

        $ret['items'] = $arr;

        return $ret;
    }
}

class parser_1_0024_01_0_00_olimstroy_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
        'Организатор отбора'                 => 'customer',
        'Местонахождение и почтовый адрес'   => 'customer_address',
    );
    
    function detail_all($id) {
        $link         = 'http://www.sc-os.ru/ru/contest/current_competitions/?id_20='.$id;

        $this->loader->debug($link);

        $content      = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);

        //$return = $this->detail_sort_3($parse_detail);

        $return['db']['customer'] = 'ГК «Олимпстрой»';
        $return['html'] = $parse_detail['html'];

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);
        
        $detail = $detail_dom->find("td.inner", 0);       

        foreach ($detail->find("p") as $p) {
            if ($strong = $p->find("strong", 0)) {
                $name  = $this->text_clear_all($strong->innertext);
                $value = preg_get("#</strong>(.*)#si", $p->innertext);
                $return[$name] = $value;
            }
        }

        $return['html'] = $this->text_clear_html_gently($detail->innertext);
        $return['html'] = preg_replace(array('#<a.*?>#i','#</a>#i'), '', $return['html']);

        $detail_dom->__destruct();

        return $return;
    }

    function text_clear_html_gently($string) {
        $string = preg_replace(array('#(id|class|style)=".*?"#ui','#</?(font|span).*?>#ui','/<!--.*?-->/sui'), '', $string);
        $string = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $string);
        $string = tidy_repair_string($string, $this->tidy_config);
        return $string;
    }

}
