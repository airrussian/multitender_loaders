<?php
/**
 * Извещение о проведении конкурсов и иные объявления о закупках
 * Написание: 22.07.2010
 */
class loader_1_0013_01_0_00_WTE extends loader_1_0000_02_0_00_temp_many {
    public $base_url    = 'http://www.w-t-e.ru/';
        
    protected $pages_list = array(
            array(
            // Список объявленных запросов (активные)
                'link' => '?inc=searching&pages=',
                'parser_name' => 'parser_1_0013_01_0_00_WTE',
            ),
            array(
            // Открытые конкурсы
                'link' => '?inc=search_tender&pages=',
                'parser_name' => 'parser_1_0013_01_0_00_WTE_k',
            ),
    );


    public $fields_list = array(
            'name',
            'date_publication',
            'price'             => 'maybenull',
            'internal_id',
            'detail_link',
            'customer'          => 'maybenull',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;
}

class parser_1_0013_01_0_00_WTE extends parser_1_0000_02_0_00_temp_many {

    protected $colomn = array(
            'Наименование'      => 'name|clear_all',
            'Цена за единицу'   => 'price|clear_all|to_price',
            'Дата внесения'     => 'date_publication|clear_all|date_convert_text_full',
            'Место расположение'=> 'region_id|clear_all',
            'Поставщик'         => 'customer|clear_all',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if (!preg_match("#россия#iu", $item['region_id'])) {
                continue;
            }

            $item['detail_link'] = preg_get("#href=\"(.*?)\"#si", $item['name_src']);
            $item['internal_id'] = abs(crc_p($item['detail_link'].$item['name']));

            $item['region_id'] = preg_get("#.*?\,.*?\,(.*)#si", $item['region_id']);

            $item['region_id'] = $this->loader->geocoder_auto($item['region_id']);

            // Отсев старья - 60 дней
            $olddate = (int) date("Ymd", time()-60*24*60*60);
            if ($item['date_publication'] < $olddate) {
                continue;
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find('table.TablePlace', 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $page = $ten->parent()->parent()->parent()->prev_sibling();

        $ret['page_total'] = (int) max(preg_get_all("#<a.*?>(\d+)</a>#sui", $page->innertext));

        $ret['page_now'] = (int) Ceil($ret['items'][1]['№'] / 10);


        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0013_01_0_00_WTE_k extends parser_1_0000_02_0_00_temp_many {

    protected $colomn = array(
            'Предмет закупки'       => 'name|clear_all',
            'Сумма'                 => 'price|clear_all|to_price',
            'Дата публикации'       => 'date_publication|clear_all|date_convert_text_full',
            'Дата процедуры'        => 'date_conf|clear_all|date_convert_text_full',
            'Расположение'          => 'region_id|clear_all',
            'Заказчик'              => 'customer|clear_all',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if (!preg_match("#россия#iu", $item['region_id'])) {
                continue;
            }
        
            $item['detail_link'] = preg_get("#href=\"(.*?)\"#si", $item['name_src']);
            $item['internal_id'] = abs(crc_p($item['detail_link'].$item['name']));

            $item['region_id'] = preg_get("#.*?\,.*?\,(.*)#si", $item['region_id']);

            $item['region_id'] = $this->loader->geocoder_auto($item['region_id']);

            $olddate = (int) date("Ymd");
            if ($item['date_conf'] < $olddate) {
                continue;
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find('table.TablePlace', 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $page = $ten->parent()->parent()->parent()->prev_sibling();

        $ret['page_total'] = (int) max(preg_get_all("#<a.*?>(\d+)</a>#sui", $page->innertext));

        $ret['page_now'] = (int) Ceil($ret['items'][1]['№'] / 10);

        $content_dom->__destruct();

        return $ret;
    }
}
