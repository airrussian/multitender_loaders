<?php
/**
 * @version > Мурманская область (гос.заказ)
 * @var     > http://gz-murman.ru/
 * @param   > run_list, run_detail
 * @todo    > 09.11.2010 - новая площадка для гос.заказа Мурманской области. Старая осталась для Муниципального заказа [5653]
 * @author  > airrussia@mail.ru
 */

class loader_1_1053_01_0_00_murmansk extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://gz-murman.ru/';
    public $list_link           = 'http://gz-murman.ru/gzw/list.aspx?mode=orders';
    public $parser_name         = 'parser_1_1053_01_0_00_murmansk';
    public $parser_name_detail  = 'parser_1_1053_01_0_00_murmansk_detail';

    public $fields_list = array(
            'num',
            'name',
            'customer',
            'type',
            'internal_id',
            'price'         => 'maybenull',
            'date_end',
            'date_conf'     => 'maybenull',
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_1053_01_0_00_murmansk extends parser_1_0000_01_0_00_one {

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");

        if ($page == 1) {
            $emul_br = $this->emul_br_init($link);
            $emul_br->exec();
            $this->loader->debug($emul_br->GetAllHeaders());
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }
        $emul_br = $this->emul_br_init($link);
        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
                'tbcount'   => $page,
        ));
        $emul_br->exec();
        $this->loader->debug($emul_br->GetAllHeaders());
        $this->asp_clear_params();
        $this->asp_set_params($emul_br);
        return $emul_br->GetBody();
    }

    protected $colomn = array(
            'Номер закупки'     => 'num|clear_all',
            'Предмет закупки'   => 'name',
            'Способ'            => 'type|clear_all',
            'Заказчик'          => 'customer|clear_all',
            'цена'              => 'price|clear_all|to_price',
            'окончания срока подачи котировочных'   => 'date_end_zk|clear_all|date_convert',
            'окончания срока подачи документов'     => 'date_end_dc',
            'вскрытия конвертов'                    => 'date_end_cf|clear_all|date_convert',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            if (is_null($item['date_end_zk']) && is_null($item['date_end_cf'])) {
                $item['date_end']   = $this->text_date_convert(preg_get("#1.*?<span.*?<i>(.*?)</i></span>#", $item['date_end_dc']));
                $item['date_conf']  = $this->text_date_convert(preg_get("#2.*?<span.*?<i>(.*?)</i></span>#", $item['date_end_dc']));
            } else {
                if (is_null($item['date_end_zk'])) {
                    $item['date_end']   = $item['date_end_cf'];
                }
                if (is_null($item['date_end_cf'])) {
                    $item['date_end']   = $item['date_end_zk'];
                }
            }
            $item['internal_id'] = preg_get("#href=['\"]order\.aspx\?link=(\d+)['\"]#si", $item['name']);
            $item['name'] = preg_get("#<span id=['\"]dataListOrders__.*?_lblName['\"]>(.*?)</span>#si", $item['name']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table#dataListOrders", 0)->outertext;

        $arr = $this->parse_table($tenders);

        foreach ($arr[0] as &$hc) {
            $hc = preg_get("#<span.*?>(.*?)</span>#si", $hc);
            $hc = $this->text_clear_all($hc);
            $hc = preg_replace("#\-\s#si", "", $hc);
        }

        $ret['items'] = $this->createstruct($arr);

        $ret['page_now'] = 1;
        $ret['page_total'] = 1;
        $ret['items_total'] = count($ret['items']);

        $content_dom->__destruct();

        return $ret;
    }
}

// FIX ME...
class parser_1_1053_01_0_00_murmansk_detail extends parser_1_0000_01_0_00_one {

    function detail_all($id) {
        $link         = "http://gz-murman.ru/gzw/order.aspx?link=";
        $this->loader->debug($link . $id);
        $content      = $this->emul_br_get_body($link . $id);
        $return['html'] = $content;
        $return['docs'] = $this->list_parse_pre($content);
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);
        $docs = array();
        if ($detail_dom->find("div#panelPbFiles",0)) {
            $doc_link = $detail_dom->find("div#panelPbFiles",0);
            foreach ($doc_link->find("input") as $input) {
                $N = (int) preg_get("#pbfiles(\d+)#si", $input->getAttribute('name'));
                if (!$N) { continue; }
                $link = $input->getAttribute('value');
                $name = $doc_link->find("a#pbfilesdoc$N", 0)->innertext;
                $pcrc = abs(crc_p($link.$name));
                $docs[] = array(
                    'detail_link'   =>  $link,
                    'name'          =>  $name,
                    'internal_id'   =>  $pcrc,
                );

            }
        }
        
        return $docs;
    }
}