<?php
/**
 * Банк Открытие
 * Написание: 24.12.2010
 */
//class loader_1_0038_01_0_00_openbank extends loader_1_0000_02_0_00_temp {
class loader_1_0038_01_0_00_openbank extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.openbank.ru/';
    public $list_link           = 'http://www.openbank.ru/ru/about/tender_committee/';
    public $parser_name         = 'parser_1_0038_01_0_00_openbank';
    public $parser_name_detail  = 'parser_1_0038_01_0_00_openbank_detail';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

//class parser_1_0038_01_0_00_openbank extends parser_1_0000_02_0_00_temp {
class parser_1_0038_01_0_00_openbank extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_name'            => 'name|clear_all',
            'P_date_publication'=> 'date_publication|date_convert_short',
    );

    function list_get_page( $link ) {
        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#id27=(\d+)#si", $item['name_src']);

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);
        $tenders = $content_dom->find("div.news_second", 0);        

        $items=array();
        foreach ($tenders->find("table") as $tender) {
            $items[] = array(
                'P_date_publication'    => $tender->find("td", 0)->find("div.date", 0)->innertext,
                'P_name'                => $tender->find("td", 1)->find("a.title", 0)->outertext,
            );
        }

        $ret['items'] = $items;
        
        $content_dom->__destruct();

        return $ret;
    }
}

/*
class parser_1_0038_01_0_00_openbank_detail extends parser_1_0000_02_0_00_temp {

    public $item_now;

    protected $detail_link = '';

    function detail_get($id) {
        $link = "http://www.openbank.ru/ru/about/tender_committee/index.php?id27=" . $id;
        $this->loader->debug("LINK = $link");

        $emul_br = $this->emul_br_init( $link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);
        $html = $content_dom->find("div.body_sec", 0)->find("div.item_box", 0);
        $docs = $html->find("a.adoc", 0)->outertext;
        $content_dom->clear();

        $return['docs'][0]['detail_link'] = preg_get("#href=['\"](.*?)['\"]#si", $docs);
        $return['docs'][0]['name'] = preg_get("#<a.*?>(.*?)</a>#si", $docs);
        $return['docs'][0]['internal_id'] = preg_get("#tender/(\d+).doc#si", $return['docs'][0]['detail_link']);

        return $return;
    }
}
*/