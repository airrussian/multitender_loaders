<?php

class loader_1_0046_01_0_00_rs extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.rsb.ru/';
    public $list_link           = 'http://www.rsb.ru/about/tenders/';
    public $parser_name         = 'parser_1_0046_01_0_00_rs';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_end',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

}

class parser_1_0046_01_0_00_rs extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_name'                => 'name|clear_all',           
            'P_date_end'            => 'date_end|clear_all|date_convert_text_full',
            'P_internal_id'         => 'internal_id',
            'P_docs'                => 'doc'
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            if ($item['date_end'] < date("Ymd")) {
                continue;
            }
            $item['doc'] = $item['doc_src'];
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div#main_content", 0);

        foreach ($tenders->find("div.event_item") as $item) {
            foreach ($item->find("ul", 0)->find("li") as $li) {
                $docs[] = array(
                    'name'          =>  $li->find("a", 0)->innertext,
                    'detail_link'   =>  $li->find("a", 0)->getAttribute("href"),
                    'internal_id'   =>  abs(crc_p($li->find("a", 0)->getAttribute("href"))),
                    'ext'           =>  preg_get("#\.([a-z0-9]+)$#is", $li->find("a", 0)->getAttribute("href"))
                );
            }

            $arr[] = array(
                'P_name'        =>  $item->find("p", 0)->find("b", 0)->innertext,
                'P_date_end'    =>  $item->find("p", 1)->find("b", 0)->innertext,
                'P_docs'        =>  $docs,
                'P_internal_id' =>  $docs[0]['internal_id']
            );
        }       

        $ret['page_now']    = 1;
        $ret['items_total'] = count($arr);
        $ret['page_total']  = 1;

        $ret['items'] = $arr;

        $content_dom->__destruct();

        return $ret;
    }
}
