<?php

/*
 * ОАО «ПО «Севмаш»
*/

class loader_1_0066_01_0_00_sevmash extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.sevmash.ru/';
    public $list_link           = 'http://www.sevmash.ru/rus/tenders.html';
    public $parser_name         = 'parser_1_0066_01_0_00_sevmash';
    public $parser_name_detail  = 'parser_1_0066_01_0_00_sevmash_detail';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'detail_link_prefix',

    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0066_01_0_00_sevmash extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Заголовок материала' =>  'name|clear_all',
        'Дата'                =>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['detail_link_prefix']    = preg_get("#href=['\"]/rus/tenders/(.+?)['\"]#si", $item['name_src']);
            $item['internal_id']    = abs(crc_p($item['detail_link_prefix']));
            
            $items[$k] = $item;
        }       

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {
        //$content = $this->text_from_win($content);

        $html = str_get_dom($content);

        $table = $html->find("table.contentpane", 0)->find("form", 0)->find("table", 0);

        $tarr = $this->parse_table($table->outertext);
        $arr = array();
        foreach ($tarr as $row) {
            if (count($row)==3) {
                $arr[] = $row;
            }
        }
        $items = $this->createstruct($arr);
        
        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;
        $ret['items'] = $items;

        return $ret;
    }
}
