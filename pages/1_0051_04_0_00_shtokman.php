<?php

/*
 * Закупки Штокмана
*/

class loader_1_0051_04_0_00_shtokman extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.shtokman.ru/';
    public $list_link           = 'http://www.shtokman.ru/procurement/published/';
    public $parser_name         = 'parser_1_0051_04_0_00_shtokman';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0051_04_0_00_shtokman extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Способ закупки'        =>  'type|clear_all',
        'Номер'                 =>  'num|clear_all',
        'Предмет закупки'       =>  'name|clear_all',
        'Прием заявок'          =>  'date_end|clear_all|date_convert_search'
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            if ($item['Статус']=='прием заявок завершен') { continue; }

            $item = $this->list_set_colomn($item, $this->colomn);
            
            $item['internal_id'] = preg_get("#/procurement/published/(\d+)/#si", $item['name_src']);

            $items[$k] = $item;
        }       

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);

        $data = $html->find("table.data", 0)->outertext;

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

