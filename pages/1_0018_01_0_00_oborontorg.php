<?php
/**
 * Торговая Система "ОБОРОНТОРГ"
 * 2010.08.10 - Запуск
 */
class loader_1_0018_01_0_00_oborontorg extends loader_1_0006_01_0_00_fabrikant {
    public $base_url  = 'http://www.oborontorg.ru/market/?action=';

    public $parser_name        = 'parser_1_0018_01_0_00_oborontorg';
    public $parser_name_detail = 'parser_1_0018_01_0_00_oborontorg_detail';

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    function run_test() {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = &$this;

        $this->parser->test();
        exit;
    }

    protected $pages_list = array(
            array(
            // Список действующих ПДО покупателя
                            'link' => 'list_public_pdo&type=1060&status_group=sg_active&from=',
                            'type' => 'Список действующих ПДО',
            ),
            array(
            // Список действующих ПДО продавца
                            'link' => 'list_public_pdo&type=1061&status_group=sg_active&from=',
                            'type' => 'Список действующих ПДО',
            ),
            array(
            // Аукционы покупателя на понижение цены (объявленые)
                            'link' => 'list_public_auctions&type=1020&status_group=sg_published&from=',
                            'type' => 'Аукционы покупателя на понижение цены',
            ),
            array(
            // Аукционы покупателя на понижение цены (активные)
                            'link' => 'list_public_auctions&type=1020&status_group=sg_activefrom=',
                            'type' => 'Аукционы покупателя на понижение цены',
            ),
            array(
            // Аукционы продавца на повышение цены (объявленые)
                            'link' => 'list_public_auctions&type=1021&status_group=sg_published&from=',
                            'type' => 'Аукционы продавца на повышение цены',
            ),
            array(
            // Аукционы продавца на повышение цены (активные)
                            'link' => 'list_public_auctions&type=1021&status_group=sg_activefrom=',
                            'type' => 'Аукционы продавца на повышение цены',
            ),
            array(
            // Запросы котировок цен покупателя (объявленых)
                            'link' => 'list_public_auctions&type=1560&status_group=sg_published&from=',
                            'type' => 'Запрос котировок',
            ),
            array(
            // Запросы котировок цен покупателя (действующих)
                            'link' => 'list_public_auctions&type=1560&status_group=sg_active&from=',
                            'type' => 'Запрос котировок',
            ),
            array(
            // Конкурсы покупателя (объявленные)
                            'link' => 'list_public_tenders&type=1120&status_group=sg_published&from=',
                            'type' => 'Конкурс',
            ),
            array(
            // Конкурсы покупателя (действующие)
                            'link' => 'list_public_tenders&type=1120&status_group=sg_active',
                            'type' => 'Конкурс',
            ),
    );
}

class parser_1_0018_01_0_00_oborontorg extends parser_1_0006_01_0_00_fabrikant {

}

class parser_1_0018_01_0_00_oborontorg_detail extends parser_1_0000_02_0_00_temp_many {
    protected $detail_link = 'http://www.oborontorg.ru/market/view.html?id=';
    protected $detail_base = 'http://www.oborontorg.ru';

    public $no_curl = true;


    public $detail_sort = array(
            'Дата публикации'       => 'date_publication|date_convert',
            'Организатор процедуры' => 'region_id|region_convert',
    );

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $this->detail_link = "http://www.oborontorg.ru" . $this->item_now['detail_link'];

        $this->loader->debug("detail id = $id");        
        $this->loader->debug("link = " . $this->detail_link);

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());
        
        return $content;
    }

    function test() {
        $this->detail_all(34335);
    }


    function detail_all($id) {
        $content = $this->detail_get($id);

        if (!$content) {
            return false;
        }

        $parse_detail = $this->list_parse_pre($content);

        $return = $this->detail_sort_2($parse_detail);

        $i=0;
        foreach ($parse_detail as $key => $val) {
            if (preg_match("#Дополнительная информация|Файл с описанием#sui", $key)) {
                $docs = preg_get_all("#<a.*?>.*?</a>#sui", $val);
                foreach($docs as $doc) {
                    $return['docs'][$i]['name'] = $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#sui", $doc));
                    $return['docs'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"]#", $doc);
                    $return['docs'][$i]['internal_id'] = abs(crc_p($return['docs'][$i]['detail_link']));
                    $i++;
                }
            }
        }
        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tbl = $content_dom->find("table.blank", 0);

        $arr = array();

        $arr['content'] = $this->detail_content_cache($content, $this->detail_link);

        foreach ($tbl->find("tr") as $tr) {

            if ($tr->attr['class']!='thead') {
                $nam_r = $tr->find("td", 0)->innertext;
                $nam_r = preg_replace("#:#sui", "", $nam_r);

                $val_r = $tr->find("td", 1)->innertext;

                if ($nam_r) {
                    $arr[$nam_r] = $val_r;
                }
            }

        }
        $content_dom->__destruct();

        return $arr;
    }


    function region_convert($text) {
        return $this->loader->geocoder_auto($text);
    }
}
