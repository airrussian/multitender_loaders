<?php
/**
 * Пологаю открытый конкурс
 * Последние объявления о тендерах УЗБЕКИСТАН
 */

class loader_1_5001_01_0_00_xariduz extends loader_1_0000_02_0_00_temp {

    public $base_url    = 'http://xarid.uz/';
    public $list_link   = 'http://xarid.uz/index.php?option=com_aktender&view=aktender&layout=list&Itemid=62&lang=ru&limitstart=';
    public $parser_name = 'parser_1_5001_01_0_00_xariduz';
    public $parser_name_detail = 'parser_1_5001_01_0_00_xariduz_detail';
    
    public $fields_list = array(
            'internal_id',
            'name',
            'date_end',
   );

    public $fields_rewrite = array(
            'type'         => 'Открытый конкурс',
            'type_id'      => 2,
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = false;

}

/**
 * LIST
 */
class parser_1_5001_01_0_00_xariduz extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
        'Наименование тендера'      => 'name|clear_all',
        'Дата окончания тендера'    => 'date_end|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        return parent::list_get_page($link, ($page-1)*15);
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = preg_get("#cid\[0\]=(\d+)#si", $item['name_src']);
            $item['date_end'] = (int) str_replace("-", "", $item['date_end']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $table = $content_dom->find("table.adminlist", 0);
        $arr = $this->parse_table($table->outertext);
        $paginator = $table->find("tfoot", 0)->find("td", 0)->innertext;

        $page_now = preg_get("#<span.*?>(\d+)</span>#si", $paginator);
        $page_total = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paginator));

        $content_dom->clear();
        
        $items = $this->createstruct($arr);

        $return = array(
                'page_total'  => $page_total,
                'page_now'    => $page_now,
                'items_total' => count($items),
                'items'       => $items,
        );
        return $return;

    }
}

class parser_1_5001_01_0_00_xariduz_detail extends parser_1_0000_02_0_00_temp {

    protected $detail_link = 'http://xarid.uz/index.php?option=com_aktender&task=view&cid[0]=';

    public $detail_sort = array(
        'Название организации'              => 'customer|clear_all',
        'Адрес организации'                 => 'customer_address|clear_all',
        'Телефон'                           => 'customer_phone|clear_all',
        'Дата начало'                       => 'date_publication|clear_all',
        'Дата окончания тендера'            => 'date_end|clear_all',
    );

    function detail_get($id) {
        $this->loader->debug("link detail: {$this->detail_link}$id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $return = $this->detail_sort_3($parse, true);
        $return['db']['date_publication'] = (int) str_replace("-", "", $return['db']['date_publication']);
        $return['db']['date_end'] = (int) str_replace("-", "", $return['db']['date_end']);

        return $return;
    }

    function detail_parse_pre($content) {

        $content_dom = str_get_html($content);
        $table = $content_dom->find("table.admintable", 0)->outertext;

        $arr = $this->parse_table($table);

        $detail = array();
        foreach ($arr as $val) {
            $key = $this->text_clear_all($val[0]);
            $key = str_replace(":", "", $key);
            $detail[$key] = $val[1];
        }

        $content_dom->clear();

        return $detail;
    }

}
