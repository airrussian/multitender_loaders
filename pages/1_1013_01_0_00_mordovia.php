<?php
/**
 * @version > Республика Мордовия (гос.заказ)
 * @var     > http://goszakaz.e-mordovia.ru/
 * @param   > run_list, run_detail
 * @todo    > 08.11.2010 - новая площадка Республики Мордовия
 * @author  > airrussia@mail.ru
 */

class loader_1_1013_01_0_00_mordovia extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://goszakaz.e-mordovia.ru/';
    public $list_link           = 'http://goszakaz.e-mordovia.ru/bid/listbid?sort=DateAdvert&sort_dir=desc&off=';
    public $parser_name         = 'parser_1_1013_01_0_00_mordovia';
    public $parser_name_detail  = 'parser_1_1013_01_0_00_mordovia_detail';

    public $fields_list = array(
            'num',
            'name',
            'date_publication',
            'customer',
            'type',
            'internal_id',
    );
    
    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_1013_01_0_00_mordovia extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Номер'             => 'num|clear_all',
            'Наименование'      => 'name|clear_all',
            'Способ'            => 'type|clear_all',
            'Дата публикации'   => 'date_publication|clear_all|date_convert',
            'Организатор'       => 'customer|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $page = ($page-1)*50;
        $this->loader->debug( $link . $page );
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = preg_get("#<a.*?href=['\"]/bid/viewbid/(\d+)['\"]>.*?</a>#si", $item['num_src']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table#ttab1", 0)->outertext;

        $tenders = preg_replace("#<tr class=['\"]line['\"].*?tr>#si", "", $tenders);

        $arr = $this->parse_table($tenders);

        $ret['items'] = $this->createstruct($arr);

        $pager = $content_dom->find("div#i-navigator", 0);
        $ret['page_now'] = preg_get("#\d+#", $pager->find("span.nav-path-cur", 0)->innertext);
        $ret['page_total'] = Ceil(max(preg_get_all("#<a.*?off=(\d+)#si", $pager->innertext))/50);
        $ret['items_total'] = count($ret['items']);

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_1013_01_0_00_mordovia_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
        "Дата и время окончания подачи"     => "date_end|date_convert",
        "Дата и время вскрытия конвертов"   => "date_conf|date_convert",
    );

    function detail_all($id) {
        $link         = "http://goszakaz.e-mordovia.ru/bid/viewbid/";
        $content      = $this->emul_br_get_body($link . $id);
        $parse_detail = $this->list_parse_pre($content);
        $return = $this->detail_sort_3($parse_detail);
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $det = $detail_dom->find("td.MainCVS",0);

        $other = $det->find("table.list", 0);
        $other = $this->parse_table($other->outertext);
        foreach ($other as $row) {
            $result[$this->text_clear_all($row[0])] = $this->text_clear_all($row[1]);
        }
        return $result;
    }
}