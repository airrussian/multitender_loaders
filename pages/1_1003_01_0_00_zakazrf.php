<?php
/**
 * http://etp.zakazrf.ru/Reductions.aspx?stage=1
 * c 2.09.2010 - новая площадка
 * @author airrussian@mail.ru
 */

class loader_1_1003_01_0_00_zakazrf extends loader_1_1000_01_0_00_zakupki {
    public $base_url           = 'http://etp.zakazrf.ru/';
    public $list_link          = 'http://etp.zakazrf.ru/Reductions.aspx?stage=1';
    public $parser_name        = 'parser_1_1003_01_0_00_zakazrf';
    public $parser_name_detail = 'parser_1_1003_01_0_00_zakazrf_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'customer'         => 'maybenull',
            'price'            => 'maybenull',
            'date_publication',
            'date_conf',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; // совершенно не ясна сортировка
    public $item_rewrite  = true;

    protected $run_detail_order = 'date_publication ASC';

    public $page_total_rewrite = false;

    public $page_last = 200;

    public $sleep_list = 7;

    function run_detail_query(&$obj) {
        if(!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }
}

class parser_1_1003_01_0_00_zakazrf extends parser_1_0000_01_0_00_one {
    protected $colomn = array(
            0                   => 'detail_link',
            'Номер'             => 'num',
            'Наименование'      => 'name|clear_all_dots',
            'цена'              => 'price|to_price',
            'Организатор ЭА'    => 'customer|clear_all',
            'время публикации'  => 'date_publication|date_convert_short',
            'Дата начала торгов'=> 'date_conf|date_convert_short',
            'Статус'            => 'status|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");

        if ($page == 1) {
            $emul_br = $this->emul_br_init($link);
            $emul_br->exec();
            $this->loader->debug($emul_br->GetAllHeaders());
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $emul_br = $this->emul_br_init($link);

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
           'ctl00$Content$ScriptManager' => 'ctl00$Content$UpdatePanel|ctl00$Content$TabControlButton',
           'Tab_ID' => '1',
           'ctl00$Content$TabControlButton' => '',
        ));
        $emul_br->exec();
        $this->loader->debug($emul_br->GetAllHeaders());
        $this->asp_clear_params();
        $this->asp_set_params($emul_br);

        $emul_br = $this->emul_br_init($link);
        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);

        if ( $page > 10 && $page%10 == 1 ) {
            $target = 'ctl00$Content$Pager$NextPageButton';
        } else {
            $newpage = $page%10;
            $newpage = $newpage ? $newpage : 10; // 20 => 10
            $target = 'ctl00$Content$Pager$Page' . $newpage . 'Button';
        }

        $emul_br->SetPost(array(
                '__EVENTTARGET'   => $target,
                '__EVENTARGUMENT' => '',
        ));

        $this->loader->debug("TARGET = $target");

        // FIXME распространить и на другие сайты...
        for ($try = 2; $try <= 5; $try++) {
            $emul_br->exec();
            if ($emul_br->GetBody()) {
                break;
            }
            $this->loader->debug("TRY $try step, SLEEP {$this->loader->sleep_list} sec...");
            sleep( $this->loader->sleep_list );
        }

        if ($emul_br->GetBody() && $page%10==1) {
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $this->loader->debug($emul_br->GetAllHeaders());

        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        //print_r($parse);
        //exit(0);
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#<a.*\?id=(\d+)#i", $item['detail_link']);

            // FIXME временный костыль от мусора который на площадке, надеюсь что уберут в скором будущем
            if (preg_match("#\(демонстрационный\)#sui", $item['name'])) { continue; }
            if (preg_match("#для Елены#sui", $item['name'])) { continue; }

            $items[] = $item;
        }

        $return = array (
                'page_total'  => ceil($parse['item_total']/sizeof($items)),
                'items_total' => $parse['item_total'],
                'items'       => $items,
        );

        return $return;
    }


    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $return['item_total'] = preg_get('/id="ctl00_Content_RecordsCountLabel">(\d+)</i', $content);

        // Парсинг таблицы
        $table = $content_dom->find('table.reporttable',0);
        $arr   = $this->parse_table($table->outertext);

        $return['items'] = $this->createstruct($arr);

        $content_dom->clear();

        return $return;
    }

}

class parser_1_1003_01_0_00_zakazrf_detail extends parser_1_1000_01_0_00_zakupki_detail {

    function detail_all($id) {

        $num = $this->item_now['num'];

        $internal_id = $this->item_now['internal_id'];

        $id = $this->loader->GetIDbyNUM($num);
        if ($id) {
            $this->loader->debug("ZAKUPKI ID = $id");
            $ret = parent::detail_all($id);
            $this->loader->detail_link = "http://etp.zakazrf.ru/ViewReductionPrint.aspx?id=$internal_id";
            $ret['content'] = $this->detail_content_cache($this->emul_br_get_body($this->loader->detail_link));
        } else {
            $this->loader->debug("NOT ITEM WITH $num ON ZAKUPKI ");
            $this->loader->NoItem($this->item_now['id']);
            $ret = false;
        }

        return $ret;
    }


}
