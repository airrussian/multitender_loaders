<?php

/*
 * Конкурсы "СлавНефть"
*/

class loader_1_0054_01_0_00_saratovneftegaz extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.sng.ru/';
    public $list_link           = 'http://www.sng.ru/tender';
    public $parser_name         = 'parser_1_0054_01_0_00_saratovneftegaz';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'doc',
            'num'   =>  'maybenull'
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0054_01_0_00_saratovneftegaz extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        'Начало приема коммерческих предложений'    =>  'date_publication|clear_all|date_convert_short',
        'Окончание приема коммерческих предложений' =>  'date_end|clear_all|date_convert_short',
        'Наименование сделки и лота'                =>  'name|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $doc = $item['Тендерная документация'];

            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end'] < date("Ymd")) { continue; }
            if (is_null($item['date_end'])) { continue; }

            $detail_link = preg_get("#tender_files/(.+?)['\"]#si", $doc);
            $internal_id = abs(crc_p($detail_link));

            $item['internal_id'] = $internal_id;

            $num = preg_get("#Лот №(.*?)\s&laquo;#si", $item['name_src']);
            $item['name'] = trim(str_replace("Лот №$num", "", $item['name']));
            $item['num'] = $num;

            $item['doc'][] = array(
                'internal_id'   =>  $internal_id,
                'name'          =>  $item['name'],
                'detail_link'   =>  $detail_link,
            );

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_html($content);

        $data = $html->find("table.tblGeniral", 0)->outertext;

        $html->clear();

        $arr  = $this->parse_table($data);

        foreach ($arr as $key => $row) {
            if (count($row)==1) {
                unset($arr[$key]);
            }
        }

        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

