<?php
/**
 * http://etp.zakazrf.ru/Reductions.aspx?stage=1
 * c 2.09.2010 - новая площадка
 * @author airrussian@mail.ru
 */

class loader_1_0004_03_0_00_zakazrf3 extends loader_1_0000_02_0_00_temp {
    public $base_url           = 'http://etp.zakazrf.ru/';
    public $list_link          = 'http://etp.zakazrf.ru/Reductions.aspx?stage=1';
    public $parser_name        = 'parser_1_0004_03_0_00_zakazrf3';
    public $parser_name_detail = 'parser_1_0004_03_0_00_zakazrf3_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'customer'         => 'maybenull',
            'price'            => 'maybenull',
            'date_publication',
            'date_conf',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; // совершенно не ясна сортировка
    public $item_rewrite  = true;

    protected $run_detail_order = 'date_publication ASC';

    public $page_total_rewrite = false;

    public $page_last = 200;

    public $sleep_list = 7;

    function geocoder_zakazrf($string) {
        $this->debug('GEOCODER, input=', $string);

        $region_id = $this->geocoder_by_index_auto($string);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($string);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_ZAKAZRF, region= $region_id ($region_name)", $string);

        return $region_id ? $region_id : false;
    }

    function test_detail($id=251) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->detail_all($id);
    }

}

class parser_1_0004_03_0_00_zakazrf3 extends parser_1_0000_02_0_00_temp {
    protected $colomn = array(
            0                   => 'detail_link',
            'Номер'             => 'num',
            'Наименование'      => 'name|clear_all_dots',
            'цена'              => 'price|to_price',
            'Организатор ЭА'    => 'customer|clear_all',
            'время публикации'  => 'date_publication|date_convert_short',
            'Дата начала торгов'=> 'date_conf|date_convert_short',
            'Статус'            => 'status|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");

        if ($page == 1) {
            $emul_br = $this->emul_br_init($link);
            $emul_br->exec();
            $this->loader->debug($emul_br->GetAllHeaders());
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $emul_br = $this->emul_br_init($link);

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
           'ctl00$Content$ScriptManager' => 'ctl00$Content$UpdatePanel|ctl00$Content$TabControlButton',
           'Tab_ID' => '1',
           'ctl00$Content$TabControlButton' => '',
        ));
        $emul_br->exec();
        $this->loader->debug($emul_br->GetAllHeaders());
        $this->asp_clear_params();
        $this->asp_set_params($emul_br);

        $emul_br = $this->emul_br_init($link);
        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);

        if ( $page > 10 && $page%10 == 1 ) {
            $target = 'ctl00$Content$Pager$NextPageButton';
        } else {
            $newpage = $page%10;
            $newpage = $newpage ? $newpage : 10; // 20 => 10
            $target = 'ctl00$Content$Pager$Page' . $newpage . 'Button';
        }

        $emul_br->SetPost(array(
                '__EVENTTARGET'   => $target,
                '__EVENTARGUMENT' => '',
        ));

        $this->loader->debug("TARGET = $target");

        // FIXME распространить и на другие сайты...
        for ($try = 2; $try <= 5; $try++) {
            $emul_br->exec();
            if ($emul_br->GetBody()) {
                break;
            }
            $this->loader->debug("TRY $try step, SLEEP {$this->loader->sleep_list} sec...");
            sleep( $this->loader->sleep_list );
        }

        if ($emul_br->GetBody() && $page%10==1) {
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $this->loader->debug($emul_br->GetAllHeaders());

        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        //print_r($parse);
        //exit(0);
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#<a.*\?id=(\d+)#i", $item['detail_link']);

            // FIXME временный костыль от мусора который на площадке, надеюсь что уберут в скором будущем
            if (preg_match("#\(демонстрационный\)#sui", $item['name'])) { continue; }
            if (preg_match("#для Елены#sui", $item['name'])) { continue; }

            $items[] = $item;
        }

        $return = array (
                'page_total'  => ceil($parse['item_total']/sizeof($items)),
                'items_total' => $parse['item_total'],
                'items'       => $items,
        );

        return $return;
    }


    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $return['item_total'] = preg_get('/id="ctl00_Content_RecordsCountLabel">(\d+)</i', $content);

        // Парсинг таблицы
        $table = $content_dom->find('table.reporttable',0);
        $arr   = $this->parse_table($table->outertext);

        $return['items'] = $this->createstruct($arr);

        $content_dom->clear();

        return $return;
    }

}

class parser_1_0004_03_0_00_zakazrf3_detail extends parser_1_0004_03_0_00_zakazrf3 {

    protected $detail_link = 'http://etp.zakazrf.ru/ViewReductionPrint.aspx?id=';

     public $detail_sort = array(
        'Сведения об организаторе торгов\/Место нахождения' => 'customer_address|clear_all',
        'Наименование организации' => 'customer|clear_all',
        'Сведения об организаторе торгов\/Адрес электронной почты' => 'customer_email|clear_all',
        'Сведения об организаторе торгов\/Номера контактных телефонов' => 'customer_phone|clear_all',
    );

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $this->detail_link = 'http://etp.zakazrf.ru/ViewReductionPrint.aspx?id='.$id;

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }


    function arr_double2one(array $arr) {
        $return = array(); $i=0;
        foreach ($arr as $row) {

            if (count($row)==2) {
                $key = $this->text_clear_all($row[0]);
                $val = $row[1];
                
            } else {
                $val = $row[0];
                $key = $i++;
            }
            $return[$key] = $val;
        }
        return $return;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);        
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $doc = $parse['Документы'];
        $docs = preg_get_all("#<a target=\"_blank\" href=\".*?DFile.*?\">.*?</a>#sui", $doc);
        $return = $this->detail_sort_3($parse);
        $return['content'] = $this->detail_content_cache($content);
        $return['db']['region_id'] = $this->loader->geocoder_zakazrf($return['db']['customer_address']);
        foreach ($docs as $doc) {
            $return['docs'][] = array(
                'internal_id' => preg_get("#id=(\d+)#si", $doc),
                'name' => preg_get("#<a.*?>(.*?)</a>#si", $doc),
            );
        }
        return $return;
    }

    function detail_parse_pre($content) {
        $html = str_get_html($content);
        $CardView = $html->find("div.CardView", 0);

        foreach ($CardView->find("h2") as $h2) {
            $next = $h2->next_sibling();
            $cat = $this->text_clear_all($h2->outertext);
            $tagname = preg_get("#^<(\S+)[\s|>]#si", $next->outertext);
            if ($tagname=='table') {
                $arr = $this->parse_table($next->outertext);
                $arr = $this->arr_double2one($arr);
                foreach ($arr as $key => $val) {
                    $return[$cat."/".$key] = $val;
                }
            } else {
                $t = $h2->next_sibling();
                $return[$cat] = "";
                do {
                   $return[$cat] .= $t->outertext;
                   $t = $t->next_sibling();
                } while (preg_get("#^<(\S+)[\s|>]#si", $t->outertext)=="div");
            }
            
        }
        return $return;
    }
}
