<?php

/*
 * Тендеры ОАО НК "РуссНефть"
*/

class loader_1_0052_01_0_00_russneft extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.russneft.ru/';
    public $list_link           = 'http://www.russneft.ru/tendersrussneft/';
    public $parser_name         = 'parser_1_0052_01_0_00_russneft';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0052_01_0_00_russneft extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        '№'                             =>  'num|clear_all',
        'Дата публикации приглашения'   =>  'date_publication|clear_all|date_convert',
        'Дата окончания приема предложений' =>  'date_end|clear_all|date_convert',
        'Предмет тендера'               =>  'name|clear_all',
        'Тендерная документация'        =>  'doc',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {           

            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end']<date("Ymd")) { continue; }

            $item['internal_id'] = abs(crc_p($item['num'] . $item['name']));
            $item['name'] = str_replace("”", "", $item['name']);

            if (is_null($item['date_publication'])) {
                list($d,$m,$y) = preg_get("#^(\d{2})(\d{2})(\d{2})#", $item['num']);
                $item['date_publication'] = "20$y$m$d";
            }

            $docs = preg_get_all("#<a.*?/a>#si", $item['doc']);

            $detail_link = preg_get("#href=['\"](.*?)['\"]#si", $item['name_src']);

            $item['doc'] = array(
                '0' =>  array(
                    'detail_link'   =>  $detail_link,
                    'name'          =>  $item['name'],
                    'internal_id'   =>  abs(crc_p($detail_link)),
                ),
            );
            foreach ($docs as $doc) {
                $name = $this->text_clear_all($doc);
                $detail_link = preg_get("#href=['\"](.*?)['\"]#si", $doc);
                $item['doc'][] = array(
                    'detail_link'   => $detail_link,
                    'name'          => $name,
                    'internal_id'   => abs(crc_p($detail_link)),
                );
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_html($content);

        $data = $html->find("td.content", 0)->find("table", 0)->outertext;

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $html->clear();

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

