<?php

/**
 * @version > Закупки ГОВ.РУ
 * @var     > http://zakupnew.gov.ru/
 * @param   > run_list, run_detail
 * @author  > airrussian@mail.ru
 */
class loader_1_1000_01_0_00_zakupki extends loader_1_0000_02_0_00_temp {

    public $base_url = 'http://zakupki.gov.ru';
    public $list_link = 'http://zakupki.gov.ru/pgz/public/action/search/simple/result?sortField=publishDate&descending=true&tabName=AP&lotView=false&index=';
    public $detail_link = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=';
    public $detailhtml_link = 'http://zakupki.gov.ru/pgz/public/action/orders/info/common_info/show?notificationId=';
    public $document_list = 'http://zakupki.gov.ru/pgz/public/action/orders/info/order_document_list_info/show?notificationId=';
    public $report_link = 'http://zakupki.gov.ru/pgz/printForm?type=PROTOCOL&id=';
    public $parser_name = 'parser_1_1000_01_0_00_zakupki';
    public $parser_name_scraperwiki = 'parser_1_1000_01_0_00_zakupki_ftp_scraperwiki';
    public $parser_name_detail = 'parser_1_1000_01_0_00_zakupki_detail';
    public $parser_name_protocol = 'parser_1_1000_01_0_00_zakupki_report';
    public $many_count = 15;
    public $many_sleep = 4;
    public $fields_list = array(
        'num',
        'name',
        'date_publication',
        'date_end' => 'maybenull',
        'customer' => 'maybenull',
        'type',
        'internal_id',
        'region'    => 'maybenull',
        'price' => 'maybenull',
        'doc'   => 'maybenull'
    );
    public $break_by_pass_count = 10;
    public $sleep_list = 15;
    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite = false;

    function GetIDbyNUM($num) {
        $item_temp = new item_temp;
        $id = false;
        if ($item_temp->Load("site_id = 1100001000 AND num LIKE '$num'")) {
            $id = $item_temp->internal_id;
        }
        return $id;
    }

    function run_repair() {
        $item_temp = new item_temp();
        if (!$item_temp->Load("site_id = $this->site_id AND date_detail>'2011-09-06 23:00:00' AND date_detail<'2011-09-12 10:00:00'")) {
            exit("NO MORE");
        }
        $item_id = $item_temp->item_id;
        if ($item_id) {
            $doc = new doc();
            $this->db->Execute("DELETE FROM doc WHERE item_id=$item_id AND site_id={$this->site_id}");
            $this->run_detail_one($item_temp->id);
        }
    }

    function NoItem($id) {
        $item_temp = new item_temp;
        if ($item_temp->Load("id=$id")) {
            $item_temp->detail_run_time = NULL;
            $item_temp->Save(TRUE);
        }
    }

    function run_detail_query(&$obj) {
        if (!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
                        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }

    function run_detail() {
        $item_temp = new item_temp;

        $this->run_detail_query($item_temp);

        $item_temp->detail_count_error ++; // при успехе убавим
        $item_temp->detail_count_load ++;
        $item_temp->detail_run_time = $item_temp->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item_temp->Save();

        $this->run_detail_one($item_temp->id);

        $item_temp->Reload("id = $item_temp->id");

        if ($item_temp->item_id) {
            $item = new item;
            $item->Load("id = $item_temp->item_id");
            $item->detail_run_time = NULL; // при успехе обнулить
            $item->detail_count_error --;

            if ($item->detail_count_error < 0) {
                $item->detail_count_error = 0;
            }
            $item->Save(TRUE);
        }

        $item_temp->detail_run_time = NULL; // при успехе обнулить
        $item_temp->detail_count_error --;

        if ($item_temp->detail_count_error < 0) {
            $item_temp->detail_count_error = 0;
        }
        $item_temp->Save();

        $this->debug("RUN_DETAIL id = $item->id");
    }

    function run_report($id = NULL) {
        $item = new item();

        $date_end = $item->DB()->DBDate(time() - 3 * 24 * 3600);

        if (is_null($id)) {
// key: site_id_date_end
            if (!$item->Load("site_id = $this->site_id AND detail_count_error = 0 AND " .
                            "detail_run_time IS NULL AND date_end <= $date_end AND " .
                            "date_report IS NULL ORDER BY date_end DESC")) {
                if (!$item->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
                                "detail_run_time IS NULL AND date_end <= $date_end AND " .
                                "date_report IS NULL ORDER BY detail_count_error ASC, id DESC")) {
                    exit('NO NEW');
                }
            }
        } else {
            if (!$item->Load("site_id = $this->site_id AND internal_id = $id")) {
                $this->debug("NO MORE");
                return false;
            }
        }

        $item->detail_count_error ++; // при успехе убавим
        $item->detail_count_load ++;
        $item->detail_run_time = $item->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item->Save(TRUE);

        $this->debug("ID = $item->id");

        $this->parser = new $this->parser_name_protocol;
        $this->parser->loader = & $this;
        $this->parser->current_item = $item;

        $content = $this->parser->get($item->internal_id);
        if (!$content) {
            $this->debug("EMPTY CONTENT");
            return false;
        }
        $parse = $this->parser->parse_report($content);
        if (!$parse) {
            return false;
        }

        print_r($parse);

        $item->price_best = $parse['price_best'];
        $item->members_count = $parse['members_count'];
        $item->date_report = $item->DB()->BindTimeStamp(time());

        $item->detail_run_time = NULL; // при успехе обнулить
        $item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }
        $item->Save(TRUE);

        $this->list_insert_item_db_doc($item->id, $parse['docs']);

        return true;
    }

    function run_report_many() {
        echo "sleep 45\n";
        sleep(mt_rand(40, 50));

        for ($i = 1; $i < $this->many_count; $i++) {
            $this->run_report();
            sleep($this->many_sleep);
        }
    }

}

class parser_1_1000_01_0_00_zakupki_ftp_scraperwiki extends parser_1_0000_02_0_00_temp {
    
    protected $colomn = array(
        'num'           => 'num',
        'internal_id'   => 'internal_id',
        'name'          => 'name',
        'date_pub'      => 'date_publication|date_convert_my',
        'date_end'      => 'date_end|date_convert_my',
        'type'          => 'type',
        'region'        => 'region',
        'url'           => 'detail_link',
    );
    
    function get($id, $limit) {
        $emul_br = $this->emul_br_init('http://free-ec2.scraperwiki.com/clif4ci/exec');

        $post = array(
            'apikey' => '3e76d0ed-52ee-4224-9426-c57ae0c18ff6',
            'cmd' => "/usr/bin/php /home/projects/zakupki/select.php id=$id limit=$limit"
        );

        $emul_br->SetPost($post);
        $emul_br->exec();
        $content = $emul_br->GetBody();                
        return $content;
    }

}

class parser_1_1000_01_0_00_zakupki extends parser_1_0000_01_0_00_one {

//public $proxy      = 'voip.telekom.org.ru:8090';

    protected $colomn = array(
        'P_internal_id' => 'internal_id',
        'P_name' => 'name|clear_all',
        'P_date_publication' => 'date_publication|clear_all|date_convert',
        'P_price' => 'price|clear_all|to_price',
    );

    function list_get_page($link, $page = 1) {
        sleep(30);
        $emul_br = $this->emul_br_init($link . $page);
        $content = $emul_br->exec();
        if (preg_match("#HTTP/1.1 302 Moved Temporarily#si", $content)) {
            $this->loader->debug($emul_br->GetAllHeaders());
            $u = preg_get("#Location: (.+)#si", $content);
            $this->loader->debug("MOVED TO = " . $this->loader->base_url . $u);
            $emul_br = $this->emul_br_init($this->loader->base_url . $u);
            $content = $emul_br->exec();
        }
        $this->loader->debug($emul_br->GetAllHeaders());
        return $content;
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach ($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = preg_get("#notificationId=(\d+)#si", $item['internal_id']);

            if (empty($item['internal_id'])) {
                continue;
            }

            $html = str_get_dom($item['name_src']);

            $item['num'] = $this->text_clear_all($html->find("a.iceCmdLnk", 0)->innertext);
            $item['type'] = $this->text_clear_all($html->find("span.blueBold", 0)->innertext);
            $item['name'] = $this->text_clear_all($html->find("a.iceOutLnk", 0)->innertext);
            $item['customer'] = $this->text_clear_all($html->find("a.iceCmdLnk", 1)->innertext);

            $item['type'] = trim(str_replace("№", "", $item['type']));

            $html->clear();

            $items[] = $item;
        }

        $return = array(
            'page_total' => $parse['page_total'],
            'page_now' => $parse['page_now'],
            'items_total' => count($items),
            'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);
        $table_items = $content_dom->find("table.searchResultTable", 0);

        if (is_null($table_items)) {
            return false;
        }

        $arr = $this->parse_table($table_items->outertext);

        $arr[0] = array(
            0 => 'P_internal_id',
            1 => 'P_name',
            2 => 'P_date_publication',
            3 => 'P_price',
            4 => 'P_date_change'
        );
        $items = $this->createstruct($arr);

        $paginator = $content_dom->find("td.width100Left", 0);

        $page_total = max(preg_get_all("#iceDatPgrCol.*?paginateTab\((\d+)\)#si", $paginator->innertext));
        $page_now = $paginator->find("td.iceDatPgrScrCol", 0)->find("a", 0)->find("span", 0)->innertext;

        $content_dom->clear();

        $return['items'] = $items;
        $return['page_now'] = $page_now;
        $return['page_total'] = $page_total;

        return $return;
    }

}

class parser_1_1000_01_0_00_zakupki_detail extends parser_1_1000_01_0_00_zakupki {

    protected $detail_link;
    protected $detail_sort = array(
        'Номер извещения' => 'num|clear_all',
        'Дата окончания подачи' => 'date_end|date_convert',
        'Дата проведение' => 'date_conf|date_convert',
        'Начальная цена' => 'price|clear_all|to_price',
        'Заказчик-наименование' => 'customer|clear_all',
        'Заказчик-почтовый адрес' => 'customer_address|clear_all',
        'Заказчик-email' => 'customer_email|clear_all',
        'Заказчик-Телефон' => 'customer_phone|clear_all',
        'Способ размещения заказа' => 'type|clear_all',
    );

    function detail_all($id) {

        $this->loader->detail_link = "http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=";

        $link = $this->loader->detail_link . $id;
        $this->loader->debug("LINK = {$link}");
        $content = $this->emul_br_get_body($link);
        if ($content) {
            $ret = $this->parse_detail($content);
        }

        $link = $this->loader->detailhtml_link . $id;
        $this->detail_link = $link;
        $this->loader->debug("LINK = {$link}");
        $content = $this->emul_br_get_body($link);
        if ($content) {
            $ret['content'] = $this->detail_content_cache($content);
        }

        $link = $this->loader->document_list . $id;
        $this->loader->debug("LINK = {$link}");
        $content = $this->emul_br_get_body($link);
        if ($content) {
            $ret['docs'] = $this->parse_detail_docs($content);
        }

        return $ret;
    }

    function parse_detail($content) {
        $parse = $this->parse_detail_pre($content);
        $arr = $this->detail_sort_3($parse['item']);
        $arr['db']['region_id'] = $this->loader->geocoder_auto($arr['db']['customer_address']);
        return $arr;
    }

    function parse_detail_pre($content) {

        $xml = simplexml_load_string($content);

        $id = (int) reset($xml->xpath('/notification/placingWay/placingWayId'));
        switch ($id) {
            case 3:
                $item['Дата окончания подачи заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p1Date'));
                $item['Дата окончания рассмотрения заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p2Date'));
                $item['Дата проведение'] = (string) reset($xml->xpath('/notification/notificationCommission/p3Date'));
                break;
            case 5:
                $item['Дата начала подачи котировочных заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p1Date'));
                $item['Дата окончания подачи заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p2Date'));
                break;
            case 11:
                $item['Дата окончания подачи заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p1Date'));
                break;
            default:
                $item['Дата окончания подачи заявок'] = (string) reset($xml->xpath('/notification/notificationCommission/p2Date'));
                break;
        }
        $item['Номер извещения'] = (string) reset($xml->xpath('/notification/notificationNumber'));
        $item['Способ размещения заказа'] = (string) reset($xml->xpath('/notification/placingWay/title'));
        $item['Начальная цена'] = (string) reset($xml->xpath('/notification/lots/lot/maxPrice'));
        $cp = reset($xml->xpath('/notification/contactInfo'));
        $item['Заказчик-наименование'] = (string) reset($cp->xpath('orgName'));
        $item['Заказчик-почтовый адрес'] = (string) reset($cp->xpath('orgFactAddress'));
        $item['Заказчик-email'] = (string) reset($cp->xpath('contactEMail'));
        $item['Заказчик-Телефон'] = (string) reset($cp->xpath('contactPhone'));
        $return['item'] = $item;

        return $return;
    }

    function parse_detail_docs($content) {
        $arr_docs = preg_get_all("#<a.*?downloadDocument\((\d+)\).*?>(.*?)</a>#si", $content);
        foreach ($arr_docs[0] as $k => $r) {
            $docs[] = array(
                'internal_id' => $arr_docs[0][$k],
                'name' => $this->text_clear_all(preg_get("#<span.*?>(.*?)</span>#si", $arr_docs[1][$k])),
            );
        }
        return $docs;
    }

    /*
      function GetIDbyNUM($num) {
      $link = "http://zakupki.gov.ru/pgz/public/action/search/simple/run?orderName=$num";
      $content = $this->emul_br_get_body($link);
      $item = $this->list_parse($content);
      if (!empty($item['items'])) {
      return $item['items'][0]['internal_id'];
      }
      return false;
      }
     */
}

class parser_1_1000_01_0_00_zakupki_report extends parser_1_1000_01_0_00_zakupki_detail {

    public $current_item;

    function get($id) {
        $link = $this->loader->document_list . $id;
        $content = $this->emul_br_get_body($link);
        $this->loader->debug($link);

        echo $content;
        return $content;
    }

    function get_report($id) {
        $link = $this->loader->report_link . $id;
        $content = $this->emul_br_get_body($link);
        $this->loader->debug($link);
        return $content;
    }

    function parse_report($content) {
        $ret = $this->pre_parse_report($content);
        if ($ret === false) {
            return false;
        }
        $ret['docs'] = $this->parse_detail_docs($content);
        $ret['docs'][] = array("detail_link" => "http://zakupki.gov.ru/pgz/public/action/protocol/info/orders/show?protocol_id=" . $ret['internal_id'], "internal_id" => $ret['internal_id'], "name" => "Протокол рассмотрения");
        return $ret;
    }

    function pre_parse_report($content) {
        $reports = preg_get_all("#<a id=\"prFrm\d+\" href=\"\#\" class=\"iceOutLnk\" onclick=\"showProtocolPrintForm\(\d+\);\".*?</a>#si", $content);

        if (empty($reports)) {
            $this->loader->debug("NO REPORT");
            return false;
        }

        foreach ($reports as $report) {
            if (preg_match("#Протокол рассмотрения#siu", $report)) {
                $internal_id = (int) preg_get("#showProtocolPrintForm\((\d+)\)#si", $report);
                $content = $this->get_report($internal_id);
                $xml = simplexml_load_string($content);
                $apps = $xml->xpath('/protocolRate/protocolLots/protocolLot/applications/application');
                foreach ($apps as $a) {
                    $rT = (string) reset($a->xpath('resultType'));
                    if ($rT == "WIN_OFFER") {
                        $price = $this->text_to_price(reset($a->xpath("price")));
                        return array(
                            'internal_id' => $internal_id,
                            'price_best' => $price,
                            'members_count' => count($apps),
                        );
                    }
                }
                return array('internal_id' => $internal_id);
            }
        }
        return false;
    }

}
