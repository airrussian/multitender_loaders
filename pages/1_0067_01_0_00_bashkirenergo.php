<?php

/*
 * ОАО "Башкирэнерго"
*/

class loader_1_0067_01_0_00_bashkirenergo extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.zakupki.bashkirenergo.ru/';
    public $list_link           = 'http://www.zakupki.bashkirenergo.ru/purchase/?PAGEN_1=';
    public $parser_name         = 'parser_1_0067_01_0_00_bashkirenergo';
    public $parser_name_detail  = 'parser_1_0067_01_0_00_bashkirenergo_detail';

    public $fields_list = array(
            'num'   =>  'maybenull',
            'name',
            'internal_id',
            'customer',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0067_01_0_00_bashkirenergo extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Наименование'      =>  'name|clear_all',
        'Организатор'       =>  'customer|clear_all',
        'Период подачи'     =>  'date_end|clear_all',
        'Дата размещения'   =>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link$page \n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_end'] = $this->text_date_convert(preg_get("#<br>(\d{2}.\d{2}.\d{4})#si", $item['date_end_src']));
            $item['internal_id'] = preg_get("#ID=(\d+)#si", $item['name_src']);
            $item['num'] = preg_get("#<b>\(.*?\d+\)</b>#si", $item['name_src']);

            if ($item['date_end']<date("Ymd")) { continue; }
            
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $html = str_get_dom($content);

        $table = $html->find("div.news-list", 0)->find("table", 0);

        $arr = $this->parse_table($table->outertext);        
        $items = $this->createstruct($arr);

        $paginator = $html->find("div.paginator_list", 0)->find("ul", 0);
        $page_now = $paginator->find("li.paginator_list_a", 0)->find("a", 0)->innertext;
        $page_total = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paginator->innertext));

        $html->clear();
        
        $ret['page_now']    = $page_now;
        $ret['items_total'] = count($items);
        $ret['page_total']  = $page_total;
        $ret['items'] = $items;

        return $ret;
    }
}
