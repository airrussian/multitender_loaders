<?php
/**
 * Основа для загрузки доков
 * FIXME: доработать
 * @author Paul-labor@yandex.ru
 */
class loader_1_0000_04_0_00_one_doc extends loader_1_0000_01_0_00_one {
    public $test_run  = true;
    public $test_nodb = true;

    public $many_sleep = 9;
    public $many_count = 3;
    public $item_sleep = 9;

    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'date_publication',
            'type',
            'price' => 'maybenull',
            'customer',
    );

    public $parser_name          = 'parser_1_0001_01_0_00_zakupki';
    public $parser_name_detail   = 'parser_1_0001_01_0_00_zakupki_detail';
    public $parser_name_doc      = 'parser_1_0001_01_0_00_zakupki_detail';
    public $parser_name_protocol = 'parser_1_0001_01_0_00_zakupki_protocol';


    public $break_by_pass = true;

    // попытка исправить регион, или обходить все??
    public $item_rewrite  = true;

    public $debug = true;

    public $site_id = 1;

    protected $list_search_params_link = 'http://zakupki.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link        = 'http://zakupki.gov.ru/Tender/Purchase.aspx';
    protected $detail_link             = 'http://zakupki.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';
    protected $error_msg               = 'При обработке запроса возникли технические неполадки';

    public    $doc_storage             = '/home/mt_loader/arh';

    function  __construct() {
        parent::__construct();
    }

    // FIXME, add region
    function update_in_process() {
        $this->debug("update_in_process: error_list_items = $this->error_list_items, test_nodb = $this->test_nodb");
        if ( ! $this->error_list_items && ! $this->test_nodb) {
            $item = new item();
            $item->DB()->Execute("UPDATE LOW_PRIORITY item SET in_process=NULL ".
                    "WHERE site_id=$this->site_id AND region_id = $this->region_id AND in_process IS NOT NULL");
        }
    }

    function test_detail_get($id = 495388) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_detail_get($id);
    }

    function test_get_detail_param_sort($id = 495388) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_get_detail_param_sort($id);
    }


    function test_detail_get_stuff($id = 720766) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        print_r($this->parser->detail_get_stuff($id));
    }

    function run_detail_one_int($id) {
        $item = new item();
        if($item->Load("site_id = $this->site_id AND internal_id = $id")) {
            $this->run_detail_one($item->id);
        }
    }

    function run_detail_test1() {
        $this->run_detail_one(433917);
    }

    function run_doc($internal_id = null) {
        $doc = new doc();

        if ( $internal_id ) {
            if (!$doc->Load("internal_id = $internal_id AND site_id = $this->site_id")) {
                exit("\nNO this ID\n");
            }
        } else {
            // key: id
            if (!$doc->Load("site_id = $this->site_id AND md5 IS NULL AND " .
            "count_error = 0 AND run_time IS NULL ORDER BY id DESC")) {
                if (!$doc->Load("site_id = $this->site_id AND md5 IS NULL AND " .
                "count_error < 10 AND run_time IS NULL ORDER BY count_error ASC, id DESC")) {
                    exit("\nNO MORE\n");
                }
            }
        }

        $doc->count_error ++; // при успехе убавим
        $doc->count_load  ++;
        $doc->run_time = $doc->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $doc->Save();

        if (!$this->run_doc_one( $doc->id )) {
            return false;
        }

        $doc->Reload("id = $doc->id");
        $doc->run_time   = NULL; // при успехе обнулить
        $doc->count_error --;
        $doc->Save();
    }

    /**
     * Запуск крупных регионов
     */
    function run_list_big() {
        $this->run_list(52);
        sleep($this->many_sleep);
        $this->run_list(77);
        sleep($this->many_sleep);
        $this->run_list(78);
    }

    function run_doc_many() {
        $this->many_count = $this->many_count * 2;

        echo "sleep 30\n";
        sleep(mt_rand(25, 35));

        for ($i = 1; $i<$this->many_count; $i++) {
            $this->run_doc();
            sleep($this->many_sleep);
        }
    }

    function run_report_many() {
        echo "sleep 45\n";
        sleep(mt_rand(40, 50));

        for ($i = 1; $i<$this->many_count; $i++) {
            $this->run_report();
            sleep($this->many_sleep);
        }
    }

    function run_doc_one($id = null) {
        $this->parser = new $this->parser_name_doc;
        $this->parser->loader = & $this;

        $doc = new doc();
        $id = (int) $id;

        if ($id) {
            if( ! $doc->Load("site_id = $this->site_id AND id = $id")) {
                return null;
            }
        }
        else {
            if( ! $doc->Load("site_id = $this->site_id AND md5 IS NULL ORDER BY id DESC")) {
                return null;
            }
        }

        $item = new item();
        if ( ! $item->Load("id = $doc->item_id")) {
            // FIXME костыль, пока не будет сделаны откаты, INNODB
            $item->DB()->Execute('DELETE FROM doc WHERE item_id = ' . $doc->item_id);
            trigger_error("Костыль, нужно удалять или INNODB");
            return null;
        }

        // storage / year / month / site_id / internal_id.md5.ext
        umask();
        list($year, $month) = explode( '-', $doc->date );
        $this->doc_fullpath = "$this->doc_storage/$year/$month/$doc->site_id";
        if ( ! is_dir ( $this->doc_fullpath ) ) {
            if ( ! mkdir( $this->doc_fullpath, 0777, true ) ) {
                exit("unable set derictory");
            }
            else {
                chmod($this->doc_fullpath, 0777);
            }
        }

        $r = $this->parser->doc_get($this->ar_to_array($doc), $this->ar_to_array($item));


        if (empty($r)) {
            return false;
        }

        $this->debug($r['filename']);
        $this->debug($r['md5']);

        //      $r['filename'] = 'file.name.doc.zip';

        if ($r['filename']) {
            $ext = preg_get("/\.([a-z]{1,5}(\.[a-z]{1,5})?)$/i", $r['filename'], 1);
            $ext = strtolower($ext);
        }
        if (empty($ext)) {
            $ext = 'unk';
        }

        // if (!empty($r['data'])) {
        // D0 CF 11 E0 A1 B1 1A E1 doc
        // Rar  - rar
        // D0 CF 11 E0 A1 B1 1A E1 xls
        // }

        if (!empty($r['data'])) {
            $file = "$this->doc_fullpath/{$r['md5']}.$ext.gz";
            $this->debug($file);
            file_put_contents($file, gzencode($r['data'], 6));
            chmod($file, 0666);
        }

        $r['md5'] = strtolower($r['md5']);

        $doc->md5  = $r['md5'];
        $doc->ext  = $ext;
        $doc->size = $r['size'];

        //FIXME преобразование дат...
        $doc->date_load = $doc->DB()->BindTimeStamp(gmdate('U'));

        $doc->Save();
        return true;
    }

    /**
     * Сохранить документ
     *
     array(
     'filename',
     'md5',
     'data',
     'size',
     );
     * @param <type> $id
     */
    function doc_save($data, $item_id, $internal_id, $date=null) {
        $doc = new doc();
        if( ! $doc->Load("site_id = $this->site_id AND item_id = $item_id AND internal_id = $internal_id")) {
            $doc->site_id     = $this->site_id;
            $doc->item_id     = $item_id;
            $doc->internal_id = $internal_id;
        }

        if (is_null($date)) {
            $date = time();
        } elseif ($date < strtotime('-5 year')) {
            exit("Very old date\n");
        }

        $year  = date('Y', $date);
        $month = date('m', $date);

        // storage / year / month / site_id / internal_id.md5.ext
        umask();
        $this->doc_fullpath = "$this->doc_storage/$year/$month/$doc->site_id";
        if ( ! is_dir ( $this->doc_fullpath ) ) {
            if ( ! mkdir( $this->doc_fullpath, 0777, true ) ) {
                exit("unable set derictory, '$this->doc_fullpath'");
            }
            else {
                chmod($this->doc_fullpath, 0777);
            }
        }

        $this->debug($data['filename']);
        $this->debug($data['md5']);

        if ($data['filename']) {
            $ext = preg_get("/\.([a-z]{1,5}(\.[a-z]{1,5})?)$/i", $data['filename'], 1);
            $ext = strtolower($ext);
        }
        if (empty($ext)) {
            $ext = 'unk';
        }

        if (!empty($data['data'])) {
            $file = "$this->doc_fullpath/{$data['md5']}.$ext.gz";
            $this->debug($file);
            file_put_contents($file, gzencode($data['data'], 6));
            chmod($file, 0666);
        }

        $data['md5'] = strtolower($data['md5']);

        $doc->name = $data['filename'];
        $doc->md5  = $data['md5'];
        $doc->ext  = $ext;
        $doc->size = $data['size'];

        $doc->date_add  = $doc->DB()->BindTimeStamp(time());
        $doc->date_load = $doc->date_add;
        $doc->date      = $doc->DB()->BindTimeStamp($date);

        $doc->Save();
    }

}

/**
 * LIST
 */

class parser_1_0000_04_0_00_one_doc extends parser_1_0000_01_0_00_one {
    public $region_param;

    public $curl_timeout = 60;

    protected $list_search_params_link  = 'http://zakupki.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link         = 'http://zakupki.gov.ru/Tender/Purchase.aspx';
    protected $detail_link              = 'http://zakupki.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';

    protected $error_msg = 'При обработке запроса возникли технические неполадки';

    protected $colomn = array(
            '0'                               => 'internal_id',
            'Номер извещения'                 => 'num|clear_all_dots',
            'Наименование'                    => 'name|clear_all_dots',
            'Заказчик'                        => 'customer|clear_all_dots',
            'Способ размещения заказа'        => 'type|clear_all_dots',
            'Дата опубликования извещения'    => 'date_publication|strip_tags|date_convert',
            'Начальная цена контракта'        => 'price|clear_all|to_price',
            // 'Наличие изменений и разъяснений' => 'dev_null',
    );

    /**
     * FIXME workzone - dont need
     *
     * @param <type> $link
     * @param <type> $page
     * @return <type>
     */
    function list_get_page( $link, $page = 1 ) {
        if ($page == 1) {
            return $this->list_get_page_first();
        }

        $this->loader->debug("NEXT PAGE = $page");

        $page = (int) $page;

        $emul_br = $this->emul_br_init($this->list_search_link);

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
                '__EVENTTARGET'   => 'ctl00$phWorkZone$' . 'ListOfNewPurchase$gvMain',
                '__EVENTARGUMENT' => 'Page$' . $page,
        ));

        $content = $emul_br->exec();

        $this->loader->debug($emul_br->GetAllHeaders());

        if(preg_match("/$this->error_msg/", $content)) {
            trigger_error('ERROR on load http, pri obrabotke voznkikli oshibki', E_USER_WARNING);
            exit("CRITIKAL exit");
        }

        return $content;
    }

    function list_parse($content) {
        exit('DEFINE_ME');
    }

    function list_parse_pre($content) {
        exit('DEFINE_ME');
    }

}

/**
 * DETAIL
 */
class parser_1_0000_04_0_00_one_doc_detail extends parser_1_0000_01_0_00_one {
    public $region_param;

    protected $list_search_params_link  = 'http://zakupki.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link         = 'http://zakupki.gov.ru/Tender/Purchase.aspx';
    protected $detail_link              = 'http://zakupki.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';

    protected $error_msg = 'При обработке запроса возникли технические неполадки';

    protected $parser_lots = array();

    // текущие данные тендера
    public    $item_now = array();

    function test_detail_get($id = 495388) {
        print_r($this->detail_all($id));
    }

    function detail_all($id) {
        $p = $this->detail_get($id);

        if(isset($p['detail_headers']['Location'])) {
            if($p['detail_headers']['Location'] === '/Tender/Purchase.aspx') {
                return array(
                        'error' => 'delete',
                );
            }
        }

        $return = $this->detail_param_sort(
                $this->detail_parse($p['detail']),
                $this->detail_parse_lots($p['lots']),
                $this->detail_parse_docs($p['docs']));
        $return['stuff'] = $p['stuff'];

        return $return;
    }

    function detail_parse($content) {
        exit('DEFINE_ME');
    }

    function test_detail_parse() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_main.htm');
        $list = $this->detail_parse($content);
        print_r($list);
        return $list;
    }

    function detail_parse_lots($content) {
        //FIXME dom_html trim content!
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table[id=ctl00_phWorkZone_Positions]', 0);

        if(empty($table)) {
            exit('error parse detail_parse_lots, '. __LINE__);
        }

        $trs = $table->find('tr[align=left]');

        foreach($trs as $key=>$tr) {
            $tds = $tr->find('td');
            $return[$key]['id']       = trim($tds[1]->innertext);
            $return[$key]['tid']      = trim($tds[0]->find('input',0)->name);
            $return[$key]['name']     = trim($tds[2]->innertext);

            $sum = $tds[3]->find('span',0)->innertext;
            $sum = (int)preg_replace('/[^\d.,]/', '', $sum);
            $cur = $tds[3]->find('span',1)->innertext;

            $return[$key]['sum']      =  (int)$this->currency_to_rub($sum, $cur);

            $return[$key]['cur_sum']  =  (int)$sum;
            $return[$key]['cur_type'] =  $cur;
        }

        $content_dom->__destruct();

        return $return;
    }

    function test_detail_parse_lots() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_lots.htm');
        $lots = $this->detail_parse_lots($content);
        print_r($lots);
        return $lots;
    }

    function detail_parse_docs($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table[id=ctl00_phWorkZone_DocumentList_ctl00_DocumentList]', 0);

        if(empty($table)) {
            if($content_dom->find('table[id=ctl00_phWorkZone_DocumentList_EmptyGrid]')) {
                return array();
            }
            exit('error parse detail_parse_docs' . __LINE__);
        }

        $trs = $table->find('tr[align=left]');

        if(empty($trs)) {
            exit('error parse detail_parse_docs' . __LINE__);
        }

        $return = array();

        foreach($trs as $key=>$tr) {
            $tds = $tr->find('td');

            $return[$key]['tid']      = trim($tds[0]->find('input',0)->name);

            //ctl00$phWorkZone$Doc$ctl02$ImageButton1
            $return[$key]['internal_id'] = (int)preg_get("/ctl(\d+)\\\$btnDownload/i", $return[$key]['tid']);

            $return[$key]['name']     = trim($tr->find('[id*=DocumentName]',0)->plaintext);
            $return[$key]['desc']     = trim($tr->find('[id*=DocumentDescription]',0)->plaintext);
            $return[$key]['date']     = $this->date_convert( trim($tr->find('[id*=DocumentDate]',0)->plaintext) );
        }

        $content_dom->__destruct();

        if(empty($return)) {
            exit('error parse detail_parse_docs' . __LINE__);
        }

        return $return;
    }

    function test_detail_parse_docs() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_docs.htm');
        $docs = $this->detail_parse_docs($content);
        print_r($docs);
        return $docs;
    }

    function text_date_end($str) {
        $dates = preg_get_all("/\d{2}.\d{2}.\d{4}/", $str);

        if(!$dates) {
            exit("error, str = $str");
        }

        $date = array_pop($dates);

        return $this->date_convert($date);
    }
    
}
