<?php
/**
 * "Организационно-правовой центр "ТЕНДЕР"
 * 27.07.2010 - Написание
 * 09.08.2010 - запуск
 * FIXME Где аукционы? /index.php?option=com_etp&Itemid=33 => пока в тестовом режиме
 * FIXME Имеются ли государственные? => да
 */
class loader_1_0017_01_0_00_tender extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'https://www.etp-tender.ru/';
    public $list_link           = 'https://www.etp-tender.ru/index.php?option=com_etp&action=reestr_kotirovok&task=&filtr=tekuschie&limit=25&limitstart=';
    public $parser_name         = 'parser_1_0017_01_0_00_tender';
    public $parser_name_detail  = 'parser_1_0017_01_0_00_tender_detail';

    public $fields_list = array(
            'num',
            'name',
            'date_publication',
            'date_end',
            'type',
            'internal_id',
            'customer',
    );

    // FIXME только коммерческие
    /*
    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );
    */

    public $break_by_pass = true;
    public $item_rewrite  = false;

    function test_detail($id=907) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);

        var_dump($arr);
    }

}

class parser_1_0017_01_0_00_tender extends parser_1_0000_02_0_00_temp {

    protected $list_colomn = array(
            'Код'               => 'num|clear_all',
            'Заказчик'          => 'customer|clear_all',
            'Наименование'      => 'name|clear_all',
            'Начало срока'      => 'date_publication|date_convert',
            'Окончание срока'   => 'date_end|date_convert',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link . ($page-1)*25 );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item);
            
            $t = preg_get("#(\S+)\-(\d+)#", $item['num']);
            $item['type']        = $t[0];
            $item['internal_id'] = preg_get("#<a.*?task=view&id=(\d+)#si", $item['name_src']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);
        
        $ten = $content_dom->find("table.etpSpisokKotirovok", 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $page = $content_dom->find("div.pagenavbar", 0)->innertext;
        $ret['page_now'] = preg_get("#<span class=['\"]pagenav['\"]>(\d+)</span>#si", $page);

        $ret['page_total'] = max(preg_get_all("#<a.*?>.*?<strong>(\d+)</strong>.*?</a>#si", $page));

        $total = $content_dom->find("div.pagenavcounter", 0)->innertext;
        $ret['items_total'] = preg_get("#Результаты \d+ - \d+ из (\d+)#sui", $total);

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0017_01_0_00_tender_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
            'Дата и время публикации'     => 'date_publication|date_convert',
            'Организатор процедуры'       => 'region_id|region_convert',
            'Наименование'                => 'customer|clear_all',
            'Почтовый адрес'              => 'customer_address|clear_all',
            'E-mail'                      => 'customer_email|clear_all',
    );

    function detail_all($id) {

        $this->detail_link = "https://www.etp-tender.ru/index.php?option=com_etp&action=kotirovka&task=view&id=".$id;

        $content      = $this->emul_br_get_body( $this->detail_link );
        $parse_detail = $this->list_parse_pre( $content );

        $docs = $parse_detail['docs'];
        unset($parse_detail['docs']);

        $return = $this->detail_sort_3( $parse_detail );

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);

        $i = 0;
        foreach ($docs as $doc) {
            $return['docs'][$i]['name']         = preg_get("#<a.*?>(.*?)</a>#si", $doc);
            $return['docs'][$i]['detail_link']  = preg_get("#<a.*?href=['\"](.*?)['\"].*?>.*?</a>#si", $doc);
            $return['docs'][$i]['internal_id']  = $this->make_internal_id($return['docs'][$i]['detail_link']);
            $i++;
        }

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $det = $detail_dom->find("table.etpFormaZakaza", 0);

        foreach ($det->find("tr") as $tr) {
            if ($tr->find("th", 0)) {
                $return[$this->text_clear_all($tr->find("th", 0)->innertext)] = $tr->find("td", 0)->innertext;
            }
        }

        $str = $detail_dom->find("div#files-container", 0)->innertext;

        $return['docs'] = preg_get_all("#<a.*?a>#si", $str);

        $detail_dom->__destruct();

        return $return;
    }

}
