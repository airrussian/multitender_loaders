<?php
/**
 * TenderPro http://www.tender.pro/
 * 26.08.2010 - написание
 * 27.08.2010 - пропуск закрытых
 */
class loader_1_0030_01_0_00_tenderpro extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.tender.pro/';
    public $list_link           = 'http://www.tender.pro/view_tenders_list.shtml?page=';
    public $parser_name         = 'parser_1_0030_01_0_00_tenderpro';
    public $parser_name_detail  = 'parser_1_0030_01_0_00_tenderpro_detail';

    public $fields_list = array(
            'name',
            'date_publication',
            'date_end',
            'internal_id',
            'customer',
            'type',
    );
    
    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function test_detail($id=76985) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);

        var_dump($arr);
    }

}

class parser_1_0030_01_0_00_tenderpro extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Название'          => 'name|clear_all',
            'Создан'            => 'date_publication',
            'Прием заявок'      => 'date_end',
            'Компания'          => 'customer|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $page = ($page-1)*100;
        return $this->emul_br_get_body( $link.$page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#tenderid=(\d+)#si", $item['name_src']);
            $item['type_src']    = preg_get("#<a.*?title=['\"](.*?)['\"]#siu", $item['name_src']);
            $item['date_publication'] = $this->text_date_convert_short(preg_replace("#/#", ".", $item['date_publication_src']));
            $item['date_end']    = $this->text_date_convert_short(preg_replace("#/#", ".", $item['date_end_src']));

            // 27.08.2010 было решено закрытые пропускать, т.к. информации почти нет
            // http://www.tender.pro/view_tender_public.shtml?tenderid=79614
            if (preg_match('/закрыт/i', $item['type_src'])) {
                continue;
            }

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table.baseTable", 0);

        $arr = $this->parse_table($tenders->outertext);

        $ret['items'] = $this->createstruct($arr);

        $pager = $content_dom->find("div.pager", 0);
        $ret['page_now'] = $pager->find("span.selected", 0)->innertext;
        $ret['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#siu", $pager->innertext));

        $ret['items_total'] = count($ret['items']);

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0030_01_0_00_tenderpro_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
        'Прием заявок до'   => 'date_end|date_convert_text_full',
        'заказчик'          => 'customer|clear_all',
        'адрес'             => 'customer_address|clear_all'
    );

    function detail_all($id) {
        $link         = "http://www.tender.pro/view_tender_public.shtml?tenderid=$id&tab=common";
        $content      = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);

        $return = $this->detail_sort_3($parse_detail);

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $det = $detail_dom->find("div#tenderForm", 0)->find("table", 0);

        $t_arr = $this->parse_table($det->outertext);

        foreach ($t_arr as $row) {
            $arr[$this->text_clear_all($row[0])] = $this->text_clear_all($row[1]);
        }

        $customer = $detail_dom->find("div#tenderHeader", 0)->find("span.small", 0)->find("a", 0);

        $arr['заказчик'] = $customer->innertext;
        $content = $this->emul_br_get_body("http://www.tender.pro/".preg_get("#href=['\"](.*?)['\"]#si", $customer->outertext));
        $arr['адрес'] = $this->parse_customer($content);

        $detail_dom->__destruct();

        return $arr;
    }

    function parse_customer($content) {
        $content = $this->text_from_win($content);
        return preg_get("#<b>Адрес:</b>(.*?)<#sui", $content);
    }

}


