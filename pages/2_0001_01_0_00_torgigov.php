<?php

/*
 * Екатеринбургская электросетевая компания
*/

class loader_2_0001_01_0_00_torgigov extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://torgi.gov.ru/';
    public $list_link           = 'http://torgi.gov.ru/lotSearch2.html';
    public $parser_name         = 'parser_2_0001_01_0_00_torgigov';
    public $parser_name_detail  = 'parser_2_0001_01_0_00_torgigov_detail';

    public $fields_list = array(           
            'name',
            'internal_id',
            'date_publication',           
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_2_0001_01_0_00_torgigov extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'P_internal_id'     =>  'internal_id',
        'P_name'            =>  'name',
        'P_date_publication'=>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {

        $link.="&m=".date("m")."&y=".date("Y");
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#/trade/notification\?id=(\d+)#si", $item['internal_id']);
            $name = preg_replace(array("#<div.*?/div>#si", "#<a.*?/a>#si"), "", $item['name_src']);
            $item['name'] = $this->text_clear_all($name);
            
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $dom = str_get_dom($content);

        $items = array();
        foreach ($dom->find("div.zapros-result") as $zaprosresult) {
            $item = $zaprosresult->find("div", 0);
            do {
                $items[] = array(
                    'P_date_publication'  =>  $item->find("div.title2", 0)->innertext,
                    'P_name'              =>  $item->innertext,
                    'P_internal_id'       =>  $item->find("a", 0)->outertext,
                );
            } while ($item = $item->next_sibling());
        }
        $dom->clear();
       
        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;
        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0069_01_0_00_eesk_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    public $detail_sort = array(
        'Место проведения'                  =>  'address|clear_all',
        'Срок предоставления'               =>  'date_end|clear_all',
    );

    function detail_get($id) {

        $this->detail_link = "http://www.eesk.ru/trade/notification?id=$id";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $return = $this->detail_sort_3($parse);
      
        //$return['content'] = $this->detail_content_cache($this->text_from_win($content));
        $return['db']['date_end']  = $this->text_date_convert_text_full(str_replace(array("«", "»"), "", preg_get("#«\d{2}».+?\d{4}#si", $return['db']['date_end'])));
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['address']);

        var_dump($return); exit;

        return $return;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_dom($content);

        $detail = $html->find("div.one-item-", 0);

        foreach ($detail->find("p") as $p) {
            $p = $this->text_clear_all($p->innertext);
            $p = trim(preg_replace("#\d+\.\s#", "", $p));
            echo $p . "\n\n";
        }

        exit;

        return $return;
    }


}

