<?php
/**
 * @author airrussian@mail.ru
 */

class loader_1_0033_01_0_00_rts extends loader_1_0000_01_0_00_one {
    public $base_url           = 'http://www.rts-tender.ru/';
    public $list_link          = 'http://rts-tender.ru/RSS.aspx';
    public $parser_name        = 'parser_1_0033_01_0_00_rts';
    public $parser_name_detail = 'parser_1_0033_01_0_00_rts_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'price'            => 'maybenull',
            'date_end',
            'date_publication',
            'date_conf',
            'detail_link_prefix',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = true;
    public $item_rewrite  = false;

    function geocoder_zakazrf($string) {
        $this->debug('GEOCODER, input=', $string);

        $region_id = $this->geocoder_by_index_auto($string);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($string);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_ZAKAZRF, region= $region_id ($region_name)", $string);

        return $region_id ? $region_id : false;
    }

    function test_detail($id=251) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->detail_all($id);
    }

}

class parser_1_0033_01_0_00_rts extends parser_1_0000_01_0_00_one {
    protected $colomn = array(
        'link'          =>  'detail_link|clear_all',
        'title'         =>  'name',
        'description'   =>  'num',
        'guid'          =>  'detail_link_prefix|clear_all',
    );

    function list_get_page( $link ) {
        $emul_br = $this->emul_br_init($link);
        $emul_br->exec();
        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] =  abs(crc_p($item['detail_link']));

            $item['price'] = $this->text_to_price(preg_get("#цена.*?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_publication'] = $this->text_date_convert(preg_get("#Дата публикации:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_end'] = $this->text_date_convert(preg_get("#Дата и время окончания срока подачи заявок на участие:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_conf'] = $this->text_date_convert(preg_get("#Дата проведения открытого аукциона:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['num'] = $this->text_clear_all(preg_get("#Номер извещения:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));

            $items[] = $item;
        }

        $return = array (
                'page_total'  => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }


    function list_parse_pre($content) {
        $xml = simplexml_load_string($content);

        $arr = array();

        foreach ($xml->xpath('/rss/channel/item') as $item) {
            $return['items'][] = array(
                'title'         => (string) reset($item->xpath('title')),
                'link'          => (string) reset($item->xpath('link')),
                'description'   => (string) reset($item->xpath('description')),
                'pubDate'       => (string) reset($item->xpath('pubDate')),
                'guid'          => (string) reset($item->xpath('guid')),
            );
        }

        return $return;
    }

}

class parser_1_0033_01_0_00_rts_detail extends parser_1_0033_01_0_00_rts {

    protected $detail_link = 'http://www.rts-tender.ru/ViewReductionPrint.aspx?id=';

     public $detail_sort = array(
        'Сведения об организаторе торгов\/Место нахождения' => 'customer_address|clear_all',
        'Наименование организации' => 'customer|clear_all',
        'Сведения об организаторе торгов\/Адрес электронной почты' => 'customer_email|clear_all',
        'Сведения об организаторе торгов\/Номера контактных телефонов' => 'customer_phone|clear_all',
    );

    function arr_double2one(array $arr) {
        $return = array(); $i=0;
        foreach ($arr as $row) {

            if (count($row)==2) {
                $key = $this->text_clear_all($row[0]);
                $val = $row[1];
                
            } else {
                $val = $row[0];
                $key = $i++;
            }
            $return[$key] = $val;
        }
        return $return;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);        
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $doc = $parse['Документы'];
        $docs = preg_get_all("#<a target=\"_blank\" href=\".*?DFile.*?\">.*?</a>#sui", $doc);
        $return = $this->detail_sort_3($parse);
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        foreach ($docs as $doc) {
            $return['docs'][] = array(
                'internal_id' => preg_get("#id=(\d+)#si", $doc),
                'name' => preg_get("#<a.*?>(.*?)</a>#si", $doc),
            );
        }
        return $return;
    }

    function detail_parse_pre($content) {
        $html = str_get_html($content);
        $CardView = $html->find("div.CardView", 0);
        foreach ($CardView->find("h2") as $h2) {
            $next = $h2->next_sibling();
            $cat = $this->text_clear_all($h2->outertext);
            $tagname = preg_get("#^<(\S+)[\s|>]#si", $next->outertext);
            if ($tagname=='table') {
                $arr = $this->parse_table($next->outertext);
                $arr = $this->arr_double2one($arr);
                foreach ($arr as $key => $val) {
                    $return[$cat."/".$key] = $val;
                }
            } else {
                $t = $h2->next_sibling();
                $return[$cat] = "";
                do {
                   $return[$cat] .= $t->outertext;
                   $t = $t->next_sibling();
                } while (preg_get("#^<(\S+)[\s|>]#si", $t->outertext)=="div");
            }
            
        }
        return $return;
    }

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

}
