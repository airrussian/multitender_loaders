<?php
/**
 * B2B Center Единая система электронной торговли
 * 2010.07.08 - запуск
 * 2010.08.27 - доработка, добавлена строка очистки повторяемых пробелов. 
 */
class loader_1_0005_01_0_00_b2bcenter extends loader_1_0000_02_0_00_temp {
    public $base_url    = 'http://www.b2b-center.ru/';
    public $list_link   = 'http://www.b2b-center.ru/market/?from=';
    public $parser_name = 'parser_1_0005_01_0_00_b2bcenter';

    public $parser_name_detail = 'parser_1_0005_01_0_00_b2bcenter_detail';

    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'customer',
            'date_publication',
            'date_end' => 'maybenull',
            'type',
            'stuff' => 'maybenull',
            'detail_link_prefix',
            'sector_id',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    function run_detail_update() {
        $item = new item;

        // key: id
        if(!$item->Load("site_id = $this->site_id AND detail_count_error < 5 AND " .
        "detail_run_time IS NULL AND region_id IN(0, NULL) ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }

        $item->detail_count_error ++;
        $item->Save(TRUE);

        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        // передаем текущие данные в парсер
        $this->parser->item_now = $this->ar_to_array($item);

        $parse = $this->parser->detail_all($item->internal_id);

        if(empty($parse)) {
            exit('Empty parse');
        }

        print_r($parse);

        $this->detail_insert($item->id, $parse);

        $item->ReLoad("id = $item->id");
        $item->detail_run_time   = NULL; // при успехе обнулить
        //$item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }
        $item->Save(TRUE);

        $this->debug("RUN_DETAIL id = $item->id");
    }

}

class parser_1_0005_01_0_00_b2bcenter extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Наименование'      => 'name',
            'Организатор'       => 'customer|clear_all',
            'Дата'              => 'date_publication',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");

        $page = ($page - 1) * 20;
        
        return $this->emul_br_get_body( $link.$page );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['name_src'] = $this->text_strip_space($item['name_src']);
            $item['type_src'] = preg_get("#<b>(.*?)</b>#sui", $item['name_src']);
            $item['name'] = $this->text_clear_all(preg_replace("#<b>.*?</b>#sui", "", $item['name_src']));
            $item['stuff'] = preg_get("#(лот\s№\s\d+.*)</a>#siu", $item['name_src']);
            $item['name'] = preg_replace("#лот\s№\s\d+.*#siu", "", $item['name']);
            if (empty($item['name'])) {
                $item['name'] = $item['stuff'];
                $item['stuff'] = null;
            }

            $item['num'] = preg_get("#№.?(\d+)#sui", $item['type_src']);
            $item['type'] = preg_replace(array("#\(.*?\)#sui","#№.?\d+#sui"), "", $item['type_src']);
            $item['detail_link_prefix'] = preg_get("#/market/(\S+.html\?id=\d+)#siu", $item['name_src']);
            $item['date_publication'] = $this->text_date_convert(preg_get("#(\d{2}.\d{2}.\d{4}).*?<br>#sui", $item['date_publication_src']));
            $item['date_end'] = $this->text_date_convert(preg_get("#<br>.*?(\d{2}.\d{2}.\d{4}).*?#sui", $item['date_publication_src']));
            $item['internal_id'] = abs(crc_p($item['detail_link_prefix']));

            $item['sector_id'] = 2;

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        //$content = $this->text_from_win($content);       

        $content_dom = str_get_html($content);

        $searchresult = $content_dom->find('div#searchresult', 0);
        
        $tbl = preg_get("#<TABLE border=0 cellspacing=1 cellpadding=5>.*?</table>#si", $searchresult->innertext);

        $arr = $this->parse_table($tbl);
        
        $pages = $searchresult->find("span.big", 0);
        $ret['page_now'] = $pages->find("b", 0)->innertext;


        $ret['page_total'] = max(preg_get_all("#market/\?from=(\d+)#sui", $content));
        $ret['page_total'] = ($ret['page_total'] / 20) + 1;

        $content_dom->__destruct();

        $ret['items']=$this->createstruct($arr);

        return $ret;
    }
}

class parser_1_0005_01_0_00_b2bcenter_detail extends parser_1_0000_02_0_00_temp {

    function test_list_parse($page = "view_tender.html?id=18886") {
        $emul_br = new emul_br();
        $emul_br->URI     = "http://www.b2b-center.ru/market/".$page;
        $content = $emul_br->exec();
        print_r( $this->list_parse($content) );
    }

    public $detail_sort = array(
            'Географическая привязка' => 'customer_address',
            'Общая стоимость'         => 'price|to_price',
            'Организация в заголовке' => 'customer1',
            'Адрес в заголовке'       => 'customer1_address',
            //'Организатор торгов'    => 'customer',
            //'Действительно до'      => 'date_end|date_convert',
            //'Размещено'             => 'date_publication|date_convert',
    );

    function detail_all($content) {
        $link         =  'http://www.b2b-center.ru/market/' . $this->item_now['detail_link_prefix'];
        echo "try get page...\n";
        $this->loader->debug("LINK = ". $link );
        $content      = $this->emul_br_get_body($link);
        $this->detail_link = $link;
        $parse_detail = $this->list_parse_pre($content);

        // sort function
        $return = $this->detail_sort_2($parse_detail);

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
 
        if ( (! $return['db']['region_id']) && isset($return['db']['customer1_address'])  ) {
            $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer1_address']);
            $return['db']['customer_address'] = $return['db']['customer1_address'];
        }

        foreach ($parse_detail as $key => $val) {
            if (preg_match("#Закупочная документация|Файл с описанием#sui", $key)) {
                $docs = preg_get_all("#<a.*?file.*?>.*?</a>#sui", $val);
                $i=0;
                foreach($docs as $doc) {
                    $return['docs'][$i]['name'] = $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#sui", $doc));
                    $return['docs'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"]#", $doc);
                    $return['docs'][$i]['internal_id'] = abs(crc_p($this->item_now['internal_id'] . $return['docs'][$i]['detail_link']));
                    $i++;
                }
            }
        }

        return $return;
    }

    function list_parse_pre($content) {
        //$content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tbl = $content_dom->find("td.main", 0)->find("table", 1);

        $arr = array();

/*        $some_stuff = $content_dom->find("td.main", 0)->find('td.thead', 0);
        $customer_1 = $some_stuff->find('a b', 0)->innertext;
        $customer_2 = $some_stuff->find('span', 0)->innertext;
        if ( $customer_1 && $customer_2 ) {
            $arr['Организация в заголовке'] = $this->text_clear_all_dots($customer_1);
            $arr['Адрес в заголовке']       = $this->text_clear_all_dots($customer_2);
        }*/

        foreach ($tbl->find("td.c2") as $c2) {
            if ($c2->find("table", 0)) {
                foreach ($c2->find("table",0)->find("tr") as $tr) {
                    $nam_r = $tr->find("td", 0)->innertext;
                    $nam_r = preg_replace("#:#sui", "", $nam_r);
                    if ($tr->find("td", 1)) {
                        $val_r = $tr->find("td", 1)->innertext;
                    } else {
                        $val_r = preg_replace("#<b>.*?</b>#sui", "", $nam_r);
                        $nam_r = preg_get("#<b>(.*?)</b>#sui", $nam_r);
                    }
                    $nam_r = $this->text_clear_all(preg_replace("#:#sui", "", $nam_r));
                    if ($nam_r) {
                        $arr[$nam_r] = $val_r;
                    }
                }
            }
        }
        
        $content_dom->__destruct();

        return $arr;
    }
}
