<?php

/**
 * Веб-портал по вопросам государственных закупок Украины
 * @version 13.05.2011
 * @author AIR <airrussian@mail.ru>
 */

class loader_1_2001_01_0_00_tendermegovua extends loader_1_0000_01_0_00_one {

    public $base_url        = 'https://tender.me.gov.ua/EDZFrontOffice/';
    public $list_link       = 'https://tender.me.gov.ua/EDZFrontOffice/menu/ru/purchaseStartSearch';

    public $parser_name     = 'parser_1_2001_01_0_00_tendermegovua';

    public $fields_list = array(
            'num',
            'name',
            'customer',
            'type',
            'date_publication',
            'date_end',
            'date_conf' => 'maybenull',
            'internal_id',
            'region_id',
    );

    public $item_rewrite  = true;


    function geocoder_by_name($str) {
        $arr = preg_get_all("#\S{4,}#sui", $str);
        $result = 200;
        foreach ($arr as $row) {
            $row = str_replace("м.", "", $row);
            $r = parent::geocoder_by_name($row);
            if ($r>100 && $r<200) {
                $result = $r;
            }
        }
        return $result;
    }

    function geocoder_region_const($region_id) {
        if (!$region_id) { return 200; } else {return $region_id;}
    }

}

class parser_1_2001_01_0_00_tendermegovua extends parser_1_0000_01_0_00_one {

    private $cookies;

    protected $colomn = array(
        '№ огол.'               => 'num|clear_all',
        'Предмет'               => 'name|uktoru|clear_all',
        'Замовник'              => 'customer|uktoru|clear_all',
        'Процедура закупівлі'   => 'type',
        '№ ВДЗ'                 => 'date_publication',
        'Кінцевий строк'        => 'date_end|date_convert_short',
        'Дата та час розкриття' => 'date_conf|date_convert_short',
        'Місце'                 => 'region_id|clear_all',
        11                      => 'internal_id'
    );


    function list_get_page( $link, $page = 1 ) {
        $br = new emul_br();
        if ($page == 1) {
            $br->URI = $this->loader->base_url;
            $br->exec();
            $this->cookies = $br->GetResponseCookieArray();
            $br->URI = $this->loader->list_link;
            $br->SetCookie($this->cookies);
            $br->SetPost(array(
                        'login'               => 'login',
                        'login:login'         => 'airrussian@mail.ru',
                        'login:password'      => '90210',
                        'login:j_id_id254.x'  => '13',
                        'login:j_id_id254.y'  => '11',
                        'javax.faces.ViewState' => 'j_id1:j_id2',
                ));
            $br->exec();
            return $br->GetBody();
        }
        if (empty($this->cookies)) { $this->loader->debug("где кука от хахлов?"); return false; }

        $br->URI = $this->loader->list_link;
        $br->SetCookie($this->cookies);
        $br->exec();
        return $br->GetBody();
    }


    function list_parse($content) {

        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['region_id'] = (int) $this->loader->geocoder_by_name($item['region_id']);

            $item['date_publication'] = $this->text_date_convert_short(preg_get("#\d{2}.\d{2}.\d{2}$#si", $item['date_publication']));

            $item['internal_id'] = (int) preg_get( "#id=(\d+)#si", $item['internal_id'] );

            if (empty($item['date_end'])) {
                $item['date_end'] = date("Ymd", strtotime(preg_replace("#(\d{4})(\d{2})(\d{2})#i", "\\1/\\2/\\3", $item['date_publication'])) + 30*24*60*60);
            }
            $items[$k] = $item;
        }


        $return = array (
            'page_total'  => 1,
            'page_now'    => 1,
            'items_total' => count($items),
            'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = tidy_repair_string($content, $this->tidy_config);

        $content = $this->text_clear_enteries($content, ENT_QUOTES);

        $HTML_DOM = str_get_html($content);
        $TABLE_DATA = $HTML_DOM->find("table.ma", 0)->outertext;
        $ARR_DATA = $this->parse_table($TABLE_DATA);
        $HTML_DOM->clear();

        $ret['items'] = $this->createstruct($ARR_DATA);

        return $ret;
    }

    function text_uktoru($str) {
        $str = $this->text_translate($str, 'uk');
        $str = str_replace("\\\"", "\"", $str);
        return $str;
    }
}