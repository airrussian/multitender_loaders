<?php

/**
 * roseltorg.ru
 * @author airrussian@mail.ru
 */

class loader_1_1001_01_0_00_roseltorg extends loader_1_1000_01_0_00_zakupki {
    public $base_url    = 'http://etp.roseltorg.ru/index.php';
    public $list_link   = 'http://etp.roseltorg.ru/trade/future/?limit=100&order=pubdate&dir=desc&type_a=on&page=';
    public $parser_name = 'parser_1_1001_01_0_00_roseltorg';
    public $parser_name_detail = 'parser_1_1001_01_0_00_roseltorg_detail';

    public $fields_list = array(
            'internal_id',
            'customer',
            'date_end',
            'date_conf',
            'name',
            'price'                 => 'maybenull',
            'num',
            'region_id'             => 'maybenull', // И так бывает, с Detail вытащится.
            'detail_link_prefix',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = true;

    protected $run_detail_order = 'date_conf ASC';

    public $page_total_rewrite = true;
    
    function run_detail_query(&$obj) {
        if(!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }

}

class parser_1_1001_01_0_00_roseltorg extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Предмет'           => 'name|text_clear_all',
            'Заказчик'          => 'customer|text_clear_all',
            'цена'              => 'price|text_clear_all|to_price',
            'Дата окончания'    => 'date_end|text_clear_all|date_convert',
            'Тип'               => 'region_id',
    );

    function list_get_page( $link, $page = 1 ) {
        $link = $link . $page;
        $this->loader->debug ("LINK = $link");
        return $this->emul_br_get($link);
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_conf']          = $item['date_end'];
            $item['num']                = preg_get("#/trade/view/\?id=(\d+)#si", $item['name_src']);
            $item['internal_id']        = abs(crc_p($item['num']));
            $item['detail_link_prefix'] = $item['num'];
            $item['region_id']          = (int) preg_get("#<div class=['\"]region_number['\"]>(\d+)</div>#si", $item['region_id']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        // Парсинг таблицы
        $table = $content_dom->find('table.tbl_torgs',0);
        $table_array = $this->parse_table($table->outertext);

        $return = array();
        $return['items'] = $this->createstruct($table_array);

        $pages = $content_dom->find('div.link_page_bot ul.link_page_bot_lst',0)->innertext;
        $last_page = array_pop(preg_get_all('/page=(\d+)/', $pages));
        $return['page_now']   = null;
        $return['page_total'] = $last_page + 1;

        $content_dom->clear();

        return $return;
    }
}

class parser_1_1001_01_0_00_roseltorg_detail extends parser_1_1000_01_0_00_zakupki_detail {

    function detail_all($id) {

        $num = $this->item_now['num'];

        $id = $this->loader->GetIDbyNUM($num);
        if ($id) {
            $this->loader->debug("ZAKUPKI ID = $id");
            $ret = parent::detail_all($id);
            $this->loader->detail_link = "http://etp.roseltorg.ru/trade/view/?id=$num";
            $ret['content'] = $this->detail_content_cache($this->emul_br_get_body($this->loader->detail_link));
        } else {
            $this->loader->debug("NOT ITEM WITH $num ON ZAKUPKI");
            $this->loader->NoItem($this->item_now['id']);
            $ret = false;
        }

        return $ret;
    }

}
