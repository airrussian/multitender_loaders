<?php

/*
 * ЗАКУПКИ ЗАО СИБИРСКАЯ АГРАРНАЯ ГРУППА
*/

class loader_1_0060_01_0_00_sag extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.zakupki.tomsk.ru/';
    public $list_link           = 'http://www.zakupki.tomsk.ru/competition/WorkCompetition.ASP?CID=1';
    public $parser_name         = 'parser_1_0060_01_0_00_sag';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end'              =>  'maybenull',
            'date_conf',
            'doc',
            'customer',
            'customer_address'      =>  'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0060_01_0_00_sag extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Предмет торгов'                =>  'name|clear_all',
        'Дата публикации'               =>  'date_publication|clear_all|date_convert',
        'Дата окончания'                =>  'date_end|clear_all|date_convert',
        'Дата начала торгов'            =>  'date_conf|clear_all|date_convert',
        'Вид размещения'                =>  'type|clear_all',
        'Документы'                     =>  'docs',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function customer_parse($cus_id) {
        $link = "http://www.zakupki.tomsk.ru/Trade/AGRO/Customer/comp%D1%81ustomer.aspx?CID=$cus_id";
        $this->loader->debug("\n\n CUSTOMER LINK = $link \n\n");
        $content = $this->emul_br_get_body( $link );
        $html = str_get_dom($content);
        $tbl  = $html->find("table#dgProducts", 0)->outertext;
        $html->clear();
        $data = $this->parse_table($tbl);
        $data = $this->createstruct($data);
        $return = array(
            'customer'  =>  $this->text_clear_all($data[1]['Наименование']),
            'customer_address'  =>  $this->text_clear_all($data[1]['Адрес']),
        );
        return $return;
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $docs = $item['docs'];

            $docs = preg_get_all("#<a.*?/a>#si", $docs);
            foreach ($docs as $doc) {
                $internal_id = preg_get("#CID1=(\d+)#si", $doc);
                $item['doc'][] = array(
                    'internal_id'   =>  $internal_id,
                    'name'          =>  $item['name'],
                );                
            }

            $item['internal_id'] = (int) preg_get("#CID=(\d+)#si", $item['name_src']);
            $cust = $this->customer_parse($item['internal_id']);
            $item = array_merge($item, $cust);
            $item['region_id'] = $this->loader->geocoder_auto($item['customer_address']);
            
            $items[$k] = $item;
        }       

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $data = preg_get("#<table border=1 WIDTH=100%>.*?</table>#si", $content);

        $arr   = $this->parse_table($data);

        $data  = array();
        foreach ($arr as $row) {
            if (count($row)>1) {
                $data[] = $row;
            }
        }

        $items = $this->createstruct($data);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

