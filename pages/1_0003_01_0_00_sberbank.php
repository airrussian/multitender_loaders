<?php
/**
 * http://sberbank-ast.ru/PurchaseList.aspx 
 * TODO перепроверить баг http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=572276
 * 2010.08.05 - смена дизайна и логики
 * @author Paul-labor@yandex.ru
 * Требует ремонта, не может получить 2-ю страницу на krsk1
 */
class loader_1_0003_01_0_00_sberbank extends loader_1_0000_02_0_00_temp {

    public $base_url = 'http://sberbank-ast.ru/';
    public $list_link = 'http://sberbank-ast.ru/purchaseList.aspx';
    public $parser_name = 'parser_1_0003_01_0_00_sberbank';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'price' => 'maybenull',
            'customer' => 'maybenull',
            'customer_address' => 'maybenull',
            'customer_email' => 'maybenull',
            'customer_phone' => 'maybenull',
            'date_publication',
            'date_conf',
            'date_end',
    );

    public $fields_add     = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass  = false;
    public $item_rewrite   = false;
    public $page_last      = 10;
    //public $parser_name_detail = 'parser_1_0003_01_0_00_sberbank_detail_temp';
    public $parser_name_detail = 'parser_1_0003_01_0_00_sberbank_detail';
    public $parser_name_report = 'parser_1_0003_01_0_00_sberbank_report';

    protected $run_detail_order = 'date_conf ASC';

    //public $sleep_list = 1;

    function list_stop(array $list) {
        return $this->list_stop_date_conf($list);
    }

    function test_run_report($id=53140) {
        $this->parser = new $this->parser_name_report;
        $this->parser->loader = & $this;

        $content = $this->parser->get_detail($id);
        if (!$content) { return false; }
        $history = $this->parser->get_history($content);

        foreach ($history as $document) {

            $document = $this->parser->get_parse($document);

            $doc = new doc();
            $doc->name          = $document['name'];
            $doc->internal_id   = $document['internal_id'];
            $doc->detail_link   = $document['detail_link'];
            $doc->date_add      = $doc->DB()->BindTimeStamp(time());
            $doc->item_id       = $id;
            $doc->site_id 	= $this->site_id;
            $doc->save();
        }
    }

    function run_report($id = null) {
        $item = new item();
        $date_end = $item->DB()->DBDate(time() - 7*24*3600);

        if (!$item->Load("site_id = $this->site_id AND detail_count_error = 0 AND " .
        "detail_run_time IS NULL AND date_end <= $date_end AND " .
        "date_report IS NULL ORDER BY date_end DESC")) {
            if (!$item->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
            "detail_run_time IS NULL AND date_end <= $date_end AND " .
            "date_report IS NULL ORDER BY detail_count_error ASC, id DESC")) {
                exit('NO NEW');
            }
        }

        $item->detail_count_error ++; // при успехе убавим
        $item->detail_count_load  ++;
        $item->detail_run_time = $item->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item->Save();

        $this->debug("ID = $item->id");

        $this->parser = new $this->parser_name_report;
        $this->parser->loader = & $this;

        $content = $this->parser->get_detail($item->internal_id);
        if (!$content) { return false; }
        $history = $this->parser->get_history($content);

        foreach ($history as $doc) {

            $data = array(
                    'filename' => $report['name'],
                    'md5'      => md5($content),
                    'size'     => strlen($content),
            );
            // 4444 - протоколы
            $this->doc_save($data, $item->id, 4444);

            /*
            $item->price_best    = $parse['price_best'];
            $item->members_count = $parse['members_count'];
            $item->date_report   = $item->DB()->BindTimeStamp(time());
            */
        }

        $item->detail_run_time = NULL; // при успехе обнулить
        $item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }
        $item->Save();

        return true;
    }
}

class parser_1_0003_01_0_00_sberbank extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'purchID' => 'internal_id',
            'purchCode' => 'num',
            'purchName' => 'name',
            'purchAmount' => 'price|to_price',
            //'custContacts_custName'         => 'customer',
            //'custContacts_custPostAddress'  => 'customer_address',
            //'custContacts_custPlace'        => 'customer_address2',
            //'orgContacts_orgPostAddress'    => 'customer_address3',
            //'orgContacts_orgPlace'          => 'customer_address4',
            //'custContacts_custEMail'        => 'customer_email',
            //'custContacts_custPhones'       => 'customer_phone',
            'publicDateStr' => 'date_publication|date_convert',
            'auctionBeginDateStr' => 'date_conf|date_convert',
            'requestDateStr' => 'date_end|date_convert',
    );
    /*
      protected $colomn = array(
      '0'                             => 'internal_id',
      'Код аукциона'                  => 'num',
      'Название аукциона'             => 'name',
      'Место поставки'                => 'customer_address',
      'цена контракта'                => 'price|to_price',
      'Организатор'                   => 'customer',
      'Дата объявления'               => 'date_publication|date_convert',
      'Дата проведения аукциона'      => 'date_conf|date_convert',
      'Дата окончания приема заявок'  => 'date_end|date_convert',
      );
    */

    function list_get_page($link, $page = 1) {
        //$sleep = mt_rand(10, 100);
        //$this->loader->debug("sleep $sleep s");
        //sleep($sleep);

        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        /*
        if (empty($this->asp_params_post)) {
            $emul_br = $this->emul_br_init($link);

            // FIXME bug in emul_br, no replace
            // FIXME http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=572276
            //$emul_br->headers = array();
            //$emul_br->SetBrowser('FX1_RU');

            $emul_br->exec();
            $this->loader->debug($emul_br->GetAllHeaders());
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }
        */
        $emul_br = $this->emul_br_init($link);

        //$emul_br->headers = array();
        //$emul_br->SetBrowser('FX1_RU');
        if ($this->asp_params_post) {
            $emul_br->SetCookie($this->asp_params_cookies);
            $emul_br->SetPost($this->asp_params_post);
            $emul_br->SetPost(array(
                '__EVENTTARGET' => 'ctl00$ctl00$phWorkZone$nextPage',
                'ctl00$ctl00$phWorkZone$xmlFilter' => '<query><purchstate>public</purchstate></query>',
                'ctl00$ctl00$phWorkZone$xmlData' => '<data></data>',
                'ctl00$ctl00$phWorkZone$phDataZone$purchID' => '54944',
            ));
        }

        $emul_br->exec();
        $this->loader->debug($emul_br->GetAllHeaders());

        $this->asp_set_params($emul_br);

        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        //print_r($parse);
        //exit(0);

        foreach ($parse['items'] as $item) {
            if (empty($item)) {
                continue;
            }
            $item = $this->list_set_colomn($item, $this->colomn);

            $items[] = $item;
        }

        $return = array(
                'page_total' => $this->loader->page_last, //$parse['page_total'],
                'page_now' => null, //$parse['page_now'],
                'items_total' => null,
                'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $data = $content_dom->find('input[name=ctl00$ctl00$phWorkZone$xmlData]', 0)->value;

        $ret['page_total'] = null;

        $content_dom->__destruct();

        if (empty($data)) { return null; }

        $ret['items'] = $this->create_xml($data);

        return $ret;
    }

    function create_xml($text) {
        $str = preg_replace(array('#&lt;#siu', '#&gt;#siu'), array('<', '>'), $text);
        $xml = simplexml_load_string($str);
        $i = 1;
        $return = array();
        foreach ($xml->row as $purchase) {
            foreach ($purchase->children() as $tags) {
                if ((string) $tags) {
                    $return[$i][$tags->getName()] = (string) $tags;
                } else {
                    // Второй уровень больше и не надо | поэтому не рекурсивно
                    foreach ($tags->children() as $tag) {
                        $return[$i][$tags->getName() . "_" . $tag->getName()] = (string) $tag;
                    }
                }
            }
            $i++;
        }
        return $return;
    }

}

/**
 * Выдерает лишь регион
 */
class parser_1_0003_01_0_00_sberbank_detail_temp extends parser_1_0003_01_0_00_sberbank {

    protected $detail_link = 'http://sberbank-ast.ru/purchaseview.aspx?id=';

    function detail_all($id) {

        $this->detail_link = 'http://sberbank-ast.ru/purchaseview.aspx?id=' . $id;

        $content = $this->emul_br_get_body( $this->detail_link );
        $this->loader->debug( $this->detail_link );

        $data = preg_get('#<textarea\s+name="ctl00\$ctl00\$phWorkZone\$xmlData".*?>(.*?)</textarea>#usi', $content);

        $xml = $this->create_xml_detail($data);

        //orgcontacts_orgplace?
        $return['db']['customer_address'] = $xml['orgcontacts_orgpostaddress'];
        $return['db']['region_id']        = $this->loader->geocoder_by_all($return['db']['customer_address']);


        $return['db']['customer_email']   = $xml['orgcontacts_orgemail'];
        $return['db']['customer_phone']   = $xml['orgcontacts_orgphones'];
        $return['db']['customer_person']  = $xml['orgcontacts_orgcontactperson'];

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        //[purchbranch_purchbranchname] => Средства наземного, воздушного и водного транспорта. Услуги транспорта и связи
        //[purchclassificator_purchokdp] => 3410131
        //[purchclassificator_purchokdpname] => Автомобили легковые большого класса для индивидуального и служебного пользования

        return $return;
    }

    /**
     * Ключи в нижнем регистре
     * @param string $text строка с xml
     * @return array
     */
    protected function create_xml_detail($text) {
        $str = preg_replace(array('#&lt;#siu', '#&gt;#siu'), array('<', '>'), $text);
        $xml = simplexml_load_string($str);

        $return = array();

        foreach ($xml->children() as $tags) {
            if ((string) $tags) {
                $return[strtolower($tags->getName())] = (string) $tags;
            } else {
                // Второй уровень больше и не надо | поэтому не рекурсивно
                foreach ($tags->children() as $tag) {
                    $return[strtolower($tags->getName() . "_" . $tag->getName())] = (string) $tag;
                }
            }
        }
        return $return;
    }

}

/**
 * Полноценный класс, формирует other
 */
class parser_1_0003_01_0_00_sberbank_detail extends parser_1_0003_01_0_00_sberbank_detail_temp {

    public $detail_sort = array(
            'Сведения об организаторе торгов \/ Наименование организации'        => 'customer',
            'Сведения об организаторе торгов \/ Место нахождения'                => 'customer_address',
            'Сведения об организаторе торгов \/ Адрес электронной почты'         => 'customer_email',
            'Сведения об организаторе торгов \/ Номера контактных телефон'       => 'customer_telephone',
            'Документы \/ Приложенные файлы'                                     => 'delete',
            'События в хронологическом порядке'                                  => 'delete',
            'Сведения об организаторе торгов \/ Специализированная организация'  => 'delete',
    );

    function detail_all($id=3434) {

        $this->detail_link = 'http://sberbank-ast.ru/purchaseview.aspx?id=' . $id;

        $content = $this->emul_br_get_body($this->detail_link);
        $this->loader->debug($this->detail_link);

        $data = preg_get('#<textarea\s+name="ctl00\$ctl00\$phWorkZone\$xmlData".*?>(.*?)</textarea>#usi', $content);
        $xml = $this->create_xml_detail($data);
        $arr = $this->detail_parse_pre($content, $xml);

        $return = $this->detail_sort_3($arr);

        $return['db']['region_id'] = $this->loader->geocoder_by_all($return['db']['customer_address']);

        $return['content'] = $this->detail_content_cache($content);

        return $return;
    }

    function key_clear($key) {
        return preg_replace("#(leaf|node):#si", "", $key);
    }


    function detail_parse_pre($content, $xml) {

        $content_dom = str_get_html($content);

        $XMLContainer = $content_dom->find("div#XMLContainer", 0);

        $arr = array();
        foreach ($XMLContainer->find("table.dt") as $tbl) {
            $razdel = $this->text_clear_all($tbl->find("td.th", 0)->innertext);
            if ($tbl->getAttribute("content")) {
                $xmlraz = $this->key_clear($tbl->getAttribute("content"));
            } else {
                $xmlraz = null;
            }
            foreach ($tbl->find("td.fr") as $fr) {
                $categ = $this->text_clear_all($fr->innertext);
                $name = $fr->next_sibling();
                if ($name->getAttribute("content")) {
                    if ($xmlraz) {
                        $arr[$razdel.' / '.$categ] = $xmlraz.'_'.$this->key_clear($name->getAttribute('content'));
                    } else {
                        $arr[$razdel.' / '.$categ] = $this->key_clear($name->getAttribute('content'));
                    }
                } else {
                    $arr[$razdel.' / '.$categ] = $this->text_clear_all($name->innertext);
                }
            }
        }

        foreach ($arr as $key => &$row) {
            foreach ($xml as $node => $value) {
                $row = preg_replace("#$node#si", $value, $row);
            }
            /* FIX ME сопоставление HTML и XML, что музора выкинуть */
            if (preg_match("#controreder|RUB|currency|function|purchbranchname|нет|(\[\])#sui", $row ) || $row=="") {
                unset($arr[$key]);
            }
        }

        $content_dom->clear();

        return $arr;
    }
}

class parser_1_0003_01_0_00_sberbank_report extends parser_1_0003_01_0_00_sberbank_detail_temp {

    protected $detail_link      = 'purchaseview.aspx?id=';
    protected $document_link    = 'ViewDocument.aspx?id=';
    protected $download_link    = 'Download.aspx?fid=';

    function get_detail($id) {
        $link = $this->loader->base_url . $this->detail_link . $id;
        $content = $this->emul_br_get_body($link);
        return $content;
    }

    function get_document($id) {
        $link = $this->loader->base_url . $this->document_link . $id;
        $content = $this->emul_br_get_body($link);
        return $content;
    }

    function get_history($content) {
        $content = preg_get('#<textarea\s+name="ctl00\$ctl00\$phWorkZone\$xmlData".*?>(.*?)</textarea>#usi', $content);
        $content = preg_replace(array('#&lt;#siu', '#&gt;#siu'), array('<', '>'), $content);

        $xml = simplexml_load_string($content);
        $chngLog = $xml->xpath('logs/chngLog');

        $result = array();
        foreach ($chngLog as $log) {

            $id = (int) $log->docid;
            $name = (string) $log->comment;
            $detail_link = $this->document_link . $id;
            
            $result[] = array(               
                'internal_id'       =>  $id,
                'name'              =>  $name,
                'detail_link'       =>  $detail_link,
            );

        }

        return $result;
    }

    function get_parse( $doc ) {
        $content = $this->emul_br_get_body($this->loader->base_url . $doc['detail_link']);
        if (!$content) { return false; }

        if (preg_get("#Протокол проведения ЭА#", $doc['name'])) {
            $doc['internal_id'] = 4444;
        }

        $d = preg_get("#<a.*?Download.aspx\?fid=(\S{8}.\S{4}.\S{4}.\S{4}.\S{12}).*?>(.*?)</a>#si", $content);
        if (empty($d)) { return $doc; }

        $doc['detail_link'] = $this->download_link . $d[0];
        $doc['name'] = $d[1];

        return $doc;

    }

    function get_report_one($id) {
        $content = $this->emul_br_get_body($this->report_link . $id);
        $this->loader->debug($this->report_link . $id);

        $doc = preg_get("#<a.*?Download.aspx\?fid=(\S{8}.\S{4}.\S{4}.\S{4}.\S{12}).*?>(.*?)</a>#si", $content);
        if (empty($doc)) { return false; }

        $return['detail_link'] = $this->dnload_link . $doc[0];
        $return['name'] = $doc[1];       
        $return['internal_id'] = abs(crc_p($doc[0]));

        return $return;
    }

}
