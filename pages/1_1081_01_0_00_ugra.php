<?php
/**
 * @version > Ханты-Мансийский автономный округ - Югра (мун.заказ)
 * @var     > http://ozhmao.ru/
 * @param   > run_list, run_detail
 * @todo    > 09.11.2010 - новая площадка для гос.заказа Мурманской области. Старая осталась для Муниципального заказа [5653]
 * @author  > airrussia@mail.ru
 */

class loader_1_1081_01_0_00_ugra extends loader_1_0000_02_0_00_temp_many {
    public $base_url            = 'http://oktregion.ru/';
    public $list_link           = 'http://oktregion.ru/mzak.php';
    public $parser_name         = 'parser_1_1081_01_0_00_ugra';
    public $parser_name_detail  = 'parser_1_1081_01_0_00_ugra_detail';

    public $fields_list = array(
            'name',
            'type',
            'internal_id',
            'num'   => 'maybenull',
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    protected $pages_list = array(
            array(
                'link' => 'mzak.php?top=100015',
                'type' => 'открытый аукцион',
            ),
            array(
                'link' => 'mzak.php?top=100004',
                'type' => 'запрос котировок',
            ),
            array(
                'link' => 'mzak.php?top=100005',
                'type' => 'открытый конкурс',
            ),
    );

}

class parser_1_1081_01_0_00_ugra extends parser_1_0000_02_0_00_temp_many {

    function list_get_page( $link, $page = 1 ) {

        $ftime = strtotime("-1 week"); $ttime = strtotime("+1 week");
        $link.= "&from_day=".date("d", $ftime)."&from_month=".date("m",$ftime)."&from_year=".date("Y", $ftime);
        $link.= "&to_day=".date("d", $ttime)."&to_month=".date("m", $ttime)."&to_year=".date("Y", $ttime);
        $link.= "&number=&doc_name=&zakazchik=0&pos=0";
        $link.= "&lim=".($page-1);

        $this->loader->debug("link = " . $link );

        $emul_br = $this->emul_br_init($link . ($page-1));
        $emul_br->exec();
        return $emul_br->GetBody();
    }

    protected $colomn = array(
            'P_name'            => 'name|clear_all',
    );

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = (int) preg_get("#mzak\.php\?id=(\d+)#si", $item['name_src']);
            $item['num'] = preg_get("#№\s?(\d+)#si",  $item['name_src']);
            
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $items = preg_get_all("#<a href=\"mzak.php\?id=\d+&lim=\d+\">.*?a>#", $content);
        $ret['items'] = array();
        foreach ($items as $item) {
            $ret['items'][]['P_name'] = $item;
        }

        $paginator = preg_get("#<p>.*?Страница.*?</p>#siu", $content);

        if ($paginator) {
            $ret['page_now']        = preg_get("#<b>(\d+)</b>#si", $paginator);
            $ret['items_total']     = preg_get("#Показано \d+\-\d+ из (\d+).#siu", $paginator);
            $ret['page_total']      = floor(($ret['items_total']-1) / 20)+1;
        } else {
            $ret['page_now']        = 1;
            $ret['items_total']     = count($ret['items']);
            $ret['page_total']      = 1;
        }

        return $ret;
    }
}

// FIX ME...
class parser_1_1081_01_0_00_ugra_detail extends parser_1_0000_02_0_00_temp_many {

    function detail_all($id) {
        $link         = "http://oktregion.ru/mzak.php?id=";
        $this->loader->debug($link . $id);
        $content      = $this->emul_br_get_body($link . $id);
        $return = $this->list_parse_pre($content);
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $news = preg_get("#<td class=\"news\">.*?</td>#sui", $content);

        $docs = preg_get_all("#<a.*?download.php\?id=\d+.*?a>#sui", $news);

        $return['db']['date_publication'] = (int) $this->text_date_convert(preg_get("#<h2>.*?(\d{2}\.\d{2}\.\d{4}).*?</h2>#siu", $news));
        foreach ($docs as $doc) {
            $return['docs'][] = array(
                'name'          => $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#si", $doc)),
                'detail_link'   => preg_get("#href=['\"](.*?)['\"]#si", $doc),
                'internal_id'   => abs(crc_p(preg_get("#href=['\"](.*?)['\"]#si", $doc)))
            );
        }

        return $return;
    }
}