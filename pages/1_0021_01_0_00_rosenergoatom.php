<?php
/**
 * Росэнергоатом
 * 2010.07.29 - написание
 * 2010.08.10 - отдано на доработку
 * 2010.08.11 - добработан.
 * 2010.08.12 - запуск
 *
 */
class loader_1_0021_01_0_00_rosenergoatom extends loader_1_0000_05_0_00_many {
    public $base_url            = 'http://www.rosenergoatom.ru/';
    public $parser_name         = 'parser_1_0021_01_0_00_rosenergoatom';

    public $fields_list = array(
            'num' => 'maybenull',
            'customer', 
            'name',
            'date_conf' => 'maybenull',
            'date_end'  => 'maybenull',
            'internal_id',
            'type',
            'doc',
    );

    protected $pages_list = array(
            array(
                'link' => 'rus/about/contests-and-tenders/ktf/announce/2011/'
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/rea_contests/',
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/ktf/',
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/kpf/',
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/ntc/',
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/kdt/',
            ),
            array(
                'link' => 'rus/about/contests-and-tenders/marketplace/',
            ),
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );


    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0021_01_0_00_rosenergoatom extends parser_1_0000_05_0_00_many {

    private $total_page = 1;

    protected $colomn = array(
            '№ заказа'                  => 'num',
            'Организатор процедуры'     => 'customer|clear_all',
            'Вид процедуры'             => 'type|clear_all',
            'Предмет процедуры'         => 'name|clear_all',
            'Дата и время окончания'    => 'date_end|clear_all|date_convert_text_full',
            'Дата и время вскрытия'     => 'date_conf|clear_all|date_convert_text_full',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $content = $this->emul_br_get_body( $link );

        $tmp = str_get_html($content);
        $contentinner = $tmp->find("div.divContentInner", 0);
        if ($contentinner->find("ul", 0)) {
            $link = $contentinner->find("ul", 0)->find("a", $page-1)->getAttribute("href");
            $link = mb_substr($link, 1);
            $this->loader->debug("\n\nLINK = ".$this->loader->base_url.$link);
            $content = $this->emul_br_get_body( $this->loader->base_url.$link);
            $this->total_page = 2;
            return $content;
        } else {
            $this->total_page = 1;
            return $content;
        }
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $docs = preg_get_all("#<a.*?href.*?a>#si", $item['name_src']);

            $item['name'] = $this->text_clear_all($docs[0]);
            $item['internal_id'] = abs(crc_p($item['name'].$item['num']));

            if (preg_match("#Отказ#siu", $item['type'])) {
                continue;
            }
            
            $i = 0;
            foreach ($docs as $doc) {
                $item['doc'][$i]['name'] = $this->text_clear_all($doc);
                $item['doc'][$i]['detail_link'] = preg_get("#href=['\"](.*?)['\"]#si", $doc);
                $item['doc'][$i]['internal_id'] = abs(crc_p($item['doc'][$i]['name'].$item['doc'][$i]['detail_link']));
                $i++;
            }

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => $this->total_page,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find("div.divContentInner", 0)->find("table.t", 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $content_dom->__destruct();

        return $ret;
    }
}

