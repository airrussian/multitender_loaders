<?php
/**
 * ТНК-BP
 * 21.07.2010 - Написание
 * 09.08.2010 - Запуск
 */
class loader_1_0010_01_0_00_tnkbp extends loader_1_0000_01_0_00_one {
    public $base_url    = 'http://www.tnk-bp.ru/';
    public $list_link   = 'http://www.tnk-bp.ru/procurement/tenders/?PAGEN_1=';
    public $parser_name = 'parser_1_0010_01_0_00_tnkbp';

    public $fields_list = array(
            'name',
            'customer',
            'date_publication',
            'date_end',
            //'detail_link_prefix', ссылка идет на список
            'type',
            'internal_id',
            'doc',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    public $page_last = 2;
}

class parser_1_0010_01_0_00_tnkbp extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            0                   => 'type|clear_all',
            'тема'              => 'name|clear_all',
            'дата публикации'   => 'date_publication|date_convert',
            'предприятие'       => 'customer|clear_all',
            'окончание сбора'   => 'date_end|date_convert',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.$page );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end'] < date("Ymd", time())) { continue; } 

            $item['name']               = $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#si", $item['name_src']));
            $item['detail_link_prefix'] = preg_get("#href=['\"](.*?)['\"]#si", $item['name_src']);
            $item['internal_id']        = $this->make_internal_id($item['detail_link_prefix']);

            $item['doc'][] = array(
                'internal_id'  => $item['internal_id'],
                'detail_link'  => $item['detail_link_prefix'],
                'name'         => 'Документация',
            );

            $item['detail_link_prefix'] = '/procurement/tenders/?all_active=all';

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $this->loader->page_last,
                'page_now'    => $parse['page_now'],
                'items_total' => count(items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        
        $content_dom = str_get_html($content);

        $table = $content_dom->find('table.table-outer', 0)->outertext;

        $arr = $this->parse_table($table);

        $ret['items']       = $this->createstruct($arr);
        $ret['page_now']    = (int) (preg_get("#(\d+)#", $content_dom->find("div.paging", 0)->find("span.active", 0)->innertext)/100) + 1;
        $ret['page_total']  = (int) max(preg_get_all("#PAGEN_1=(\d+)#si", $content_dom->find("div.paging", 0)->innertext));

        $content_dom->__destruct();

        return $ret;
    }
}

