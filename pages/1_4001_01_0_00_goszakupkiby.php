<?php
/**
 * Годовые планы государственных закупок - Портал государственных закупок
 * @author airrussian@mail.ru
 */

class loader_1_4001_01_0_00_goszakupkiby extends loader_1_0000_01_0_00_one {

    public $base_url    = 'http://www.goszakupki.by/';
    public $list_link   = 'http://www.goszakupki.by/auction/all?p=';
    public $parser_name = 'parser_1_4001_01_0_00_goszakupkiby';
    public $parser_name_detail = 'parser_1_4001_01_0_00_goszakupkiby_detail';
    
    public $fields_list = array(
            'internal_id',
            'name',
            'customer',
            'date_end',
            'price',
            'num' => 'maybenull',
   );

    public $fields_rewrite = array(
            'type'         => 'Открытый аукцион в электронной форме',
            'type_id'      => 6,
            'region_id'    => 400,
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = false;

    function geocoder_search_name($text) {
        $crc_a = morf_crc($text);

        $crc_a = array_unique($crc_a);
        $crc_a = array_filter($crc_a);

        if(empty($crc_a)) {
            return false;
        }

        $socr = array('Респ', 'край', 'обл', 'Аобл', 'округ', 'АО', 'г', 'пгт', 'п', 'с', 'д', 'mail');
        $all = $this->db->GetAll("SELECT region_id, name, socr FROM geocoder ".
                "WHERE crc_name IN(".implode(',', $crc_a). ") AND region_id>300 AND region_id<400 ".
                "GROUP by socr,crc_name ORDER BY FIELD (socr, '".implode(array_reverse($socr), "','")."') DESC");
        //print_r($first);
        $first = $all ? $all[0]['region_id'] : false;
        return $this->geocoder_region_const($first);
    }


}

/**
 * LIST
 */
class parser_1_4001_01_0_00_goszakupkiby extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Описание предмета аукциона'    => 'name|clear_all',
            'Начальная цена'                => 'price|clear_all|to_price',
            'Дата окончания'                => 'date_end|clear_all|date_convert',
            'Организация'                   => 'customer|clear_all',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = (int) preg_get("#<a.*?view/(\d+)#", $item['name_src']);
            $item['name'] = preg_get("#<a.*?>(.*?)</a>#si", $item['name_src']);
            $item['price'] = $this->text_to_price(0.005 * $item['price']);
            $item['num'] = preg_get("#<strong>.*?\((.*?)\)</strong>#si", $item['name_src']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $table = $content_dom->find("table#auctions-list", 0)->outertext;
        $arr = $this->parse_table($table);

        $i = -1;
        foreach ($arr as $key => $row) {
            if ($key % 3==0) { $i++; }
            foreach ($row as $ind => $row) {
                if ($ind % 2 == 0) { $name = $row; }
                if ($ind % 2 == 1) {
                    $items[$i][$name] = $row;
                }
            }
        }

        $paging = $content_dom->find("div.paging", 0);
        $page_total = (int) max(preg_get_all("#<a.*?\?p=(\d+).*?</a>#si", $paging->innertext));
        $page_now = (int) $paging->find("span.sel", 0)->innertext;

        $content_dom->clear();

        $return = array (
                'page_total'  => $page_total,
                'page_now'    => $page_now,
                'items_total' => preg_get("#<p>Всего: (\d+)</p>#si", $content),
                'items'       => $items,
        );
        return $return;

    }
}

class parser_1_4001_01_0_00_goszakupkiby_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link = 'http://www.goszakupki.by/auction/all/view/';

     public $detail_sort = array(
        'Юридический адрес'                 => 'customer_address|clear_all',
        'Дата вскрытия конвертов'           => 'date_end|clear_all|date_convert',
        '<'                                 => 'delete',
    );

    function detail_get($id) {
        $this->loader->debug("link detail: {$this->detail_link}$id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }


    function arr_double2one(array $arr) {
        $return = array(); $i=0;
        foreach ($arr as $row) {

            if (count($row)==2) {
                $key = $this->text_clear_all($row[0]);
                $val = $row[1];

            } else {
                $val = $row[0];
                $key = $i++;
            }
            $return[$key] = $val;
        }
        return $return;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $return = $this->detail_sort_3($parse, true);

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['other']['Место нахождения организации']);
        if (!$return['db']['region_id']) {
            $return['db']['region_id'] = $this->loader->region_id;
        }

        return $return;
    }

    function detail_parse_pre($content) {

        $content_dom = str_get_html($content);
        $table = $content_dom->find("div#auctBlockCont", 0)->find("table.w100", 0)->outertext;

        $table = preg_replace("#<th colspan.*?</th>#si", "", $table);
        $table = preg_replace("#<tr>\s+</tr>#si", "", $table);

        $arr = $this->parse_table($table);
        foreach ($arr as $row) {
            if (count($row)==1) {
                unset($arr[key($row)]);
            }
        }
        $arr = $this->arr_double2one($arr);

        $content_dom->clear();

        return $arr;
    }

}
