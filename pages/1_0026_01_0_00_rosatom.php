<?php
/**
 * РосАтом
 * 2010.08.05 - написание
 * 2010.08.12 - запуск
 * 2010.08.17 - ремонт, регион не всегда определятся, цена то же
 */
class loader_1_0026_01_0_00_rosatom extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://zakupki.rosatom.ru/';
    public $list_link           = 'http://zakupki.rosatom.ru/Web.aspx?node=currentorders&ostate=P&page=';
    public $parser_name         = 'parser_1_0026_01_0_00_rosatom';   


    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'date_end',
            'customer_address',
            'customer',
            'num',
            'region_id'           => 'maybenull',
            'detail_link_prefix',
            'price'               => 'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    public $page_last = 61;

    function list_stop(array $list) {
        return $this->list_stop_date_conf($list);
    }

}

class parser_1_0026_01_0_00_rosatom extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Цена'              => 'name',
            'Заказчик'          => 'customer|clear_all',
            'Дата публикации'   => 'date_publication|date_convert',
            'Дата окончания'    => 'date_end|date_convert',
            'Статус'            => 'status|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $link = $link . $page;
        $this->current_link = $link;
        $this->loader->debug($link);
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end'] < date("Ymd")) {
                continue;
            }

            $nn = preg_get("#<a href=['\"](.*?)['\"]>(.*?)<br.*?>(.*?)</a>#si", $item['name']);

            $tt = preg_get("#<span.*?>Способ размещения:(.*?)<br.*?Место поставки:(.*?)<br#si", $item['name']);

            $pp = preg_get("#<td style=\"text-align:right; border:0\">(.*?)</td>#si", $item['name']);

            $item['price'] = $this->text_to_price(preg_replace("#,#", "", $pp));

            $item['detail_link_prefix'] = preg_get('/\d+/', $nn[0]);

            $item['num'] = trim($nn[1]);
            $item['name'] = $nn[2];

            $item['type'] = $tt[0];

            $cc = preg_get("#(.*?)<br.*?<span.*?>(.*?)</span>#si", $item['customer_src']);

            $item['customer']    = $cc[0];
            $item['customer_address'] = $cc[1];
            $item['region_id']   = $this->loader->geocoder_auto($item['customer_address']);
            $item['internal_id'] = $this->make_internal_id($item['detail_link_prefix']);

            $item['date_conf']    = $item['date_end'];

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table#search_results_table", 0);

        $arr = $this->parse_table($tenders->outertext);

        for ($i=0; $i<count($arr[0])-1; $i++) {
            $arr[0][$i] = $arr[0][$i+1];
        }
        unset($arr[0][7]);

        $ret['items'] = $this->createstruct($arr);

        $paging = $content_dom->find("div.paging", 0);

        $ret['page_now']   = preg_get("#\d+#si", preg_replace("#<a.*?a>#si", "", $paging->innertext));
        $ret['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paging->innertext));

        $content_dom->__destruct();

        return $ret;
    }

}
