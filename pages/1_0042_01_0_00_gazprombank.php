<?php
/**
 * Газпромбанк
 * Написание: 31.01.2011
 */
class loader_1_0042_01_0_00_gazprombank extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.gazprombank.ru/';
    public $list_link           = 'http://www.gazprombank.ru/tenders/list/index.php?all_region=y';
    public $parser_name         = 'parser_1_0042_01_0_00_gazprombank';
    public $parser_name_detail  = 'parser_1_0042_01_0_00_gazprombank_detail';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0042_01_0_00_gazprombank extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        '0' => 'date_end|clear_all|date_convert_text_full',
        '1' => 'name|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['date_end'] = $this->text_date_convert_text_full(preg_get("#<p>До (\d+ \S+ \d+) г\.</p>#sui", $item['date_end_src']));
            $item['internal_id'] = (int) preg_get("#list/(\d+)#si", $item['name_src']);
            if ($item['date_end'] < date("Ymd")) {
                continue;
            }
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        //$content = $this->text_from_win($content);
        $content_dom = str_get_html($content);

        $tendery = $content_dom->find("table.tendery", 0);
        $arr = $this->parse_table($tendery->outertext);

        $ret['page_now']    = 1;
        $ret['page_total']  = 1;

        $ret['items'] = $arr;
        
        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0041_01_0_00_euroset_detail extends parser_1_0000_01_0_00_one {

    public $item_now;

    protected $detail_link = '';

    function detail_get($id) {
        $this->detail_link = "http://euroset.ru/corp/tender/tender/$id/";
        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $return['content'] = $this->detail_content_cache($content);

        $content_dom = str_get_html($content);
        $html = $content_dom->find("div#component", 0)->find("table", 1)->find("td", 0)->innertext;
        $content_dom->clear();


        $html = tidy_repair_string($html, $this->tidy_config);
        $html = preg_replace("#<a.*?</a>#si", "", $html);
        $html = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $html);
        $html = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $html);
        $html = preg_replace(array("#<script.*?</script>#si", "#<style.*?</style>#si", "#<!\[CDATA.*?</script>#si"), "", $html);
        $html = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $html);
        $html = preg_replace("#Этот e-mail адрес защищен.*?Javascript#siu", "", $html);
        $return['html'] = $html;

        $de = preg_get("#<strong>.*?(\d+ \D+ \d{4}) года.*?</strong>#sui", $html);
        $return['db']['date_end'] = $this->text_date_convert_text_full($de);


        return $return;
    }
}