<?php

/*
 * ОАО ИСС имени академика М.Ф. Решетнёва
*/

class loader_1_0061_01_0_00_npopm extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.npopm.ru/';
    public $list_link           = 'http://www.npopm.ru/?cid=tender';
    public $parser_name         = 'parser_1_0061_01_0_00_npopm';
    public $parser_name_detail  = 'parser_1_0061_01_0_00_npopm_detail';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0061_01_0_00_npopm extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'P_name'               =>  'name|clear_all',
        'P_date'               =>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#acid=(\d+)#si", $item['name_src']);

            $items[$k] = $item;
        }       

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_dom($content);
       
        $tenders = $html->find("div#News", 0)->find("dl", 0);

        foreach ($tenders->find("dt") as $dt) {
            $items[] = array(
                'P_date'    =>  $dt->innertext,
                'P_name'    =>  $dt->next_sibling()->innertext,
            );
        }

        $html->clear();

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;
        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0061_01_0_00_npopm_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    public $detail_sort = array(
        'Наименование заказчика'            =>  'customer|clear_all',
        'Место нахождения заказчика'        =>  'customer_address|clear_all',
        'Дата, время и место вскрытия конвертов'   =>  'date_end|clear_all|date_convert',
        'Дата подведения итогов конкурса'   =>  'date_conf|clear_all|date_convert',
    );

    function detail_get($id) {

        $this->detail_link = "http://www.npopm.ru/?cid=tender&acid=$id&template=kotirovka.html";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $docs = $parse['docs']; unset($parse['docs']);

        $return = $this->detail_sort_3($parse);

        $return['docs'] = $docs;

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        foreach ($return['other'] as $k => $v) {
            if (is_null($v)) { unset($return['other'][$k]); }
        }

        return $return;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);
        $dom = str_get_html($content);

        $detail = $dom->find("div#News", 0);

        $konkurs = $detail->find("table.konkurs", 0)->outertext;
        $konkurs = $this->parse_table($konkurs);

        $return = array();
        foreach ($konkurs as $konkur) {
            if (isset($konkur[1]) && !empty($konkur[1])) {
                $return['Основна информация / '.$konkur[0]] = $konkur[1];
            }
        }

        $dl = $detail->find("dl.dlList2Col", 0);
        $docs = array();
        foreach ($dl->find("dt") as $dt) {
            $detail_link = $dt->next_sibling()->find("a", 0)->getAttribute("href");
            $docs[] = array(
                'name'          =>  $dt->innertext,
                'detail_link'   =>  $detail_link,
                'internal_id'   =>  abs(crc_p($detail_link)),
            );
        }

        $return['docs'] = $docs;

        $dom->clear();

        return $return;
    }


}

