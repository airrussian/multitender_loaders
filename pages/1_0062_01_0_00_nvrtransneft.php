<?php

/*
 * ОАО ИСС имени академика М.Ф. Решетнёва
*/

class loader_1_0062_01_0_00_nvrtransneft extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.nvr.transneft.ru/';
    public $list_link           = 'http://www.nvr.transneft.ru/index.php?sub=11';
    public $parser_name         = 'parser_1_0062_01_0_00_nvrtransneft';
    public $parser_name_detail  = 'parser_1_0062_01_0_00_nvrtransneft_detail';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'doc',
            'price',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0062_01_0_00_nvrtransneft extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'P_name'               =>  'name',
        'P_info'               =>  'info',
        'P_doc'                =>  'doc',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['name'] = $this->text_clear_all(preg_replace("#.*?\|#si", "", $item['name']));
            $date = $this->text_clear_all(preg_get("#Период проведения.*?</div>#si", $item['info']));

            $date = preg_get_all("#(\d{2}.\d{2}.\d{4})#si", $date);

            $item['date_publication'] = $this->text_date_convert($date[0]);
            $item['date_end']         = $this->text_date_convert($date[1]);
            $item['price']            = $this->text_to_price(preg_get("#Стоимость:</b>.*?</div>#si", $item['info']));

            if ($item['date_end']<date("Ymd")) { continue; }

            $item['internal_id']      = abs(crc_p($item['name']));

            $docs = preg_get_all("#<a.*?/a>#si", $item['doc']);
            $item['doc'] = array();
            foreach ($docs as $doc) {
                $detail_link = preg_get("#href=\"(.*?)\"#si", $doc);
                $item['doc'][] = array(
                    'internal_id'   =>  abs(crc_p($detail_link)),
                    'name'          =>  $this->text_clear_all($doc),
                    'detail_link'   =>  $detail_link
                );
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $html = str_get_dom($content);
       
        $tenders = $html->find("div#main_content", 0);

        foreach ($tenders->find("div.smi3") as $tender) {
            $items[] = array(
                'P_name'  =>  $tender->find("div.tip4", 0)->innertext,
                'P_info'  =>  $tender->find("div.tip4", 0)->next_sibling()->innertext,
                'P_doc'   =>  $tender->find("div.tip4", 0)->next_sibling()->next_sibling()->outertext,
            );
        }

        $html->clear();

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;
        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0062_01_0_00_nvrtransneft_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    public $detail_sort = array(
        'Наименование заказчика'            =>  'customer|clear_all',
        'Место нахождения заказчика'        =>  'customer_address|clear_all',
        'Дата, время и место вскрытия конвертов'   =>  'date_end|clear_all|date_convert',
        'Дата подведения итогов конкурса'   =>  'date_conf|clear_all|date_convert',
    );

    function detail_get($id) {

        $this->detail_link = "http://www.npopm.ru/?cid=tender&acid=$id&template=kotirovka.html";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);

        $docs = $parse['docs']; unset($parse['docs']);

        $return = $this->detail_sort_3($parse);

        $return['docs'] = $docs;

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        foreach ($return['other'] as $k => $v) {
            if (is_null($v)) { unset($return['other'][$k]); }
        }

        return $return;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);
        $dom = str_get_html($content);

        $main_content = $dom->find("div#main_content", 0);

        $konkurs = $detail->find("table.konkurs", 0)->outertext;
        $konkurs = $this->parse_table($konkurs);

        $return = array();
        foreach ($konkurs as $konkur) {
            if (isset($konkur[1]) && !empty($konkur[1])) {
                $return['Основна информация / '.$konkur[0]] = $konkur[1];
            }
        }

        $dl = $detail->find("dl.dlList2Col", 0);
        $docs = array();
        foreach ($dl->find("dt") as $dt) {
            $detail_link = $dt->next_sibling()->find("a", 0)->getAttribute("href");
            $docs[] = array(
                'name'          =>  $dt->innertext,
                'detail_link'   =>  $detail_link,
                'internal_id'   =>  abs(crc_p($detail_link)),
            );
        }

        $return['docs'] = $docs;

        $dom->clear();

        return $return;
    }


}

