<?php
/**
 * Система информационной поддержки конкурентных закупок
 *
 * FIXME какова сортировка?
 *
 * 2010.07.19 - написание
 * 2010.08.09 - запуск
 */
class loader_1_0009_01_0_00_tzselektra extends loader_1_0000_02_0_00_temp {
    public $base_url    = 'http://tzselektra.ru/';
    public $list_link   = 'http://tzselektra.ru/?np=';
    public $parser_name = 'parser_1_0009_01_0_00_tzselektra';

    public $parser_name_detail = 'parser_1_0009_01_0_00_tzselektra_detail';

    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'customer',
            'date_publication',
            'date_end'          => 'maybenull',
            'type',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = false; // пока не ясно с сортировкой
    public $item_rewrite  = false;

    function test_detail($id=4772192) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;
        $this->parser->detail_all($id);
    }
}

class parser_1_0009_01_0_00_tzselektra extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            0                       => 'internal_id',
            'Информация по закупке' => 'name|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.$page );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#/tenderl/(\d+)/#si", $item['internal_id_src']);
            $alltender = preg_get_all("#<td.*?class=['\"]tbl2?['\"].*?>(.*?)</td>#si", $item['name_src']);

            $desc = preg_get_all("#(.*?)<br>#si", $alltender[0]);

            $item['type']             = $this->text_clear_all($desc[0]);
            $item['num']              = $this->text_clear_all(preg_get("#№(\d+/\d+)#sui", $desc[1]));
            $item['name']             = $this->text_clear_all(preg_replace("#".$item['num']."#sui", "", $desc[1]));
            $item['customer']         = $this->text_clear_all(preg_replace("#Организатор #sui", "", $desc[2]));
            $item['date_publication'] = $this->text_date_convert(preg_get("#Дата начала приема заявок:(.*?)<br>#sui", $alltender[0]));
            $item['date_end']         = $this->text_date_convert(preg_get("#Окончание приема заявок:(.*)#sui", $item['name_src']));

            for ($i=1; $i<count($alltender); $i++) {
                $item['lots'][] = $alltender[$i];
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find('table#trades', 0);

        $arr = $this->parse_table($tenders->outertext);

        $stats = $content_dom->find('table#stats', 0);
        
        $pages = preg_get("#<tr><td><table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"5\"><tr><td align=\"center\">(.*?)</td>#sui", $content);

        $ret['page_now'] = preg_get("#<b>.*?(\d+).*?</b>#si", $pages);

        $ret['page_total'] = max(preg_get_all("#<a.*?>.?(\d+).?</a>#si", $content));

        $ret['items_total'] = preg_get("#<td.*?>Объявлено закупок</td>.*?<td.*?>(\d+)</td>#sui", $stats->innertext);

        $content_dom->__destruct();

        $ret['items']=$this->createstruct($arr);

        return $ret;
    }
}

class parser_1_0009_01_0_00_tzselektra_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
            'Реквизиты Заказчика'     => 'customer_address',
            'Общая стоимость'         => 'price|to_price',
            'Организация в заголовке' => 'customer1',
            'Адрес в заголовке'       => 'customer1_address',
            //'Организатор торгов'    => 'customer',
            //'Действительно до'      => 'date_end|date_convert',
            //'Размещено'             => 'date_publication|date_convert',
    );

    function detail_all($content) {
        $link         =  'http://tzselektra.ru/tenderl/' . $content;      
        $this->loader->debug("LINK = $link");
        
        $this->detail_link = $link;
        
        $content      = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);

        // sort function
        $return = $this->detail_sort_2($parse_detail);

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        foreach ($parse_detail as $key => $val) {
            if (preg_match("#Закупочная документация|Файл с описанием|Извещения#sui", $key)) {
                $docs = preg_get_all("#<a.*?file.*?>.*?</a>#sui", $val);
                $i=0;
                foreach($docs as $doc) {
                    $return['docs'][$i]['name'] = $this->text_clear_all(preg_get("#<a.*?>(.*?)</a>#sui", $doc));
                    $return['docs'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"]#", $doc);
                    $return['docs'][$i]['internal_id'] = abs(crc_p($this->item_now['internal_id'] . $return['docs'][$i]['detail_link']));
                    $i++;
                }
            }
        }

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);
        
        $content_dom = str_get_html($content);

        $table = $content_dom->find("td.Content", 0)->find("td.Content", 0)->outertext;

        $tmp_arr = $this->parse_table($table);

        $arr = array();
        foreach ($tmp_arr as $row) {
            $arr[$this->text_clear_all($row[0])] = $row[1];
        }

        $content_dom->__destruct();

        return $arr;
    }
}
