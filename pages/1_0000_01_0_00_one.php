<?php
/**
 * Base loader for page with list
 * @author Paul-labor@yandex.ru
 */
class loader_1_0000_01_0_00_one extends loader_1000_one {
    public $many_sleep = 9;
    public $many_count = 3;
    public $item_sleep = 9;

    function set_site() {
        if ( ! (int) $this->site_id) {
            $this->site_id = preg_get( '/loader_([\d_]+)/', get_class($this));
            $this->site_id = (int) preg_replace('/\D/', '', $this->site_id);
        }

        $this->debug( "SITE_ID = $this->site_id");

        $this->site = new site();
        if( ! $this->site->Load( "id = $this->site_id" ) ) {
            $this->debug( "Fatal: NO this site in db" );
            exit();
        }

        $this->region_id = $this->site->region_id;
    }

    function run_list_scraperwiki() {
        
        $id = 1;
        if (!is_null($this->site->params)) {
            $params = unserialize($this->site->params);
        }
        $id = isset($params['nextid']) ? $params['nextid'] : 1;
        $limit = isset($params['limit']) ? $params['limit'] : 100;
        
        $content = $this->parser->get($id, $limit);
        $list = $this->parser->list_parse($content);        
        
        if (empty($list['items'])) {
            exit('list_empty');
        }        
        
        $this->fields_list_normalize();

        $i=1;
        foreach ( $list['items'] as $item ) {
            $this->debug("ITEM $i of " . count($items));
            
            $region_dict = new region_dict;
            if ($region_dict->Load('name LIKE ? AND site_id=?', array($item['region'], $this->site_id))) {
                $item['region_id'] = $region_dict->region_id;
            }            
            
            $this->list_insert_item($item);
            $i++;
        }
                
        if ($this->error_list_items == 0) {
            $this->site->params = serialize(array('nextid' => $list['id_max'], 'limit' => $limit));
            $this->site->Save(TRUE);                       
        }
    }
    
    
    /**
     * удалить застопорившиеся для всех сайтов
     */
    function run_clear() {
        $time = $this->db->DBTimeStamp(time() - 3*60*60);
        $this->debug("RUN run_clear");
        $this->db->Execute(
                "UPDATE LOW_PRIORITY item SET detail_run_time = NULL WHERE detail_run_time IS NOT NULL AND " .
                "detail_run_time < " . $this->db->DBTimeStamp(time() - 3*60*60));
        $this->db->Execute(
                "UPDATE item_temp SET detail_run_time = NULL WHERE detail_run_time IS NOT NULL AND " .
                "detail_run_time < " . $this->db->DBTimeStamp(time() - 3*60*60));
        $this->db->Execute(
                "UPDATE LOW_PRIORITY doc SET run_time = NULL WHERE run_time IS NOT NULL AND " .
                "run_time < " . $this->db->DBTimeStamp(time() - 3*60*60));
    }

    function run_detail_many() {
        echo "sleep 10\n";
        sleep(mt_rand(7, 11));

        for ($i = 1; $i<$this->many_count; $i++) {
            $this->run_detail();
            sleep($this->many_sleep);
        }
    }

    function run_detail_all() {
        echo "sleep 10\n";
        sleep(mt_rand(7, 11));

        for ($i = 1; $i<1000; $i++) {
            $this->run_detail();
            sleep($this->many_sleep);
        }
    }

    function run_detail() {
        $item = new item();

        // key: id
        if(!$item->Load("site_id = $this->site_id AND detail_count_error = 0 AND " .
        "detail_run_time IS NULL AND date_detail IS NULL ORDER BY id DESC")) {
            if(!$item->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
            "detail_run_time IS NULL AND date_detail IS NULL ORDER BY detail_count_error ASC, id DESC")) {
                exit("\nNO MORE\n");
            }
        }

        $item->detail_count_error ++; // при успехе убавим
        $item->detail_count_load  ++;
        $item->detail_run_time = $item->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item->Save(TRUE);

        $this->run_detail_one( $item->id );

        $item->Reload("id = $item->id");
        $item->detail_run_time   = NULL; // при успехе обнулить
        $item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }

        $item->Save(TRUE);
    }

    /**
     * Загрузка одно детайла
     * @return boolen
     */
    function run_detail_one($id) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->debug("<h3>RUN detail = $id</h3>");

        $id = (int) $id;
        $item = new item();
        if( ! $item->Load("site_id = $this->site_id AND id = $id")) {
            exit("VERY CRITICAL ERROR");
        }

        // передаем текущие данные в парсер
        $this->parser->item_now = $this->ar_to_array($item);

        $parse = $this->parser->detail_all($item->internal_id);

        print_r($parse);

        if(empty($parse)) {
            exit();
        }

        $this->detail_insert($id, $parse);

        $this->debug("<h3>RUN detail = $id</h3>");

        return true;
    }

    function geocoder_by_name($name) {
            $crc_name = crc_p(mb_strtolower(preg_replace('#[^a-zа-яё]#siu', '', $name), 'utf-8'));
        if (!$crc_name) return false;
        $socr = array('г','пгт', 'п', 'с', 'д');
        $first = $this->db->GetOne("SELECT region_id FROM geocoder " .
                "WHERE crc_name=$crc_name ORDER BY FIELD (socr, '".implode(array_reverse($socr), "','")."') DESC");
        return $this->geocoder_region_const($first);
    }

    function geocoder_by_index_auto($string) {
        return $this->geocoder_by_index(preg_get('/(\d{6})/', $string));
    }

    function geocoder_by_index($index) {
        $index = (int)$index;
        if (!$index) return false;
        $first = $this->db->GetOne("SELECT DISTINCT region_id FROM geocoder ".
                "WHERE indx=$index ORDER BY FIELD (socr, 'mail') DESC");

        if (empty ($first)) {
            $index1 = $index - $index%1000;
            $index2 = $index1 + 999;
            $first = $this->db->GetOne("SELECT DISTINCT region_id FROM geocoder ".
                    "WHERE indx>=$index1 AND indx<=$index2 ORDER BY FIELD (socr, 'mail') DESC");
        }
        return $this->geocoder_region_const($first);
    }

    function geocoder_region_const($region_kladr) {
        $region_kladr = (int) $region_kladr;
        if (!$region_kladr) return false;
        $region_const_id = $this->db->GetOne("SELECT region_const_id FROM geocoder ".
                "WHERE region_id=$region_kladr AND region_const_id<>0");
        return $region_const_id ? $region_const_id : false;
    }

    function geocoder_search_name($text) {
        $crc_a = morf_crc($text);

        $crc_a = array_unique($crc_a);
        $crc_a = array_filter($crc_a);

        if(empty($crc_a)) {
            return false;
        }

        $socr = array('Респ', 'край', 'обл', 'Аобл', 'округ', 'АО', 'г', 'пгт', 'п', 'с', 'д', 'mail');
        $all = $this->db->GetAll("SELECT region_id, name, socr FROM geocoder ".
                "WHERE crc_name IN(".implode(',', $crc_a). ") ".
                "GROUP by socr,crc_name ORDER BY FIELD (socr, '".implode(array_reverse($socr), "','")."') DESC");
        //print_r($first);
        $first = $all ? $all[0]['region_id'] : false;
        return $this->geocoder_region_const($first);
    }

    /**
     * Поиск адреса по страке, сначало индекс, затем населенный пункт
     * false, если ничего не находит
     * @param  string $text   строка, содержащая адрес
     * @return int            id региона, FALSE если ненайденно
     */
    function geocoder_auto($text) {
        $region_id = $this->geocoder_by_index_auto($text);
        if ( ! $region_id ) {
            $region_id = $this->geocoder_search_name($text);
        }
        return $region_id;
    }

    /**
     * Поиск адреса по страке, сначало индекс, затем населенный пункт
     * NULL, если ничего не находит
     * @param  string $text   строка, содержащая адрес
     * @return int            id региона, NULL если ненайденно
     */
    function geocoder_by_all($text) {
        $region_id = $this->geocoder_auto($text);
        return $region_id ? $region_id : NULL;
    }

}

/**
 * Base parser for page with list
 * @author Paul-labor@yandex.ru
 */
class parser_1_0000_01_0_00_one extends parser_1000_one {
    public $item_now;

    function text_words_filter($content) {
        //require_once $this->conf['libdir'] . '/morf.php';
        $content = $this->text_clear_hard($content);
        $array = explode(' ', morf_base_filter($content, true));
        $array = array_unique($array);
        return implode(' ', $array);
    }

    /**
     * Делаем сурагатный нумерик ключ для internal_id
     * @param string $text
     * @return int
     */
    protected function make_internal_id($text) {
        return abs(crc_p($text));
    }
    
    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
               
        $items = array();
        foreach ($parse as $key => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = abs($item['internal_id_src']);
            $items[$key] = $item; 
        }        

        $return = array(
            'id_max' => max(array_keys($parse)),
            'id_min' => min(array_keys($parse)),
            'items' => $items
        );
        
        return $return;
    }
    
    function list_parse_pre($content) {
                
        $return = json_decode($content, true);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                echo 'OK';
                break;
            case JSON_ERROR_DEPTH:
                echo 'Stack is full';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo 'no correct mode';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo 'no correct control char';
                break;
            case JSON_ERROR_SYNTAX:
                echo 'Sintax JSON is bad';
                break;
            case JSON_ERROR_UTF8:
                echo 'no correct charset utf8';
                break;
            default:
                echo 'other error';
                break;
        }
        return $return;
       
    }    

}
