<?php
/**
 * Объединённая Металлургическая Компания
 * Написание: 31.01.2011
 */
class loader_1_0044_01_0_00_omk2 extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://omk.zakupim.ru/';
    public $list_link           = 'http://omk.zakupim.ru/view_table';
    public $parser_name         = 'parser_1_0044_01_0_00_omk2';
    public $parser_name_detail  = 'parser_1_0044_01_0_00_omk2_detail';

    public $fields_list = array(
        'date_end',
        'internal_id',
        'customer',
        'name',
        'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0044_01_0_00_omk2 extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
        'Дата окончания'    =>  'date_end|clear_all|date_convert',
        'Номер лота'        =>  'internal_id|clear_all',
        'Завод'             =>  'customer|clear_all',
        'Наименование'      =>  'name|clear_all',
        'Дата размещения'   =>  'date_publication|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            if ($item['date_end'] < date("Ymd")) {
                continue;
            }
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);
        $content_dom = str_get_html($content);

        $tendery = $content_dom->find("div#view_table", 0);
        $t_arr = $this->parse_table($tendery->outertext);
        unset($t_arr[count($t_arr)-1]); unset($t_arr[1]); unset($t_arr[2]);
        foreach ($t_arr as $r) { $arr[] = $r; }

        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['page_total']  = 1;

        $ret['items'] = $items;
        
        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0044_01_0_00_omk2_detail extends parser_1_0000_02_0_00_temp {
    public $item_now;

    protected $detail_link = '';

    function detail_get($id) {
        $this->detail_link = "http://omk.zakupim.ru/view/" . $id;

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $return['content'] = $this->detail_content_cache($content);
        $html = preg_get("#<td class=bodypanel>(.*?)</td>#siu", $content);                

        $html = tidy_repair_string($html, $this->tidy_config);
        $html = preg_replace("#<a.*?</a>#si", "", $html);
        $html = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $html);
        $html = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $html);
        $html = preg_replace(array("#<script.*?</script>#si", "#<style.*?</style>#si", "#<!\[CDATA.*?</script>#si"), "", $html);
        $html = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $html);

        $return['html'] = $html;
        $contact = preg_get("#Контакты:</b>(.*?)<#siu", $html);

        $return['db']['region_id'] = $this->loader->geocoder_auto($contact);
        return $return;
    }
    
}
