<?php

/*
 * Конкурсы "СлавНефть"
*/

class loader_1_0055_01_0_00_b2bsibur extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://b2b.sibur.ru/';
    public $list_link           = 'http://b2b.sibur.ru/pages/exchange/exchange.jsp?disp_status=0&page=';
    public $parser_name         = 'parser_1_0055_01_0_00_b2bsibur';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'customer',
            'price' =>  'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0055_01_0_00_b2bsibur extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        '№'                         =>  'internal_id',
        'Наименование'              =>  'name|clear_all',
        'Лимитная стоимость/ Валюта'=>  'price|clear_all|to_price',
        'Дата опубликования'        =>  'date_publication',
        'Дата вскрытия конвертов'   =>  'date_end',
        'Организатор'               =>  'customer|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link$page \n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_publication'] = $this->date_convert(preg_get("#<span id='doc_gtd'>(.*?)</span>#si", $item['date_publication_src']));
            $item['date_end'] = $this->date_convert(preg_get("#<span id='doc_rtd'>(.*?)</span>#si", $item['date_end_src']));

            if ($item['date_end'] < date("Ymd")) { continue; }
            if (is_null($item['date_end'])) { continue; }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $tbl = preg_get("#<table cellspacing=2 cellpadding=2 border=0 style='width:1000px;'>.*?</table>#si", $content);
        $tbl = tidy_repair_string($tbl, $this->tidy_config);

        $arr  = $this->parse_table($tbl);

        $items = $this->createstruct($arr);

        $pager = preg_get("#<div id='doc_bot_fr'>.*?</div>#si", $content);

        $ret['page_now']    = preg_get("#(\d+)&nbsp;#si", $pager);
        $ret['items_total'] = count($items);
        $ret['page_total']  = max(preg_get_all("#\d+#si", $pager));

        $ret['items'] = $items;

        return $ret;
    }
}

