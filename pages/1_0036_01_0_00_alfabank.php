<?php
/**
 * Альфа-Банк
 * Написание: 24.12.2010
 */
class loader_1_0036_01_0_00_alfabank extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://alfabank.ru/';
    public $list_link           = 'http://alfabank.ru/tenders/current/';
    public $parser_name         = 'parser_1_0036_01_0_00_alfabank';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0036_01_0_00_alfabank extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_internal_id'     => 'internal_id',
            'P_name'            => 'name|clear_all',
            'P_date_publication'=> 'date_publication|date_convert',
            'P_doc'             => 'doc',
    );

    function list_get_page( $link ) {
        $this->loader->debug("\n\n LINK = $link\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['doc'] = $item['doc_src'];

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div.tender-list", 0);

        $items = array();
        foreach ($tenders->find("div.tender-item") as $tender) {
            $res = $tender->find("div.resources", 0);
            $docs = array();
            foreach ($res->find("div.dn-file") as $file) {
                $docs[] = array(
                    'detail_link'   => $file->find("div.name", 0)->find("a",0)->getAttribute('href'),
                    'name'          => $file->find("div.title",0)->innertext,
                    'internal_id'   => preg_get("#\d+#", $file->find("div.name", 0)->find("a",0)->innertext),
                );
            }
            $items[] = array(
                'P_date_publication'    =>  $tender->find("div.date",0)->innertext,
                'P_name'                =>  $tender->find("div.title",0)->innertext,
                'P_internal_id'         =>  preg_get("#tenders/(\d+)/#si", $docs[0]['detail_link']),
                'P_docs'                =>  $docs,
            );
        }

        $ret['items'] = $items;
        
        $content_dom->__destruct();

        return $ret;
    }
}
