<?php

/*
 * Закупки по Программе развития СВФУ
 */

class loader_1_0050_02_0_00_svfu extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.ysu.ru/';
    public $list_link           = 'http://www.ysu.ru/content/zakupki/';
    public $parser_name         = 'parser_1_0050_02_0_00_svfu';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'doc'              => 'maybenull',
            'price'            => 'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0050_02_0_00_svfu extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            '№'                                 =>  'num|clear_all',
            'Наименование'                      =>  'name|clear_all',
            'Способ размещения'                 =>  'type|clear_all',
            'Дата начала подачи заявок'         =>  'date_publication|clear_all|date_convert',
            'Дата оконч. приема заявок'         =>  'date_end|clear_all|date_convert',
            'Протоколы'                         =>  'report',
            'Начальная цена'                    =>  'price|to_price',
            'Наличие изменений и разьяснений'   =>  'date_change|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $item = $this->list_set_colomn($item, $this->colomn);
            
            if ($item['date_end'] < date("Ymd")) {
                continue;
            }

            if (preg_match("#лот#si", $item['price_src'])) {
                $item['price'] = NULL;
            }

            if (is_null($item['name']) || is_null($item['num'])) {
                continue;
            }

            if (preg_match("#снат|отменен#sui", $item['date_change_src'])) {
                continue;
            }

            if (isset($item['date_change'])) {
                
            }

            $item['internal_id'] = abs(crc_p($item['num'] . $item['name']));

            $item['doc'][] = array(
                'name'          =>  $item['name'],
                'internal_id'   =>  $item['internal_id'],
                'detail_link'   =>  preg_get("#href=\"(.*?)\"#si", $item['name_src']),
            );

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        var_dump($return);

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);
        $content = tidy_repair_string($content, $this->tidy_config);

        $html = str_get_html($content);
        $data = $html->find("div#content", 0);
        $table_tenders = preg_get("#<table.*</table>#si", $data);
        $html->clear();

        $arrayTenders = $this->parse_table($table_tenders);

        $items = $this->createstruct($arrayTenders);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;       

        return $ret;
    }
}

