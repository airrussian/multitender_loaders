<?php

/*
 * Конкурсы "СлавНефть"
*/

class loader_1_0053_01_0_00_slavneft extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.slavneft.ru/';
    public $list_link           = 'http://www.slavneft.ru/supplier/tenders/';
    public $parser_name         = 'parser_1_0053_01_0_00_slavneft';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'doc',
            'customer',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0053_01_0_00_slavneft extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        'Дата публикации'   =>  'date_publication|clear_all|date_convert',
        'Дата проведения'   =>  'date_end|clear_all|date_convert',
        'Предмет конкурса'  =>  'name|clear_all',
        'Заказчик'          =>  'customer|clear_all',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            if (!preg_match("#Объявлен#siu", $item['Состояние'])) {
                continue;
            }

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = abs(crc_p($item['name_src']));

            $item['doc'][] = array(
                'internal_id'   =>  $item['internal_id'],
                'name'          =>  $item['name'],
                'detail_link'   =>  preg_get("#href=['\"](.*?)['\"]#si", $item['name_src']),
            );

            $items[$k] = $item;
        }        

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);

        $data = $html->find("table.contest", 0)->outertext;

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $html->clear();

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

