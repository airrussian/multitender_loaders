<?php
/**
 * @author airrussian@mail.ru
 */

class loader_1_1002_01_0_00_sberbank extends loader_1_1000_01_0_00_zakupki {

    public $base_url    = 'http://sberbank-ast.ru/';
    public $list_link   = 'http://sberbank-ast.ru/purchaseList.aspx';
    public $parser_name = 'parser_1_1002_01_0_00_sberbank';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'price' => 'maybenull',
            'customer' => 'maybenull',
            'customer_address' => 'maybenull',
            'customer_email' => 'maybenull',
            'customer_phone' => 'maybenull',
            'date_publication',
            'date_conf',
            'date_end',
    );

    public $fields_add     = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass  = false;
    public $item_rewrite   = false;
    public $page_last      = 10;

    //public $parser_name_detail = 'parser_1_0003_01_0_00_sberbank_detail_temp';
    public $parser_name_detail = 'parser_1_1002_01_0_00_sberbank_detail';

    protected $run_detail_order = 'date_conf ASC';

    //public $sleep_list = 1;

    function list_stop(array $list) {
        return $this->list_stop_date_conf($list);
    }

    function run_detail_query(&$obj) {
        if(!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }
   
}

class parser_1_1002_01_0_00_sberbank extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'purchID' => 'internal_id',
            'purchCode' => 'num',
            'purchName' => 'name',
            'purchAmount' => 'price|to_price',
            //'custContacts_custName'         => 'customer',
            //'custContacts_custPostAddress'  => 'customer_address',
            //'custContacts_custPlace'        => 'customer_address2',
            //'orgContacts_orgPostAddress'    => 'customer_address3',
            //'orgContacts_orgPlace'          => 'customer_address4',
            //'custContacts_custEMail'        => 'customer_email',
            //'custContacts_custPhones'       => 'customer_phone',
            'publicDateStr' => 'date_publication|date_convert',
            'auctionBeginDateStr' => 'date_conf|date_convert',
            'requestDateStr' => 'date_end|date_convert',
    );

    function list_get_page($link, $page = 1) {

        $this->loader->debug("\n\nREAL PAGE = $page\n\n");

        $emul_br = $this->emul_br_init($link);

        if ($this->asp_params_post) {
            $emul_br->SetCookie($this->asp_params_cookies);
            $emul_br->SetPost($this->asp_params_post);
            $emul_br->SetPost(array(
                    '__EVENTTARGET' => 'ctl00$ctl00$phWorkZone$nextPage',
                    'ctl00$ctl00$phWorkZone$xmlFilter' => '<query><purchstate>public</purchstate></query>',
                    'ctl00$ctl00$phWorkZone$xmlData' => '<data></data>',
                    'ctl00$ctl00$phWorkZone$phDataZone$purchID' => '54944',
            ));
        }

        $emul_br->exec();
        $this->loader->debug($emul_br->GetAllHeaders());

        $this->asp_set_params($emul_br);

        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        //print_r($parse);
        //exit(0);

        foreach ($parse['items'] as $item) {
            if (empty($item)) {
                continue;
            }
            $item = $this->list_set_colomn($item, $this->colomn);

            $items[] = $item;
        }

        $return = array(
                'page_total' => $this->loader->page_last, //$parse['page_total'],
                'page_now' => null, //$parse['page_now'],
                'items_total' => null,
                'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $data = $content_dom->find('input[name=ctl00$ctl00$phWorkZone$xmlData]', 0)->value;

        $ret['page_total'] = null;

        $content_dom->__destruct();

        if (empty($data)) {
            return null;
        }

        $ret['items'] = $this->create_xml($data);

        return $ret;
    }

    function create_xml($text) {
        $str = preg_replace(array('#&lt;#siu', '#&gt;#siu'), array('<', '>'), $text);
        $xml = simplexml_load_string($str);
        $i = 1;
        $return = array();
        foreach ($xml->row as $purchase) {
            foreach ($purchase->children() as $tags) {
                if ((string) $tags) {
                    $return[$i][$tags->getName()] = (string) $tags;
                } else {
                    // Второй уровень больше и не надо | поэтому не рекурсивно
                    foreach ($tags->children() as $tag) {
                        $return[$i][$tags->getName() . "_" . $tag->getName()] = (string) $tag;
                    }
                }
            }
            $i++;
        }
        return $return;
    }

}

class parser_1_1002_01_0_00_sberbank_detail extends parser_1_1000_01_0_00_zakupki_detail {

    
    function detail_all($id) {

        $num = $this->item_now['num'];
        
        $internal_id = $this->item_now['internal_id'];

        $id = $this->loader->GetIDbyNUM($num);
        if ($id) {
            $this->loader->debug(" ZAKUPKI ID = $id");
            $ret = parent::detail_all($id);
            $this->loader->detail_link = "http://sberbank-ast.ru/purchaseview.aspx?id=$internal_id";
            $ret['content'] = $this->detail_content_cache($this->emul_br_get_body($this->loader->detail_link));
        } else {
            $this->loader->debug(" NOT ITEM WITH $num ON ZAKUPKI ");
            $this->loader->NoItem($this->item_now['id']);
            $ret = false;
        }

        return $ret;
    }


}
