<?php
/*
   - 27 Oct 2009
      на zakupki.gov.ru изменили кодировку

   - 16 Mar 2010
      839 строк -> 1400 (добавление функционала)

  FIXME $params = array('cookies', 'post'), asp_set_params()
  FIXME exit в списках, из-за чего прерывается работа
*/

/**
 * zakupki.gov.ru
 * @author Paul-labor@yandex.ru
 */
class loader_1_0001_01_0_00_zakupki extends loader_1_0000_01_0_00_one {
    public $test_run  = true;
    public $test_nodb = true;

    public $many_sleep = 9;
    public $many_count = 3;
    public $item_sleep = 9;

    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'date_publication',
            'type',
            'price' => 'maybenull',
            'customer',
    );

    public $parser_name          = 'parser_1_0001_01_0_00_zakupki';
    public $parser_name_detail   = 'parser_1_0001_01_0_00_zakupki_detail';
    public $parser_name_doc      = 'parser_1_0001_01_0_00_zakupki_detail';
    public $parser_name_protocol = 'parser_1_0001_01_0_00_zakupki_protocol';


    public $break_by_pass = true;

    // попытка исправить регион, или обходить все??
    public $item_rewrite  = true;

    public $debug = true;

    public $site_id = 1;

    protected $list_search_params_link = 'http://zakupkiold.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link        = 'http://zakupkiold.gov.ru/Tender/Purchase.aspx';
    protected $detail_link             = 'http://zakupkiold.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';
    protected $error_msg               = 'При обработке запроса возникли технические неполадки';

    public    $doc_storage             = '/home/mt_loader/arh';

    function  __construct() {
        parent::__construct();
    }

    /**
     * RUN load item
     * @param $region id from region, not internal
     */
    function run_list($region_id = null) {
        // DEBUG
        // $region_id = 1;
        $this->debug('START');

        $region = new region_dict();

        if($region_id) {
            $region_id = (int) $region_id;
            if (!$region->Load("site_id = $this->site_id AND region_id = $region_id")) {
                exit("Unknown region");
            }
            $regionArray = array($region);
        }
        else {
            $regionArray = $region->Find('site_id = ' . $this->site_id);
        }

        $this->debug('START regions, count = ' . sizeof($regionArray));

        $i = 0;
        foreach($regionArray as $region) {
            $this->region_dict_id = $region->id;
            $this->region_id      = $region->region_id;

            $item = new item();

            $this->parser->region_param = $region->internal_param;

            $this->debug('START region = ' . $this->region_id . ',' . $region->internal_param);

            parent::run_list();

            if ( $this->test_run && ($i > 1) ) {
                exit("TEST RUN");
            }

            sleep($this->item_sleep);

            $i ++;
        }
    }

    // FIXME, add region
    function update_in_process() {
        $this->debug("update_in_process: error_list_items = $this->error_list_items, test_nodb = $this->test_nodb");
        if ( ! $this->error_list_items && ! $this->test_nodb) {
            $item = new item();
            $item->DB()->Execute("UPDATE LOW_PRIORITY item SET in_process=NULL ".
                    "WHERE site_id=$this->site_id AND region_id = $this->region_id AND in_process IS NOT NULL");
        }
    }

    function test_detail_get($id = 495388) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_detail_get($id);
    }

    function test_get_detail_param_sort($id = 495388) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->test_get_detail_param_sort($id);
    }


    function test_detail_get_stuff($id = 720766) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        print_r($this->parser->detail_get_stuff($id));
    }

    function run_detail_one_int($id) {
        $item = new item();
        if($item->Load("site_id = $this->site_id AND internal_id = $id")) {
            $this->run_detail_one($item->id);
        }
    }

    function run_detail_test1() {
        $this->run_detail_one(433917);
    }

    function run_doc($id = null) {
        $doc = new doc();

        // key: id
        if (!$doc->Load("site_id = $this->site_id AND md5 IS NULL AND " .
        "count_error = 0 AND run_time IS NULL ORDER BY id DESC")) {
            if (!$doc->Load("site_id = $this->site_id AND md5 IS NULL AND " .
            "count_error < 10 AND run_time IS NULL ORDER BY count_error ASC, id DESC")) {
                exit("\nNO MORE\n");
            }
        }

        $doc->count_error ++; // при успехе убавим
        $doc->count_load  ++;
        $doc->run_time = $doc->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $doc->Save();

        if (!$this->run_doc_one( $doc->id )) {
            return false;
        }

        $doc->Reload("id = $doc->id");
        $doc->run_time   = NULL; // при успехе обнулить
        $doc->count_error --;
        $doc->Save();
    }

    /**
     * Запуск крупных регионов
     */
    function run_list_big() {
        $this->run_list(52);
        sleep($this->many_sleep);
        $this->run_list(77);
        sleep($this->many_sleep);
        $this->run_list(78);
    }

    function run_doc_many() {
        $this->many_count = $this->many_count * 2;

        echo "sleep 30\n";
        sleep(mt_rand(25, 35));

        for ($i = 1; $i<$this->many_count; $i++) {
            $this->run_doc();
            sleep($this->many_sleep);
        }
    }

    function run_report_many() {
        echo "sleep 45\n";
        sleep(mt_rand(40, 50));

        for ($i = 1; $i<$this->many_count; $i++) {
            $this->run_report();
            sleep($this->many_sleep);
        }
    }

    /**
     *
     * @param <type> $id
     */
    function run_report($id = null) {
        $item = new item();

        $date_end = $item->DB()->DBDate(time() - 7*24*3600);

        // key: site_id_date_end
        if (!$item->Load("site_id = $this->site_id AND detail_count_error = 0 AND " .
        "detail_run_time IS NULL AND date_end <= $date_end AND " .
        "date_report IS NULL ORDER BY date_end DESC")) {
            if (!$item->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
            "detail_run_time IS NULL AND date_end <= $date_end AND " .
            "date_report IS NULL ORDER BY detail_count_error ASC, id DESC")) {
                exit('NO NEW');
            }
        }

        $item->detail_count_error ++; // при успехе убавим
        $item->detail_count_load  ++;
        $item->detail_run_time = $item->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item->Save(TRUE);

        $this->debug("ID = $item->id");

        $this->parser = new $this->parser_name_protocol;
        $this->parser->loader = & $this;

        $content = $this->parser->get($item->internal_id);
        if (!$content) {
            return false;
        }
        $parse = $this->parser->parse_simple($content);
        if (!$parse) {
            return false;
        }

        print_r($parse);

        $data = array(
                'data'     => $content,
                'filename' => 'Протокол рассмотрения.html',
                'md5'      => md5($content),
                'size'     => strlen($content),
        );

        // 4444 - протоколы
        $this->doc_save($data, $item->id, 4444);

        $item->price_best    = $parse['price_best'];
        $item->members_count = $parse['members_count'];
        $item->date_report   = $item->DB()->BindTimeStamp(time());

        $item->detail_run_time = NULL; // при успехе обнулить
        $item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }
        $item->Save(TRUE);

        return true;
    }

    function run_doc_one($id = null) {
        $this->parser = new $this->parser_name_doc;
        $this->parser->loader = & $this;

        $doc = new doc();
        $id = (int) $id;

        if ($id) {
            if( ! $doc->Load("site_id = $this->site_id AND id = $id")) {
                return null;
            }
        }
        else {
            if( ! $doc->Load("site_id = $this->site_id AND md5 IS NULL ORDER BY id DESC")) {
                return null;
            }
        }

        $item = new item();
        if ( ! $item->Load("id = $doc->item_id")) {
            // FIXME костыль, пока не будет сделаны откаты, INNODB
            $item->DB()->Execute('DELETE FROM doc WHERE item_id = ' . $doc->item_id);
            trigger_error("Костыль, нужно удалять или INNODB");
            return null;
        }

        // storage / year / month / site_id / internal_id.md5.ext
        umask();
        list($year, $month) = explode( '-', $doc->date );
        $this->doc_fullpath = "$this->doc_storage/$year/$month/$doc->site_id";
        if ( ! is_dir ( $this->doc_fullpath ) ) {
            if ( ! mkdir( $this->doc_fullpath, 0777, true ) ) {
                exit("unable set derictory");
            }
            else {
                chmod($this->doc_fullpath, 0777);
            }
        }

        $r = $this->parser->doc_get($this->ar_to_array($doc), $this->ar_to_array($item));


        if (empty($r)) {
            return false;
        }

        $this->debug($r['filename']);
        $this->debug($r['md5']);

        //      $r['filename'] = 'file.name.doc.zip';

        if ($r['filename']) {
            $ext = preg_get("/\.([a-z]{1,5}(\.[a-z]{1,5})?)$/i", $r['filename'], 1);
            $ext = strtolower($ext);
        }
        if (empty($ext)) {
            $ext = 'unk';
        }

        // if (!empty($r['data'])) {
        // D0 CF 11 E0 A1 B1 1A E1 doc
        // Rar  - rar
        // D0 CF 11 E0 A1 B1 1A E1 xls
        // }

        if (!empty($r['data'])) {
            $file = "$this->doc_fullpath/{$r['md5']}.$ext.gz";
            $this->debug($file);
            file_put_contents($file, gzencode($r['data'], 6));
            chmod($file, 0666);
        }

        $r['md5'] = strtolower($r['md5']);

        $doc->md5  = $r['md5'];
        $doc->ext  = $ext;
        $doc->size = $r['size'];

        //FIXME преобразование дат...
        $doc->date_load = $doc->DB()->BindTimeStamp(gmdate('U'));

        $doc->Save();
        return true;
    }

    /**
     * Сохранить документ
     *
     array(
     'filename',
     'md5',
     'data',
     'size',
     );
     * @param <type> $id
     */
    function doc_save($data, $item_id, $internal_id, $date=null) {
        $doc = new doc();
        if( ! $doc->Load("site_id = $this->site_id AND item_id = $item_id AND internal_id = $internal_id")) {
            $doc->site_id     = $this->site_id;
            $doc->item_id     = $item_id;
            $doc->internal_id = $internal_id;
        }

        if (is_null($date)) {
            $date = time();
        } elseif ($date < strtotime('-5 year')) {
            exit("Very old date\n");
        }

        $year  = date('Y', $date);
        $month = date('m', $date);

        // storage / year / month / site_id / internal_id.md5.ext
        umask();
        $this->doc_fullpath = "$this->doc_storage/$year/$month/$doc->site_id";
        if ( ! is_dir ( $this->doc_fullpath ) ) {
            if ( ! mkdir( $this->doc_fullpath, 0777, true ) ) {
                exit("unable set derictory, '$this->doc_fullpath'");
            }
            else {
                chmod($this->doc_fullpath, 0777);
            }
        }

        $this->debug($data['filename']);
        $this->debug($data['md5']);

        if ($data['filename']) {
            $ext = preg_get("/\.([a-z]{1,5}(\.[a-z]{1,5})?)$/i", $data['filename'], 1);
            $ext = strtolower($ext);
        }
        if (empty($ext)) {
            $ext = 'unk';
        }

        if (!empty($data['data'])) {
            $file = "$this->doc_fullpath/{$data['md5']}.$ext.gz";
            $this->debug($file);
            file_put_contents($file, gzencode($data['data'], 6));
            chmod($file, 0666);
        }

        $data['md5'] = strtolower($data['md5']);

        $doc->name = $data['filename'];
        $doc->md5  = $data['md5'];
        $doc->ext  = $ext;
        $doc->size = $data['size'];

        $doc->date_add  = $doc->DB()->BindTimeStamp(time());
        $doc->date_load = $doc->date_add;
        $doc->date      = $doc->DB()->BindTimeStamp($date);

        $doc->Save();
    }

}

class parser_1_0001_01_0_00_zakupki extends parser_1_0000_01_0_00_one {
    public $region_param;

    public $curl_timeout = 60;

    protected $list_search_params_link  = 'http://zakupkiold.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link         = 'http://zakupkiold.gov.ru/Tender/Purchase.aspx';
    protected $detail_link              = 'http://zakupkiold.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';

    protected $error_msg = 'При обработке запроса возникли технические неполадки';

    protected $colomn = array(
            '0'                               => 'internal_id',
            'Номер извещения'                 => 'num|clear_all_dots',
            'Наименование'                    => 'name|clear_all_dots',
            'Заказчик'                        => 'customer|clear_all_dots',
            'Способ размещения заказа'        => 'type|clear_all_dots',
            'Дата опубликования извещения'    => 'date_publication|strip_tags|date_convert',
            'Начальная цена контракта'        => 'price|clear_all|to_price',
            // 'Наличие изменений и разъяснений' => 'dev_null',
    );

    /**
     * FIXME workzone - dont need
     *
     * @param <type> $link
     * @param <type> $page
     * @return <type>
     */
    function list_get_page( $link, $page = 1 ) {
        if ($page == 1) {
            return $this->list_get_page_first();
        }

        $this->loader->debug("NEXT PAGE = $page");

        $page = (int) $page;

        $emul_br = $this->emul_br_init($this->list_search_link);

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
                '__EVENTTARGET'   => 'ctl00$phWorkZone$' . 'ListOfPurchase$gvMain',
                '__EVENTARGUMENT' => 'Page$' . $page,
        ));

        $content = $emul_br->exec();

        $this->loader->debug($emul_br->GetAllHeaders());

        if(preg_match("/$this->error_msg/", $content)) {
            trigger_error('ERROR on load http, pri obrabotke voznkikli oshibki', E_USER_WARNING);
            exit("CRITIKAL exit");
        }

        return $content;
    }

    /**
     * Первая страница, поиск
     * @param <type> $region_id
     * @param <type> $region_param
     * @return <type>
     */
    function list_get_page_first() {
        $param = explode('|', $this->region_param);

        if(sizeof($param) !== 2) {
            exit("ERROR PARAM, line " . __LINE__);
        }

        $reg_id  = $param[0];
        $reg_kod = $param[1];
       
        // очишаем с прошлого раза, запрос по новой
        $this->asp_clear_params();

        // GET 1. ViewState, Cookie
        $emul_br = $this->emul_br_init($this->list_search_params_link);
        $emul_br->exec();
        $this->asp_set_params($emul_br);

        $this->loader->debug('After emul');
        $this->loader->debug($emul_br->GetAllHeaders());

        if(!isset($this->asp_params_cookies['ASP.NET_SessionId'])) {
            trigger_error("no cookie ASP.NET_SessionId");
            exit("CRITIKAL exit");
        }

        // GET 2. SearchParams
        $emul_br = $this->emul_br_init($this->list_search_params_link);
        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);
        $emul_br->SetPost(array(
                '__EVENTTARGET' => 'ctl00$phWorkZone$pf$btn_fb',
                '__EVENTARGUMENT' => '',
        ));

        $emul_br->SetPost(array(
                'ctl00$phWorkZone$pf$rp_r$ctl' . $reg_id . '$cb' => 'on',
                'ctl00$phWorkZone$pf$rp_r$ctl' . $reg_id . '$hd' => $reg_kod,
        ));

        $content = $emul_br->exec();       
        $this->asp_set_params($emul_br);

        $this->loader->debug($emul_br->GetAllHeaders());

        if(preg_match("/$this->error_msg/", $content)) {
            trigger_error('ERROR on load http, Pri obrabotke voznikli tehnicheskiet nepoladki...', E_USER_WARNING);
            exit("CRITIKAL exit");
        }

        if(strpos($this->list_search_link, $emul_br->GetResponseHeader('Location')) === false) {
            trigger_error("no header Location");
            $this->debug($emul_br->GetAllHeaders());
            exit("CRITIKAL exit");
        }

        // GET 3. Get Page
        $emul_br = $this->emul_br_init($this->list_search_link);
        $emul_br->referer = $this->list_search_params_link;
        $emul_br->SetCookie($this->asp_params_cookies);
        $content = $emul_br->exec();

        $this->asp_set_params($emul_br);

        $this->loader->debug($emul_br->GetAllHeaders());

        if(preg_match("/$this->error_msg/", $content)) {
            trigger_error('ERROR on load http, Pri obrabotke voznikli tehnicheskiet nepoladki...', E_USER_WARNING);
            exit("CRITIKAL exit");
        }

        return $content;
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = (int)preg_get('/PurchaseId=(\d+)/', $item['internal_id']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    protected $list_parse_pre_find = 'table#ctl00_phWorkZone_ListOfPurchase_gvMain';

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);
        $table       = $content_dom->find($this->list_parse_pre_find,0)->outertext;
        $content_dom->clear();

        $table_array = $this->parse_table($table);
        $return = array();

        if (preg_match('/<table/i', end(end($table_array)) ) ) {
            list($pages,) = array_pop($table_array);
            $return['page_now']   = (int)preg_get('#<span>(\d+)</span>#', $pages);
            $return['page_total'] = (int)count(preg_get_all('#</td>#', $pages));
        } else {
            $return['page_now']   = 1;
            $return['page_total'] = 1;
        }

        $return['items'] = $this->createstruct($table_array);

        return $return;
    }



}

/**
 * DETAIL
 */
class parser_1_0001_01_0_00_zakupki_detail extends parser_1_0000_01_0_00_one {
    public $region_param;

    protected $list_search_params_link  = 'http://zakupkiold.gov.ru/Tender/PurchaseSearchParams.aspx';
    protected $list_search_link         = 'http://zakupkiold.gov.ru/Tender/Purchase.aspx';
    protected $detail_link              = 'http://zakupkiold.gov.ru/Tender/ViewPurchase.aspx?PurchaseId=';

    protected $error_msg = 'При обработке запроса возникли технические неполадки';

    protected $parser_lots = array();

    // текущие данные тендера
    public    $item_now = array();

    function detail_all($id) {
        $p = $this->detail_get($id);

        if(isset($p['detail_headers']['Location'])) {
            if($p['detail_headers']['Location'] === '/Tender/Purchase.aspx') {
                return array(
                        'error' => 'delete',
                );
            }
        }

        $return = $this->detail_param_sort(
                $this->detail_parse($p['detail']),
                $this->detail_parse_lots($p['lots']),
                $this->detail_parse_docs($p['docs']));
        $return['stuff'] = $p['stuff'];

        return $return;
    }

    function detail_parse($content) {
        $content_dom = str_get_html($content);
        $trs = $content_dom->find('table[rules=rows] tr');

        if(!$trs) {
            exit("error parse, detail_parse");
        }

        foreach($trs as $tr) {
            if ( ! $tr->find('td span') ) {
                continue;
            }

            $name = $tr->find('td span', 0)->innertext;
            $td1  = $tr->find('td', 1);

            if ( $td1->find('span') ) {
                $value = $td1->find('span',0)->innertext;
            }
            else if ( $td1->find('a', 0) ) {
                $value = $td1->find('a', 0)->innertext;
            }
            else {
                $value = $td1->innertext;
            }

            if(empty($value)) {
                continue;
            }

            $return[trim($name)] = trim($value);
        }

        $content_dom->__destruct();

        return $return;
    }

    function test_detail_parse() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_main.htm');
        $list = $this->detail_parse($content);
        print_r($list);
        return $list;
    }

    function detail_parse_lots($content) {
        //FIXME dom_html trim content!
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table[id=ctl00_phWorkZone_Positions]', 0);

        if(empty($table)) {
            exit('error parse detail_parse_lots, '. __LINE__);
        }

        $trs = $table->find('tr[align=left]');

        foreach($trs as $key=>$tr) {
            $tds = $tr->find('td');
            $return[$key]['id']       = trim($tds[1]->innertext);
            $return[$key]['tid']      = trim($tds[0]->find('input',0)->name);
            $return[$key]['name']     = trim($tds[2]->innertext);

            $sum = $tds[3]->find('span',0)->innertext;
            $sum = (int)preg_replace('/[^\d.,]/', '', $sum);
            $cur = $tds[3]->find('span',1)->innertext;

            $return[$key]['sum']      =  (int)$this->currency_to_rub($sum, $cur);

            $return[$key]['cur_sum']  =  (int)$sum;
            $return[$key]['cur_type'] =  $cur;
        }

        $content_dom->__destruct();

        return $return;
    }

    function test_detail_parse_lots() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_lots.htm');
        $lots = $this->detail_parse_lots($content);
        print_r($lots);
        return $lots;
    }

    function detail_parse_docs($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table[id=ctl00_phWorkZone_DocumentList_ctl00_DocumentList]', 0);

        if(empty($table)) {
            if($content_dom->find('table[id=ctl00_phWorkZone_DocumentList_EmptyGrid]')) {
                return array();
            }
            exit('error parse detail_parse_docs' . __LINE__);
        }

        $trs = $table->find('tr[align=left]');

        if(empty($trs)) {
            exit('error parse detail_parse_docs' . __LINE__);
        }

        $return = array();

        foreach($trs as $key=>$tr) {
            $tds = $tr->find('td');

            $return[$key]['tid']      = trim($tds[0]->find('input',0)->name);

            //ctl00$phWorkZone$Doc$ctl02$ImageButton1
            $return[$key]['internal_id'] = (int)preg_get("/ctl(\d+)\\\$btnDownload/i", $return[$key]['tid']);

            $return[$key]['name']     = trim($tr->find('[id*=DocumentName]',0)->plaintext);
            $return[$key]['desc']     = trim($tr->find('[id*=DocumentDescription]',0)->plaintext);
            $return[$key]['date']     = $this->date_convert( trim($tr->find('[id*=DocumentDate]',0)->plaintext) );
        }

        $content_dom->__destruct();

        if(empty($return)) {
            exit('error parse detail_parse_docs' . __LINE__);
        }

        return $return;
    }

    function test_detail_parse_docs() {
        $content = file_get_contents(dirname(__FILE__) . '/detail_parse_docs.htm');
        $docs = $this->detail_parse_docs($content);
        print_r($docs);
        return $docs;
    }


    function detail_param_sort($detail_main, $detail_lots, $detail_docs) {
        if( empty($detail_main) || empty($detail_lots) ) {
            exit("ERROR detail_param_sort");
        }

        // FIXME - регулярные выражения
        // FIXME - фильтры определения пустых значений
        $favorite = array(
                'Номер извещения' => 'num',
                // 'Наименование размещения заказа' => '',
                // 'Способ размещения заказа' => '',
                // 'Год финансирования' => '',
                // 'Организатор торгов' => '',
                // 'Контактное лицо' => '',
                // 'Срок, место и порядок предоставления документации' => '',
                // 'Официальный сайт, на котором размещена документация' => '',
                // 'Внесение платы за документацию' => '',
                // 'Обеспечение заявки' => '',
                // 'Обеспечение исполнения контракта' => '',
                // 'Место вскрытия конвертов с заявками на участие в конкурсе' => '',
                'Дата и время вскрытия конвертов с заявками (время местное)'              => 'date_end:date_convert',
                'Дата и время окончания срока подачи котировочных заявок (время местное)' => 'date_end:date_convert',
                'Дата окончания подачи заявок (время местное)'                            => 'date_end:date_convert',
                'Дата и время проведения аукциона (время местное)'                        => 'date_conf:date_convert',
                // 'Место рассмотрения заявок' => '',
                // 'Дата рассмотрения заявок (время местное)' => '',
                // 'Место подведения итогов' => '',
                // 'Дата заседания комиссии, осуществляющей оценку и сопоставление заявок (время местное)' => '',
                // 'Преимущества для организаций и учреждений УИС' => '',
                // 'Преимущества для организаций и учреждений инвалидов' => '',
                'Заказчик' => 'customer',
                'Адрес заказчика' => 'customer_address',
                'Контактные реквизиты заказчика' => 'customer_phone',
                'Адрес электронной почты' => 'customer_email',
                // 'Иная информация' => '',
        );

        foreach($detail_main as $key=>$val) {
            if(isset($favorite[$key])) {
                if($favorite[$key] === 'delete') {
                    //                    continue;
                }
                elseif(preg_match('/(.*?):(.*)/', $favorite[$key], $m)) {
                    $return['db'][$m[1]] = call_user_func(array($this, $m[2]), $val);
                    //                    continue;
                }
                else {
                    $return['db'][$favorite[$key]] = $val;
                }
            }
            $return['other'][$key] = $val;
        }

        $return['other']['Лоты'] = 'HEADER';

        foreach($detail_lots as $key=>$val) {
            $return['other'][$val['id'] . '. ' . $val['name']] = $val['sum'] . ' руб.';
        }

        // FIXME: возможность документов с прямой загрузкой, не только через сайт.
        // возможность хранить полную ссылку и лишь индетификатор
        //

        foreach($detail_docs as $key=>$val) {
            $return['docs'][$key] = array(
                    'internal_id' => $val['internal_id'],
                    'name'        => $val['name'],
                    'date'        => $val['date'],
            );
        }


        if(sizeof($detail_lots) > 1) {
            $sum = 0;
            foreach($detail_lots as $val) {
                $sum += $val['sum'];
            }
            $return['db']['price'] = $sum;
        }

        return $return;
    }

    function test_detail_param_sort() {
        $list = $this->test_detail_parse();
        $lots = $this->test_detail_parse_lots();
        $docs = $this->test_detail_parse_docs();
        print_r($this->detail_param_sort($list, $lots, $docs));
    }

    function test_get_detail_param_sort($id = 495388) {
        $p = $this->detail_get($id);

        //print_r($p);

        print_r($this->detail_param_sort(
                $this->detail_parse($p['detail']),
                $this->detail_parse_lots($p['lots']),
                $this->detail_parse_docs($p['docs'])));
    }

    function detail_get($id) {
        $return = array();

        // 1. Get detail
        //
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $return['detail'] = trim($emul_br->GetBody());

        //FIXME
        $return['detail_headers'] = $emul_br->GetResponseHeaderArray();

        $this->loader->debug($emul_br->GetAllHeaders());

        // 2. Get list lots
        //
        $emul_br = $this->emul_br_init( $this->detail_link . $id . '&TS=1' );
        $emul_br->exec();
        $return['lots'] = trim($emul_br->GetBody());


        $this->loader->debug($emul_br->GetAllHeaders());

        // 3. Get list docs
        //
        $emul_br = $this->emul_br_init( $this->detail_link . $id . '&TS=2' );
        $emul_br->exec();
        $return['docs'] = trim($emul_br->GetBody());

        $this->loader->debug($emul_br->GetAllHeaders());

        // 4. Get stuff
        //
        $return['stuff'] = $this->detail_get_stuff($id);

        return $return;
    }

    function detail_get_stuff($id) {
        $pos = array();

        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id . '&TS=1' );
        $emul_br->exec();

        $this->asp_clear_params();
        $this->asp_set_params($emul_br);

        $content = trim($emul_br->GetBody());

        $lots_array = $this->detail_parse_lots($content);

        foreach ($lots_array as $k => $lot) {
            $emul_br->SetCookie($this->asp_params_cookies);
            $emul_br->SetPost($this->asp_params_post);

            $emul_br->SetPost(array(
                    $lot['tid'] . '.x' => rand(1, 13),
                    $lot['tid'] . '.y' => rand(1, 13),
            ));

            $emul_br->exec();

            $location = $emul_br->GetResponseHeader('Location');
            if ($location) {
                $content = $this->emul_br_get_body('http://zakupkiold.gov.ru' . $location);

                $table = preg_get('#id="ctl00_phWorkZone_BidPos".*?</table>#si', $content);
                $table = "<table " . $table;
                $table_array = $this->parse_table($table);

                array_shift($table_array);

                $pos[] = "Лот {$lot['id']}:";

                foreach ($table_array as $v) {
                    if (preg_match('/Новая позиция/ui', $v[1])) {
                        continue;
                    }
                    $pos[] = $this->text_clear_all_dots($v[1]);
                }
            }
        }

        if (count($pos) <= 1) {
            return NULL;
        }

        // FIXME если в названии есть лот
        if (count($pos) <= 2) {
            $lot  = $this->text_clear_hard($pos[1]);
            $name = $this->text_clear_hard($this->item_now['name']);

            if (!empty($lot) && !empty($name)) {
                if (preg_match("/$lot/u", $name)) {
                    return NULL;
                }
            }
        }

        $return = implode("\n", $pos);
        return $return;
    }

    function test_detail_get($id = 495388) {
        print_r($this->detail_get($id));
    }

    /**
     * DOC
     */
    /**
     * Получить документ
     * @param array $doc
     * @param array $item
     * @return array
     */
    function doc_get($doc, $item) {
        $this->loader->debug("zakupki, docs_load");

        $emul_br = $this->emul_br_init( $this->detail_link . $item['internal_id'] . '&TS=2' );
        $emul_br->exec();

        $this->asp_clear_params();
        $this->asp_set_params($emul_br);

        $emul_br = $this->emul_br_init( $this->detail_link . $item['internal_id'] . '&TS=2' );

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);

        $emul_br->SetPost(array(
                'ctl00$phWorkZone$DocumentList$ctl00$DocumentList$ctl' . sprintf("%02d", $doc['internal_id']) . '$btnDownload.x' => rand(1,13),
                'ctl00$phWorkZone$DocumentList$ctl00$DocumentList$ctl' . sprintf("%02d", $doc['internal_id']) . '$btnDownload.y' => rand(1,13),
                )
        );

        $emul_br->exec();

        $return['data'] = $emul_br->GetBody();

        $return['md5']  = md5($return['data']);
        $return['size'] = strlen($return['data']);

        //FIXME
        if( empty($return['data']) ) {
            exit("empty data from DOC");
        }

        //Content-Disposition: attachment; filename="Èçâåùåíèå î ïðîâåäåíèè îòêðûòîãî êîíêóðñà ¹19.doc"
        $f = $emul_br->GetResponseHeader('Content-Disposition');
        $f = preg_get("/filename=(.*)/i", $f);
        $f = preg_replace(array("/^['\"]+/i", "/['\"]+$/i"), "", $f);
        $f = preg_replace("/['\"]/i", "_", $f);

        $return['filename'] = $f;

        $this->loader->debug($emul_br->GetAllHeaders());

        return $return;
    }

}



/**
 * REGION
 */
class parser_1_0001_01_0_00_zakupki_region extends parser_1_0000_01_0_00_one {

    function region_load() {
        $list = $this->region_list();
        $this->region_insert_all($list);
    }

    function item_load_region_insert($list) {
        foreach ($list['items'] as $item) {
            if ( $item['status'] !== $this->item_need_section ) {
                continue;
            }

            $item['trans']          = 0;

            $item['region_dict_id'] = $this->region_dict_id;
            $item['region_id']      = $this->region_id;

            /*
             * FIXME: New - always new
            */
            list($item['status_dict_id'], $item['status_id']) = $this->status_ids('New');
            list($item['type_dict_id'], $item['type_id']) = $this->type_ids($item['type']);

            if(!$this->item_insert_one($item)) {
                $this->debug("<h3>BREAK, this item in DB</h3>");
                return false;
            }
        }
        return true;
    }

    function region_list($test = 1) {
        if(!$test) {
            $content = $this->emul_br_get($this->list_search_link);

            if(strlen($content) < 500) {
                trigger_error('ERROR on load http', E_USER_WARNING);
                exit("CRITIKAL exit");
            }
        }
        else {
            $content = file_get_contents(dirname(__FILE__) . "/test_parse_search_form.htm");
        }

        $content_dom = str_get_html($content);

        $regs_tds = $content_dom->find('table#tbl_reg td');

        foreach($regs_tds as $reg) {
            $name      = $reg->find('label',0)->innertext;
            $checkbox  = $reg->find('input[type=checkbox]',0)->name;
            $hidden    = $reg->find('input[type=hidden]',0)->name;
            $reg_id    = preg_get("/\\\$ctl(\d+)/i", $checkbox);
            $reg_kod   = $reg->find('input[type=hidden]',0)->value;

            $return[] = array(
                    'name'      => $name,
                    'checkbox'  => $checkbox,
                    'hidden'    => $hidden,
                    'reg_id'    => $reg_id,
                    'reg_kod'   => $reg_kod,
                    'internal_id'    => (int)$reg_id,
                    'internal_param' => "$reg_id|$reg_kod",
            );
        }

        $content_dom->clear();

        return $return;
    }
}


/**
 * zakupki.gov.ru парсинг протоколов
 */
class parser_1_0001_01_0_00_zakupki_protocol extends parser_1_0000_01_0_00_one {
    public  $base_protocol_link      = 'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=';
    private $base_protocol_list_link = 'http://zakupkiold.gov.ru/Tender/ViewPurchase.aspx?TS=3&PurchaseId=';

    protected $links = array (
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=517934&TypeId=5',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=602677&TypeId=5',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=515898&TypeId=5',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=455872&TypeId=3',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=456111&TypeId=4',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=456104&TypeId=4',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=456123&TypeId=5',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=455937&TypeId=4',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=455959&TypeId=3',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=456075&TypeId=3',
            'http://zakupkiold.gov.ru/Protocol.aspx?PurchaseId=455746&TypeId=5',
    );

    protected $cur_link = '';

    function get($id) {
        $list_link = $this->base_protocol_list_link . $id;
        $this->loader->debug($list_link);
        $content = $this->emul_br_get_body($list_link);
        if (strlen($content) < 1000) {
            return false;
        }

        $links = preg_get_all('/onclick="notification\(\D+\d+, (\d+)\)/', $content);

        $typeid = (int) @ array_pop($links);

        $this->loader->debug("TypeId=$typeid");

        if (!$typeid) {
            echo "FIXME";
            return false;
        }

        $this->cur_link = $this->base_protocol_link . $id . "&TypeId=$typeid";

        $this->loader->debug($this->cur_link);

        $content = $this->emul_br_get_body($this->cur_link);
        if (strlen($content) < 1000) {
            return false;
        }
        return $content;
    }

    /**
     * выбираем все вхождения цены с записью RUB
     * @param <type> $content
     * @return <type>
     */
    function parse_simple($content) {
        $prices = preg_get_all('#(\d+\.?\d*)\s{0,2}RUB#i',$content);
        if (count($prices)>5) {
            trigger_error("Тoo many fields in ".$this->cur_link);
        }

        if (count($prices)<1) {
            trigger_error("Where prices? LINK = ".$this->cur_link);
            return false;
        }

        $arr = array();
        foreach ($prices as $price) {
            $arr[] = (int)$price;
        }

        sort($arr);
        $tables = preg_get_all('#(<table.*?</table>)#is',$content);
        foreach ($tables as $table) {
            if (preg_get('#участника\sразмещения\sзаказа#ius',$table)) {
                $count_members = substr_count($table,'<tr')-1;
                break;
            }
        }
        return array(
                'members_count' => $count_members,
                'price_best'    => $arr[0],
                'price_max'     => array_pop($arr),
        );
    }

    var $max_reg = '#Максимальная\s*цена#ius';
    //выбираем нужные нам ключ=>значение
    function min_max($content) {
        $arr = $this->list_parse_pre($content);
        foreach ($arr as $key=>$val) {
            if (preg_get($this->max_reg,$key)) {
                $max = (int)preg_replace("/[^\d.,]/", "", $val);
            }
            $min_ = preg_get('#предложен.*?бол.*?низк.*?цен.*?(\d+\.\d+)#ius',$key);
            if (!empty($min_)) {
                $min = (int)$min_;
            }
        }

        return array(
                'min' => $min,
                'max' => $max,
        );
    }

    //парсим протокол на ключ=>значение
    function list_parse_pre($content) {
        $content = preg_replace ('#(</?thead.*?>|</?tbody.*?>|<style.*?</style>)#is',"",$content);
        $res_arr = array();
        //разбор таблиц
        $tables = preg_get_all("#<table.*?>.*?</table>#ius",$content);
        $i = 0;
        if (count($tables)>0)
            foreach($tables as $tbl) {
                $i++;
                $trd = $this->parse_table($tbl);
                $arr = array();
                if (count($trd[0])>2) {
                    for ($j=1;$j<count($trd);$j++) {
                        for($r=0;$r<count($trd[0]);$r++)
                            $arr['table'.$i.')'.$this->cl($j.'_'.$trd[0][$r])]=$this->cl($trd[$j][$r]);
                    }
                }elseif (count($trd[0])==2) {
                    for ($j=0;$j<count($trd);$j++) {
                        if (empty($trd[$j][0]) && !empty($trd[$j][1])) $trd[$j][0]=$j.'';
                        $arr['table'.$i.')'.$this->cl($trd[$j][0])]=$trd[$j][1];
                    }
                }
                if (!empty($arr))
                    $res_arr+=$arr;
            }
        $content = preg_replace("#<table.*?>.*?</table>#ius",'<br>',$content);
        $at = explode('<br>',$content);
        $i=0;

        while ($i<count($at)) {
            // ищем схему 1: <b>key</b>value
            if (empty($at[$i])) {
                $i++;
                continue;
            }
            list($text_b,$text_val) = preg_get('#<b>(.*?)</b>(.*)#',$at[$i]);
            if (!empty($text_b) && !empty($text_val)) {
                $this->add_to_array($res_arr,$this->text_clear_all($text_b),$text_val);
                $i++;
                continue;
            }
            // схема 2: <b>key</b><br>value

            $text = $this->text_clear_all($at[$i]);
            $text_b = preg_get('#<b>(.*?)</b>#',$at[$i]);
            if (!empty($text_b) && $i+1<count($at) && $text_b==$text && !empty($at[$i+1])) {
                $this->add_to_array($res_arr,$text,$this->text_clear_all($at[$i+1]));
                $i++;
                $i++;
                continue;
            }
            // схема 3-4: key:value и key:<br>value
            list($key,$val) = preg_get('#(.*?)(?<!\d):(.*)#is',$text);
            if (!empty($key)) {
                if ($i+1==count($res_arr)) {
                    $this->add_to_array($res_arr,$key,$val);
                    $i++;
                    continue;
                }
                if (!empty($val)) {
                    $this->add_to_array($res_arr,$key,$val);
                    $i++;
                    continue;
                }
                if ($i+1<count($res_arr) && !empty($at[$i+1])) {
                    $this->add_to_array($res_arr,$key,$this->text_clear_all($at[$i+1]));
                }
            }
            //схема 5 - key-число\значение
            list($key,$val) = preg_get('#(.*?)-\s*(\d+\.\d+)#',$text);
            if (!empty($key) && !empty($val)) {
                $this->add_to_array($res_arr,$this->text_clear_all($key),$this->text_clear_all($val));
                $i++;
                continue;
            }
            //схема 6 - только текст
            $this->add_to_array($res_arr,$text,'');
            $i++;
        }
        return $res_arr;
    }

    function add_to_array(&$arr,$key,$val) {
        $i = 1;
        $key_ = $key.'';
        while (array_key_exists($key_,$arr)) {
            $key_=$key.$i.'';
            $i++;
        }
        $arr[$key_]=$val;
        return $arr;
    }

    function cl($c) {
        return $this->text_clear_all($c);
    }

}
