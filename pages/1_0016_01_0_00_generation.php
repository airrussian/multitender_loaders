<?php
/**
 * Система проведения тендеров ПГ "Генерация"
 * 23.07.2010 - Написание
 * 09.08.2010 - запуск
 */
class loader_1_0016_01_0_00_generation extends loader_1_0000_02_0_00_temp {

    public $base_url = 'http://tenders.generation.ru/';
    public $list_link = 'http://tenders.generation.ru/tenderlist.php';
    public $parser_name = 'parser_1_0016_01_0_00_generation';
    public $parser_name_detail = 'parser_1_0016_01_0_00_generation_detail';
    public $fields_list = array(
        'num',
        'name',
        'date_end',
        'type',
        'price'         => 'maybenull',
        'internal_id',
        'customer'      => 'maybenull',
    );
    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );
    public $break_by_pass = false;
    public $item_rewrite  = false;

    protected $run_detail_order = 'date_end ASC';

    function test_detail($id=5544) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->parser->detail_all($id);
    }

}

class parser_1_0016_01_0_00_generation extends parser_1_0000_02_0_00_temp {

    protected $list_colomn = array(
        'Код'           => 'internal_id|clear_all',
        'Наименование'  => 'name|clear_all',
        'Статус'        => 'type|clear_all',
        'Окончание'     => 'date_end|date_convert_sql',
    );

    function list_get_page($link, $page = 1) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body($link);
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach ($parse['items'] as $item) {
            $item = $this->list_set_colomn($item);

            if (preg_match("#Закончен#siu", $item['type'])) {
                continue;
            }

            $item['num']  = $item['internal_id'];
            $item['type'] = trim(preg_replace("#\(.*?\)#", "", $item['type']));

            $items[] = $item;
        }

        $return = array(
            'page_total' => 1,
            'page_now' => 1,
            'items_total' => $parse['items_total'],
            'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find('div#pagemsgdiv', 0)->nextSibling()->nextSibling()->nextSibling();

        $str = $ten->outertext;

        $arr = $this->parse_table($ten->outertext);
        for ($i = 0; $i < count($arr) - 1; $i++) {
            $arr[$i] = $arr[$i + 1];
        }
        unset($arr[count($arr) - 1]);

        $ret['items'] = $this->createstruct($arr);

        $ret['page_now'] = 1;
        $ret['page_total'] = 1;
        $ret['items_total'] = count($ret['items']);

        $content_dom->__destruct();

        return $ret;
    }

}

class parser_1_0016_01_0_00_generation_detail extends parser_1_0000_02_0_00_temp {

    protected $detail_link = 'http://tenders.generation.ru/tendercard.php?tenderid=';

    function detail_all($id) {
        $link = $this->detail_link . $id;
        $content = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);

        $return = $this->detail_sort_2($parse_detail);

        $docs = preg_get_all("#<a.*?a>#sui", $parse_detail['Вложения: (документация по тендеру)']);

        $i = 0;
        foreach ($docs as $doc) {
            $return['doc'][$i]['name'] = preg_get("#<a.*?>(.*?)</a>#si", $doc);
            $return['doc'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"].*?>#si", $doc);
            $return['doc'][$i]['internal_id'] = abs(crc_p($return['doc'][$i]['detail_link'] . $return['doc'][$i]['name']));
            $i++;
        }

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['other']['Условия поставки:']);

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $info = $detail_dom->find("table.breakline_bottom", 1)->find("table.breakline_bottom", 0);

        $arr = $this->parse_table($info->outertext);

        $detail = array();
        foreach ($arr as $row) {
            $detail[$this->text_clear_all($row[0])] = $row[1];
        }

        $detail_dom->__destruct();

        return $detail;
    }

}

