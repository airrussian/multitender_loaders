<?php
/**
 * http://konkurs.agzrt.ru/Reductions.aspx?stage=1
 * http://konkurs.agzrt.ru/ViewReduction.aspx?id=49498
 * @author Paul-labor@yandex.ru
 */
class loader_1_0004_02_0_00_zakazrf2 extends loader_1_0004_01_0_00_zakazrf1 {
    public $base_url           = 'http://konkurs.agzrt.ru/';
    public $list_link          = 'http://konkurs.agzrt.ru/Reductions.aspx?stage=1';
    public $parser_name        = 'parser_1_0004_02_0_00_zakazrf2';
    public $parser_name_detail = 'parser_1_0004_02_0_00_zakazrf2_detail';

    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'date_conf',
            'date_end',
            'customer',
            'price'      => 'maybenull',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; // совершенно не ясна сортировка
    public $item_rewrite  = true;

    protected $run_detail_order = 'date_conf ASC';

    public $page_total_rewrite = false;

    public $sleep_list = 3;

    // получение страниц СКОРЕЕ ВСЕГО требует переработки
    public $page_last = 50;

}

class parser_1_0004_02_0_00_zakazrf2 extends parser_1_0004_01_0_00_zakazrf1 {
    protected $colomn = array(
            'Номер'             => 'num',
            'Время начала'      => 'date_conf|date_convert_short',
            'Название'          => 'name',
            'Заказчик'          => 'customer',
            'Общая цена'        => 'price|to_price',
            7                   => 'detail_link',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            
            $item['internal_id'] = (int) preg_get('/id=(\d+)/iu', $item['detail_link']);
            $item['date_end']    = $item['date_conf'];

            $items[] = $item;
        }

        $return = array (
                'page_total'  => ceil($parse['items_total']/sizeof($items)),
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $span  = $content_dom->find('span#ctl00_Content_RecordsCountLabel', 0);
        $return['items_total'] = $span->innertext;

        $table = $content_dom->find('table#ctl00_Content_ReductionsGrid',0);
        $arr = $this->parse_table($table->outertext);

        $return['items']=$this->createstruct($arr);

        $content_dom->clear();

        return $return;
    }

}

class parser_1_0004_02_0_00_zakazrf2_detail extends parser_1_0004_01_0_00_zakazrf1_detail {

    protected $detail_link = 'http://konkurs.agzrt.ru/ViewReduction.aspx?id=';

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }

        $content = tidy_repair_string($content, $this->tidy_config);

        $header = preg_get('#<hr />(.*?)<hr />#uis', $content);
        $return['html'] = $this->text_clear_html_gently($header);

        if (empty($return['html'])) {
            echo $content;
            exit('EMPTY return.html');
        }

        $this->content_html = $content;

        $return['db']['customer_email']   = $this->span('ctl00_Content_ReductionViewForm_CustomerEMailLabel');
        $return['db']['customer_address'] = $this->span('ctl00_Content_ReductionViewForm_CustomerPostalAddressLabel');
        $customer_address2                = $this->span('ctl00_Content_ReductionViewForm_CustomerPhysicalAddressLabel');

        $return['db']['customer_phone']   = $this->span('ctl00_Content_ReductionViewForm_CustomerPhoneLabel');
        $return['db']['date_publication'] = $this->text_date_convert_short($this->span('ctl00_Content_ReductionViewForm_PublicationDateLabel'));

        $possitions = $this->span_many('Content_ReductionViewForm_ViewLotsListControl_LotsList_ctl\d+_SubjectLabel');
        if (count($possitions) > 1) {
            $stuff = '';
            foreach ($possitions as $v) {
                $stuff .= "$v\n";
            }
            $return['db']['stuff'] = trim($stuff);
        }

        // определяем регион
        $region_id = $this->loader->geocoder_zakazrf($return['db']['customer_address']);
        if (empty($region_id) || $region_id < 0) {
            $region_id = $this->loader->geocoder_zakazrf($customer_address2);
        }
        if (empty($region_id) || $region_id < 0) {
            $region_id = 0;
        }
        $return['db']['region_id'] = $region_id;

        $docs_array = preg_get_all_order('#File\.ashx\?id=([\w\.\-]+)">(.*?)</a>#iu', $content);
        if ($docs_array) {
            foreach ($docs_array as $d) {
                $return['docs'][] = array(
                        'name'        => $d[2],
                        'detail_link' => $d[1],
                        'internal_id' => crc_p($d[1]),
                );
            }
        }

        return $return;
    }

}
