<?php
/**
 * Базовый класс для площадок, где необходимая сразу информация находиться
 * лишь в детайле. Загружает информацию в item лишь при загрузке детайла.
 * @author Paul-labor@yandex.ru
 */
class loader_1_0000_02_0_00_temp extends loader_1_0000_01_0_00_one {
    public $item_table = 'item_temp';
    public $page_total_rewrite = true;

    function update_in_process() {
        $this->debug("update_in_process: error_list_items = $this->error_list_items, test_nodb = $this->test_nodb");
        if ( !$this->error_list_items && !$this->test_nodb ) {
            $this->db->Execute("UPDATE item_temp SET in_process=NULL WHERE site_id=$this->site_id AND in_process IS NOT NULL");
        }
    }

    // FIXME clear detail_run_time

    protected $run_detail_order = 'id DESC';
    

    function run_detail() {
        $item_temp = new item_temp;

        // key: id
        if(!$item_temp->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }

        $item_temp->detail_count_error ++; // при успехе убавим
        $item_temp->detail_count_load  ++;
        $item_temp->detail_run_time = $item_temp->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item_temp->Save();

        $this->run_detail_one( $item_temp->id );

        $item_temp->Reload("id = $item_temp->id");

        if ($item_temp->item_id) {
            $item = new item;
            $item->Load("id = $item_temp->item_id");
            $item->detail_run_time   = NULL; // при успехе обнулить
            $item->detail_count_error --;

            if ($item->detail_count_error < 0) {
                $item->detail_count_error = 0;
            }
            $item->Save(TRUE);
        }

        $item_temp->detail_run_time   = NULL; // при успехе обнулить
        $item_temp->detail_count_error --;

        if ($item_temp->detail_count_error < 0) {
            $item_temp->detail_count_error = 0;
        }
        $item_temp->Save();

        $this->debug("RUN_DETAIL id = $item->id");
    }

    /**
     * Загрузка одно детайла
     * @return boolen
     */
    function run_detail_one($id) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $this->debug("<h3>RUN detail = $id</h3>");

        $id = (int) $id;
        $item_temp = new item_temp;
        if( ! $item_temp->Load("site_id = $this->site_id AND id = $id")) {
            exit("VERY CRITICAL ERROR, file " . __FILE__);
        }

        // передаем текущие данные в парсер
        $this->parser->item_now = $this->ar_to_array($item_temp);

        $parse = $this->parser->detail_all($item_temp->internal_id);

        if(empty($parse)) {
            exit('Empty parse');
        }
        print_r($parse);

        // переносим из item_temp в item
        $item = new item;
        $item->Load("site_id = $this->site_id AND internal_id = $item_temp->internal_id");

        $copy = $this->parser->item_now;
        unset($copy['id']);

        $this->array_to_ar($item, $copy);

        $item->Save(TRUE);

        $item_temp->item_id = $item->id;
        $item_temp->Save();

        $this->detail_insert($item->id, $parse);

        // переносим из item в item_temp (синхронизация)
        $item->ReLoad("id = $item->id");
        $item_temp->ReLoad("id = $item_temp->id");

        $copy = $this->ar_to_array($item);
        unset($copy['id']);

        $this->array_to_ar($item_temp, $copy);
        $item_temp->Save();

        return true;
    }

}

/**
 * Base parser for page with list
 * @author Paul-labor@yandex.ru
 */
class parser_1_0000_02_0_00_temp extends parser_1_0000_01_0_00_one {
    

}
