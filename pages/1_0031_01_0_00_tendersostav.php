<?php
/**
 * TenderSostav http://www.tensostav.ru/
 * 15.09.2010 - написание 
 *
 */
class loader_1_0031_01_0_00_tendersostav extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.tensostav.ru/';
    public $list_link           = 'http://www.tensostav.ru/index_new.php?tab=0&srhIDREGION=5&page=';
    public $parser_name         = 'parser_1_0031_01_0_00_tendersostav';
    public $parser_name_detail  = 'parser_1_0031_01_0_00_tendersostav_detail';

    public $fields_list = array(
            'name',
            'date_publication',
            'date_end',
            'internal_id',
            'customer',
            'price' => 'maybenull',
    );
    
    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

    function test_detail($id=4133) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);

        var_dump($arr);
    }

    function geocoder_tendersostav($string) {
        $this->debug('GEOCODER, INPUT=', $string);

        if (preg_match("#Россия#sui", $string)) { return false; }
        if (preg_match("#Санкт-Петербург#sui", $string)) { return 78; }

        $region_id = $this->geocoder_by_index_auto($string);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($string);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_tendersostav, region= $region_id ($region_name)", $string);

        return $region_id ? $region_id : false;
    }
}

class parser_1_0031_01_0_00_tendersostav extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Период подачи заявки'  => 'date_publication|date_convert',
            'Тендер'                => 'name|clear_all',
            'Регион'                => 'region_id|clear_all',
            'Компания'              => 'customer|clear_all',
            'Сумма'                 => 'price|to_price',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.$page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_publication']>21000000) {
                continue;
            }
            $item['price'] = 30 * $item['price'];
            $item['date_end'] = $this->text_date_convert(preg_get("#-(.*)#si", $item['date_publication_src']));
            $item['internal_id'] = preg_get("#href=\"/showtender/(\d+)/\"#si", $item['name_src']);
            $item['region_id'] = $this->loader->geocoder_tendersostav($item['region_id']);
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table.searchTable", 0);

        $arr = $this->createstruct($this->parse_table($tenders->outertext));
        $ret['items'] = $arr;

        $info = $tenders->prev_sibling();
        $pager = $info->find("div.searchPages", 0);
        $ret['page_total'] = max(preg_get_all("#<a.*>(\d+)</a>#si", $pager->innertext));
        $ret['page_now'] = $pager->find("span",0)->innertext;
        $ret['items_total'] = preg_get("#Найдено по запросу.*?<b.*?>(\d+)</b>#sui", $info->innertext);

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0031_01_0_00_tendersostav_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
        'Адрес'         => 'customer_address|clear_all',
        'Телефон'       => 'customer_phone|clear_all',
        'E-mail'        => 'customer_email|clear_all',
    );

    function detail_all($id) {
        $link         = "http://www.tensostav.ru/showtender/$id";
        $content      = $this->emul_br_get_body($link);
        $parse_detail = $this->list_parse_pre($content);
        $return = $this->detail_sort_3($parse_detail);
        $region_id = $this->loader->geocoder_tendersostav($return['db']['customer_address']);
        if (!$region_id) {
            $region_id = $this->loader->geocoder_tendersostav($return['other']['Регион']);
        }
        $return['db']['region_id'] = $region_id;
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $det = $detail_dom->find("table#presenTable", 0);
        
        foreach ($det->find("div.formPresentation") as $div) {
            $key = $div->find("label", 0)->innertext;
            $val = trim(preg_get("#<div.*label>:(.*?)</div>#si", $div->outertext));
            $key = str_replace($eng_keys, $rus_keys, $key);
            $arr[$key] = $val;
        }

        $detail_dom->__destruct();

        return $arr;
    }

}


