<?php
/**
 * ЭТП ММВБ «Госзакупки»
 * @author Paul-labor@yandex.ru
 */

class loader_1_0008_01_0_00_mmvb extends loader_1_0000_02_0_00_temp {

    public $base_url    = 'http://www.etp-micex.ru/';
    public $list_link   = 'http://www.etp-micex.ru/auction/catalog/all/page/';
    public $parser_name = 'parser_1_0008_01_0_00_mmvb';
    public $parser_name_detail = 'parser_1_0008_01_0_00_mmvb_detail';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'price'      => 'maybenull',
            'customer'   => 'maybenull',
            'date_publication',
            'date_end',
            'date_conf',
            'status'
    );
    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');

    public $break_by_pass = true;

    public $debug = true;

    function test_detail_get($id = 37) {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $arr = $this->parser->detail_all($id);
        var_dump($arr);
    }
}

/**
 * LIST 
 */
class parser_1_0008_01_0_00_mmvb extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Номер извещения'           => 'num|clear_all',
            'Наименование'              => 'name|clear_all',
            'цена контракта'            => 'price|to_price',
            'Организатор'               => 'customer',
            'Дата и время публикации'   => 'date_publication|date_convert',
            'Дата и время окончания'    => 'date_end|date_convert',
            'Дата и время начала'       => 'date_conf|date_convert',
            'Статус'                    => 'status'
    );

    function list_get_page( $link, $page = 1 ) {
        $page = (int) $page;
        $this->loader->debug("NEXT PAGE = $page");
        $this->loader->debug("LINK = " . $link.$page."/auctionTypeId/2400/");
        $content = $this->emul_br_get($link.$page."/auctionTypeId/2400/");
        return $content;
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            /*
            if (preg_match("#завершен|не состоялся|отменён#sui", $item['status'])) {
                continue;
            }*/

            $item['num'] = preg_get("#</a>.*?\((.*?)\)#sui", $item['num_src']);
            $item['internal_id'] = preg_get("#<a.*?auctionId/(\d+).*?</a>#sui", $item['num_src']);
            $item['name'] = preg_replace("#\(MVB\d+\)#", "", $item['name']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        
        $content = tidy_repair_string($content, $this->tidy_config);

        $content_dom = str_get_html($content);

        $return['items_total'] = $content_dom->find("div#total_pages", 0)->innertext;
        $paginator = $content_dom->find("div#paginator", 0);
        $return['page_now'] = $paginator->find("em",0)->innertext;
        $return['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paginator->innertext));

        $table = $content_dom->find("table#supplier-auction-catalog", 0)->outertext;

        /* $table = preg_get("#<table.*?id=['\"]supplier-auction-catalog['\"].*?>.*?</table>#sui", $content);* */



        $arr = $this->parse_table($table);

        $content_dom->__destruct();

        $return['items'] = $this->createstruct($arr);

        return $return;
    }
}

/*
 * detail
*/
class parser_1_0008_01_0_00_mmvb_detail extends parser_1_0000_02_0_00_temp {
    protected $detail_link  = 'http://www.etp-micex.ru/auction/catalog/view/auctionId/';
    
    
    public $detail_sort = array(
        "Сведения об Организаторе ЭА\/Наименование организации"      => 'customer',
        "Сведения об Организаторе ЭА\/Почтовый адрес"                => 'customer_address',
        "Сведения об Организаторе ЭА\/Адрес электронной почты"       => 'customer_email',
        "Сведения об Организаторе ЭА\/Номера контактных телефонов"   => 'customer_phone',
       );

    function detail_all($id) {
        $content = $this->detail_get($id);
        return $this->detail_parse($content);
    }

    function detail_get($id) {
        $this->loader->debug("NEXT PAGE = $id");
        $this->detail_link = 'http://www.etp-micex.ru/auction/catalog/view/auctionId/'.$id;
        $content = $this->emul_br_get( $this->detail_link );
        return $content;
    }

    function detail_parse($content) {

        $detail_parse = $this->detail_parse_pre($content);

        $return = $this->detail_sort_3($detail_parse);
        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);
        $return['content'] = $this->detail_content_cache($content);

        return $return;
    }

    function detail_parse_pre($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find("table#view-auction-information", 0);
        foreach ($table->find("fieldset") as $fieldset) {
            $group_name = $fieldset->find("legend", 0)->innertext;
            $tblgrp = $fieldset->find("table", 0);
            foreach ($tblgrp->find("tr") as $tr) {
                if ($tr->find("td", 0) && $tr->find("td", 1)) {
                    $name  = $tr->find("td", 0)->innertext;
                    $value = $tr->find("td", 1)->innertext;
                    $arr[$group_name."/".$name] = $value;
                }
            }
        }        
        return $arr;
    }

}
