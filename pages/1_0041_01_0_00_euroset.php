<?php
/**
 * Евросеть
 * Написание: 31.01.2011
 */
class loader_1_0041_01_0_00_euroset extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://euroset.ru/corp/tender/tender/';   
    public $parser_name         = 'parser_1_0041_01_0_00_euroset';
    public $parser_name_detail  = 'parser_1_0041_01_0_00_euroset_detail';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function run_list($region_id = null) {
        $this->debug('START');

        $regions = $this->parser->get_regions($this->base_url);

        $this->debug('START regions, count = ' . sizeof($regionArray));

        foreach($regions as $key => $region) {
            $this->region_dict_id = $region['id'];
            $this->region_id      = $region['id'];

            $item = new item();
           
            $this->debug('START region = ' . $this->region_id . ',' . $region->internal_param);
            $this->list_link = $this->base_url . "?arrFilter_pf[THEME]=&set_filter=Y&arrFilter_pf[TO_MAG]=$key&PAGEN_1=";

            parent::run_list();

            //sleep($this->item_sleep);
        }
        $this->list_link = $this->base_url;
        parent::run_list();
    }
}

class parser_1_0041_01_0_00_euroset extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_name'                => 'name|clear_all',
            'P_date_publication'    => 'date_publication|clear_all|date_convert_text_full',
            'P_date_end'            => 'date_end|clear_all|date_convert_text_full',
            'P_internal_id'         => 'internal_id',
    );

    function get_regions($link) {
        $content = $this->emul_br_get_body( $link );
        $content = $this->text_from_win($content);
        $content_dom = str_get_html($content);
        $arr = array();
        foreach ($content_dom->find("div.town",0)->find("option") as $option) {
            $key = (int) preg_get("#value=['\"](\d+)['\"]#si", $option->outertext);
            $val = $this->text_clear_all($option->innertext);
            $region_id = $this->loader->geocoder_search_name($val);
            if ($region_id) { $arr[$key] = $region_id; }
        }
        return $arr;
    }

    function list_get_page( $link, $page=1 ) {
        if (preg_match("#PAGEN_1#si", $link)) { $link .= $page; } else { $link .= "?PAGEN_1=$page"; }
        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            if ($item['date_end'] < date("Ymd")) {
                continue;
            }
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);
        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div.corp_left_column", 0);
        $date_end = NULL;
        foreach ($tenders->find("div.pr_holder") as $tender) {
            $date_end = @ $tender->find("p.date", 0)->innertext ? $tender->find("p.date", 0)->innertext : $date_end;
            $arr[] = array(
                'P_name'                => $tender->find("a.pr_shortcaut_holder_a", 0)->innertext,
                'P_internal_id'         => preg_get("#/corp/tender/tender/(\d+)/#si", $tender->find("a.pr_shortcaut_holder_a", 0)->outertext),
                'P_date_publication'    => preg_get("#опубликовано:(.*?)</p>#siu", $tender->innertext),
                'P_date_end'            => $date_end,
            );
        }

        $paginator = $content_dom->find("div.cont_cent", 0);
        $ret['page_now']    = (int) $paginator->find("b",0)->innertext;
        $ret['items_total'] = (int) preg_get("#Тендеры.*?из*.?(\d+)#siu", $paginator->innertext);
        $ret['page_total']  = (int) Ceil($ret['items_total'] / 10);

        $ret['items'] = $arr;
        
        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0041_01_0_00_euroset_detail extends parser_1_0000_01_0_00_one {

    public $item_now;

    function detail_get($id) {
        $this->detail_link = "http://euroset.ru/corp/tender/tender/$id/";
        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $return['content'] = $this->detail_content_cache($content);

        $content_dom = str_get_html($content);
        $html = $content_dom->find("div#component", 0)->find("table", 1)->find("td", 0)->innertext;
        $content_dom->clear();


        $html = tidy_repair_string($html, $this->tidy_config);
        $html = preg_replace("#<a.*?</a>#si", "", $html);
        $html = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $html);
        $html = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $html);
        $html = preg_replace(array("#<script.*?</script>#si", "#<style.*?</style>#si", "#<!\[CDATA.*?</script>#si"), "", $html);
        $html = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $html);
        $html = preg_replace("#Этот e-mail адрес защищен.*?Javascript#siu", "", $html);
        $return['html'] = $html;

        $de = preg_get("#<strong>.*?(\d+ \D+ \d{4}) года.*?</strong>#sui", $html);
        $return['db']['date_end'] = $this->text_date_convert_text_full($de);


        return $return;
    }
}