<?php
/**
 * Online Contract
 * 23.07.2010 - Написание
 * 09.08.2010 - запуск
 */
class loader_1_0015_01_0_00_onlinecontract extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.onlinecontract.ru/';
    public $list_link           = 'http://www.onlinecontract.ru/tenders/c_';
    public $parser_name         = 'parser_1_0015_01_0_00_onlinecontract';   

    public $fields_list = array(
            'name',
            'date_end',
            'type',
            'price'         => 'maybenull',
            'internal_id',
            'customer'      => 'maybenull',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0015_01_0_00_onlinecontract extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'тип'             => 'internal_id|clear_all',
            'предмет закупки' => 'name|clear_all',
            'начальная цена'  => 'price|to_price',
            'дата завершения' => 'date_end|date_convert',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.$page.".html" );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['name']        = $this->text_clear_all(preg_get("#<strong>(.*?)</strong>#si", $item['name_src']));
            $item['customer']    = $this->text_clear_all(preg_get("#<a.*?jumpcompany.*?>(.*?)</a>#si", $item['name_src']));
                        
            $item['date_end']    = $this->text_date_convert(preg_get("#\d{2}.\d{2}.\d{4}#", $item['date_end_src']));
            
            $items[] = $item;

        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);
        
        $content = preg_replace("#<!--.*?-->#si", "", $content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find('div.content2', 0)->find("table", 0); 

        $ret['items'] = $this->createstruct($this->parse_table($ten->outertext));

        $paginator = $content_dom->find("div.b-pager__pages", 0);
        $ret['page_now'] = $paginator->find("a.b-pager__current", 0)->innertext;
        $ret['page_total'] = max(preg_get_all("#<a.*?>(\d+)</a>#sui", $paginator->innertext));
        $ret['items_total'] = NULL;
        
        $content_dom->__destruct();

        return $ret;
    }
}
