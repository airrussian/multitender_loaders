<?php
/**
 * ЭТП ММВБ «Госзакупки»
 * @author airrussian@mail.ru
 */

class loader_1_1004_01_0_00_mmvb extends loader_1_1000_01_0_00_zakupki {

    public $base_url    = 'http://www.etp-micex.ru/';
    public $list_link   = 'http://www.etp-micex.ru/auction/catalog/all/auctionTypeId/2400/page/1/rss/1/';
    public $parser_name = 'parser_1_1004_01_0_00_mmvb';
    public $parser_name_detail = 'parser_1_1004_01_0_00_mmvb_detail';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',           
            'customer'   => 'maybenull',
            'date_publication',
            'date_end',
            'date_conf',           
    );
    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');

    public $break_by_pass = true;

    public $debug = true;


    function run_detail_query(&$obj) {
        if(!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }

}

/**
 * LIST 
 */
class parser_1_1004_01_0_00_mmvb extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
        'P_name'        =>  'name|clear_all',
        'P_link'        =>  'internal_id',
        'P_date'        =>  'date_publication|clear_all',
        'P_desc'        =>  'desc',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("LINK = $link");
        $content = $this->emul_br_get_body($link);
        return $content;
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id']        = preg_get("#auctionId/(\d+)#sui", $item['internal_id']);
            $item['name']               = trim(preg_replace("#\(MVB\S+\)#", "", $item['name']));
            $item['num']                = preg_get("#\(MVB\d+/(\d+)\)#siu", $item['name_src']);
            $item['customer']           = trim(preg_get("#<br /><strong>Организатор ЭА: </strong>(.*?)<br />#siu", $item['desc']));
            $item['date_publication']   = $this->text_date_convert(preg_get("#<br /><strong>Дата публикации извещения: </strong> (\d{2}.\d{2}.\d{4})<br />#siu", $item['desc']));
            $item['date_end']           = $this->text_date_convert(preg_get("#<br /><strong>Дата и время окончания срока подачи заявок: </strong> (\d{2}.\d{2}.\d{4})<br />#siu", $item['desc']));
            $item['date_conf']          = $this->text_date_convert(preg_get("#<br /><strong>Дата проведения электронного аукциона: </strong> (\d{2}.\d{2}.\d{4})<br />#siu", $item['desc']));

            $items[] = $item;
        }       

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
       
        $xml = simplexml_load_string($content);

        $items = array();
        foreach ($xml->xpath("/rss/channel/item") as $item) {
           
            $items[] = array(
                'P_name'  =>  (string) reset($item->xpath("title")),
                'P_link'  =>  (string) reset($item->xpath("link")),
                'P_desc'  =>  (string) reset($item->xpath("description")),
                'P_date'  =>  (string) reset($item->xpath("pubDate")),
            );
        }
      
        $return['items_total'] = 1;       
        $return['page_now'] = 1;
        $return['page_total'] = 1;
        $return['items'] = $items;

        return $return;
    }
}

class parser_1_1004_01_0_00_mmvb_detail extends parser_1_1000_01_0_00_zakupki_detail {

    function detail_all($id) {

        $num = $this->item_now['num'];

        $internal_id = $this->item_now['internal_id'];

        $id = $this->loader->GetIDbyNUM($num);
        if ($id) {
            $this->loader->debug("ZAKUPKI ID = $id");
            $ret = parent::detail_all($id);
            $this->loader->detail_link = "http://www.etp-micex.ru/auction/catalog/view/auctionId/$internal_id/";
            $ret['content'] = $this->detail_content_cache($this->emul_br_get_body($this->loader->detail_link));
        } else {
            $this->loader->debug("NOT ITEM WITH $num ON ZAKUPKI ");
            $this->loader->NoItem($this->item_now['id']);
            $ret = false;
        }

        return $ret;
    }


}