<?php
/**
 * http://roseltorg.ru/ - Единая электронная торговая площадка
 */

/**
 * roseltorg.ru
 * @author Paul-labor@yandex.ru
 */
class loader_1_0002_02_0_00_roseltorg extends loader_1_0000_02_0_00_temp {
    public $base_url    = 'http://roseltorg.ru/';
    public $list_link   = 'http://etp.roseltorg.ru/trade/future/?type_b=on&limit=250&page=';
    public $parser_name = 'parser_1_0002_01_0_00_roseltorg';
    public $parser_name_detail = 'parser_1_0002_01_0_00_roseltorg_detail';

    public $fields_list = array(
            'internal_id',
            'customer',
            'date_end'              => 'maybenull',
            'date_conf'             => 'maybenull',
            'name',
            'price'                 => 'maybenull',
            'detail_link_prefix',
            'num',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; 
    //public $item_rewrite  = true;

    protected $run_detail_order = 'date_conf ASC';

    public $page_total_rewrite = true;

    public function run_list() {
        $this->in_process       = 0;
        $this->list_pass_items  = 0;
        $this->list_in_db_items = 0;
        $this->error_list_items = 0;
        $break_by_pass_count = $this->break_by_pass_count;

        $page_total = 1;
        for ( $page = 1; $page <= $page_total; $page++ ) {
            $this->debug("<h3>RUN $page page of $page_total</h3>");

            if ($this->break_by_pass) {
                // FIXME other files
                //if ( ($this->list_pass_items) && ($this->in_process === 0) ) {
                $this->debug( "<h3>list_in_db_items = $this->list_in_db_items</h3>" );
                if ( ($this->list_in_db_items) && ($this->in_process === 0) ) {
                    $break_by_pass_count--;
                    if ($break_by_pass_count > 0) {
                        $this->debug( "<h3>BREAK by pass IN next, count = $break_by_pass_count</h3>" );
                    } else {
                        $this->debug( "<h3>BREAK by pass</h3>" );
                        break;
                    }

                }
            }

            if ( ! $this->validate_load(
            $content = $this->parser->list_get_page(
            $this->list_link, $page)
            ) ) {
                return false;
            }

            $list = $this->parser->list_parse( $content );

            if ( empty($list) ) {
                $this->debug("ERROR, CRITICAl, no connect?");
                return false;
            }

            if ($page === 1) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if (empty ($page_total)) {
                    $page_total = 1;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            // page_total rewrite
            if ($this->page_total_rewrite && $page > 1 ) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            if ( empty( $list['items'] ) ) {
                $this->debug( "<h3>Список items пустой, ошибка парсинга</h3>" );
                return false;
            }

            $this->list_insert(array_reverse($list['items']));

            if ( $this->list_stop($list) ) {
                $this->debug( "<h3>BREAK by list_stop</h3>" );
                break;
            }

            $this->debug("SLEEP $this->sleep_list s...");
            sleep( $this->sleep_list );
        }
        $this->update_in_process();
    }

    function run_x() {
        $file = file_get_contents('http://etp.roseltorg.ru/trade/view/?id=A_26113');
        ini_set('pcre.backtrack_limit', 1000000);

        $file = preg_replace("/�/", '', $file);

        $return = preg_get('#div id="text">(.+?)<hr#is', $file);

        echo $return;
        
    }

    function geocoder_roseltorg($content) {
        $content_cut = $this->parser->text_clear_roseltorg($content);

        $this->debug('GEOCODER_ROSELTORG, input=', $content_cut);

        $region_id = $this->geocoder_roseltorg_search_index($content_cut);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($content_cut);
        }

        if (!$region_id) {
            $i_id = preg_get('#&id=(\d{4,})#i', $content);
            $address = $this->parser->detail2_get_address($i_id);

            $this->debug('Address = ' . $address);

            $region_id = $this->geocoder_roseltorg_search_index($address);

            if (!$region_id) {
                $region_id = $this->geocoder_search_name($address);
            }
        }

        if (!$region_id) {
            $region_id = $this->geocoder_roseltorg_search_index($content);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_ROSELTORG, region= $region_id ($region_name)", $content_cut);

        return $region_id ? $region_id : false;
    }

    function geocoder_roseltorg_search_index($content) {
        $indexes = preg_get_all('/(\d{6})/', $content);
        if ($indexes) {
            foreach ($indexes as $index) {
                $index = (int) $index;
                if ($index == 117312 && count($indexes) == 1) {
                    continue;
                }
                $region_id = $this->geocoder_by_index($index);
                if ($region_id) {
                    return $region_id;
                }
            }
        }
        return false;
    }

}

class parser_1_0002_01_0_00_roseltorg extends parser_1_0000_01_0_00_one {

    //http://roseltorg.ru/set/announce.php?id=7718

    protected $colomn = array(
            'Предмет'           => 'name|text_clear_all',
            'Заказчик'          => 'customer|text_clear_all',
            'цена'              => 'price|text_clear_all|to_price',
            'Дата окончания'    => 'date_end|text_clear_all|date_convert',
            'Тип'               => 'region_id',
    );

    function list_get_page( $link, $page = 1 ) {
        $link = $link . $page;
        $this->loader->debug ("LINK = $link");
        return $this->emul_br_get($link);
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_conf']          = $item['date_end'];
            $item['detail_link_prefix'] = preg_get("#<a href=\"(.*?)\">#si", $item['name_src']);
            $item['internal_id']        = $this->make_internal_id($item['detail_link_prefix']);
            $item['num']                = preg_get("#/trade/view/\?id=(\S+)#si", $item['detail_link_prefix']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        // Парсинг таблицы
        $table = $content_dom->find('table.tbl_torgs tbody',0);

        $table_array = $this->parse_table($table->outertext);

        foreach ($table_array[0] as &$val) { $val = $this->text_clear_all($val); }

        $return = array();
        $return['items'] = $this->createstruct($table_array);

        $pages = $content_dom->find('div.link_page_bot ul.link_page_bot_lst',0)->innertext;
        $last_page = array_pop(preg_get_all('/page=(\d+)/', $pages));
        $return['page_now']   = null;
        $return['page_total'] = $last_page + 1;

        $content_dom->clear();

        return $return;
    }


    function text_clear_roseltorg($text) {
        $text = preg_replace(array('/<(span|font|strong)[^>]*>/mui', '/<b>/ui',
                '#</(span|font|strong|b|a)>#mui', '/<!--.*?-->/', '#<a .*?>#i'), ' ', $text);
        $text = preg_replace('/\h+/u', ' ', $text);
        $text = preg_replace('/\v+/u', '',  $text);
        $text = preg_replace(array('#<p.*?>#','#</p>#', '#<(br|hr).*?>#'), "\n", $text);
        $text = preg_replace(array('#^\h+#mu','#\h+$#mu'), '', $text);

        $text_a = explode("\n", $text);

        $text_a = preg_grep('/(место|адрес|\d{6}\D|\D{2}г\.|ул\.)/ui', $text_a);
        $text_a = preg_grep('/^.{25,999}$/ui',   $text_a);

        $text_a_f = array();
        foreach ($text_a as $v) {
            preg_replace(array(
                    '/(117312.*?Октября.*?9)/ui',
                    '/roseltorg.ru/ui',
                    //'/электронной.*?почты/ui', в одной строке
                    ), 'COUNT', $v, -1, $count);
            if ( ! $count ) {
                $text_a_f[] = $v;
            }
        }

        $text_a = array_slice($text_a_f, 0, 5);

        $text = implode("\n", $text_a);

        $text = preg_replace(array(
                '/место/ui',
                '/нахожден\S+/ui',
                '/заказчи\S+/ui',
                '/почтовый/ui',
                '/адрес/ui',
                '/почты/ui',
                '/ул\. \S+/',
                ), ' ', $text);

        $text = preg_replace('/\h+/u', ' ', $text);

        return $text;
    }


}

class parser_1_0002_01_0_00_roseltorg_detail extends parser_1_0002_01_0_00_roseltorg {
    protected $detail_link = 'http://roseltorg.ru/set/announce.php?id=';

    protected $detail_link_common = 'http://etp.roseltorg.ru/';

    protected $detail_sort = array(
            'Местонахождения'          => 'customer_address',
            'Реестровый номер'         => 'num',
            'Форма торгов'             => 'type',
            'окончания срока подачи'   => 'date_end|date_convert_search',
            'Дата проведения торгов'   => 'date_conf|date_convert_search',
    );

    public function detail_all($id) {
        if (preg_match('/^M?O?S?\d+$/', $this->item_now['num'])) {
            //print_r($this->detail_all_mso());
            //exit;
            return $this->detail_all_mso();
        } elseif (preg_match('/^A_\d+$/', $this->item_now['num'])) {
            //print_r($this->detail_all_a());
            //exit;
            return $this->detail_all_a();
        } else {
            trigger_error('UNKNOWN TYPE');
            exit;
        }
    }


    private function detail_all_mso() {
        $this->detail_link = $this->detail_link_common . $this->item_now['detail_link_prefix'];
        $this->loader->debug($this->detail_link);
        $content = $this->emul_br_get_body($this->detail_link);

        $content_dom = str_get_html($content);

        // Парсинг таблицы
        $struct = array();

        foreach($content_dom->find('.data-row') as $row) {
            $key = trim($row->find('.data-left',0)->plaintext);
            $key = preg_replace('/:$/', '', $key);
            $val = trim($row->find('.data-right',0)->plaintext);
            if ( !($key && $val)) {
                continue;
            }
            $struct[$key] = $val;
        }

        $return = $this->detail_sort_3($struct);

        $return['content'] = $this->detail_content_cache($content);

        $return['db']['region_id']  =  $this->loader->geocoder_by_all($return['db']['customer_address']);

        list($return['db']['type_dict_id'],$return['db']['type_id']) = $this->get_type_id($return['db']['type']);

        $return['docs'] = array();
        foreach ( preg_get_all_order('#href="(http.*?file.php\?id=(\d+))">(.*?)</a#i', $content) as $v ) {
            $return['docs'][] = array(
                    'name'        => $v['3'],
                    'detail_link' => $v['1'],
                    'internal_id' => $this->make_internal_id($v['1']),
            );
        }

        $content_dom->clear();

        return $return;
    }

    private function detail_all_a() {
        $this->detail_link = $this->detail_link_common . $this->item_now['detail_link_prefix'];
        $this->loader->debug($this->detail_link);
        $content = $this->emul_br_get_body($this->detail_link);

        $return['content'] = $this->detail_content_cache($content);

        $content = tidy_repair_string($content, $this->tidy_config);
        $dom = str_get_html($content);

        $main_box = $dom->find("div.main_box", 0);

        $return['html'] = $main_box->find("div#text", 0)->innertext;
        $return['html'] = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $return['html']);
        $return['html'] = tidy_repair_string($return['html'], $this->tidy_config);

        if (empty($return['html'])) {
            //echo $content;
            exit('EMPTY return.html');
        }

        $docs = preg_get_all("#<a href=\"https://www.roseltorg.ru/export/frame/files.*?\">.*?</a>#si", $main_box->find("div.data-block", 1)->innertext);

        foreach ($docs as $doc) {
            $doc = preg_get("#href=\"(.*?)\">(.*?)</a>#si", $doc);
            $return['docs'][] = array(
                'name'          =>  $doc[1],
                'detail_link'   =>  $doc[0],
                'internal_id'   =>  abs(crc_p($doc[0])),
            );
        }

        // определяем регион
        $region_id = $this->loader->geocoder_roseltorg($content);
        if (empty($region_id) || $region_id < 0) {
            $region_id = 0;
        }
        $return['db']['region_id'] = $region_id;

        return $return;
    }

    private function get_type_id($text) {
        $text = $this->text_clear_all($text);

        if (!$text) {
            return array(null, null);
        }
        return $this->loader->fields_add_dict_one_db('type', $text);
    }

    private function detail_all_old() {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }

        $content = mb_convert_encoding($content, 'utf-8', 'cp1251');
        $content = tidy_repair_string($content, $this->tidy_config);

        $return['html'] = preg_get('#<div id="text">(.*?)<hr#uis', $content);
        $return['html'] = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $return['html']);
        $return['html'] = tidy_repair_string($return['html'], $this->tidy_config);

        if (empty($return['html'])) {
            echo $content;
            exit('EMPTY return.html');
        }

        $doc_id   = preg_get('#https://www.roseltorg.ru/auction/guiding/aab\?PrintableVersion=enabled&id=(\d+)#i', $content);

        if ($doc_id) {
            $link = "https://www.roseltorg.ru/auction/guiding/aab?doc_dem_type=documentation&id=$doc_id";
            $return['docs'][] = array(
                    'name' => 'Документы',
                    'detail_link' => $link,
                    'internal_id' => crc_p($link),
            );
            // есть у всех?
            $link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=$doc_id";
            $return['docs'][] = array(
                    'name' => 'Подробная информация',
                    'detail_link' => $link,
                    'internal_id' => crc_p($link),
            );

        }

        // определяем регион
        $region_id = $this->loader->geocoder_roseltorg($content);
        if (empty($region_id) || $region_id < 0) {
            $region_id = 0;
        }
        $return['db']['region_id'] = $region_id;

        return $return;

        //
        $doc2_ids = preg_get_all('#https://www.roseltorg.ru/auction/guiding/view_auction\?id=(\d+)#i', $content);
        if ($doc2_ids) {
            $doc2_ids = array_unique($doc2_ids);
            foreach ($doc2_ids as $id) {
                $link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=$id";
                $return['docs'][] = array(
                        'detail_link' => $link,
                        'internal_id' => crc_p($link),
                );
            }
        }
    }

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    protected $detail2_link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=";
    /**
     * Новая попытка получения адресса
     * пример: https://www.roseltorg.ru/auction/guiding/view_auction?id=15325
     * @param <type> $id
     */
    function detail2_get_address($internal_id) {
        $this->loader->debug("DETAIL_GET_ADDRESS = $this->detail2_link$internal_id");
        $emul_br = $this->emul_br_init( $this->detail2_link . $internal_id );
        $emul_br->exec();
        $address = preg_get("/Mainzakazchik_info = '(.*?)'/i", $emul_br->GetBody());
        $address = mb_convert_encoding($address, 'utf-8', 'cp1251');
        return $address;
    }

}
