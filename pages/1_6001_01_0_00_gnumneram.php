<?php
/**
 * Пологаю открытый конкурс
 * Последние объявления о тендерах УЗБЕКИСТАН
 */

class loader_1_6001_01_0_00_gnumneram extends loader_1_0000_01_0_00_one {

    public $base_url    = 'http://gnumner.am/ru/';
    public $list_link   = 'http://gnumner.am/ru/category/19/';
    public $parser_name = 'parser_1_6001_01_0_00_gnumneram';
    
    public $fields_list = array(
            'internal_id',
            'name',
            'date_end',
   );

    public $fields_rewrite = array(
            'type'         => 'Открытый конкурс',
            'type_id'      => 2,
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = false;

}

/**
 * LIST
 */
class parser_1_6001_01_0_00_gnumneram extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'p_internal_id'     => 'internal_id|clear_all',
        'p_name'            => 'name|clear_all',
        'p_date_end'        => 'date_end|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $link = $link . $page . ".html";
        $this->loader->debug($link);
        return $this->emul_br_get_body($link);
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = preg_get("#download/(\d+).html#si", $item['internal_id_src']);
            $item['date_publication'] = $this->text_date_convert(preg_get("#с (\S+) до \S+#siu", $item['date_end']));
            $item['date_end'] = $this->text_date_convert(preg_get("#с \S+ до (\S+)#siu", $item['date_end']));

            if ($item['date_end'] < date("Ymd")) { continue; }
            
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $contentbox = $content_dom->find("div.content_box_3_content", 0);


        $arr = array();
        foreach ($contentbox->find("div.article_items") as $article_items) {

            if (!$article_items->find("a.pdf_link", 0)) { continue; }

            $arr[] = array(
                'p_internal_id' => $article_items->find("a.pdf_link", 0)->getAttribute("href"),
                'p_name'        => $article_items->find("a.pdf_link", 0)->innertext,
                'p_date_end'    => $article_items->find("span.valid_date", 0)->innertext,
            );
        }

        $page_now = $contentbox->find("span.pager", 0)->innertext;
        foreach ($contentbox->find("a.pager") as $pager) { $l[] = (int) $pager->innertext; }
        $page_total = max($l);

        $content_dom->clear();
        
        $return = array(
                'page_total'  => $page_total,
                'page_now'    => $page_now,
                'items_total' => count($arr),
                'items'       => $arr,
        );
        return $return;

    }
}
