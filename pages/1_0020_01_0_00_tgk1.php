<?php
/**
 * OAO "ТГК-1"
 * Написание: 28.07.2010
 * Изменение: 20.10.2010
 */
class loader_1_0020_01_0_00_tgk1 extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.tgc1.ru/';
    public $list_link           = 'http://www.tgc1.ru/tenders/open/';
    public $parser_name         = 'parser_1_0020_01_0_00_tgk1';

    public $fields_list = array(
            'name',
            'date_publication',
            'date_end',
            'internal_id',
            'type',
            'detail_link_prefix',
    );

    public $fields_rewrite = array(
        'type'         => 'Коммерческий',
        'type_dict_id' => 1000,
        'type_id'      => 100,
        'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0020_01_0_00_tgk1 extends parser_1_0000_01_0_00_one {

    protected $list_colomn = array(
            '№ закупки'                 => 'num',
            'Способ закупки'            => 'type',
            'Предмет закупки'           => 'name|clear_all',
            'Дата начала процедуры'     => 'date_publication|clear_all',
            'Окончание приема заявок'   => 'date_end|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item);

            $item['date_publication'] = $this->text_date_convert_short(preg_replace("#/#", ".", $item['date_publication']));
            $item['date_end'] = $this->text_date_convert_short(preg_replace("#/#", ".", $item['date_end']));
            $item['detail_link_prefix'] = preg_get("#href=\"(.*?)\"#sui", $item['name_src']);
            $item['internal_id'] = (int) abs(crc_p($item['detail_link_prefix']));
            // Страховочный вариант
            if (empty($item['name'])) {
                $item['name'] = $this->text_clear_all(preg_get("#<a.*?title=['\"](.*?)['\"]#sui", $item['name_src']));
            }
            
            $items[] = $item;
        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $ten = $content_dom->find("table.tender_list", 0);

        $arr = $this->parse_table($ten->outertext);

        $ret['items'] = $this->createstruct($arr);

        $content_dom->__destruct();

        return $ret;
    }
}

