<?php
/**
 * ОГК-1
 * Написание: 5.08.2010
 * Нет смысла подключать находяться на b2b-energo
 *
 * Внимание на строку 58. В поле заказчик указаны станции, считаю правильным добавить название компании к ним,
 * но пока закомментировал. 
 */
class loader_1_0027_01_0_00_ogk1 extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.ogk1.com/';
    public $list_link           = 'http://www.ogk1.com/trade/index.php?sort=open&order=down';
    public $parser_name         = 'parser_1_0027_01_0_00_ogk1';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'date_end',           
            'customer',            
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;
}

class parser_1_0027_01_0_00_ogk1 extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Уведомления'       => 'name|clear_all',
            'Станция'          => 'customer|clear_all',
            'Дата публикации'   => 'date_publication|date_convert',            
            'Дата и время подачи предложений'   => 'date_end|clear_all|date_convert',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        $this->current_link = $link;
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if ($item['date_end'] < date("Ymd")) {
                continue;
            }

            //$item['customer'] = "Закупки ОАО 'ОГК-1' ".$item['customer'];

            $item['internal_id'] = abs(crc_p($item['name_src']));

            $item['doc'][0]['internal_id'] = $item['internal_id'];
            $item['doc'][0]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"].*?>#si", $item['name_src']);
            $item['doc'][0]['name'] = $item['name'];

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table", 0);

        $arr = $this->parse_table($tenders->outertext);

        $ret['items'] = $this->createstruct($arr);
        
        $content_dom->__destruct();

        return $ret;
    }
}
