<?php

/*
 * ОАО &quot;Сетевая компания&quot; :: Закупки :: Уведомления
*/

class loader_1_0056_01_0_00_netcompany extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.gridcom-rt.ru/';
    public $list_link           = 'http://www.gridcom-rt.ru/purchase/notice-';
    public $parser_name         = 'parser_1_0056_01_0_00_netcompany';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
            'price'     =>  'maybenull',
            'doc',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0056_01_0_00_netcompany extends parser_1_0000_01_0_00_one {

    protected $colomn = array(       
        'Номер лота'                    =>  'num|clear_all',
        'Период проведения закупки'     =>  'date_publication|clear_all|date_convert',
        'Название закупки'              =>  'name|clear_all',
        'цена'                          =>  'price|to_price',       
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link \n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {

            $docs = $item['Документы'];

            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_publication'] = $this->text_date_convert(preg_get("#^\d{1,2}.\d{2}.\d{4}#si", $item['date_publication_src']));
            $item['date_end'] = $this->text_date_convert(preg_get("#\d{1,2}.\d{2}.\d{4}$#si", $item['date_publication_src']));

            if ($item['date_end']<date("Ymd")) { continue; }

            $item['internal_id'] = abs(crc_p($item['num'] . $item['name']));
         
            $docs = preg_get_all("#<a.*?/a>#si", $docs);

            foreach ($docs as $doc) {

                $detail_link = preg_get("#href=['\"](.*?)['\"]#si", $doc);
                $name = $this->text_clear_all($doc);

                $item['doc'][] = array(
                    'detail_link'   =>  $detail_link,
                    'name'          =>  $name,
                    'internal_id'   =>  abs(crc_p($detail_link)),
                );
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );       

        return $return;
    }

    function list_parse_pre($content) {

        $html = str_get_html($content);
        $data = $html->find("table.contentTbl", 0)->outertext;
        $html->clear();

        $arr  = $this->parse_table($data);
        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

