<?php
/**
 * Сочи 2014
 * Аукционы
 * Написание: 24.12.2010
 */
class loader_1_0039_02_0_00_sochi2014 extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://sochi2014.com/tenders/';
    public $list_link           = 'http://sochi2014.com/auctions/?status=active';
    public $parser_name         = 'parser_1_0039_02_0_00_sochi2014';
    public $parser_name_detail  = 'parser_1_0039_02_0_00_sochi2014_detail';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0039_02_0_00_sochi2014 extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'P_name'            => 'name|clear_all',
            'P_date_publication'=> 'date_publication|date_convert_text_full',
    );

    private $cookie;

    function list_get_page( $link ) {

        $br = $this->emul_br_init( $this->loader->base_url );
        $br->exec();
        $this->cookie = $br->GetResponseCookieArray();

        $this->loader->debug("\n\n LINK = $link\n\n");
        
        $br = $this->emul_br_init( $link );
        $br->SetCookie($this->cookie);
        return $br->exec();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#auctions/(\d+)/#si", $item['name_src']);

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        //$content = $this->text_from_win($content);

        $content_dom = str_get_html($content);
        //$tenders = $content_dom->find("div.b-tenders-previews", 0);

        $items=array();
        foreach ($content_dom->find("div.b-tenders-article") as $tender) {
            $items[] = array(
                'P_date_publication'    => $tender->find("p.b-tenders-article-date",0)->innertext,
                'P_name'                => $tender->find("div.b-tenders-article-contents", 0)->find("a", 0)->outertext,
            );
        }
        
        $ret['items'] = $items;

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0039_02_0_00_sochi2014_detail extends parser_1_0039_02_0_00_sochi2014 {

    public $item_now;

    protected $detail_link = '';

    function detail_get($id) {
        $link = "http://sochi2014.com/auctions/$id/";

        $content = parent::list_get_page($link);

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) { 
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $content_dom = str_get_html($content);
        $return = array('db'=>array());
        foreach ($content_dom->find("h3.b-downloads-article-title") as $doc) {
            $doc = $doc->find("a", 0);
            $href = $doc->GetAttribute("href");
            $return['docs'][] = array(
                'detail_link'   => $href,
                'name'          => $this->text_clear_all($doc->innertext),
                'internal_id'   => abs(crc_p($href)),
            );
        }       
        $content_dom->clear();
        return $return;
    }
}