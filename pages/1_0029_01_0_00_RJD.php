<?php
/**
 * РЖД. Российский Железные Дороги.
 * Написание: 5.08.2010
 * Доработка: 6.09.2010
 */
class loader_1_0029_01_0_00_RJD extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://tender.rzd.ru/';
    public $list_link           = 'http://tender.rzd.ru/isvp/public/tender?STRUCTURE_ID=4078&layer_id=4893&page4893_1465=';
    public $parser_name         = 'parser_1_0029_01_0_00_RJD';

    public $fields_list = array(
            'name',
            'num',
            'internal_id',
            'date_end',           
            'region_id',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function list_stop(array $list) {
        $first = array_shift($list['items']);
        if ( ($two = @array_shift($list['items'])) ) {
            $first = $two;
        }
        if ($first['date_end'] < date("Ymd")) {
            return true;
        }
    }
}

class parser_1_0029_01_0_00_RJD extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'Номер'             => 'num|clear_all',
            'Предмет тендера'   => 'name|clear_all',           
            'Дата проведения'   => 'date_end|clear_all|date_convert',
            'Место проведения'  => 'location',
            'Статус'            => 'status',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        $items = array();
        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#&id=(\d+)#si", $item['num_src']);
            $item['region_id'] = $this->loader->geocoder_by_name($item['location']);
            $item['name'] = trim(preg_replace("#Внимание! Изменения!#sui", "", $item['name']));

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("table.Striped", 0);

        $tbl = preg_replace("#thead#si", "tr", $tenders->outertext);

        $arr = $this->parse_table($tbl);

        $ret['items'] = $this->createstruct($arr);

        $pages = $tenders->next_sibling();

        $ret['page_now'] = $pages->find("span.curPage", 0)->innertext;
        $ret['page_total'] = max(preg_get_all("#page4893_1465=(\d+)#si", $pages->innertext));
       
        $content_dom->__destruct();

        return $ret;
    }
}
