<?php
/**
 * http://zakazrf.ru/Reductions.aspx?stage=1
 * 26.08.2010 - в списке в редких случаях заказчика может не быть. добавлен maybenull на customer
 */

/**
 * http://zakazrf.ru/Reductions.aspx?stage=1
 * @author Paul-labor@yandex.ru
 */
class loader_1_0004_01_0_00_zakazrf1 extends loader_1_0000_02_0_00_temp {
    public $base_url           = 'http://zakazrf.ru/';
    public $list_link          = 'http://zakazrf.ru/Reductions.aspx?stage=1';
    public $parser_name        = 'parser_1_0004_01_0_00_zakazrf1';
    public $parser_name_detail = 'parser_1_0004_01_0_00_zakazrf1_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'customer'         => 'maybenull',
            'price'            => 'maybenull',
            'date_publication',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; // совершенно не ясна сортировка
    public $item_rewrite  = true;

    protected $run_detail_order = 'date_publication ASC';

    public $page_total_rewrite = false;

    public $page_last = 200;

    public $sleep_list = 7;

    function geocoder_zakazrf($string) {
        $this->debug('GEOCODER, input=', $string);

        $region_id = $this->geocoder_by_index_auto($string);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($string);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_ROSELTORG, region= $region_id ($region_name)", $string);

        return $region_id ? $region_id : false;
    }

}

class parser_1_0004_01_0_00_zakazrf1 extends parser_1_0000_02_0_00_temp {
    protected $colomn = array(
            'Номер'             => 'num',
            'Дата публикации'   => 'date_publication|date_convert_short',
            'Наименование'      => 'name|clear_all_dots',
            'Заказчик'          => 'customer|clear_all_dots',
            'Общая цена'        => 'price|to_price',
            6                   => 'detail_link',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");

        if ($page == 1) {
            $emul_br = $this->emul_br_init($link);
            $emul_br->exec();
            $this->loader->debug($emul_br->GetAllHeaders());
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $emul_br = $this->emul_br_init($link);

        $emul_br->SetCookie($this->asp_params_cookies);
        $emul_br->SetPost($this->asp_params_post);

        if ( $page > 10 && $page%10 == 1 ) {
            $target = 'ctl00$Content$Pager$NextPageButton';
        } else {
            $newpage = $page%10;
            $newpage = $newpage ? $newpage : 10; // 20 => 10
            $target = 'ctl00$Content$Pager$Page' . $newpage . 'Button';
        }

        $emul_br->SetPost(array(
                '__EVENTTARGET'   => $target,
                '__EVENTARGUMENT' => '',
        ));

        $this->loader->debug("TARGET = $target");

        // FIXME распространить и на другие сайты...
        for ($try = 2; $try <= 5; $try++) {
            $emul_br->exec();
            if ($emul_br->GetBody()) {
                break;
            }
            $this->loader->debug("TRY $try step, SLEEP {$this->loader->sleep_list} sec...");
            sleep( $this->loader->sleep_list );
        }

        // если пролистать дальше, то пропадает NEXT на 21 страницу
        // 20%10 == 0, но её записывать не нужно
        if ($emul_br->GetBody() && ($page-1)%10 < 5) {
            $this->asp_clear_params();
            $this->asp_set_params($emul_br);
        }

        $this->loader->debug($emul_br->GetAllHeaders());

        return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        //print_r($parse);
        //exit(0);
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#<a.*\?id=(\d+)#i", $item['detail_link']);

            $items[] = $item;
        }

        $return = array (
                'page_total'  => ceil($parse['item_total']/sizeof($items)),
                'items_total' => $parse['item_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content_dom = str_get_html($content);

        $return['item_total'] = preg_get('/id="ctl00_Content_RecordsCountLabel">(\d+)</i', $content);

        // Парсинг таблицы
        $table = $content_dom->find('table.reporttable',0);
        $arr   = $this->parse_table($table->outertext);

        $return['items'] = $this->createstruct($arr);

        $content_dom->clear();

        return $return;
    }

}

class parser_1_0004_01_0_00_zakazrf1_detail extends parser_1_0004_01_0_00_zakazrf1 {

    protected $detail_link = 'http://zakazrf.ru/ViewReduction.aspx?id=';

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }

        $content = tidy_repair_string($content, $this->tidy_config);

        $header = preg_get('#<div style="width:100%;">\s*?(<table width="100%" border="0">.*?</table>)#uis', $content);
        $return['html'] = $this->text_clear_html_gently($header);

        $this->content_html = $content;

        $return['db']['customer_email']   = $this->span('ctl00_Content_ReductionViewForm_CustomerEMailLabel');
        $return['db']['customer_address'] = $this->span('ctl00_Content_ReductionViewForm_CustomerPostAddressLabel');
        $return['db']['customer_phone']   = $this->span('ctl00_Content_ReductionViewForm_CustomerContactPhoneLabel');
        $return['db']['date_end']         = $this->text_date_convert($this->span('ctl00_Content_ReductionViewForm_ViewLotsListControl_LotsList_ctl01_SubmissionEndDateLabel'));
        $return['db']['date_conf']        = $this->text_date_convert($this->span('ctl00_Content_ReductionViewForm_ViewLotsListControl_LotsList_ctl01_TradeBeginDateLabel'));

        $possitions = $this->span_many('Content_ReductionViewForm_ViewLotsListControl_LotsList_ctl\d+_SubjectLabel');
        if (count($possitions) > 1) {
            $stuff = '';
            foreach ($possitions as $v) {
                $stuff .= "$v\n";
            }
            $return['db']['stuff'] = trim($stuff);
        }

        // определяем регион
        $region_id = $this->loader->geocoder_zakazrf($return['db']['customer_address']);
        if (empty($region_id) || $region_id < 0) {
            $region_id = 0;

            exit('UNKNOWN REGION');

        }
        $return['db']['region_id'] = $region_id;

        if (empty($return['html'])) {
            echo $content;
            exit('EMPTY return.html');
        }

        $docs_array = preg_get_all_order('#href="DFile\.ashx\?id=(\d+)">(.*?)</a>#iu', $content);
        if ($docs_array) {
            foreach ($docs_array as $d) {
                $return['docs'][] = array(
                        'name'        => $d[2],
                        'internal_id' => $d[1],
                );
            }
        }

        // СЛИШКОМ МНОГО
        /*
        $lots = preg_get_all('#<td width="95%" valign="top" rowspan="1".*?(<table.*?</table>)#ius', $content);
        foreach ($lots as &$l) {
            $l = $this->text_clear_html_gently($l);
            $l = preg_replace('#</?a.*?>#ui', '', $l);
        }
        print_R($lots);
        */
        return $return;
    }

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function text_clear_html_gently($string) {
        $string = preg_replace(array('#(id|class|style)=".*?"#ui','#</?(font|span).*?>#ui','/<!--.*?-->/sui'), '', $string);
        $string = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $string);
        $string = tidy_repair_string($string, $this->tidy_config);
        return $string;
    }

    protected $content_html;

    function span($name) {
        $return = preg_get('#<span[^>]*?id="'.$name.'"[^>]*?>(.*?)</span>#ui', $this->content_html);
        $return = trim(preg_replace('#\s+#u', ' ', $return));
        return empty($return) ? NULL : $return ;
    }

    function span_many($name) {
        $return = preg_get_all('#<span[^>]*?id="[^"]*?'.$name.'[^"]*?"[^>]*?>(.*?)</span>#ui', $this->content_html);
        if (empty($return)) {
            return array();
        }
        foreach ($return as &$v) {
            $v = trim(preg_replace('#\s+#u', ' ', $v));
        }
        return $return;
    }

}
