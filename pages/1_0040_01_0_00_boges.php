<?php
/**
 * ОАО "Богучанская ГЭС":
 * Написание: 21.01.2011
 */
class loader_1_0040_01_0_00_boges extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.boges.ru/';
    public $list_link           = 'http://www.boges.ru/index.php?option=com_content&view=category&id=8&Itemid=9&limitstart=';
    public $parser_name         = 'parser_1_0040_01_0_00_boges';
    public $parser_name_detail  = 'parser_1_0040_01_0_00_boges_detail';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
            'num'               => 'maybenull',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    function list_stop(array $list) {
        foreach($list['items'] as $item) {
            if ($item['date_publication'] < date("Ymd", time()-30*24*60*60)) {
                return true;
            }
        }
        return true;
    }
}

class parser_1_0040_01_0_00_boges extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            '1'            => 'name|clear_all',
            '2'             => 'date_publication|date_convert',
    );

    function list_get_page( $link, $page=1 ) {

        $link .= ($page-1)*20;

        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#id=(\d+)#si", $item['name_src']);
            $item['num'] = preg_get("#№\s?(\D+\-\D+\-\d+)#si", $item['name_src']);

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);
        $tenders = $content_dom->find("div#component", 0)->find("form", 0)->find("table", 0)->outertext;

        $items = $this->parse_table($tenders);

        $pag = preg_get("#Страница (\d+) из (\d+)#sui", $items[22][0]);
        unset($items[22]);unset($items[21]);unset($items[20]);
        
        
        $ret['page_total'] = $pag[1];
        $ret['page_now'] = $pag[0];

        $ret['items'] = $items;
        
        $content_dom->__destruct();


        return $ret;
    }
}

class parser_1_0040_01_0_00_boges_detail extends parser_1_0000_02_0_00_temp {

    public $item_now;

    protected $detail_link = '';

    function detail_get($id) {
        $this->detail_link = "http://www.boges.ru/index.php?option=com_content&view=article&id=" . $id;
        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $return['content'] = $this->detail_content_cache($content);

        $content_dom = str_get_html($content);
        $html = $content_dom->find("div#component", 0)->find("table", 1)->find("td", 0)->innertext;
        $content_dom->clear();


        $html = tidy_repair_string($html, $this->tidy_config);
        $html = preg_replace("#<a.*?</a>#si", "", $html);
        $html = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $html);
        $html = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $html);
        $html = preg_replace(array("#<script.*?</script>#si", "#<style.*?</style>#si", "#<!\[CDATA.*?</script>#si"), "", $html);
        $html = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $html);
        $html = preg_replace("#Этот e-mail адрес защищен.*?Javascript#siu", "", $html);
        $return['html'] = $html;

        $de = preg_get("#<strong>.*?(\d+ \D+ \d{4}) года.*?</strong>#sui", $html);
        $return['db']['date_end'] = $this->text_date_convert_text_full($de);


        return $return;
    }
}