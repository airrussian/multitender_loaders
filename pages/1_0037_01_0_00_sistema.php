<?php
/**
 * AФК Система
 * Написание: 24.12.2010
 */
class loader_1_0037_01_0_00_sistema extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.sistema.ru/';
    public $list_link           = 'http://www.sistema.ru/zakupki/';
    public $parser_name         = 'parser_1_0037_01_0_00_sistema';
    public $parser_name_detail  = 'parser_1_0037_01_0_00_sistema_detail';

    public $fields_list = array(
            'name',           
            'internal_id',
            'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;
}

class parser_1_0037_01_0_00_sistema extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'P_detail_link'     => 'detail_link',
            'P_name'            => 'name|clear_all',
            'P_date_publication'=> 'date_publication|date_convert_text_full',
    );

    function list_get_page( $link ) {
        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] = preg_get("#nm=(\d+)#si", $item['detail_link']);

            $items[$k] = $item;

        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div.news", 0)->innertext;

        $tenders = preg_get_all("#<h4 style=\"margin-bottom: 0px;\">.*?<div class=\"notice\">.*?</div>#si", $tenders);

        $items=array();
        foreach ($tenders as $tender) {
            $items[] = array(
                'P_detail_link'         => preg_get("#<h4.*?<a href=\"(.*?)\">.*?</h4>#si", $tender),
                'P_date_publication'    => preg_get("#<small>(.*?)</small>#si", $tender),
                'P_name'                => preg_get("#<div class=\"notice\">(.*?)</div>#si", $tender),
            );
        }
        
        $ret['items'] = $items;
        
        $content_dom->__destruct();

        return $ret;
    }
}
class parser_1_0037_01_0_00_sistema_detail extends parser_1_0000_02_0_00_temp {

    public $item_now;

    protected $detail_link = '';

    public $detail_sort = array(
            'P_date_publication'    => 'date_publication|clear_all|date_convert',
            'P_date_end'            => 'date_end|clear_all|date_convert',
            'docs'                  => 'delete',
            'html'                  => 'delete',
    );

    function detail_get($id) {

        $link = "http://www.sistema.ru/zakupki?oo=2&fnid=68&nm=" . $id;
        $this->loader->debug("detail id = $link");

        $emul_br = $this->emul_br_init( $link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {

        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }
        $docs = $parse['docs'];
        $html = $parse['html'];

        $return = $this->detail_sort_3($parse);

        if (empty($return['db']['date_publication'])) {
            $return['db']['date_publication'] = $this->text_date_convert_text_full($parse['P_date_publication']);
        }
        if (empty($return['db']['date_end'])) {
            $return['db']['date_end'] = $this->text_date_convert_text_full($parse['date_end']);
        }

        $return['html'] = $html;
        $return['docs'] = $docs;

        return $return;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $content = preg_get("#<div class=\"bigkegl\">.*?<html>.*?<body.*?>.*?<td>(.*?)</td>.*?</body>#si",$content);

        $return['P_date_publication']   = preg_get("#<div>Дата начала приема предложений.*?(\d+.\S+.\d{4}).*?</div>#siu", $content);
        $return['P_date_end']           = preg_get("#<div>Дата окончания приема предложений.*?(\d+.\S+.\d{4}).*?</div>#siu", $content);

        $return['docs'][0]['detail_link']   = preg_get("#<a href=\"(http://www.sistema.ru/doc/doc.asp\?obj=\d+).*?Закупочной документации.*?</a>#siu", $content);
        $return['docs'][0]['name']          = 'Закупочной документации';
        $return['docs'][0]['internal_id']   = preg_get("#<a href=\"http://www.sistema.ru/doc/doc.asp\?obj=(\d+).*?Закупочной документации.*?</a>#siu", $content);

        $return['html'] = $this->text_clear_html_gently($content);
        $return['html'] = preg_replace(array('#<a.*?>#i','#</a>#i'), '', $return['html']);
        $return['html'] = preg_replace("#<p>Назад</p>#siu", "", $return['html']);        

        return $return;
    }
}
