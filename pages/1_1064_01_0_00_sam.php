<?php
/**
 * @version > Мурманская область (гос.заказ)
 * @var     > http://gz-murman.ru/
 * @param   > run_list, run_detail
 * @todo    > 09.11.2010 - новая площадка для гос.заказа Мурманской области. Старая осталась для Муниципального заказа [5653]
 * @author  > airrussia@mail.ru
 */

class loader_1_1064_01_0_00_sam extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://www.goszakaz.samregion.ru/';
    public $list_link           = 'http://www.goszakaz.samregion.ru/';
    public $parser_name         = 'parser_1_1064_01_0_00_sam';
    public $parser_name_detail  = 'parser_1_1064_01_0_00_sam_detail';

    public $fields_list = array(
            'name',
            'type',
            'internal_id',
            'date_end',
            'detail_link_prefix',
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;


    public function run_list() {
        
        $this->in_process       = 0;
        $this->list_pass_items  = 0;
        $this->list_in_db_items = 0;
        $this->error_list_items = 0;
        $break_by_pass_count = $this->break_by_pass_count;

        $page_total = 1;
        for ( $page = 1; $page <= $page_total; $page++ ) {
            $this->debug("<h3>RUN $page page of $page_total</h3>");

            if ($this->break_by_pass) {
                // FIXME other files
                //if ( ($this->list_pass_items) && ($this->in_process === 0) ) {
                $this->debug( "<h3>list_in_db_items = $this->list_in_db_items</h3>" );
                if ( ($this->list_in_db_items) && ($this->in_process === 0) ) {
                    $break_by_pass_count--;
                    if ($break_by_pass_count > 0) {
                        $this->debug( "<h3>BREAK by pass IN next, count = $break_by_pass_count</h3>" );
                    } else {
                        $this->debug( "<h3>BREAK by pass</h3>" );
                        break;
                    }

                }
            }

            if ( ! $this->validate_load(
            $content = $this->parser->list_get_page(
            $this->list_link, $page)
            ) ) {
                continue;
            }

            $list = $this->parser->list_parse( $content );

            if ( empty($list) ) { continue; }

            if ($page === 1) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if (empty ($page_total)) {
                    $page_total = 1;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            // page_total rewrite
            if ($this->page_total_rewrite && $page > 1 ) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            if ( empty( $list['items'] ) ) {
                $this->debug( "<h3>Список items пустой, ошибка парсинга</h3>" );
                continue;
            }

            $this->list_insert(array_reverse($list['items']));

            if ( $this->list_stop($list) ) {
                $this->debug( "<h3>BREAK by list_stop</h3>" );
                break;
            }

            $this->debug("SLEEP $this->sleep_list s...");
            sleep( $this->sleep_list );
        }
        $this->update_in_process();
    }

    function validate_load($content) {
        if ( (strlen($content) < 1000) ) {
            //FIXME to function + /html
            //TODO отслеживать загрузку по наличию </html> и размеру
            //trigger_error("empty_page");
            return false;
        }
        return true;
    }

}

class parser_1_1064_01_0_00_sam extends parser_1_0000_01_0_00_one {

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nPAGE = $page\n\n");

        $t = floor(($page-1)/3);
        $m = $page - $t * 3 - 1;

        $page_name = date("FY", time()+$m*30*24*60*60);

        $page_type = array('{%date}', 'Kot_{%date}', 'Auk_{%date}', '{%date}_municipal', 'Kot_{%date}_municipal');
        $link = str_replace("{%date}", $page_name, $page_type[$t]);
        
        $link = $this->loader->base_url . $link . "/";

        $this->loader->debug($link);

        $emul_br = $this->emul_br_init($link);
        $emul_br->exec();
        return $emul_br->GetBody();
    }

    protected $colomn = array(
            'P_date_end'        => 'date_end|clear_all|date_convert',
            'P_detail_link'     => 'detail_link_prefix',
            'P_name'            => 'name|clear_all',
            'P_internal_id'     => 'internal_id|clear_all',
    );

    function list_parse($content) {

        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            
            $item['type'] = 'Открытый конкурс';
            if (preg_match("#kot#si", $item['detail_link_prefix'])) { $item['type'] = 'Запрос котировок'; }
            if (preg_match("#Auk#si", $item['detail_link_prefix'])) { $item['type'] = 'Аукцион'; }

            $items[] = $item;
        }

        $return = array (
                'page_total'  => 15,
                'page_now'    => 1,
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $arr = preg_get_all("#<p><b>\d{2}.\d{2}.\d{4}</b>.*?<a.*?a>.*?</p>#si", $content);

        foreach ($arr as $row) {
            $ret['items'][] = array(
                'P_date_end'      => preg_get("#<b>(.*?)</b>#si", $row),
                'P_detail_link'   => preg_get("#<a.*?href=['\"](.*?)['\"]#si", $row),
                'P_name'          => preg_get("#<a.*?>(.*?)</p>#si", $row),
                'P_internal_id'   => abs(crc_p(preg_get("#<a.*?href=['\"](.*?)['\"]#si", $row))),
            );
        }

        $ret['page_now'] = 1;
        $ret['page_total'] = 1;
        $ret['items_total'] = count($ret['items']);

        return $ret;
    }
}

// FIX ME...
class parser_1_1064_01_0_00_sam_detail extends parser_1_0000_01_0_00_one {

    function detail_all($id) {
        $link         = "http://gz-murman.ru/gzw/order.aspx?link=";
        $this->loader->debug($link . $id);
        $content      = $this->emul_br_get_body($link . $id);
        $return['html'] = $content;
        $return['docs'] = $this->list_parse_pre($content);
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);
        $docs = array();
        if ($detail_dom->find("div#panelPbFiles",0)) {
            $doc_link = $detail_dom->find("div#panelPbFiles",0);
            foreach ($doc_link->find("input") as $input) {
                $N = (int) preg_get("#pbfiles(\d+)#si", $input->getAttribute('name'));
                if (!$N) { continue; }
                $link = $input->getAttribute('value');
                $name = $doc_link->find("a#pbfilesdoc$N", 0)->innertext;
                $pcrc = abs(crc_p($link.$name));
                $docs[] = array(
                    'detail_link'   =>  $link,
                    'name'          =>  $name,
                    'internal_id'   =>  $pcrc,
                );

            }
        }
        
        return $docs;
    }
}