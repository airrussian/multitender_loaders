<?php
/**
 * @author airrussian@mail.ru
 */

class loader_1_1005_01_0_00_rts extends loader_1_1000_01_0_00_zakupki {
    public $base_url           = 'http://www.rts-tender.ru/';
    public $list_link          = 'http://www.rts-tender.ru/RSS.aspx';
    public $parser_name        = 'parser_1_1005_01_0_00_rts';
    public $parser_name_detail = 'parser_1_1005_01_0_00_rts_detail';

    public $fields_list = array(
            'internal_id',
            'name',
            'num',
            'price'            => 'maybenull',
            'date_end',
            'date_publication',
            'date_conf',
            'detail_link_prefix',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = true;
    public $item_rewrite  = false;
    
    function run_detail_query(&$obj) {
        if(!$obj->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
        "detail_run_time IS NULL AND item_id IS NULL ORDER BY detail_count_error ASC, $this->run_detail_order")) {
            exit("\nNO MORE\n");
        }
    }
}

class parser_1_1005_01_0_00_rts extends parser_1_0000_02_0_00_temp {
    protected $colomn = array(
        'link'          =>  'detail_link|clear_all',
        'title'         =>  'name',
        'description'   =>  'num',
        'guid'          =>  'detail_link_prefix|clear_all',
    );

    function list_get_page( $link ) {
        $this->loader->debug($link);        
        //$emul_br = $this->emul_br_init($link);
        //$emul_br->exec();
        return file_get_contents($link);
        //return $emul_br->GetBody();
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);
        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['internal_id'] =  abs(crc_p($item['detail_link']));

            $item['price'] = $this->text_to_price(preg_get("#цена.*?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_publication'] = $this->text_date_convert(preg_get("#Дата публикации:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_end'] = $this->text_date_convert(preg_get("#Дата и время окончания срока подачи заявок на участие:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['date_conf'] = $this->text_date_convert(preg_get("#Дата проведения открытого аукциона:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));
            $item['num'] = $this->text_clear_all(preg_get("#Номер извещения:\s?</td><td>(.*?)</td></tr>#sui", $item['num']));

            $items[] = $item;
        }

        $return = array (
                'page_total'  => 1,
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }


    function list_parse_pre($content) {
        
        $content = preg_replace("#<\?xml-stylesheet.+?\?>#si", "", $content);
        $xml = simplexml_load_string($content);
        $arr = array();

        foreach ($xml->xpath('/rss/channel/item') as $item) {
            $return['items'][] = array(
                'title'         => (string) reset($item->xpath('title')),
                'link'          => (string) reset($item->xpath('link')),
                'description'   => (string) reset($item->xpath('description')),
                'pubDate'       => (string) reset($item->xpath('pubDate')),
                'guid'          => (string) reset($item->xpath('guid')),
            );
        }

        return $return;
    }

}

class parser_1_1005_01_0_00_rts_detail extends parser_1_1000_01_0_00_zakupki_detail {

    function detail_all($id) {

        $num = $this->item_now['num'];

        $prefix = $this->item_now['detail_link_prefix'];

        $id = $this->loader->GetIDbyNUM($num);
        if ($id) {
            $this->loader->debug("ZAKUPKI ID = $id");
            $ret = parent::detail_all($id);
            $this->loader->detail_link = "http://www.rts-tender.ru/ViewReduction.aspx?Guid=$prefix";
            $ret['content'] = $this->detail_content_cache($this->emul_br_get_body($this->loader->detail_link));
        } else {
            $this->loader->debug("NOT ITEM WITH $num ON ZAKUPKI ");
            $this->loader->NoItem($this->item_now['id']);
            $ret = false;
        }

        return $ret;
    }
}
