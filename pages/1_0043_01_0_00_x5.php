<?php
/**
 * X5 Retail Group
 * Написание: 1.02.2011
 */
class loader_1_0043_01_0_00_x5 extends loader_1_0000_05_0_00_many {
    public $site_id             = '1004301000';
    public $base_url            = 'https://tender.x5.ru/';   
    public $parser_name         = 'parser_1_0043_01_0_00_x5';
    public $parser_name_detail  = 'parser_1_0043_01_0_00_x5_detail';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'date_end',
    );

   protected $pages_list = array(
        array('link'    =>    'https://tender.x5.ru/auction/guiding/list_auction/3-start' ),
        array('link'    =>    'https://tender.x5.ru/auction/guiding/list_auction/2-start' ),
        array('link'    =>    'https://tender.x5.ru/auction/guiding/list_auction/1-start' ),
        array('link'    =>    'https://tender.x5.ru/auction/guiding/list_auction/4-start' ),
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = true;
}

class parser_1_0043_01_0_00_x5 extends parser_1_0000_05_0_00_many {

    protected $colomn = array(
        'Номер'         =>  'num|clear_all',
        'Название'      =>  'name|clear_all',
        'Начало подачи' =>  'date_publication|clear_all|date_convert',
        'Завершение'    =>  'date_end|clear_all|date_convert',
    );

    function list_get_page( $link, $page=1 ) {
        $this->loader->debug("\n\n LINK = $link\n\n");       
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            if ($item['date_end']<date("Ymd") || $item['name']=='Демонстрационный конкурс') { continue; }
            $item['internal_id'] = (int) preg_get("#id=(\d+)#siu", $item['num_src']);
            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);
        $content_dom = str_get_html($content);

        $tenders = $content_dom->find("div#MainCVS", 0)->find("table#mytable", 0);
        $arr = $this->parse_table($tenders->outertext);

        $items = $this->createstruct($arr);

        $ret['page_now']    = 1;
        $ret['page_total']  = 1;

        $ret['items'] = $items;
        
        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0043_01_0_00_x5_detail extends parser_1_0000_05_0_00_many {

    protected $detail_link;

    function detail_get($id) {
        $this->detail_link = "https://tender.x5.ru/auction/guiding/view_auction?id=$id";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $return['db'] = array();

        $return['content'] = $this->detail_content_cache($content);

        $dom = str_get_html($content);

        $html = $dom->find("form#view_auction", 0)->find("table.list", 0);

        $return['html'] = $html->outertext;
        $return['html'] = tidy_repair_string($return['html'], $this->tidy_config);

        $dom->clear();

        return $return;
    }


}