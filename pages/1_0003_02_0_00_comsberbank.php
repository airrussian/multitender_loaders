<?php
/**
 * Сбербанк - АСТ. закупки для коммерческих заказчиков
 */
class loader_1_0003_02_0_00_comsberbank extends loader_1_0003_01_0_00_sberbank {

    public $base_url = 'http://com.sberbank-ast.ru/';
    public $list_link = 'http://com.sberbank-ast.ru/OpenPurchaseList.aspx';
    public $parser_name = 'parser_1_0003_02_0_00_sberbank';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'customer' => 'maybenull',
            'date_publication',
            'date_end',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass  = false;
    public $item_rewrite   = false;
    public $page_last      = 10;
    //public $parser_name_detail = 'parser_1_0003_01_0_00_sberbank_detail_temp';
    public $parser_name_detail = 'parser_1_0003_02_0_00_sberbank_detail';

    protected $run_detail_order = 'date_conf ASC';

    function list_stop(array $list) {
        return $this->list_stop_date_conf($list);
    }

    function test_run_report($id=53140) {
        $this->parser = new $this->parser_name_report;
        $this->parser->loader = & $this;

        $content = $this->parser->get_detail($id);
        if (!$content) {
            return false;
        }
        $history = $this->parser->get_history($content);

        foreach ($history as $document) {

            $document = $this->parser->get_parse($document);

            $doc = new doc();
            $doc->name          = $document['name'];
            $doc->internal_id   = $document['internal_id'];
            $doc->detail_link   = $document['detail_link'];
            $doc->date_add      = $doc->DB()->BindTimeStamp(time());
            $doc->item_id       = $id;
            $doc->site_id 	= $this->site_id;
            $doc->save();
        }
    }

    function run_report($id = null) {
        $item = new item();
        $date_end = $item->DB()->DBDate(time() - 7*24*3600);

        if (!$item->Load("site_id = $this->site_id AND detail_count_error = 0 AND " .
        "detail_run_time IS NULL AND date_end <= $date_end AND " .
        "date_report IS NULL ORDER BY date_end DESC")) {
            if (!$item->Load("site_id = $this->site_id AND detail_count_error < 10 AND " .
            "detail_run_time IS NULL AND date_end <= $date_end AND " .
            "date_report IS NULL ORDER BY detail_count_error ASC, id DESC")) {
                exit('NO NEW');
            }
        }

        $item->detail_count_error ++; // при успехе убавим
        $item->detail_count_load  ++;
        $item->detail_run_time = $item->DB()->BindTimeStamp(gmdate('U')); // при успехе обнулить
        $item->Save();

        $this->debug("ID = $item->id");

        $this->parser = new $this->parser_name_report;
        $this->parser->loader = & $this;

        $content = $this->parser->get_detail($item->internal_id);
        if (!$content) {
            return false;
        }
        $history = $this->parser->get_history($content);

        foreach ($history as $doc) {

            $data = array(
                    'filename' => $report['name'],
                    'md5'      => md5($content),
                    'size'     => strlen($content),
            );
            // 4444 - протоколы
            $this->doc_save($data, $item->id, 4444);

            /*
            $item->price_best    = $parse['price_best'];
            $item->members_count = $parse['members_count'];
            $item->date_report   = $item->DB()->BindTimeStamp(time());
            */
        }

        $item->detail_run_time = NULL; // при успехе обнулить
        $item->detail_count_error --;

        if ($item->detail_count_error < 0) {
            $item->detail_count_error = 0;
        }
        $item->Save();

        return true;
    }
}

class parser_1_0003_02_0_00_sberbank extends parser_1_0003_01_0_00_sberbank {

    protected $colomn = array(
            'PurchaseID'    => 'internal_id',
            'PurchaseCode'  => 'num',
            'PurchaseType'  => 'type',
            'PurchaseName'  => 'name',
            'CustomerName'  => 'customer',
            'OpenDate'      => 'date_publication|date_convert',
            'CloseDate'     => 'date_end|date_convert'
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach ($parse['items'] as $item) {
            if (empty($item)) {
                continue;
            }

            $item = $this->list_set_colomn($item, $this->colomn);

            if (preg_match("#Тест#si", $item['name'])) {
                continue;
            }

            $items[] = $item;
        }

        $return = array(
                'page_total' => $this->loader->page_last, //$parse['page_total'],
                'page_now' => null, //$parse['page_now'],
                'items_total' => null,
                'items' => $items,
        );

        return $return;
    }


}

/**
 * Полноценный класс, формирует other
 */
class parser_1_0003_02_0_00_sberbank_detail extends parser_1_0003_01_0_00_sberbank_detail {

    public $detail_sort = array(

    );

    private function convertXmlObjToArr( $obj, &$arr ) {
        $children = $obj->children();
        $executed = false;
        foreach ($children as $elementName => $node) {
            if( array_key_exists( $elementName , $arr ) ) {
                if(array_key_exists( 0 ,$arr[$elementName] ) ) {
                    $i = count($arr[$elementName]);
                    $this->convertXmlObjToArr ($node, $arr[$elementName][$i]);
                } else {
                    $tmp = $arr[$elementName];
                    $arr[$elementName] = array();
                    $arr[$elementName][0] = $tmp;
                    $i = count($arr[$elementName]);
                    $this->convertXmlObjToArr($node, $arr[$elementName][$i]);
                }
            } else {
                $arr[$elementName] = array();
                $this->convertXmlObjToArr($node, $arr[$elementName]);
            }
            $executed = true;
        }
        if(!$executed&&$children->getName()=="") {
            $arr = (String)$obj;
        }

        return ;
    }

    function detail_merge($html, $data) {

        foreach ($html as &$tag) {
            $xmlTags = preg_get_all('#leaf:([a-z]+)#si', $tag);

            foreach ($xmlTags as $xmlTag) {
                if (isset($data[$xmlTag])) {
                    $tag = str_replace("leaf:$xmlTag", $data[$xmlTag], $tag);
                } else {
                    $tag = str_replace("leaf:$xmlTag", "", $tag);
                }
            }
            $tag = $this->text_clear_all($tag);
        }

        unset($html['Тип торгов']);
        foreach ($html as $key => $tag) {
            if (empty($tag)) {
                unset($html[$key]);
            }
        }

        return $html;
    }

    function detail_all($id=3434) {

        $this->detail_link = 'http://com.sberbank-ast.ru/PurchaseView.aspx?id=' . $id;

        $content = $this->emul_br_get_body($this->detail_link);
        $this->loader->debug($this->detail_link);

        $html = $this->detail_parse_pre($content);
        $data = $this->detail_parse_xml($content);

        if (isset($data['purchasedocuments']['file'])) {
            $doc = array();
            foreach ($data['purchasedocuments']['file'] as $file) {
                $doc[] = array(
                        'name'          =>  $file['filename'],
                        'detail_link'   =>  $file['fileid'],
                        'internal_id'   =>  abs(crc_p($file['fileid']))
                );
            }
        }

        if (isset($data['bids']['bid'])) {
            $stuff = "";
            foreach ($data['bids']['bid'] as $st) {
                $stuff.= $st['BidNo'] . ". " . $st['BidName'] . (!empty($st['BidComments']) ? ' - '.$st['BidComments'] : '' ) . "\n";
            }
        }

        $arr = $this->detail_merge($html, $data);

        $return = $this->detail_sort_3($arr);

        $return['docs'] = $doc;
        $return['db']['stuff'] = $stuff;

        return $return;
    }

    function key_clear($key) {
        return preg_replace("#(leaf|node):#si", "", $key);
    }

    function detail_parse_xml($content) {
        $xml = preg_get('#<textarea\s+name="ctl00\$ctl00\$phWorkZone\$xmlData".*?>(.*?)</textarea>#usi', $content);
        $str = preg_replace(array('#&lt;#siu', '#&gt;#siu'), array('<', '>'), $xml);
        $xml = simplexml_load_string($str);
        $arr = array();
        $this->convertXmlObjToArr($xml, $arr);

        return $arr;
    }

    function detail_parse_pre($content) {
        $html = str_get_dom($content);
        $table = $html->find("table.dt", 0)->outertext;
        $tbl = $this->parse_table($table);
        $html->clear();
        $tbl[1][0] = preg_replace("#<td content=\"(.*?)\">#si", "<td>\\1", $tbl[1][0]);
        $tbl[1][0] = preg_replace("#<span content=\"(.*?)\">#si", "<span>\\1", $tbl[1][0]);
        $tbl = $this->parse_table($tbl[1][0]);

        $arr = array();
        foreach ($tbl as $row) {
            $arr[$row[0]] = $row[1];
        }

        return $arr;
    }
}

