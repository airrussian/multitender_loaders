<?php
/**
 * @version > Пермский край (мун.заказ)
 * @var     > http://goszakaz.perm.ru/public.cms/?eid=9726705 // 4 квартал 2010 года
 * @param   > run_list, run_detail
 * @todo    > 03.12.2010 - площадка гос.заказа пермского края (6527)
 * @author  > airrussia@mail.ru
 */

class loader_1_1027_01_0_00_perm extends loader_1_0000_01_0_00_one {
    public $base_url            = 'http://goszakaz.perm.ru/';
    public $list_link           = 'http://goszakaz.perm.ru/public.cms/?eid=9726705';
    public $parser_name         = 'parser_1_1027_01_0_00_perm';
    public $parser_name_detail  = 'parser_1_1027_01_0_00_perm_detail';

    public $fields_list = array(
            'name',
            'customer' => 'maybenull',
            'type',
            'internal_id',
            'date_publication',
    );

    public $break_by_pass = false;
    public $item_rewrite  = false;

    public function run_list() {
        $this->in_process       = 0;
        $this->list_pass_items  = 0;
        $this->list_in_db_items = 0;
        $this->error_list_items = 0;
        $break_by_pass_count = $this->break_by_pass_count;

        $type = $this->parser->list_get_types($this->list_link);

        foreach ($type as $t) {
            $this->parser->type = $t['type'];

            if ( ! $this->validate_load(
                    $content = $this->parser->list_get_page($this->base_url . $t['link']))) {
                return false;
            }

            $list = $this->parser->list_parse( $content );

            if ( empty($list) ) {
                $this->debug("ERROR, CRITICAl, no connect?");
                return false;
            }

            if ( empty( $list['items'] ) ) {
                $this->debug( "<h3>Список items пустой, ошибка парсинга</h3>" );
                return false;
            }

            $this->list_insert(array_reverse($list['items']));

            $this->debug("SLEEP $this->sleep_list s...");
            sleep( $this->sleep_list );

        }
       
        $this->update_in_process();
    }

}

class parser_1_1027_01_0_00_perm extends parser_1_0000_01_0_00_one {

    public $type;

    function list_get_types ( $link ) {
        $this->loader->debug("query all type");
        $content = $this->list_get_page( $link );
        $content = $this->text_from_win($content);
        $content = preg_get("#<div id=['\"]content['\"]>.*?</div>#siu", $content);
        $a = preg_get_all("#<td width=['\"]100%['\"] valign=['\"]top['\"]>(.*?public\.cms/\?eid=\d+.*?)</td>#sui", $content);
        $return = array();
        foreach ($a as $r) {
            $type = preg_get("#<a.*?<b>(.*?)</b>.*?</a>#si", $r);
            $t = array('Запросы котировок', 'Открытые конкурсы', 'Открытые аукционы в электронной форме', 'Открытые аукционы', 'Предварительный отбор');
            $rt = array('запрос котировок', 'открытый конкурс', 'открытый аукцион в электронной форме', 'открытый аукцион', 'предварительный отбор');
            foreach ($t as $k => $tt) { if (preg_match("#$tt#si", $type)) { $type = $rt[$k]; }}
            $return[] = array(
                'type' => $type,
                'link' => preg_get("#href=['\"](.*?)['\"]#si", $r)
            );
        }
        return $return;
    }

    function list_get_page( $link ) {
        $this->loader->debug("link = ".$link);

        $emul_br = $this->emul_br_init($link);
        $emul_br->exec();
        return $emul_br->GetBody();
    }

    protected $colomn = array(
        'P_name'        => 'name|clear_all',
        'P_customer'    => 'customer|clear_all',
    );

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $dp = preg_get("#^\d+\.\d+.\d+#si", $item['name']);
            if (preg_match("#\d+\.\d+.\d{4}#si", $dp)) {
                $item['date_publication'] = $this->text_date_convert($dp);
            } else {
                $item['date_publication'] = $this->text_date_convert_short($dp);
            }
            if (empty($item['date_publication'])) { $item['date_publication'] = date("Ymd"); } 
            $item['name'] = preg_replace("#^\d+\.\d+.\d+#si", "", $item['name']);
            $item['internal_id'] = preg_get("#public\.cms/\?eid=(\d+)#si", $item['customer_src']);
            $item['customer'] = preg_replace("#(заказчик|oрганизатор)(:|\s\-)#sui", "",$item['customer']);
            $item['customer'] = trim(str_replace("...", "", $item['customer']));
            $item['type'] = $this->type;

            $items[] = $item;
        }

        $return = array (
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $links = preg_get_all("#<td width=['\"]100%['\"] valign=['\"]top['\"]>(.*?public\.cms/\?eid=\d+.*?)</td>#sui", $content);

        $ret['items'] = array();
        
        foreach ($links as $tender) {
            $ret['items'][] = array(
                'P_name'        => preg_get("#<b>(.*?)</b>#si", $tender),
                'P_customer'    => preg_get("#<a.*?a>#si", $tender)
            );
        }

        $ret['items_total'] = count($ret['items']);

        return $ret;
    }
}

class parser_1_1027_01_0_00_perm_detail extends parser_1_0000_01_0_00_one {

    function detail_all($id) {
        $link         = "http://goszakaz.perm.ru/public.cms/?eid=";
        $this->loader->debug($link . $id);
        $content      = $this->emul_br_get_body($link . $id);        
        $return['docs'] = $this->list_parse_pre($content);
        $return['db'] = '';
        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content = preg_get("#<TABLE WIDTH=['\"]100%['\"] CELLPADDING=['\"]5['\"] CELLSPACING=['\"]0['\"] border=['\"]0['\"]>(.*?)</table>#siu", $content);
       
        $doct = preg_get_all("#<td>.*?<a.*?cms\.root.*?</td>#siu", $content);

        $docs = array();
        foreach ($doct as $d) {
            $docs[] = array(
                'name' => trim(preg_replace("#[^a-я\s\d]#siu", "", $this->text_clear_all(preg_get("#<td>(.*?)<a#si", $d)))),
                'detail_link' => preg_get("#href=['\"](.*?)['\"]#si", $d),
                'internal_id' => abs(crc_p(preg_get("#href=['\"](.*?)['\"]#si", $d))),
            );
        }
        return $docs;
    }
}