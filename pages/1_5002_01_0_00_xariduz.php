<?php
/**
 * Пологаю открытый конкурс
 * Последние объявления о тендерах УЗБЕКИСТАН
 */

class loader_1_5002_01_0_00_xariduz extends loader_1_0000_02_0_00_temp {

    public $base_url    = 'http://xarid.uz/';
    public $list_link   = 'http://xarid.uz/index.php?option=com_akauction&layout=list&lang=ru';
    public $parser_name = 'parser_1_5002_01_0_00_xariduz';
    public $parser_name_detail = 'parser_1_5002_01_0_00_xariduz_detail';
    
    public $fields_list = array(
            'internal_id',
            'name',
            'date_end',
            'customer',
            'customer_address',
            'num',
            'region_id',
   );

    public $fields_rewrite = array(
            'type'         => 'Открытый аукцион в электронной форме',
            'type_id'      => 6,
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = false;


    function run_list($region_id = null) {
        // DEBUG
        // $region_id = 1;
        $this->debug('START');

        $region = new region_dict();

        if($region_id) {
            $region_id = (int) $region_id;
            if (!$region->Load("site_id = $this->site_id AND region_id = $region_id")) {
                exit("Unknown region");
            }
            $regionArray = array($region);
        }
        else {
            $regionArray = $region->Find('site_id = ' . $this->site_id);
        }

        $this->debug('START regions, count = ' . sizeof($regionArray));

        $i = 0;
        foreach($regionArray as $region) {
            $this->parser->region_id = $region->region_id;
            ;
            $this->parser->region_internal_id = $region->internal_id;

            $this->debug('START region = ' . $region->region_id );

            parent::run_list();

            sleep($this->item_sleep);

            $i ++;
        }
    }

}

/**
 * LIST
 */
class parser_1_5002_01_0_00_xariduz extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
        'Номер лота'                => 'num|clear_all',
        'Срок окончания'            => 'date_end|clear_all',
        'Наименование заказа'       => 'name|clear_all',
        'Наименование организации'  => 'customer|clear_all',
        'Адрес'                     => 'customer_address|clear_all',
        'Цена'                      => 'price|clear_all',
    );

    function list_get_page( $link, $page = 1 ) {
        $link .= "&regid=".$this->region_internal_id."&limitstart=";
        $this->loader->debug("LINK = $link \n PAGE = $page");
        return parent::list_get_page($link, ($page-1)*15);
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['num'] = preg_get("#(.*?)/#si", $item['num']);
            $item['internal_id'] = $item['num'];
            $item['date_end'] = $this->text_date_convert(preg_get("#/(.*)$#si", $item['date_end']));
            $item['region_id'] = $this->region_id;
            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content_dom = str_get_html($content);

        $table = $content_dom->find("table.adminlist", 0);
        $arr = $this->parse_table($table->outertext);
        $paginator = $table->find("tfoot", 0)->find("td", 0)->innertext;

        $page_now = preg_get("#<span.*?>(\d+)</span>#si", $paginator);
        $page_total = max(preg_get_all("#<a.*?>(\d+)</a>#si", $paginator));

        $content_dom->clear();
        
        $items = $this->createstruct($arr);

        $return = array(
                'page_total'  => $page_total,
                'page_now'    => $page_now,
                'items_total' => count($items),
                'items'       => $items,
        );
        return $return;

    }
}

class parser_1_5002_01_0_00_xariduz_detail extends parser_1_0000_02_0_00_temp {

    protected $detail_link = 'http://savdo.xarid.uz/vyat/tradecontent.jsp?src=51&typeOffr=2&id=';

    public $detail_sort = array(
        'Просмотров'    => 'delete',
        'Организация'   => 'customer|clear_all',
    );

    function detail_get($id) {
        $this->loader->debug("link detail: {$this->detail_link}$id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $return = $this->detail_sort_3($parse, true);
        return $return;
    }

    function detail_parse_pre($content) {

        $content = preg_get("#Условия.*?</table>#siu", $content);
        $content = tidy_repair_string($content, $this->tidy_config);

        $content_dom = str_get_html($content);
        $i=-1;
        foreach ($content_dom->find("tr") as $tr) {
            if ($tr->find("td.td1", 0)) {
                $key = $this->text_clear_all($tr->find("td.td1", 0)->innertext);
                $arr[$key] = $tr->find("td.td1", 1)->innertext;
            }
        }

        $content_dom->clear();

        return $arr;
    }

}
