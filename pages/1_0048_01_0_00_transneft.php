
<?php

/**
 * ОАО «АК «Транснефть»
 */

class loader_1_0048_01_0_00_transneft extends loader_1_0000_02_0_00_temp {

    public $base_url            = 'http://zakupki.transneft.ru/';
    public $list_link           = 'http://zakupki.transneft.ru/ru/tenders/index.php?status_id11=1&ctid11=&from11=';
    public $parser_name         = 'parser_1_0048_01_0_00_transneft';
    public $parser_name_detail  = 'parser_1_0048_01_0_00_transneft_detail';

    public $fields_list = array(
            'num',
            'name',
            'internal_id',
            'date_publication',
            'customer'
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;

}

class parser_1_0048_01_0_00_transneft extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'Дата'              =>  'date_publication|date_convert',
            'Заказчик'          =>  'customer|clear_all',
            'Статус'            =>  'status',
            'Название'          =>  'name|clear_all'
    );

    function list_get_page( $link, $page ) {
        $this->loader->debug("\n\n LINK = $link$page \n\n");
        return $this->emul_br_get_body( $link . $page );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            if (!preg_match("#В процессе#sui", $item['status'])) { continue; }

            $item['num']    = preg_get("#<strong>(.*?)</strong>#si", $item['name_src']);
            $item['date_publication'] = date("Ymd", strtotime($item['date_publication']));
            $item['internal_id']      = (int) preg_get("#cid11=(\d+)#si", $item['name_src']);

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $dom = str_get_dom($content);

        $tbl = $dom->find("table.lotDetailsTable", 0)->outertext;

        $tbl = tidy_repair_string($tbl, $this->tidy_config);
        $items = $this->parse_table($tbl);        
        $items = $this->createstruct($items);

        $pag = $dom->find("p.pager",0)->innertext;

        $dom->clear();

        $ret['page_now']    = preg_get("#<strong>(\d+)</strong>#si", $pag);
        $ret['items_total'] = count($items);
        $ret['page_total']  = max(preg_get_all("#<a.*?>(\d+)</a>#si", $pag));

        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0048_01_0_00_transneft_detail extends parser_1_0000_02_0_00_temp {

    protected $detail_link;

    public $detail_sort = array(
            'Дата и время окончания приема заявок'          => 'date_end|date_convert',
            'Дата и время вскрытия конвертов с заявками'    => 'date_conf|date_convert',
            'Стоимость'                                     => 'price|to_price',
            'Предмет конкурса'                              => 'name|clear_all',
    );

    function detail_get($id) {
        $this->detail_link = "http://zakupki.transneft.ru/ru/tenders/index.php?cid11=$id";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }
        $parse = $this->detail_sort_3( $parse );
        $parse['content'] = $this->detail_content_cache($this->text_from_win($content));

        return $parse;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $dom = str_get_html($content);

        $html = $dom->find("table.tenderCardTable", 0);

        foreach ($html->find("tr.dataRow") as $dataRow) {
            $key = $this->text_clear_all($dataRow->find("td", 0)->innertext);
            if ($key) {
                $val = $this->text_clear_all($dataRow->find("td", 1)->innertext);
                $return[$key] = $val;
            }
        }
        $dom->clear();

        return $return;
    }

}
