<?php
/**
 * http://roseltorg.ru/ - Единая электронная торговая площадка
 * 2010.08.03 - смена дизайна, индетификаторов. Решено новый лоадер соорудить.
 */

/**
 * roseltorg.ru
 * @author Paul-labor@yandex.ru
 */
class loader_1_0002_01_0_00_roseltorg extends loader_1_0000_02_0_00_temp {
    public $base_url    = 'http://roseltorg.ru/';
    public $list_link   = 'http://roseltorg.ru/auctions.php?p=';
    public $parser_name = 'parser_1_0002_01_0_00_roseltorg';
    public $parser_name_detail = 'parser_1_0002_01_0_00_roseltorg_detail';

    public $fields_list = array(
            'internal_id',
            'customer',
            'date_end',
            'date_conf',
            'name',
            'price'      => 'maybenull',
    );

    public $fields_add = array('type' => 'Открытый аукцион в электронной форме');
    public $break_by_pass = false; // совершенно не ясна сортировка
    //public $item_rewrite  = true;

    protected $run_detail_order = 'date_conf ASC';

    public $page_total_rewrite = true;

    function geocoder_roseltorg($content) {
        $content_cut = $this->parser->text_clear_roseltorg($content);

        $this->debug('GEOCODER_ROSELTORG, input=', $content_cut);

        $region_id = $this->geocoder_roseltorg_search_index($content_cut);

        if (!$region_id) {
            $region_id = $this->geocoder_search_name($content_cut);
        }

        if (!$region_id) {
            $i_id = preg_get('#&id=(\d{4,})#i', $content);
            $address = $this->parser->detail2_get_address($i_id);

            $this->debug('Address = ' . $address);

            $region_id = $this->geocoder_roseltorg_search_index($address);

            if (!$region_id) {
                $region_id = $this->geocoder_search_name($address);
            }
        }

        if (!$region_id) {
            $region_id = $this->geocoder_roseltorg_search_index($content);
        }

        if ($region_id) {
            $region_name = $this->db->GetOne("SELECT name FROM region WHERE id = $region_id");
        } else {
            $region_name = 'NONE';
        }

        $this->debug("GEOCODER_ROSELTORG, region= $region_id ($region_name)", $content_cut);

        return $region_id ? $region_id : false;
    }

    function geocoder_roseltorg_search_index($content) {
        $indexes = preg_get_all('/(\d{6})/', $content);
        if ($indexes) {
            foreach ($indexes as $index) {
                $index = (int) $index;
                if ($index == 117312 && count($indexes) == 1) {
                    continue;
                }
                $region_id = $this->geocoder_by_index($index);
                if ($region_id) {
                    return $region_id;
                }
            }
        }
        return false;
    }


}

class parser_1_0002_01_0_00_roseltorg extends parser_1_0000_01_0_00_one {

    //http://roseltorg.ru/set/announce.php?id=7718

    protected $colomn = array(
            'Предмет'     => 'name|text_clear_all_dots',
            'Заказчик'    => 'customer|text_clear_all_dots',
            'цена'        => 'price|to_price',
            'проведения'  => 'date_end|date_convert',
            0             => 'internal_id|to_int',
    );

    function list_get_page( $link, $page = 1 ) {
        $link = $link . ($page - 1);
        return $this->emul_br_get($link);
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_conf'] = $item['date_end'];

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => null,
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        // FIXME Дата<br />проведения

        $content = iconv('cp1251', 'utf-8', $content);
        $content_dom = str_get_html($content);

        // Парсинг таблицы
        $table = $content_dom->find('table#so-table-category',0);

        //
        // Нужно, ибо internal_id внутри JS
        //
        $table_array = array();

        $thead = $table->first_child();
        $tr = $thead->children(0);
        foreach($tr->children() as $th) {
            $table_array[0][] = $th->innertext;
        }

        $i=1;
        $tbody = $table->last_child();
        foreach($tbody->children() as $tr) {
            if ($tr->tag !== 'tr') continue;
            if (!empty($tr->attr['onclick'])) {
                $table_array[$i][] = preg_get("#openAuction2\('(\d+)'\);#i", $tr->attr['onclick']);
            }
            foreach($tr->children() as $td) {
                if ($td->tag !== 'td') continue;
                if (!preg_match("#<img.*#i", $td->innertext)) {
                    $table_array[$i][] = $td->innertext;
                }
            }
            $i++;
        }

        $pages = array_pop($table_array);
        $pages = $pages[0];

        $return = array();
        $return['items'] = $this->createstruct($table_array);
        $last_page = array_pop(preg_get_all('/auctions.php\?p=(\d+)/', $pages));
        if ($last_page < 10) {
            exit('WHERE page?? ' . __FILE__ .', '. __LINE__);
        }
        $return['page_now']   = preg_get("#<b>(\d+)</b>#i", $pages);
        $return['page_total'] = $last_page + 1;

        $content_dom->clear();

        return $return;
    }


    function text_clear_roseltorg($text) {
        $text = preg_replace(array('/<(span|font|strong)[^>]*>/mui', '/<b>/ui',
                '#</(span|font|strong|b|a)>#mui', '/<!--.*?-->/', '#<a .*?>#i'), ' ', $text);
        $text = preg_replace('/\h+/u', ' ', $text);
        $text = preg_replace('/\v+/u', '',  $text);
        $text = preg_replace(array('#<p.*?>#','#</p>#', '#<(br|hr).*?>#'), "\n", $text);
        $text = preg_replace(array('#^\h+#mu','#\h+$#mu'), '', $text);

        $text_a = explode("\n", $text);

        $text_a = preg_grep('/(место|адрес|\d{6}\D|\D{2}г\.|ул\.)/ui', $text_a);
        $text_a = preg_grep('/^.{25,999}$/ui',   $text_a);

        $text_a_f = array();
        foreach ($text_a as $v) {
            preg_replace(array(
                    '/(117312.*?Октября.*?9)/ui',
                    '/roseltorg.ru/ui',
                    //'/электронной.*?почты/ui', в одной строке
                    ), 'COUNT', $v, -1, $count);
            if ( ! $count ) {
                $text_a_f[] = $v;
            }
        }

        $text_a = array_slice($text_a_f, 0, 5);

        $text = implode("\n", $text_a);

        $text = preg_replace(array(
                '/место/ui',
                '/нахожден\S+/ui',
                '/заказчи\S+/ui',
                '/почтовый/ui',
                '/адрес/ui',
                '/почты/ui',
                '/ул\. \S+/',
                ), ' ', $text);

        $text = preg_replace('/\h+/u', ' ', $text);

        return $text;
    }


}

class parser_1_0002_01_0_00_roseltorg_detail extends parser_1_0002_01_0_00_roseltorg {
    protected $detail_link = 'http://roseltorg.ru/set/announce.php?id=';
    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }

        $content = mb_convert_encoding($content, 'utf-8', 'cp1251');
        $content = tidy_repair_string($content, $this->tidy_config);

        $return['html'] = preg_get('#<div id="text">(.*?)<hr#uis', $content);
        $return['html'] = preg_replace(array('/(id|class|style)=".*?"/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#<(font|span).*>#ui', '#</(font|span)>#ui', '/<!--.*?-->/ui'), '', $return['html']);
        $return['html'] = preg_replace(array('#\h+#u', '#\v+#u'), array(' ', "\n"), $return['html']);
        $return['html'] = tidy_repair_string($return['html'], $this->tidy_config);

        if (empty($return['html'])) {
            echo $content;
            exit('EMPTY return.html');
        }

        $doc_id   = preg_get('#https://www.roseltorg.ru/auction/guiding/aab\?PrintableVersion=enabled&id=(\d+)#i', $content);

        if ($doc_id) {
            $link = "https://www.roseltorg.ru/auction/guiding/aab?doc_dem_type=documentation&id=$doc_id";
            $return['docs'][] = array(
                    'name' => 'Документы',
                    'detail_link' => $link,
                    'internal_id' => crc_p($link),
            );
            // есть у всех?
            $link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=$doc_id";
            $return['docs'][] = array(
                    'name' => 'Подробная информация',
                    'detail_link' => $link,
                    'internal_id' => crc_p($link),
            );

        }

        // определяем регион
        $region_id = $this->loader->geocoder_roseltorg($content);
        if (empty($region_id) || $region_id < 0) {
            $region_id = 0;
        }
        $return['db']['region_id'] = $region_id;

        return $return;

        //
        $doc2_ids = preg_get_all('#https://www.roseltorg.ru/auction/guiding/view_auction\?id=(\d+)#i', $content);
        if ($doc2_ids) {
            $doc2_ids = array_unique($doc2_ids);
            foreach ($doc2_ids as $id) {
                $link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=$id";
                $return['docs'][] = array(
                        'detail_link' => $link,
                        'internal_id' => crc_p($link),
                );
            }
        }
    }

    function detail_get($id) {
        $this->loader->debug("detail id = $id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    protected $detail2_link = "https://www.roseltorg.ru/auction/guiding/view_auction?id=";
    /**
     * Новая попытка получения адресса
     * пример: https://www.roseltorg.ru/auction/guiding/view_auction?id=15325
     * @param <type> $id
     */
    function detail2_get_address($internal_id) {
        $this->loader->debug("DETAIL_GET_ADDRESS = $this->detail2_link$internal_id");
        $emul_br = $this->emul_br_init( $this->detail2_link . $internal_id );
        $emul_br->exec();
        $address = preg_get("/Mainzakazchik_info = '(.*?)'/i", $emul_br->GetBody());
        $address = mb_convert_encoding($address, 'utf-8', 'cp1251');
        return $address;
    }

}
