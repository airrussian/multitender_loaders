<?php

class loader_1_0047_01_0_00_vtb24 extends loader_1_0000_01_0_00_one {

    public $base_url            = 'http://www.vtb24.ru/';
    public $list_link           = 'http://www.vtb24.ru/rss/?type=tenders';
    public $parser_name         = 'parser_1_0047_01_0_00_vtb24';
    public $parser_name_detail  = 'parser_1_0047_01_0_00_vtb24_detail';

    public $fields_list = array(
            'name',
            'internal_id',
            'date_publication',
    );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $break_by_pass = false;
    public $item_rewrite  = true;

}

class parser_1_0047_01_0_00_vtb24 extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
            'title'             => 'name',
            'pubDate'           => 'date_publication',
            'link'              => 'internal_id',
    );

    function list_get_page( $link ) {
        $this->loader->debug("\n\n LINK = $link\n\n");
        return $this->emul_br_get_body( $link );
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_publication'] = date("Ymd", strtotime($item['date_publication']));
            $item['internal_id']      = preg_get("#about/tenders/(\d+)#si", $item['internal_id']);

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => count($items),
                'items'       => $items,
        );

        var_dump($return);

        return $return;
    }

    function list_parse_pre($content) {

        $content = $this->text_from_win($content);

        $xml = simplexml_load_string($content);

        $items = $xml->xpath('/rss/channel/item');
        foreach ($items as &$item) {
            $item = array(
                'title'     =>  (string) $item->title,
                'link'      =>  (string) $item->link,
                'pubDate'   =>  (string) $item->pubDate
            );
        }

        $ret['page_now']    = 1;
        $ret['items_total'] = count($items);
        $ret['page_total']  = 1;

        $ret['items'] = $items;

        return $ret;
    }
}

class parser_1_0047_01_0_00_vtb24_detail extends parser_1_0000_01_0_00_one {

    protected $detail_link;

    function detail_get($id) {
        $this->detail_link = "http://www.vtb24.ru/about/tenders/$id/";

        $this->loader->debug("LINK = $this->detail_link");

        $emul_br = $this->emul_br_init( $this->detail_link );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }

    function detail_all($id) {
        $content = $this->detail_get( $id );
        $return = $this->detail_parse( $content );
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        if (!$parse) {
            return false;
        }

        return $parse;
    }

    function detail_parse_pre($content) {

        $content = $this->text_from_win($content);

        $return['db'] = array();

        $return['content'] = $this->detail_content_cache($content);

        $dom = str_get_html($content);

        $html = $dom->find("div#Content-reducer", 0);

        $return['html'] = $html->innertext;
        $return['html'] = tidy_repair_string($return['html'], $this->tidy_config);

        $docs = array();
        foreach ($html->find("div.file") as $file) {
            $d = $file->find("a", 0);
            $docs[] = array(
                'name'          =>  $d->innertext,
                'detail_link'   =>  $d->getAttribute("href"),
                'internal_id'   =>  abs(crc_p($d->getAttribute("href")))
            );
        }
        $return['docs'] = $docs;

        $dom->clear();

        return $return;
    }

}
