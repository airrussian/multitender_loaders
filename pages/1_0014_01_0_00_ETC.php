<?php
/**
 * ETC
 * 23.07.2010 - Написание
 * 09.08.2010 - пока отложено, нет тендеров
 */
class loader_1_0014_01_0_00_ETC extends loader_1_0000_02_0_00_temp {
    public $base_url            = 'http://www.erus.ru/';
    public $list_link           = 'http://www.erus.ru/public.purchase/default.asp?bddtsetpage=';
    //public $list_link           = 'http://www.erus.ru/public.purchase/finished.asp?bddtsetpage=';
    public $parser_name         = 'parser_1_0014_01_0_00_ETC';
    public $parser_name_detail  = 'parser_1_0014_01_0_00_ETC_detail';

    public $fields_list = array(
            'name',
            'date_publication',
            'type',
            'internal_id',
            'doc',
            'detail_link',
    );

    public $break_by_pass = true;
    public $item_rewrite  = false;


    function test_detail($url = "/public.purchase/getfile.asp?doctype=ASIS&flid=4753199") {
        $this->parser = new $this->parser_name_detail;
        $this->parser->loader = & $this;

        $det = $this->parser->detail_all($url);

        var_dump($det);
    }

}

class parser_1_0014_01_0_00_ETC extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'p_name'        => 'name|clear_all',
            'p_type'        => 'type|clear_all',
            'p_detail_link' => 'detail_link',
            'p_doc'         => 'doc',
    );

    function list_get_page( $link, $page = 1 ) {
        $this->loader->debug("\n\nREAL PAGE = $page\n\n");
        return $this->emul_br_get_body( $link.$page );
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $k => $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['date_publication'] = $this->text_date_convert(preg_get("#<img.*?>(.*?)&nbsp;#sui", $item['name_src']));
            $item['num'] = preg_get("#<img.*?&nbsp;(\d+)&nbsp;#sui", $item['name_src']);
            $item['name'] = $this->text_clear_all(preg_get("#<img.*?&nbsp;\d+&nbsp;(.*)#sui", $item['name_src']));
            $item['detail_link'] = preg_get("#href=['\"](.*?)['\"]#sui", $item['detail_link_src']);
            $item['internal_id'] = abs(crc_p($item['detail_link'].$item['name']));

            $doc = preg_get_all("#(<a.*?a>)#si", $item['doc_src']);

            $i=0;
            $item['doc'] = array();
            foreach ($doc as $d) {
                $item['doc'][$i]['name'] = preg_get("#<a.*?>(.*?)</a>#si", $d);
                $item['doc'][$i]['name'] = preg_replace("# - сохранить#sui", "", $item['doc'][$i]['name']);
                $item['doc'][$i]['detail_link'] = preg_get("#<a.*?href=['\"](.*?)['\"].*?>#si", $d);
                $item['doc'][$i]['internal_id'] = abs(crc_p($item['doc'][$i]['name'].$item['doc'][$i]['detail_link'].$item['internal_id']));
                $i++;
            }

            $items[$k] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $ten = $content_dom->find('table.htmltable_table', 0);

        $i=0;
        foreach ($ten->find("td.htmltable_splitter_cell") as $row) {

            $str = $this->text_clear_all($row->innertext);
            if ($str=='') { continue; }

            $arr[$i]['p_type'] = $row->find("div", 0)->find("div", 0)->find("sup", 0)->innertext;
            $arr[$i]['p_detail_link'] = $row->find("div", 0)->find("div", 0)->find("sup", 1)->innertext;
            $arr[$i]['p_name'] = $row->find("div", 0)->find("h3", 0)->innertext;
            $doc = "";
            foreach ($row->find("table") as $d) { $doc .= $d->outertext; }
            $arr[$i]['p_doc'] = $doc;
            $i++;
        }
        
        $ret['items'] = $arr;
        $ret['items_total'] = NULL;
        $ret['page_total'] = 1;
        $ret['page_now'] = 1;

        if ($pg = $content_dom->find("tr#puradd_tbp", 0)) {
            $pg = $pg->find("center", 0)->innertext;
            $ret['items_total'] = (int) preg_get("#позиций: (\d+)#sui", $pg);
            $ret['page_total'] = (int) max(preg_get_all("#<a.*?>(\d+)</a>#sui", $pg));
            $ret['page_now'] = (int) preg_get("#<b>\[(\d+)\]</b>#sui", $pg);
        } 

        $content_dom->__destruct();

        return $ret;
    }
}

class parser_1_0014_01_0_00_ETC_detail extends parser_1_0000_02_0_00_temp {

    public $detail_sort = array(
        'p_customer'            => 'customer',
        'p_customer_address'    => 'customer_address',
        'p_customer_postadress' => 'customer1_address',
        'p_customer_email'      => 'customer_email',
        'p_customer_telephone'  => 'customer_telephone',
    );


    function detail_all($content) {
        $this->detail_link  =  $this->loader->base_url . $this->item_now['detail_link_prefix'];
        $this->loader->debug("LINK = ".$this->detail_link);
        $content      = $this->emul_br_get_body($this->detail_links);
        $parse_detail = $this->list_parse_pre($content);

        // sort function
        $return = $this->detail_sort_2($parse_detail);

        $return['content'] = $this->detail_content_cache($this->text_from_win($content));

        $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer_address']);

        if ( (! $return['db']['region_id']) && isset($return['db']['customer1_address'])  ) {
            $return['db']['region_id'] = $this->loader->geocoder_auto($return['db']['customer1_address']);
            $return['db']['customer_address'] = $return['db']['customer1_address'];
        }

        return $return;
    }

    function list_parse_pre($content) {
        $content = $this->text_from_win($content);

        $detail_dom = str_get_html($content);

        $cus = $detail_dom->find("span#_ctl0_organizations_GovCustomerDIV", 0);

        $arr['p_customer'] = $cus->find("span#_ctl0_organizations_GovCustomerID_Title", 0)->innertext;
        $arr['p_customer_address'] = $cus->find("span#_ctl0_organizations_GovCustomerAddress", 0)->innertext;
        $arr['p_customer_postadress'] = $cus->find("span#_ctl0_organizations_GovCustomerPostalAddress", 0)->innertext;
        $arr['p_customer_email'] = $cus->find("span#_ctl0_organizations_GovCustomerEmail", 0)->innertext;
        $arr['p_customer_telephone'] = $cus->find("span#_ctl0_organizations_GovCustomerTel", 0)->innertext;

        $detail_dom->__destruct();

        return $arr;
        
    }

}

