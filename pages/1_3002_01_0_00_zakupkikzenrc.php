<?php
/**
 * Годовые планы государственных закупок - Портал государственных закупок
 * @author airrussian@mail.ru
 */

class loader_1_3002_01_0_00_zakupkikzenrc extends loader_1_0000_02_0_00_temp {

    public $base_url    = 'http://www.zakupki.kz.enrc.com/';
    public $list_link   = 'http://www.zakupki.kz.enrc.com/tender/ajaxList.do';
    public $parser_name = 'parser_1_3002_01_0_00_zakupkikzenrc';
    public $parser_name_detail = 'parser_1_3002_01_0_00_zakupkikzenrc_detail';
    public $fields_list = array(
            'internal_id',
            'num',
            'name',
            'customer',
            'date_publication',
            'region_id',
            'doc',
   );

    public $fields_rewrite = array(
            'type'         => 'Коммерческий',
            'type_dict_id' => 1000,
            'type_id'      => 100,
            'sector_id'    => 2,
    );

    public $page_total_rewrite = true;
    public $break_by_pass = true;
    public $item_rewrite  = true;
}

/**
 * LIST
 */
class parser_1_3002_01_0_00_zakupkikzenrc extends parser_1_0000_02_0_00_temp {

    protected $colomn = array(
            'internal_id'   => 'internal_id|clear_all',
            'number'        => 'num|clear_all',
            'nameProduct'   => 'name|clear_all',
            'orgName'       => 'customer|clear_all',
            'registerDate'  => 'date_publication|date_convert',
            'specificationName'     => 'document_name|clear_all',
            'specificationFileId'   => 'document_id|clear_all',
    );

    private $cookie;

    function list_get_page( $link, $page = 1 ) {
        
        if ($page==1) {
            $br = $this->emul_br_init("http://www.zakupki.kz.enrc.com/tender/list.do");
            $br->exec();
            $this->cookie = $br->GetResponseCookieArray();
        }

        $br = $this->emul_br_init($link);
        $br->SetCookie($this->cookie);

        $post = array(
            'start'                 => ($page-1)*100,
            'limit'                 => 100,
            'sort'                  => 'status',
            'dir'                   => 'ASC',
            'filter[0][field]'      => 'status',
            'filter[0][data][type]' => 'list',
            'filter[0][data][value]'=> 0,
        );

        $br->set_post($post);
        $content = $br->exec();

        return $content;
    }


    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        foreach($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);

            $item['doc'][0] = array(
                'internal_id'   =>  $item['document_id'],
                'name'          =>  $item['document_name'],
                'detail_link'   =>  $item['document_name'] . "?orderId=".$item['internal_id']."&fileId=".$item['document_id'],
            );

            $items[] = $item;
        }

        $return = array (
                'page_total'  => $parse['page_total'],
                'page_now'    => $parse['page_now'],
                'items_total' => $parse['items_total'],
                'items'       => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {

        $data = preg_get("#\{\"total\":\"\d+\",\"data\":\[(.*?)\]\}#si", $content);
        $data = preg_get_all("#\{(.*?)\}#si", $data);

        $arr = array();
        foreach ($data as $key => $row) {
            $row = preg_get("#\"(.+)\"#si", $row);
            $colvals = explode('","', $row);
            foreach ($colvals as $cv) {
                $col = explode('":"', $cv);
                if ($col[0]=='name') { $col[0] = 'nameProduct'; }
                if ($col[0]=='id') { $col[0] = 'internal_id'; }
                if ($col[0]=='specification') { $col[0] = 'specificationName'; }
                $arr[$key][$col[0]] = $col[1];
            }
        }

        $return = array (
                'page_total'  => 1,
                'page_now'    => 1,
                'items_total' => preg_get("#{\"total\":\"(\d+)\"#si", $content),
                'items'       => $arr,
        );
        return $return;

    }
}

class parser_1_3002_01_0_00_zakupkikzenrc_detail extends parser_1_0000_02_0_00_temp {

    protected $detail_link = 'http://www.zakupki.kz.enrc.com/tender/view.do?id=';

     public $detail_sort = array(
        'Юридический адрес'                 => 'customer_address|clear_all',
        'Дата вскрытия конвертов'           => 'date_end|clear_all|date_convert',
        'class="hr"'                        => 'delete',
        'Электронный адрес'                 => 'delete',
        'Техническое задание к спецификации'=> 'delete',
    );

    function detail_get($id) {
        $this->loader->debug("link detail: http://www.zakupki.kz.enrc.com/tender/view.do?id=$id");

        $emul_br = $this->emul_br_init( $this->detail_link . $id );
        $emul_br->exec();
        $content = trim($emul_br->GetBody());

        return $content;
    }


    function arr_double2one(array $arr) {
        $return = array(); $i=0;
        foreach ($arr as $row) {

            if (count($row)==2) {
                $key = $this->text_clear_all($row[0]);
                $val = $row[1];

            } else {
                $val = $row[0];
                $key = $i++;
            }
            $return[$key] = $val;
        }
        return $return;
    }

    function detail_all($id) {
        $content = $this->detail_get($id);
        if (preg_match('/Unable to connect to PostgreSQL server/ui', $content)) {
            exit;
        }
        $return = $this->detail_parse($content);
        return $return;
    }

    function detail_parse($content) {
        $parse = $this->detail_parse_pre($content);
        $return = $this->detail_sort_3($parse, true);
        return $return;
    }

    function detail_parse_pre($content) {
        $content = preg_get("#<h3>.*?<table width=\"100%\" border=\"0\">(.*?)</table>#si", $content);
        $tarr = preg_get_all("#<tr>.*?class=\"txt4\".*?>(.*?)</td>.*?<td.*?>(.*?)</td>.*?</tr>#si", $content);

        $arr =array();
        foreach ($tarr[0] as $key => $val) {
            $arr[$val] = $tarr[1][$key];
        }

        return $arr;
    }

}
