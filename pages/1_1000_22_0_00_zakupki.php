<?php

/**
 * @version > Закупки ГОВ.РУ
 * @var     > http://zakupnew.gov.ru/
 * @param   > run_list, run_detail
 * @author  > airrussian@mail.ru
 */
class loader_1_1000_22_0_00_zakupki extends loader_1_0000_01_0_00_one {

    public $base_url = 'http://zakupki.gov.ru/';
    public $list_link = 'http://zakupki.gov.ru/223/purchase/public/notification/search.html?customerOrgId=&customerOrgId=&purchase=&_createdCustomerRepresentative=on&applSubmissionCloseDateTo=&creatorAgencyId=&creatorAgencyId=&purchaseMethodName=&activeTab=4&customerOrgName=&creatorAgency=&searchWord=&_purchaseStages=on&_purchaseStages=on&_purchaseStages=on&_purchaseStages=on&purchaseStages=APPLICATION_FILING&purchaseStages=COMMISSION_ACTIVITIES&purchaseStages=PLACEMENT_COMPLETE&_cooperativeOnly=on&applSubmissionCloseDateFrom=&purchaseMethodId=&fullTextSearchType=INFOS_AND_DOCUMENTS&d-3771889-p=';
    public $detail_link = 'http://zakupki.gov.ru/pgz/printForm?type=NOTIFICATION&id=';
    public $parser_name = 'parser_1_1000_22_0_00_zakupki';
    public $parser_name_detail = 'parser_1_1000_22_0_00_zakupki_detail';
    public $fields_list = array(
        'num',
        'name',
        'date_publication',
        'customer',
        'internal_id',
        'detail_link_prefix',
        'price' => 'maybenull',
    );
    public $fields_rewrite = array(
        'type' => 'ФЗ223',
        'type_id' => 50,
        'region_id' => 100,
    );
    public $break_by_pass = true;
    public $item_rewrite = false;

}

class parser_1_1000_22_0_00_zakupki extends parser_1_0000_01_0_00_one {

    protected $colomn = array(
        'Реквизиты закупки' => 'name|clear_all',
        'Этап размещения' => 'status|clear_all',
        'Опубликовано' => 'date_publication|date_convert',
        'Начальная цена' => 'price|to_price',
        'Последнее событие' => 'update|date_convert',
    );

    function list_get_page($link, $page = 1) {

        $link.=$page;

        $this->loader->debug("NEXT PAGE = $link");

        $emul_br = $this->emul_br_init($link);
        $content = $emul_br->exec();

        $this->loader->debug($emul_br->GetAllHeaders());

        return $content;
    }

    function list_parse($content) {
        $parse = $this->list_parse_pre($content);

        $items = array();
        foreach ($parse['items'] as $item) {
            $item = $this->list_set_colomn($item, $this->colomn);
            $item['internal_id'] = preg_get("#purchaseId=(\d+)#si", $item['name_src']);
            $item['detail_link_prefix'] = preg_get("#common-info.html\?(.+?)[\"']#si", $item['name_src']);
            $item['customer_src'] = preg_get("#<span class=\"cust\">(.*?)</span>#si", $item['name_src']);
            $item['customer'] = $this->text_clear_all(preg_replace("#Заказчик:#siu", "", $item['customer_src']));
            $a = preg_get_all("#<a.*?>(.*?)</a>#si", $item['name_src']);
            $item['name'] = $this->text_clear_all(preg_get("#<p>(.*?)</p>#si", $a[1]));
            $item['type'] = $this->text_clear_all(preg_get("#<p>(.*?)№#si", $a[0]));
            $item['num'] = $this->text_clear_all(preg_get("#<p>.*?№(\d+)#si", $a[0]));
            $items[] = $item;
        }

        $return = array(
            'page_total' => $parse['page_total'],
            'page_now' => $parse['page_now'],
            'items_total' => $parse['items_total'],
            'items' => $items,
        );

        return $return;
    }

    function list_parse_pre($content) {
        //$content = $this->text_from_win($content);

        $content_dom = str_get_html($content);

        $items = $content_dom->find("table#noticeInfoCompetence", 0);

        $arr = $this->parse_table($items->outertext);

        $paging = $content_dom->find("div.paging", 0);

        $pager = $paging->find("ul.pager", 0);

        $now = $pager->find("span", 0)->innertext;

        $PX = $paging->find("span.pagertxt", 0);

        $rec_s = $this->text_to_int($PX->find("span", 0)->innertext, ",");
        $rec_f = $this->text_to_int($PX->find("span", 1)->innertext, ",");
        $rec_t = $this->text_to_int($PX->find("span", 2)->innertext, ",");

        $onpage = $rec_f - ($rec_s - 1);

        $page = Ceil($rec_t / $onpage);

        $content_dom->clear();

        $return['items'] = $this->createstruct($arr);
        $return['page_now'] = $now;
        $return['page_total'] = 777;
        $return['items_total'] = $rec_t;

        return $return;
    }

}

class parser_1_1000_22_0_00_zakupki_detail extends parser_1_0000_01_0_00_one {

    protected $detail_sort = array(
        'Общие сведения о закупке/Номер извещения' => 'num|clear_all',
        'Общие сведения о закупке/Способ размещения закупки' => 'type|clear_all',
        'Общие сведения о закупке/Наименование закупки' => 'name|clear_all',
        //'Общие сведения о закупке/Редакция' =>  '',
        'Заказчик/Заказчик' =>  'customer|clear_all',    
        'Заказчик/ИНН \ КПП' => 'customer_inn|clear_all',
        'Заказчик/ОГРН' => 'customer_ogrn|clear_all',
        'Заказчик/Адрес места нахождения' =>    'customer_address|clear_all',        
        //'Контактное лицо/Организация' => 'cu',
        //'Контактное лицо/Контактное лицо' => '',
        'Контактное лицо/Электронная почта' => 'customer_email|clear_all',
        'Контактное лицо/Телефон' => 'customer_phone|clear_all',
        //'Контактное лицо/Факс' => '',
        'Порядок размещения закупки/Дата публикации извещения'  =>  'date_publication|clear_all|date_convert',
        'Порядок размещения закупки/Дата и время окончания подачи заявок'   =>  'date_end|clear_all|date_convert',        
        //'Предоставление документации/Срок предоставления' => '',
        //'Предоставление документации/Место предоставления' => '',
        //'Предоставление документации/Порядок предоставления' => '',
        //'Предоставление документации/Официальный сайт, на котором размещена документация' => '',
        //'Предоставление документации/Внесение платы за предоставление конкурсной документации' => '',
    );

    function detail_all($id) {
        $prefix = $this->item_now['detail_link_prefix'];

        $this->loader->detail_link = "http://zakupki.gov.ru/223/purchase/public/purchase/info/common-info.html?" . $prefix;

        $link = $this->loader->detail_link;
        $this->loader->debug("LINK = {$link}");
        $content = $this->emul_br_get_body($link);

        $parse = $this->parse_detail($content);

        return $parse;
    }

    function parse_detail($content) {
        $parse = $this->parse_detail_pre($content);
        $arr = $this->detail_sort_3($parse);
        $arr['db']['region_id'] = $this->loader->geocoder_auto($arr['db']['customer_address']);
        return $arr;
    }

    function parse_detail_pre($content) {

        $dom = str_get_html($content);
        $tablet = $dom->find("#tabtable", 0)->find(".tablet", 0);

        $return = array();

        foreach ($tablet->children() as $table) {
            if (strcasecmp($table->tag, 'table') != 0) {
                continue;
            }
            $caption = "";
            foreach ($table->children() as $tag) {
                if (strcasecmp($tag->tag, 'caption') == 0) {
                    $caption = $this->text_clear_all($tag->innertext);
                }
                if (strcasecmp($tag->tag, 'tbody') == 0) {
                    foreach ($tag->children() as $tr) {
                        if (strcasecmp($tr->tag, 'tr') == 0) {
                            $label = $this->text_clear_all($tr->find("td", 0)->innertext);
                            if ($tr->find("td", 1)) {
                                $value = $tr->find("td", 1)->innertext;
                            } else {
                                $value = null;
                            }

                            $return[$caption . '/' . $label] = $value;
                        }
                    }
                }
                if (strcasecmp($tag->tag, 'tr') == 0) {
                    $label = $this->text_clear_all($tag->find("td", 0)->innertext);
                    if ($tag->find("td", 1)) {
                        $value = $tag->find("td", 1)->innertext;
                    } else {
                        $value = null;
                    }

                    $return[$caption . '/' . $label] = $value;
                }
            }
        }

        return $return;
    }

}
