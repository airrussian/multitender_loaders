<?php

require_once dirname(__FILE__) . "/config.php";

require_once $conf['libsdir'] . '/p_php.php';
require_once $conf['libsdir'] . "/simple_html_dom.php";
require_once $conf['libsdir'] . "/parse.php";
require_once $conf['libsdir'] . "/morf.php";

require_once dirname(__FILE__) . "/loader_base.php";

require_once $conf['libsdir'] . '/adodb5/adodb.inc.php';
require_once $conf['libsdir'] . '/adodb5/adodb-active-record.inc.php';
require_once $conf['libsdir'] . '/adodb5/adodb-exceptions.inc.php';

require_once $conf['libsdir'] . '/class.emul_br_4a.php';

global $db;

$db = ADONewConnection($conf['db']['tenders']['dns']);
$db->debug=$conf['db']['tenders']['debug'];

if(!$db->Execute('set names utf8')) {
    exit("error connect\n");
}

$db->Execute('SET SQL_LOW_PRIORITY_UPDATES=1');

$GLOBALS['ADODB_ACTIVE_DEFVALS'] = true;

ADOdb_Active_Record::SetDatabaseAdapter($db);

require_once dirname(__FILE__) . "/models.php";

function __autoload($class_name) {
    if( preg_match("/^loader_/", $class_name) ) {
        $class_name = preg_get("/^loader_(.*)/", $class_name);

        if (is_file(dirname(__FILE__) . "/site/$class_name.php")) {
            require_once dirname(__FILE__) . "/site/$class_name.php";
        } elseif (is_file(dirname(__FILE__) . "/pages/$class_name.php")) {
            require_once dirname(__FILE__) . "/pages/$class_name.php";
        }

        return true;
    }
}
