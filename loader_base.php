<?php
/*
 * Базовый класс.
*/

/**
 * Что должно быть:
 *   - работа с базой
 * Чего быть не должно принципиально:
 *   - загрузка страниц, любого контента
 *   - парсинг страниц
 */
class loader_base {
    /**
     * Количество ошибочных
     * после проверки validate
     */
    public $error_list_items = 0;

    /**
     * Поля для добавки
     * @var array
     */
    public $fields_add = array();

    /**
     * Перезатирать значениями из fields_add?
     * @var boolen
     */
    public $fields_add_rewrite = true;

    /**
     * Перезатиреть значения перед самой вставкой
     * @var array
     */
    public $fields_rewrite = array();


    /**
     * Количество пропушенных items
     */
    public $list_pass_items  =0;
    public $list_in_db_items =0;

    public $fields_id_name = 'internal_id';

    /**** OLD ****/
    public $site    = 'DEFINE_ME';
    public $site_id   = 'DEFINE_ME'; // FROM CLASS NAME
    public $region_id = 'DEFINE_ME'; // FROM DB
    public $base_url  = 'DEFINE_ME'; // DEFINE ME

    /**
     * название парсера
     * @var string
     */
    public $parser_name = 'DEFINE_ME';
    public $parser      = null;

    /**
     * Время задержки между страницами
     * @var int
     */
    public $sleep_list = 3;

    /**
     * Кэш для функции some_ids_cache
     * @var array
     */
    private $some_ids_cache = array();

    /**
     * Переписывать item при наличие в базе?
     * @var bool
     */
    public $item_rewrite = false;

    /**
     * in_process from last items??
     * @var int
     */
    public $in_process = 0;

    /**
     * Тестовый запуск?
     * @var bool
     */
    public $test_run  = true;
    public $test_nodb = true;

    public $path_cache  = '/usr/home/tenders/www/arh.multitender.ru/data/cache';
    public $doc_storage = '/usr/home/tenders/www/arh.multitender.ru/data/docs';

    /**
     * Дебаг мод?
     */
    public $debug = true;

    public $item_table = 'item';

    function __construct() {
        $this->set_site();
        $this->debug("start " . get_class($this));
    }

    function set_site() {
        $class = get_class($this);

        if( preg_match( "/^loader_(\d+)/", $class ) ) {
            if( ! ( $site_id = (int) preg_get( "/loader_(\d+)/", $class ) ) ) {
                //FIXME debug and exit
                $this->debug( "NO ID in class name, class=" . $class );
                exit();
            }

            if( ($site_id % 100) == 0 ) {
                return false;
            }

            $this->site = new site();

            if( ! $this->site->Load( "id = $site_id" ) ) {
                //FIXME debug and exit
                $this->debug( "Fatal: NO this site in db" );
                exit();
            }            
                       
            $this->site_id   = $this->site->id;
            $this->region_id = $this->site->region_id;
        }
    }

    /**
     * FIXME: дописать
     * Остановить загрузку?
     * @param array $list
     * @return boolen
     */
    function list_stop($list) {
        return false;
    }

    /**
     * Вставка списка.
     * @param array $items
     */
    function list_insert( array $items ) {
        if (empty($items)) {
            trigger_error("почему-то пустой");
            return false;
        }

        $this->fields_list_normalize();

        $i=1;
        foreach ( $items as $item ) {
            $this->debug("ITEM $i of " . count($items));
            //TODO прерывание по счетчику ошибок?
            $this->list_insert_item($item);
            $i++;
        }

    }
    /**
     * Вставка в одной строки
     * @param array $item
     * @return bool вставленно или нет?
     */
    function list_insert_item( array $item ) {
        $item = $this->fields_normalize($item);
        $this->fields_add($item);

        if ( ! $this->fields_check( $item, $this->fields_list ) ) {
            $this->debug("some ERROR in Fields");
            // TODO вывести список

            if ( ! $this->test_nodb ) {
                $item = new $this->item_table;
                $item->DB()->Execute("UPDATE site SET error_counter = error_counter + 1 WHERE id = $this->site_id");
            }

            $this->error_list_items ++;
            $this->debug("ERROR COUNTER:{$this->error_list_items}");
            return false;
        }

        $this->debug("ERROR COUNTER:{$this->error_list_items}");

        $this->list_insert_item_db( $item );

        return true;
    }
    /*
    final function list_insert_item($item) {
        $this->fields_add( $item );
        if ( ! $this->fields_validate( $item ) ) {
            $this->debug("ERROR in Fields");
            $this->error_list_items++;
            return false;
        }

        $this->list_insert_item_db( $item );
    }
    */


    /**
     * Добавление значений полей, к примеру type_id, status_id
     * @param array $item 1D
     */
    function fields_add(array &$item) {

        // добавляем общие поля
        foreach( $this->fields_add as $key=>$val) {
            if (empty($this->fields_list[$key])) {
                $this->fields_list[$key] = 'required';
            }
            if ($this->fields_add_rewrite || empty($item[$key])) {
                $item[$key] = $val;
            }
        }
        $this->fields_list_normalize();

        $this->fields_add_dict($item);

        foreach( $this->fields_rewrite as $key=>$val) {
            $this->fields_list[$key] = 'maybenull';
            $item[$key] = $val;
        }

        if (!isset($item['region_id'])) {
            $item['region_id'] = $this->region_id;
        }

        //TODO добавить customer если нужно
        if ( isset( $this->fields_list['customer'] )) {
            $item[ 'customer_id' ] = $this->fields_add_customer( $item['customer'] );
            //$this->fields_list['customer_id'] = 'required';
        }
    }

    //TODO дописать!
    //FIXME string $customerfields_add_dict_one_db
    function fields_add_customer($customer) {
        //$this->debug(preg_replace("/[а-я]/ui", $customer));
        //if(!$dict->Load('name = ? AND region_id = ', array($name))) {
        return null;
    }

    /**
     * Добавление полей из словаря
     * @param array $item 1D
     */
    final function fields_add_dict(array &$item) {
        // словари, существующие в базе
        $list_av_fields = array(
                'type',
                'status',
        );

        foreach($list_av_fields as $f) {
            //FIXME утрясти переменную fields_list
            if( in_array($f, $this->fields_list) || in_array($f, array_keys($this->fields_list), true) ) {
                if( !isset( $item[ $f . '_id'] ) ) {
                    //    if( $this->test_nodb ) {
                    //        list( $item[ $f . '_dict_id' ], $item[ $f . '_id'] ) = array(null, null);
                    //    } else {
                    list( $item[ $f . '_dict_id' ], $item[ $f . '_id'] ) = $this->fields_add_dict_one_db( $f, $item[$f] );
                    //    }
                }
            }
        }

    }

    /**
     * Подставляет префикс
     * Пример: Запрос котировок => Коммерческий / Запрос котировок
     * @var string
     */
    protected $fields_add_type_prefix    = '';

    /**
     * Разделитель для fields_add_type_prefix
     * @var string
     */
    protected $fields_add_type_delimiter = ' / ';

    /**
     * Возвращает array(dict->id, dict->some_id)
     * return array - dict and [type]_id
     * @param string $type
     * @param string $name
     * @param int    $id_internal
     * @return array
     */
    function fields_add_dict_one_db($type, $name, $id_internal = null) {
        $name = mb_strtolower($name, 'utf-8');

        if(isset($this->some_ids_cache[$type][$name])) {
            return $this->some_ids_cache[$type][$name];
        }

        $dict = new some_dict($type . '_dict');

        if($dict->ErrorMsg())
            exit("Unknown $type, mysql error:" . $dict->ErrorMsg());

        if(!$dict->Load('name = ?', array($name))) {
            $dict->name        = $name;
            $dict->site_id     = $this->site_id;
            $dict->internal_id = $id_internal;

            //FIXME test_nowritedb
            if( ! $this->test_nodb ) {
                $dict->Save();
                $this->notice("New dict in $type, $name");
            }
        }

        $this->some_ids_cache[$type][$name] = array($dict->id, $dict->{$type . '_id'});

        return $this->some_ids_cache[$type][$name];
    }

    /**
     * Список полей для вставки
     * @var array
     */
    public $fields_list = array(
            'internal_id',
            'name',
            'type_dict_id',
            'status_dict_id',
            'type_id',
            'status_id',
            'price');
    public $fields_detail = array('list');

    /**
     * Нормализовать список полей, добавить обязательные
     */
    function fields_list_normalize() {
        $fields_list = array();
        $fields_list['region_id'] = 'maybenull';

        foreach( $this->fields_list as $f=>$r ) {
            if(is_int($f)) {
                $f = $r;
                $r = 'required';
            }
            $fields_list[$f] = $r;
        }

        if(isset($fields_list['type'])) {
            $fields_list['type_dict_id']   = 'maybenull';
            $fields_list['type_id']        = 'maybenull';
        }

        if(isset($fields_list['status'])) {
            $fields_list['status_dict_id'] = 'maybenull';
            $fields_list['status_id']      = 'maybenull';
        }

        //TODO add customer
        //        if(isset($fields_list['customer'])) {
        //            $fields_list['customer_id'] = 'required';
        //        }


        $this->fields_list = $fields_list;
    }
   
    /**
     * Вставить в базу данных новую строку проверка по $this->fields_id_name
     * Вставка полей обозначенных в свойствах
     */
    final function list_insert_item_db( $item_array ) {
        $this->fields_list_normalize();

        $ar_skip = array();
        foreach($item_array as $key=>$val) {
            if(isset($this->fields_list[$key])) {
                $ar_skip[$key] = $val;
            }
            else {
                $ar_skip["$key-SKIP"] = $val;
            }
        }

        $this->debug("list_insert_item_db", $ar_skip);

        //FIXME и ещё одна функция для вставки детайлов?
        // список обязательных для вставки
        //
        // error - сих значений нет в массиве

        //FIXME fields_list_base
        //$fields_list_base = array('region_id');


        $item_insert_param = array_keys($this->fields_list);

        $item = new $this->item_table;

        $item_array[ $this->fields_id_name ] = (int) $item_array[ $this->fields_id_name ];

        $insert = false;

        //FIXME проверить есть ли сии поля в базе данных через array_to_ar
        //FIXME проверка уникальности??
        if( $item->Load( "site_id = $this->site_id AND $this->fields_id_name = " . $item_array[ $this->fields_id_name ] ) ) {
            $this->in_process += $item->in_process;

            $this->list_in_db_items++;
            if( ! $this->item_rewrite ) {
                $this->list_pass_items++;
                return false;
            }

            $item->date_update_list = $item->DB()->BindTimeStamp( gmdate('U') );
        }
        else {
            $insert = true;
            $item->site_id    = $this->site_id;
            $item->in_process = 1;
            // FIXME нужна дата в GMT, BIND же её портит
            $item->date_add         = $item->DB()->BindTimeStamp( gmdate('U') );
        }

        $this->debug( "IN_PROCESS={$item->in_process}, IN_PROCESS+={$this->in_process}" );

        //FIXME вывести список только для вставки, очишенный от всякого мусора
        $this->array_to_ar( $item, $item_array,  $item_insert_param );

        if( ! $this->test_nodb ) {
            $item->Save(TRUE);
        }

        $this->debug("DETAIL ID $item->id");

        //FIXME проверка doc
        //FIXME сформировать чистый массив
        if($item->id) {
            if( isset( $this->fields_list['doc'] ) && (!empty( $item_array['doc'] ) ) ) {
                $this->list_insert_item_db_doc($item->id, $item_array['doc']);
            }
        }

        return $insert;
    }

    /**
     * вставить доки
     * FIXME поправить для doc_link
     * FIXME нормалайз
     * FIXME проверять перезаписыват или нет
     * @param array $d
     * @return bool
     */
    final function list_insert_item_db_doc( $id, array $docs ) {

        $this->debug("RUN list_insert_item_db_doc, id = $id");

        if( empty( $docs ) ) {
            return false;
        }

        foreach($docs as $d) {
            if ( empty( $d['internal_id'] ) ) {
                $this->debug("WHERE internal_id?");
                continue;
            }
            if ( empty( $d['name'] ) ) {
                $this->debug("WHERE name?");
                continue;
            }

            $doc = new doc();
            $doc->Load("site_id = $this->site_id AND item_id = $id AND internal_id = {$d['internal_id']}");

            $doc->item_id     = $id;
            $doc->site_id     = $this->site_id;

            $doc->name        = $d['name'];
            $doc->internal_id = $d['internal_id'];
            if (isset($d['detail_link'])) {
                $doc->detail_link = $d['detail_link'];
            }
            if (isset($d['ext'])) {
                $doc->ext = $d['ext'];
            }

            $doc->date        = $doc->DB()->BindTimeStamp(gmdate('U'));

            $doc->date_add    = $doc->DB()->BindTimeStamp(gmdate('U'));

            //FIXME как сгенерировать зарпос, без отправки в AR???
            if( ! $this->test_nodb ) {
                $doc->Save();
            }
        }
    }


    /*
    public $validate = array(
    'internal_id' => 'int,not_null,>1',
    'name'        => 'string,>1000',
    'detail_link' => 'string,>1000',
    );
    */
    //FIXME добавить запись в закупки
    //FIXME проверять в списке fileds_list
    /**
     * Проверить типы, все OK - true, ещё раз перед вставкой в базу
     * @param array $item 1D
     * @return boolen
     */
    function fields_validate( &$item ) {
        foreach($item as $key=>$val) {
            if( ! isset($this->fields_list[$key]) ) {
                continue;
            }

            switch ($key) {
                case 'price':
                case 'internal_id':
                //fixme > 1??
                    if(! is_int($val) ) {
                        $this->debug("Ошибка $key=$val");
                        return false;
                    }
                    break;

                case 'date_end':
                case 'date_conf':
                case 'date_publication':
                    if (! is_int( $val ) ) {
                        $this->debug("Ошибка $key=$val");
                        return false;
                    }
                    //FIXME вставить дату публикации ФЗ94
                    if ( ($val > 20200101) || ($val < 19900101 ) ) {
                        $this->debug("Ошибка $key=$val");
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    /**
     * Вставка в одной строки
     * FIXME перенести, объеденить с list
     * @param array $item
     * @return bool вставленно или нет?
     */
    final function detail_insert($id, $parse) {
        //$this->fields_add( $item );
        print_r($parse);

        // если ошибка
        if (isset($parse['error'])) {
            $item->date_detail = 20200101230000;
            $item->Save(TRUE);
            return true;
        }

        if ( ! $this->fields_validate( $parse['db'] ) ) {
            $this->debug("ERROR in Fields");
            //$this->error_list_items++;
            exit();
            return false;
        }

        $item = new item();
        if( !$item->Load( "site_id = $this->site_id AND id = " . $id ) ) {
            exit('ERROR detail insert one');
        }

        $detail = new detail();
        $detail->Load('id = ' . $id);
        $detail->id = $id;
        if (empty($parse['other'])) {
            $detail->other = NULL;
        } else {
            $detail->other = serialize($parse['other']);
        }

        // Обрезка до 64K, размер TEXT в MySQL
        if ( strlen($detail->other) > 63*1024 ) {
            $parse_new = array();
            foreach($parse['other'] as $key=>$val) {
                if ( strlen(serialize($parse_new)) > 63*1024 ) {
                    break;
                }
                $parse_new[$key] = $val;
            }
            $detail->other = serialize($parse_new);
        }

        //FIXME проверка html
        if (empty($parse['html'])) {
            $detail->html = NULL;
        } else {
            $detail->html = $parse['html'];
        }

        $detail->is_cached = 0;
        if (!empty($parse['content'])) {
            $md5 = md5($id . 'SOLT:CACHE-DETAIL:90210');

            $path = $this->path_cache;
            $path .= "/" . substr($md5, -1);
            if (!file_exists($path)) { mkdir($path, 0777); }
            $path .= "/" . substr($md5, -3, 2);
            if (!file_exists($path)) { mkdir($path, 0777); }

            $file = $path . "/" . $md5 . ".html.gz";
            
            file_put_contents($file, gzencode($parse['content'], 6));
            chmod($file, 0777);
            if (file_exists($file)) { 
                $detail->is_cached = 1; $this->debug("cache page save: $file");
            } else {
                $this->debug("fail save cache page");
            }
        }

        if ( empty($item->type_id) && (!empty($parse['db']['type'])) ) {
            list( $item->type_dict_id, $item->type_id ) = $this->fields_add_dict_one_db( 'type', $parse['db']['type'] );
        }

        $array_db = array_keys($parse['db']);

        $this->array_to_ar($item, $parse['db'], $array_db);
        $item->date_detail = $item->DB()->BindTimeStamp(gmdate('U'));


        if ( empty($parse['stuff']) ) {
            // запрет обнуления
            if ( empty($item->stuff) ) {
                $item->stuff = NULL;
            }
        } else {
            $item->stuff = $parse['stuff'];
        }

        $item->Save(TRUE);
        $detail->Save();

        if ( ! empty($parse['docs']) ) {
            $this->list_insert_item_db_doc( $item->id, $parse['docs']);
        }


    }

    function debug($str) {
        if($this->debug) {
            echo "\n<hr>\n$str <br />\n";
            echo "memory: ";
            $mem_usage = memory_get_usage(true);
            if ($mem_usage < 1024)
                echo $mem_usage . " B";
            elseif ($mem_usage < 1024*1024)
                echo round($mem_usage/1024,2) . " KB";
            else
                echo round($mem_usage/(1024*1024),2) . " MB";

            $numargs = func_num_args();
            $args = func_get_args();
            for ($i = 1; $i < $numargs; $i++) {
                echo "<br />\n";
                print_r($args[$i]);
                echo "<br />\n";
            }

            echo "<hr>\n";
        }
    }


    /**
     * Оповещение о каком-либо отлавливаемом неприятном событии
     * @param string $str
     */
    final function notice($str) {
        //FIXME переписать функцию оповещения
        $this->debug($str);
    }

    /**
     * $list = array('array' => 'obj');
     *
     * @param ADOdb_Active_Record $obj
     * @param array $array
     * @param array $list
     */
    function array_to_ar(ADOdb_Active_Record &$obj, array &$array, array &$list = null) {
        if(!$list) {
            $list = array_keys($array);
        }

        foreach ($list as $from=>$name) {
            if(is_int($from)) {
                $obj->$name = $array[$name];
            }
            else {
                $obj->$name = $array[$from];
            }
        }
    }

    /**
     * Active Record to Array
     */
    function ar_to_array(ADOdb_Active_Record &$obj) {
        $array = array();
        foreach ($obj->GetAttributeNames() as $name) {
            $array[$name]   = $obj->$name;
        }
        return $array;
    }

    /**
     * Нормилизовать типы согласно БД, возможно стоит перенести в parser
     * @param array $item 1D
     * @return boolen
     */
    final function fields_normalize( $item ) {
        $return = array();

        //
        $this->fields_list_normalize();
        foreach($this->fields_list as $key=>$req) {
            if(empty($item[$key])) {
                $item[$key] = null;
            }
        }

        foreach($item as $key=>$val) {
            if ( is_string($val) ) {
                $val = $this->parser->text_clear_enteries($val);
            }

            if ( is_numeric($val) ) {
                $val = (int) ($val);
            }

            $return[$key] = $val;

            if( ! isset($this->fields_list[$key]) ) {
                continue;
            }

            //FIMXE проверки???
            //if ( is_null($val) ) {
            //    continue;
            //}

            switch ($key) {
                case 'name':
                    if( empty($val) ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
                    break;
                case 'price':
                    $return[$key] = (int) $val;
                    break;
                case 'internal_id':
                    $return[$key] = (int) $val;
                    if( $return[$key] < 1 ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
                    break;
                case 'date_end':
                case 'date_conf':
                case 'date_publication':
                //FIXME а если нул?
                    $return[$key] = (int) $val;
                    if ( ($val > 20200101) || ($val < 19900101 ) ) {
                        trigger_error("Ошибка $key=$val");
                        $return[$key] = null;
                    }
                    break;
            }
        }
        return $item;
    }

    /**
     * Проверить наличие по правилам, предварительная проверка?
     * @return boolen
     */
    final function fields_check( $item, $rules ) {
        $return = $this->fields_normalize($item);

        foreach($rules as $f=>$r) {
            if(is_int($f)) {
                $f = $r;
                $r = 'required';
            }

            switch ($r) {
                case 'required':
                    if( empty( $item[$f] ) ) {
                        $this->debug("ERROR_SKIP: Where is " . $f . "?", $item);
                        return false;
                    }
                    break;
                case 'maybenull':
                    if( is_null($item[$f]) ) {
                        continue;
                    }
                    if( empty( $item[$f] ) ) {
                        $this->debug("ERROR_SKIP: Where is " . $f . "?", $item);
                        return false;
                    }
                    break;

                case 'maybe':
                    break;

                default:
                    return false;
            }
        }
        return true;
    }

    /**
     * Проверка списка при написании парсера
     * @param <type> $items
     */
    final function dev_list_check( $content ) {
        $list = $this->list_parse( $content );
        foreach( $list['items'] as &$item ) {
            $item = $this->fields_normalize( $item );
            if( ! $this->fields_check($item, $fileds_list)) {
                trigger_error("не хватает некоторых полей");
            }
        }
    }

    // останавливаем по date_conf, ибо сортировка по ней
    function list_stop_date_conf(array $list) {
        if(empty($list['items'])) {
            return true;
        }
        $first = array_shift($list['items']);
        if ( ($two = @array_shift($list['items'])) ) {
            $first = $two;
        }
        if (!empty($first['date_conf'])) {
            $date_conf = (int) $first['date_conf'];
            if ( $date_conf < 20000101 ) {
                return false;
            }
            $old_date = (int) gmdate('Ymd', ( time() - 1*(24*60*60) ) );
            if ( $date_conf < $old_date ) {
                return true;
            }
        }
        return false;
    }

}

// FIXME: где распалогать набор проверок, в каком классе?
require_once dirname(__FILE__) . '/parser_base.php';
