<?php

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

class site extends ADOdb_Active_Record {
    var $_table = 'site';
    var $id;
    var $name;
    var $desc;
    var $url;
    var $region_id;
    var $params;
}

class some_table extends ADOdb_Active_Record {
    var $_table = 'DEFINE_ME';
    var $id;
    var $name;
    var $site_id;
}

class region extends ADOdb_Active_Record {
    var $_table = 'region';
    var $id;
    var $name;
    var $site_id;
}

class region_dict extends ADOdb_Active_Record {
    var $_table = 'region_dict';
    var $id;
    var $name;
    var $site_id;
    var $region_id;
    var $internal_id;
    var $internal_param;
    var $date_add;
}

class some_dict extends ADOdb_Active_Record {
    var $_table = 'DEFINE_ME';
    var $name;
    var $site_id;
}

class item extends ADOdb_Active_Record {
    var $_table = 'item';
    var $id;
    var $name;
    var $site_id;
    var $type_id_dict;
    var $status_id_dict;

    var $internal_id;

    var $date_publication; // дата публикации
    var $date_end;         // дата окончания
    var $date_conf;        // дата заседания
}

class item_temp extends item {
    var $_table = 'item_temp';
    var $item_id;
}

class detail extends ADOdb_Active_Record {
    var $_table = 'detail';
    var $id;
    var $other;
    var $html;
}

class doc extends ADOdb_Active_Record {
    var $_table = 'doc';
    var $site_id;
    var $item_id;
    var $internal_id;
    var $name;
    var $size;
    var $date;
    var $md5;
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
