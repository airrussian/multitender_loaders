<?php

class html_parser extends base_parser {

    /**
     * Хранит объект, построенный DOM из HTML
     * @var object
     */
    public $dom;

    /**
     * Разбирает содержимое тега TABLE и возвращает массив
     * @param type $content
     */
    public function table2arr($content) {
        $content_dom = str_get_html($content);
        $table = $content_dom->find('table ', 0);
        if (empty($table)) {
            return null;
        }


        $return = array(
            'caption' => array(),
            'thead' => array(),
            'tbody' => array(),
            'tfoot' => array(),
        );

        $table_captions = array();

        foreach ($table->children() as $tag) {

            $tagName = mb_strtolower($tag->tag);

            switch ($tagName) {
                case 'caption':
                    $return['caption'][] = $tag->innertext;
                    break;
                case 'thead':
                    foreach ($tag->children() as $trN => $tr) {
                        if (strcasecmp($tr->tag, 'tr') == 0) {
                            foreach ($tr->children() as $txN => $tx) {
                                if ((strcasecmp($tx->tag, 'td') == 0) || (strcasecmp($tx->tag, 'th') == 0)) {
                                    $return['thead'][$trN][$txN] = $tx->innertext;
                                } else {
                                    $this->log->setMessage('В структуре таблица возможно ошибка: ' + $content);
                                }
                            }
                        } else {
                            $this->log->setMessage('В структуре таблица возможно ошибка: ' + $content);
                        }
                    }
                    break;
                case 'tfoot':
                    foreach ($tag->children() as $trN => $tr) {
                        if (strcasecmp($tr->tag, 'tr') == 0) {
                            foreach ($tr->children() as $txN => $tx) {
                                if ((strcasecmp($tx->tag, 'td') == 0) || (strcasecmp($tx->tag, 'th') == 0)) {
                                    $return['thead'][$trN][$txN] = $tx->innertext;
                                } else {
                                    $this->log->setMessage('В структуре таблица возможно ошибка: ' + $content);
                                }
                            }
                        } else {
                            $this->log->setMessage('В структуре таблица возможно ошибка: ' + $content);
                        }
                    }
                    break;
                case 'tbody':
                    foreach ($tag->children() as $tr)
                    break;
            }
        }


        // Проверяем возможно у таблицы есть заголовок (может быть несколько)
        $tablecaption = '';
        if ($captions = $table->find("caption")) {
            foreach ($captions as $caption) {
                $tablecaption = $tablecaption + $caption->innertext;
            }
        }

        // Проверяем есть ли THEAD
        $head = array();
        if ($thead = $table->find("THEAD", 0)) {
            foreach ($thead->children() as $thead_tr_n => $tr) {
                if (strcasecmp($tr->tag, 'tr') == 0) {
                    foreach ($this->children() as $thead_tr_tx_n => $tx) {
                        if ((strcasecmp($tx->tag, 'td') == 0) || (strcasecmp($tx->tag, 'th') == 0)) {
                            $head[$thead_tr_tx_n] = (isset($head[$thead_tr_tx_n]) ? $head[$thead_tr_tx_n] : '') + $tx->innertext;
                        }
                    }
                }
            }
        }

        $content_dom->__destruct();

        return $return;
    }

}
