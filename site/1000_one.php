<?php
/**
 * Базовый класс
 */
class loader_1000_one extends loader_base {

    public $list_link = 'DEFINE_ME';

    public $fields_list = array(
            'name',
            'internal_id',
            'type',
            'date_publication',
    );

    public $parser_name = 'parser_1000_one';
    public $parser      = null;

    public $page_total_max = 777;
    public $page_last      = 777;

    public $break_by_pass = false;

    public $break_by_pass_count = 1;

    public $page_total_rewrite = false;

    function __construct() {
        parent::__construct();
        // $this->list_link   = $this->base_url . $this->list_link ;
        $this->parser = new $this->parser_name;
        $this->parser->loader = &$this;

        $db = new site;
        $this->db = $db->DB();
    }

    /*
     * Запустить одну страницу
    */
    public function run_list() {
        $this->in_process       = 0;
        $this->list_pass_items  = 0;
        $this->list_in_db_items = 0;
        $this->error_list_items = 0;
        $break_by_pass_count = $this->break_by_pass_count;

        $page_total = 1;
        for ( $page = 1; $page <= $page_total; $page++ ) {
            $this->debug("<h3>RUN $page page of $page_total</h3>");

            if ($this->break_by_pass) {
                // FIXME other files
                //if ( ($this->list_pass_items) && ($this->in_process === 0) ) {
                $this->debug( "<h3>list_in_db_items = $this->list_in_db_items</h3>" );
                if ( ($this->list_in_db_items) && ($this->in_process === 0) ) {
                    $break_by_pass_count--;
                    if ($break_by_pass_count > 0) {
                        $this->debug( "<h3>BREAK by pass IN next, count = $break_by_pass_count</h3>" );
                    } else {
                        $this->debug( "<h3>BREAK by pass</h3>" );
                        break;
                    }

                }
            }

            if ( ! $this->validate_load(
            $content = $this->parser->list_get_page(
            $this->list_link, $page)
            ) ) {
                return false;
            }

            $list = $this->parser->list_parse( $content );

            if ( empty($list) ) {
                $this->debug("ERROR, CRITICAl, no connect?");
                return false;
            }

            if ($page === 1) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if (empty ($page_total)) {
                    $page_total = 1;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            // page_total rewrite
            if ($this->page_total_rewrite && $page > 1 ) {
                if ( ! ($page_total = (int) $list['page_total']) ) {
                    trigger_error("page_total empty?");
                    return false;
                }

                if ( $page_total > $this->page_total_max ) {
                    trigger_error("VERY BIG PAGES!, page_total = $page_total, page_total_max = $this->page_total_max");
                    return false;
                }

                $this->debug("<h3>total_pages = $page_total</h3>");

                if ($page_total > $this->page_last) {
                    $page_total = $this->page_last;
                }
            }

            if ( empty( $list['items'] ) ) {
                $this->debug( "<h3>Список items пустой, ошибка парсинга</h3>" );
                return false;
            }

            $this->list_insert(array_reverse($list['items']));

            if ( $this->list_stop($list) ) {
                $this->debug( "<h3>BREAK by list_stop</h3>" );
                break;
            }

            $this->debug("SLEEP $this->sleep_list s...");
            sleep( $this->sleep_list );
        }
        $this->update_in_process();
    }

    function update_in_process() {
        $this->debug("update_in_process: error_list_items = $this->error_list_items, test_nodb = $this->test_nodb");
        //if ( !$this->error_list_items && !$this->test_nodb ) {
        if ( !$this->error_list_items && !$this->test_nodb ) {
            $item = new item();
            $item->DB()->Execute("UPDATE LOW_PRIORITY $this->item_table SET in_process=NULL WHERE site_id=$this->site_id AND in_process IS NOT NULL");
        }
    }

    function validate_load($content) {
        if ( (strlen($content) < 1000) ) {
            //FIXME to function + /html
            //TODO отслеживать загрузку по наличию </html> и размеру
            trigger_error("empty_page");
            return false;
        }
        return true;
    }

    function list_stop(array $list) {
        return false;
    }

}

class parser_1000_one extends parser_base {
    function list_get_page( $link, $page = 1 ) {
        $link = $link . $page;
        $this->loader->debug($link);
        $this->list_get_set_links($link);
        return $this->emul_br_get_body($link);
    }
}
