<?php
/**
 * Базовый класс для страницы начинающихся с 0
 */
class loader_1000_one_zero extends loader_1000_one {
    public $page_last     = 25;
}

class parser_1000_one_zero extends parser_1000_one {
    function list_get_page( $link, $page = 1 ) {
        $link = $link . ($page-1);
        $this->loader->debug($link);
        return $this->emul_br_get_body($link);
    }
}
